<?php $page_allowed = 1;
$page = "Creative Professionals";
include("inc/support/common.php");include("backend/config/config.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php include("inc/head-includes.php"); ?>
	
	<style type="text/css">
		.banner .curved-element { border: 0px; }
		.content { margin-bottom: 70px; }
	</style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">
    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
            <?php include("inc/header.php"); ?>
        </div>
        </div>
        <div class="content">
            <div class="banner small-banner  about-bn">


                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">Creative Professionals</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="text-content rte wow fadeInLeft">
                            <h2 class="heading text-center small-text-section">
                                Creative professionals in advertising, marketing and design help connect companies to their audience.  
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="text-content">
                                    <?= getContentPage(20); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="image-content">
                                    <div class="about-img">
			
										<div>
											<img src="<?= $config['website_url'];?>images/creative-slide1.jpg" alt=""/>
										</div>

									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="container">
                        <div class="row">
							<div class="text-center see-more">
								<a href="<?= $config['website_url'];?>vacancies" class="btn-ghost-primary btn"><span>Search Jobsl&nbsp;&nbsp;&nbsp;<i class="fal fa-arrow-from-left"></i></span></a>
								<a href="<?= $config['website_url'];?>login" class="btn-ghost-primary btn"><span>Register for Job Alerts&nbsp;&nbsp;&nbsp;<i class="fal fa-arrow-from-left"></i></span></a>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
            <?php include("inc/footer.php"); ?>

<?php include("inc/modals.php"); ?>

<?php include("inc/footer-js.php"); ?>




</body>
</html>