<?php $page_allowed = 1;
include("inc/support/common.php");include("backend/config/config.php"); 

if(isset($_POST['submit'])) {
	$file = uploadFile('file', false, true);
	$name = $_POST['name'];
	$email = $_POST['email'];
	$tel = $_POST['tel'];
	$subject = $_POST['subject'];
	$message = $_POST['message'];
	
	
		/* Send mail */
		include_once('blocks/phpmailer/class.phpmailer.php');
		$mail = new PHPMailer();
		$mail->SetFrom("webmaster@i-dentify-solutions.com", "I-Dentify");

		//$mail->AddAddress("denise@i-dentify-solutions.com", "Denise Baxter");
		$mail->AddAddress("theteam@i-dentify-solutions.com", "I-Dentify Team");
		//$mail->AddCC("dave@boldidentities.com","Tom Wilde");

		$mail->Subject = "New Inquiry via Identify HR Website ";

		ob_start();
		include('blocks/phpmailer/templates/contact.php');
		$mail->MsgHTML(ob_get_contents());

		ob_end_clean();

		if(!$mail->Send()) {

			echo "ERROR";

		} else {

			header("Location: ".$websiteURL."?sent=1");

		}
		

	
}

$pageID = 39;

$content = $db->prepare("SELECT * FROM `content_pages` WHERE `content_page_id` = :content_page_id LIMIT 1;");
$content->execute(array(
    "content_page_id" => $pageID
));
$content = $content->fetch();


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo $content['meta_title']; ?></title>
    <link href='<?php echo $content['canonical']; ?>' rel='canonical'>
    <meta content='<?php echo $content['meta_desc']; ?>' name='Description'>
    <meta content='<?php echo $content['meta_keywords']; ?>' name='Keywords'>
    <meta content='<?php echo $content['meta_title']; ?>' name='twitter:title'>
    <meta content='<?php echo $content['meta_desc']; ?>' name='twitter:description'>
    <meta property="og:title" content="<?php echo $content['meta_title']; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:description" content="<?php echo $content['meta_desc']; ?>"/>
    <meta property="og:image" content="<?= $config['website_url'];?>images/og.jpg"/>


    <?php include("inc/head-includes.php"); ?>
						
	<style type="text/css">
		.banner .curved-element { border: 0px; }
	</style>

	
					<script type="text/javascript">
						  function initMap() {
							var office = {lat:55.952843, lng:-3.201777,};
							var map = new google.maps.Map(document.getElementById('map'), {
							  zoom: 15,
							  icon: 'img/logo.jpg',
							  center: office,
								scrollwheel: false,
								navigationControl: false,
								mapTypeControl: false,
								scaleControl: false,
								draggable: true,
								styles: [
							{
								"featureType": "administrative",
								"elementType": "geometry",
								"stylers": [
									{
										"color": "#a7a7a7"
									}
								]
							},
							{
								"featureType": "administrative",
								"elementType": "labels.text.fill",
								"stylers": [
									{
										"visibility": "on"
									},
									{
										"color": "#737373"
									}
								]
							},
							{
								"featureType": "administrative.country",
								"elementType": "geometry.fill",
								"stylers": [
									{
										"visibility": "simplified"
									}
								]
							},
							{
								"featureType": "landscape",
								"elementType": "geometry.fill",
								"stylers": [
									{
										"visibility": "on"
									},
									{
										"color": "#efefef"
									}
								]
							},
							{
								"featureType": "poi",
								"elementType": "geometry.fill",
								"stylers": [
									{
										"visibility": "on"
									},
									{
										"color": "#dadada"
									}
								]
							},
							{
								"featureType": "poi",
								"elementType": "labels",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "poi",
								"elementType": "labels.icon",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "road",
								"elementType": "labels.text.fill",
								"stylers": [
									{
										"color": "#696969"
									}
								]
							},
							{
								"featureType": "road",
								"elementType": "labels.icon",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "road.highway",
								"elementType": "geometry.fill",
								"stylers": [
									{
										"color": "#ffffff"
									}
								]
							},
							{
								"featureType": "road.highway",
								"elementType": "geometry.stroke",
								"stylers": [
									{
										"visibility": "on"
									},
									{
										"color": "#b3b3b3"
									}
								]
							},
							{
								"featureType": "road.arterial",
								"elementType": "geometry.fill",
								"stylers": [
									{
										"color": "#ffffff"
									}
								]
							},
							{
								"featureType": "road.arterial",
								"elementType": "geometry.stroke",
								"stylers": [
									{
										"color": "#d6d6d6"
									}
								]
							},
							{
								"featureType": "road.local",
								"elementType": "geometry.fill",
								"stylers": [
									{
										"visibility": "on"
									},
									{
										"color": "#ffffff"
									},
									{
										"weight": 1.8
									}
								]
							},
							{
								"featureType": "road.local",
								"elementType": "geometry.stroke",
								"stylers": [
									{
										"color": "#d7d7d7"
									}
								]
							},
							{
								"featureType": "transit",
								"elementType": "all",
								"stylers": [
									{
										"color": "#808080"
									},
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "water",
								"elementType": "geometry.fill",
								"stylers": [
									{
										"color": "#d3d3d3"
									}
								]
							}
						]


							});
							var marker = new google.maps.Marker({
							  position: office,
							  map: map,
							  icon: "<?= $config['website_url'];?>images/pin.png"
							});
						  }
						</script>
    <script type="text/javascript" async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjy8iY_UP4-YWKLuW7B4Y4LqMaVP0WWGA&callback=initMap"></script>	
	
	
</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">
    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
            <?php include("inc/header.php"); ?>
        </div>
        </div>
        <div class="content">
            <div class="banner small-banner  about-bn" style="background-image: url(<?= $config['website_url'];?>images/contact-banner.jpg); background-size: cover; background-position:center;">


                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">Contact Us</span>
                                        <h1 class="heading">We’d love to hear from you</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
				
    <?php include("inc/ctas.php"); ?>
				
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="text-content rte wow fadeInLeft">
                            <p class="text1 text-center">Send us a message
		                    </p>
                            <p class="text2 text-center"> Simply fill out the form below and one of our team will respond as soon as possible.</p>
                        </div>
                    </div>
                </div>
            </div>
				
            <div class="container">
                <div class="row">
					<div class="wow fadeInRight">
						
                     <div class="col-md-4">
						<div id="map"></div>
	
                    </div>
					
                    <div class="col-md-8">
						
						<div id="contact-form">

						<form method="post" id="form" enctype="multipart/form-data">

							<div class="contact-form-left">

								<input name="name" type="text" id="name" placeholder="Your Full Name" required>

							</div>

							<div class="contact-form-right">

								<input name="email" type="text" id="email" placeholder="Your Email Address" required>

							</div>

							<div class="contact-form-left">

								<input name="tel" type="text" id="tel" placeholder="Your Telephone Number" required>

							</div>

							<div class="contact-form-right">

								<input name="subject" type="text" id="subject" placeholder="Message Subject" value="<?= $_GET['subject']; ?>" required>

							</div>

							<div class="clr"></div>

							<div class="contact-form-full">

								<textarea name="message" class="textarea" id="message" placeholder="Your message here..." required></textarea>

							</div>
							<div class="contact-form-left" style="margin-top:20px;margin-bottom:30px;">
                                <span class="cv-label"><i class="fas fa-cloud-upload-alt"></i>  Upload A Document</span>
								<input name="file" type="file" class="fileInput">
							</div>
							<div class="contact-form-right" style="margin-top:20px;margin-bottom:30px;">

							<input type="submit" name="submit" id="submit" value="Send Message">
							</div>
						</form>

					</div>
				</div>
				</div>	
                </div>
            </div>
				
				
				
				
                
                
                
            </div>
        </div>
            <?php include("inc/footer.php"); ?>

<?php include("inc/modals.php"); ?>

<?php include("inc/footer-js.php"); ?>



</body>
</html>