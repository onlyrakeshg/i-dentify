<?php $page_allowed = 1;
include("inc/support/common.php");include("backend/config/config.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php include("inc/head-includes.php"); ?>
	
	<style type="text/css">
		.banner .curved-element { border: 0px; }
		.content { margin-bottom: 70px; }
	</style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">
    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
            <?php include("inc/header.php"); ?>
        </div>
        </div>
        <div class="content">
            <div class="banner small-banner  about-bn">


                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">Sectors</span>
                                        <h1 class="heading">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="text-content rte wow fadeInLeft">
                            <h2 class="heading text-center small-text-section">
                                lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been but also the leap into
                                electronic

                            </h2>
                            <p class="text1 text-center">Lorem Ipsum is simply dummy text of the printing and
                                typesetting industry. Lorem Ipsum has been the industry's
                            </p>
                            <p class="text2 text-center"> Lorem Ipsum is simply dummy text of the printing and
                                typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500s, when an unknown printer took a galley of
                                type and scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into
                                electronic</p>
                            <p class="text2 text-center"> Lorem Ipsum is simply dummy text of the printing and
                                typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500s, when an unknown printer took a galley of
                                type and scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into
                                electronic</p>
                        </div>
                    </div>
                </div>
            </div>
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="text-content">
                                    <p class="text1 text-left">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry. Lorem Ipsum has been the industry's
                                    </p>
                                    <p class="text2 text-left">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                        has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                        took a galley of type and scrambled it to make a type specimen book. It has survived not
                                        only five centuries, but also the leap into electronic Lorem Ipsum has been the industry's.
                                    </p>
                                    <p class="text2 text-left">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                        has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                        took a galley of type and scrambled it to make a type specimen book. It has survived not.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="image-content">
                                    <div class="about-img" id="about-img">
			
										<div>
											<img src="images/slide1.jpg" alt=""/>
										</div>

										<div>
											<img src="images/slide2.jpg" alt=""/>
										</div>

										<div>
											<img src="images/slide3.jpg" alt=""/>
										</div>

										<div>
											<img src="images/slide4.jpg" alt=""/>
										</div>

										<div>
											<img src="images/slide5.jpg" alt=""/>
										</div>

									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
            <?php include("inc/footer.php"); ?>

<?php include("inc/modals.php"); ?>

<?php include("inc/footer-js.php"); ?>




</body>
</html>