<?php $page_allowed = 1;
$page = "Vacancies";

include("inc/support/common.php");include("backend/config/config.php"); 

$pageID = 41;

$content = $db->prepare("SELECT * FROM `content_pages` WHERE `content_page_id` = :content_page_id LIMIT 1;");
$content->execute(array(
    "content_page_id" => $pageID
));
$content = $content->fetch();


$getSectors = $db->prepare("SELECT * FROM `industry_sectors` ORDER BY `industry_sector_id` ASC");
$getSectors->execute();
$getSectors = $getSectors->fetchAll();

$getLocations = $db->prepare("SELECT * FROM `locations` ORDER BY `name` ASC");
$getLocations->execute();
$getLocations = $getLocations->fetchAll();

$sqlVacancySearch = "SELECT * FROM `vacancies` WHERE (`client_id` IS NULL OR (`client_id` IS NOT NULL AND `allow_on_portal` = TRUE)) AND `deleted` = '0'";


if(count($_POST) > 0) {
	$keyword = $_POST['keyword'];
	if ($keyword != "") {
		$sqlVacancySearch .= " AND (`title` LIKE :title OR `description` LIKE :title)";
		$searchTokens['title'] = '%' . $keyword . '%';
	}
	
	$sectors = $_POST['sectors'];
	if($sectors && is_array($sectors) && count($sectors) > 0) {
		$valid_sectors = array();
		foreach($sectors as $sector) {
			$valid_sectors[] = preg_replace('/[^0-9.]/', '', $sector);
		}
		$sqlVacancySearch .= " AND (`vacancy_id` IN (SELECT `vacancy_id` FROM `vacancies_sectors` WHERE `industry_sector_id` IN ('".implode("', '", $valid_sectors)."')))";
	}
	
	$locations = $_POST['locations'];		
	if($locations && is_array($locations) && count($locations) > 0) {
		$valid_locations = array();
		foreach($locations as $location) {
			$valid_locations[] = preg_replace('/[^0-9.]/', '', $location);
		}
		$sqlVacancySearch .= " AND (`vacancy_id` IN (SELECT `vacancy_id` FROM `vacancies_locations` WHERE `location_id` IN ('".implode("', '", $valid_locations)."')))";
	}
	
	$contract = $_POST['contract'];
	if(is_array($contract)) {
		$contract = implode(",", $_POST['contract']);
	}
	
	$expContract = explode(",", $contract);
	if ($contract != "") {
		$sqlVacancySearch .= " AND `contract_type` <> '' AND (";
		foreach ($expContract as $i => $value) {
			$sqlVacancySearch .= "`contract_type` = :contract OR ";
			$searchTokens['contract'] = $value;
		}
		$sqlVacancySearch = substr($sqlVacancySearch, 0, -4);
		$sqlVacancySearch .= ")";
	}
}

$sqlVacancySearch .= " ORDER BY `vacancy_id` DESC";

//echo $sqlVacancySearch;

$qryGetVacancies = $db->prepare($sqlVacancySearch);
$qryGetVacancies->execute($searchTokens);
$resGetVacancies = $qryGetVacancies->fetchAll();
$totalVacancies = $qryGetVacancies->rowCount();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo $content['meta_title']; ?></title>
    <link href='<?php echo $content['canonical']; ?>' rel='canonical'>
    <meta content='<?php echo $content['meta_desc']; ?>' name='Description'>
    <meta content='<?php echo $content['meta_keywords']; ?>' name='Keywords'>
    <meta content='<?php echo $content['meta_title']; ?>' name='twitter:title'>
    <meta content='<?php echo $content['meta_desc']; ?>' name='twitter:description'>
    <meta property="og:title" content="<?php echo $content['meta_title']; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:description" content="<?php echo $content['meta_desc']; ?>"/>
    <meta property="og:image" content="<?= $config['website_url'];?>images/og.jpg"/>

    <?php include("inc/head-includes.php"); ?>
	
	<link rel="stylesheet" type="text/css" href="css/vacancies.css"/>
	
	<style type="text/css">
		.banner .curved-element { border: 0px; }
	</style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">
    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
            <?php include("inc/header.php"); ?>
        </div>
        </div>
        <div class="content">
            <div class="banner small-banner  about-bn" style="background-image: url(<?= $config['website_url'];?>images/jobs-banner.jpg); background-size: cover; background-position:center;">



                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">Job Opportunities</span>
                                        <h1 class="heading">Take control of your career</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-xs-12 filterbtn">
                    <form name="jobfrm" action="" method="post">
                        <input type="text" placeholder="keyword or Job Title" name="keyword" class="keyw"
                               value="<?php echo isset($keyword) ? $keyword : ''; ?>">
                        <input type="submit" name="filter" value="Update Filter">
						
                        <div class="search-sec">
                            <div class="search-title">Sector</div>
                            <div class="checkpos">
                                <div class="checkbox">
                                    <?php if (is_array($getSectors) && count($getSectors) > 0) { ?>
                                        <?php foreach ($getSectors as $sector) { ?>
                                            <?php
                                            ?>
                                            <label class="search_sector">
												<input type="checkbox" name="sectors[]"  value="<?php echo $sector['industry_sector_id']; ?>"
                                                    <?php echo isset($_POST['sectors']) && is_array($_POST['sectors']) && in_array($sector['industry_sector_id'], $_POST['sectors']) ? 'checked' : ''; ?>>
                                                <?php echo $sector['name']; ?>
                                            </label>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div> <!-- /checkpos -->
							
                        </div> <!-- /search-sec -->
                        <div class="search-sec">
                            <div class="search-title">Location</div>
                            <div class="checkpos">
                                <div class="checkbox">
                                    <?php if (is_array($getLocations) && count($getLocations) > 0) { ?>
                                        <?php foreach ($getLocations as $location) { ?>
                                            <label><input type="checkbox" name="locations[]"
                                                          value="<?php echo $location['location_id']; ?>"
                                                    <?php echo isset($_POST['locations']) && is_array($_POST['locations']) && in_array($location['location_id'], $_POST['locations']) ? 'checked' : ''; ?>>
                                                <?php echo $location['name']; ?>
                                            </label>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div> <!-- /checkpos -->
                        </div> <!-- /search-sec -->
                        <div class="search-sec">
                            <div class="search-title">Contract Type</div>
                            <div class="checkpos">
                                <div class="checkbox">
                                           
									<label>
										<input type="checkbox" name="contract[]" value="Permanent" <?php if(stristr("Permanent", $contract)) { echo "checked"; } ?>>
										Permanent
									</label>
                                           
                                    <label>
										<input type="checkbox" name="contract[]" value="Contract" <?php if(stristr("Contract", $contract)) { echo "checked"; } ?>>
										Contract
									</label>
                                           
                                    <label>
										<input type="checkbox" name="contract[]" value="Temporary" <?php if(stristr("Temporary", $contract)) { echo "checked"; } ?>>
										Temporary
									</label>
                                           
                                    <label>
										<input type="checkbox" name="contract[]" value="Interim" <?php if(stristr("Interim", $contract)) { echo "checked"; } ?>>
										Interim
									</label>
                                            
                                </div>
                            </div> <!-- /checkpos -->
                        </div> <!-- /search-sec -->
                        <input type="submit" name="filter" value="Update Filter">
                    </form>
                </div>
                <div class="col-md-8 col-xs-12">
                    <?php
                    if ($totalVacancies > 0) {
                        $count = 0;
                        foreach ($resGetVacancies as $row) {
                            $vacancyID = $row['vacancy_id'];
							$getLocations = $db->prepare("SELECT * FROM `vacancies_locations` WHERE `vacancy_id` = :vacancy_id");
							$getLocations->execute(array("vacancy_id"=>$vacancyID));
							$getLocations = $getLocations->fetch();

							$locationID = $getLocations['location_id'];
							$getLocationName = $db->prepare("SELECT * FROM `locations` WHERE `location_id` = :location_id");
							$getLocationName->execute(array("location_id"=>$locationID));
							$getLocationName = $getLocationName->fetch();

							$getSectors = $db->prepare("SELECT * FROM `vacancies_sectors` WHERE `vacancy_id` = :vacancy_id");
							$getSectors->execute(array("vacancy_id"=>$vacancyID));
							$getSectors = $getSectors->fetch();

							$sectorID = $getSectors['industry_sector_id'];
							$getSector = $db->prepare("SELECT * FROM `industry_sectors` WHERE `industry_sector_id` = :industry_sector_id");
							$getSector->execute(array("industry_sector_id"=>$sectorID));
							$getSector = $getSector->fetch();
                            ?>
                            <article class="job-post">
                                <div class="jobcode">#<?php echo $row['ref']; ?></div>
                                <h4 class="job-title"><?php echo $row['title']; ?></h4>
                                <div class="info-holder">
                                    
                                        <i class="far fa-map-marker-alt"></i>&nbsp;<?php echo $getLocationName['name']; ?>
                                   
                                </div>
                                <p class="shortdesc"><?php echo substr(strip_tags($row['description']), 0, 180); ?>...</p>
                                <div class="read-btn"><a href="<?= $config['website_url'];?>vacancy/<?= $row['slug']; ?>">Read
                                        More</a></div>
                            </article> <!-- job-post -->
                        <?php }
                    } ?>
                    <?php if ($totalVacancies > 10) { ?>
                        <div class="pagination"></div> <?php } ?>
                </div>
                </div>
            </div>
                
                
                
            </div>
        </div>

<?php include("inc/footer.php"); ?>

<?php include("inc/modals.php"); ?>

<?php include("inc/footer-js.php"); ?>

</body>
</html>