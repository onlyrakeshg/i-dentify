<?php $page_allowed = 1;
$page = "Meet The Team";
include("inc/support/common.php");include("backend/config/config.php"); 

$pageID = 16;

$content = $db->prepare("SELECT * FROM `content_pages` WHERE `content_page_id` = :content_page_id LIMIT 1;");
$content->execute(array(
    "content_page_id" => $pageID
));
$content = $content->fetch();




?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo $content['meta_title']; ?></title>
    <link href='<?php echo $content['canonical']; ?>' rel='canonical'>
    <meta content='<?php echo $content['meta_desc']; ?>' name='Description'>
    <meta content='<?php echo $content['meta_keywords']; ?>' name='Keywords'>
    <meta content='<?php echo $content['meta_title']; ?>' name='twitter:title'>
    <meta content='<?php echo $content['meta_desc']; ?>' name='twitter:description'>
    <meta property="og:title" content="<?php echo $content['meta_title']; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:description" content="<?php echo $content['meta_desc']; ?>"/>
    <meta property="og:image" content="<?= $config['website_url'];?>images/og.jpg"/>

    <?php include("inc/head-includes.php"); ?>
	
	<style type="text/css">
		.about-holder { margin-top: 70px; margin-bottom: 70px;  }
		.banner .curved-element { border: 0px; }
		.content { margin-bottom: 70px; }
	</style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">
    
    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
                <?php include("inc/header.php"); ?>
            </div>
        </div>
        <div class="content">
            <div class="banner small-banner about-bn" style="background-image: url(<?= $config['website_url'];?>images/team-banner.jpg); background-size: cover; background-position:center;">
                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">Meet the Identify Team</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
            <!--<div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="text-content rte wow fadeInRight" >
                            <?= getContentPage(16); ?>
                        </div>
                    </div>
                </div>
            </div>-->
                <div class="white-bg content text-center">
                    
                    <div class="container">
                        <div class="row no-gutters match-height about-holder">
							
                            <div class="col-sm-6 col-md-4 col-of-row team-member">
                                <div class="team-profile">
                                    <div class="profile-img">
                                        <img src="<?= $config['website_url'];?>images/team-denise.jpg" alt="">
                                    </div>
                                    <div class="details">
                                        <h5 class="profile-name">Denise Baxter – CEO</h5>
                                        <span class="profile-position">
                                            Denise is the founder of Identify. She has been on both the HR and recruiter side of the desk.
                                        </span>
                                        <div class="social-links">
                                            <a href="https://www.linkedin.com/in/denisebaxter/" target="_blank">
                                                <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="col-sm-6 col-md-4 col-of-row team-member">
                                <div class="team-profile">
                                    <div class="profile-img">
                                        <img src="<?= $config['website_url'];?>images/team-roz.jpg" alt="">
                                    </div>
                                    <div class="details">
                                        <h5 class="profile-name">
                                            Roz Britton - Head of Communications and Policy Practice
                                        </h5>
                                        <span class="profile-position">
                                            Roz is a former Director of Communications, who previously worked in the Prime Minister’s Office and Cabinet Office Communications Directorate.
                                        </span>
                                        <div class="social-links">
                                            <a href="https://www.linkedin.com/in/roz-britton-5a566a106/" target="_blank">
                                                <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
							
                            <div class="col-sm-6 col-md-4 col-of-row team-member">
                                <div class="team-profile">
                                    <div class="profile-img">
                                        <img src="<?= $config['website_url'];?>images/team-keeleigh.jpg" alt="">
                                    </div>
                                    <div class="details">
                                        <h5 class="profile-name">
                                           Keeleigh Harrild – Operations Manager
                                        </h5>
                                        <span class="profile-position">
                                            Keeleigh is based in Kent working remotely for Identify and has worked within recruitment for over 8 years on back office and candidate support.
                                        </span>
                                        <div class="social-links">
                                            <a href="https://www.linkedin.com/in/keeleigh-harrild-03837833/" target="_blank">
                                                <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
							<div class="col-sm-6 col-md-4 col-of-row team-member">
                                <div class="team-profile">
                                    <div class="profile-img">
                                        <img src="<?= $config['website_url'];?>images/team-lisa.jpg" alt="">
                                    </div>
                                    <div class="details">
                                        <h5 class="profile-name">
                                           Lisa Hume – Resourcing Consultant
                                        </h5>
                                        <span class="profile-position">
                                            Lisa is based from our Mull office and has a wealth of recruitment admin experience.
                                        </span>
                                        <div class="social-links">
                                            <a href="https://www.linkedin.com/in/lhume/" target="_blank">
                                                <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
							<div class="col-sm-6 col-md-4 col-of-row team-member">
                                <div class="team-profile">
                                    <div class="profile-img">
                                        <img src="<?= $config['website_url'];?>images/team-kenny.jpg" alt="">
                                    </div>
                                    <div class="details">
                                        <h5 class="profile-name">
                                            Kenny McAllister - Chief Commercial Officer
                                        </h5>
                                        <span class="profile-position">
                                            Kenny has held senior positions in the Recruitment industry and Software sector (large-scale ERP for HR and HCM).
                                        </span>
                                        <div class="social-links">
                                            <a href="https://www.linkedin.com/in/kennymcallister/" target="_blank">
                                                <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("inc/footer.php"); ?>

<?php include("inc/modals.php"); ?>

<?php include("inc/footer-js.php"); ?>




</body>
</html>