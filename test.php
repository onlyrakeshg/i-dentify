<?php $page_allowed = 1;
include("inc/support/common.php");include("backend/config/config.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php include("inc/head-includes.php"); ?>
	
	<style type="text/css">
		.banner .curved-element { border: 0px; }
	</style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">

    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
            <?php include("inc/header.php"); ?>
        </div>
        </div>
        <div class="content">
            <div class="banner small-banner  about-bn">


                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">A little more about us</span>
                                        <h1 class="heading">Running a superior organization calls for superior talent</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="text-content rte wow fadeInLeft">
							
                            

							
                        </div>
                    </div>
                </div>
            </div>
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="text-content">
                                    <?= getContentPage(32); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="image-content">
                                    <div class="about-img" id="about-img">
			
										<div>
											<img src="images/home-slide1.jpg" alt=""/>
										</div>

										<div>
											<img src="images/home-slide2.jpg" alt=""/>
										</div>

										<div>
											<img src="images/home-slide3.jpg" alt=""/>
										</div>

										<div>
											<img src="images/home-slide4.jpg" alt=""/>
										</div>

									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
                
            </div>
        </div>
           
		<?php include("inc/footer.php"); ?>

		<?php include("inc/modals.php"); ?>

		<?php include("inc/footer-js.php"); ?>

</body>
</html>