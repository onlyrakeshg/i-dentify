<?php

$page_allowed = 1;
include("inc/support/common.php");
include("backend/config/config.php");

foreach ($_SESSION as $i => $value) {
    unset($_SESSION[$i]);
}

header("Location: " . (isset($_GET['returnURL']) && $_GET['returnURL'] ? $_GET['returnURL'] : $config['website_url']));