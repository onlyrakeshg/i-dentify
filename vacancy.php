<?php

$page_allowed = 1;
include("inc/support/common.php");
include("backend/config/config.php");


$slug = $_GET['slug'];

$getVacancy = $db->prepare("SELECT * FROM `vacancies` WHERE `slug` = :slug");
$getVacancy->execute(array("slug" => $_GET['slug']));
$getVacancy = $getVacancy->fetch();

/* GET LOCATION */

$getLocation = $db->prepare("SELECT * FROM `vacancies_locations` WHERE `vacancy_id` = :vacID");
$getLocation->execute(array("vacID" => $getVacancy['vacancy_id']));
$getLocation = $getLocation->fetch();

$locationID = $getLocation['location_id'];

$getLocation = $db->prepare("SELECT * FROM `locations` WHERE `location_id` = :locationID");
$getLocation->execute(array("locationID" => $locationID));
$getLocation = $getLocation->fetch();

/* GET SECTOR */

$getSector = $db->prepare("SELECT * FROM `vacancies_sectors` WHERE `vacancy_id` = :vacID");
$getSector->execute(array("vacID" => $getVacancy['vacancy_id']));
$getSector = $getSector->fetch();

$sectorID = $getSector['industry_sector_id'];

$getSector = $db->prepare("SELECT * FROM `industry_sectors` WHERE `industry_sector_id` = :sectorID");
$getSector->execute(array("sectorID" => $sectorID));
$getSector = $getSector->fetch();

$dateAdded = $getVacancy['date'];
$dateAdded = date('Y-m-d', strtotime("+3 months", strtotime($dateAdded)));

/* */

$getSimilarVacancies = $db->prepare("SELECT * FROM `vacancies_sectors` WHERE `industry_sector_id` = :sectorID LIMIT 0,3");
$getSimilarVacancies->execute(array("sectorID" => $sectorID));
$getSimilarVacancies = $getSimilarVacancies->fetchAll();

foreach ($getSimilarVacancies as $vacancy) {
    $similar_vacancies[] = $vacancy['vacancy_id'];
}

$getConsultant = $db->prepare("SELECT * FROM `team` WHERE `user_id` = :consultant");
$getConsultant->execute(array("consultant" => $getVacancy['consultant_id']));
$getConsultant = $getConsultant->fetch();

$page = $getVacancy['title'];


if (isset($_POST['apply_now'])) {
    $file = uploadFile('file', false, true);
    $name = $_POST['name'];
    $email = $_POST['email'];
    $tel = $_POST['tel'];
    if (!is_array($file['error'])) {

        $candidate_id = NULL;
        $bh_candidate_id = NULL;
        $application_id = NULL;
        $bh_notes = NULL;


        $qryAddCV = $db->prepare("
            INSERT INTO
                `cv_library`
            SET
                `vacancy_id` = :vacancy_id,
                `filename` = :filename,
                `name` = :name,
                `email` = :email,
                `tel` = :tel,
                `date` = :date,
                `candidate_id` = :candidate_id,
                `bh_candidate_id` = :bh_candidate_id,
                `application_id` = :application_id,
                `bh_notes` = :bh_notes;
        ");

        $qryAddCV->execute(array(
            'vacancy_id' => $getVacancy['vacancy_id'],
            'filename' => $file['filename'],
            'name' => $name,
            'tel' => $tel,
            'email' => $email,
            'date' => date('Y-m-d H:i:s'),
            'candidate_id' => $candidate_id,
            'bh_candidate_id' => $bh_candidate_id,
            'application_id' => $application_id,
            'bh_notes' => $bh_notes,
        ));

        require_once 'blocks/phpmailer/class.phpmailer.php';
        $mail = new PHPMailer;
        $mail->SetFrom($email, $name);
        $mail->AddAddress($getConsultant['email'], $getConsultant['title']);
		$mail->AddAddress("theteam@i-dentify-solutions.com", "I-Dentify Team");
        //$mail->AddAddress("dave@boldidentities.com", "Dave");
        //$mail->AddCC("tom@boldidentities.com", "Tom Wilde");
        $mail->IsHTML(true);
        $mail->Subject = 'Job Application for Vacancy #' . $getVacancy['ref'] . '(' . $getVacancy['title'] . ')';
        ob_start();
        include('blocks/phpmailer/templates/application.php');
        $mail->Body = ob_get_contents();
        ob_end_clean();
        $mail->AltBody = 'Note: Our emails are alot nicer with HTML enabled!';
        $mail->Send();
        header("Location: " . $config['website_url'] . "vacancy/" . $_GET['slug'] . "/?applied=1");
    } else {
        $file_error = 1;
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo $getVacancy['title']; ?></title>
    <link href='<?= $config['website_url'];?>vacancy/<?= $getVacancy['canonical']; ?>' rel='canonical'>
    <meta content='<?php echo $getVacancy['meta_desc']; ?>' name='Description'>
    <meta content='<?php echo $getVacancy['meta_keywords']; ?>' name='Keywords'>
    <meta content='<?php echo $getVacancy['meta_title']; ?>' name='twitter:title'>
    <meta content='<?php echo $getVacancy['meta_desc']; ?>' name='twitter:description'>
    <meta property="og:title" content="<?php echo $getVacancy['meta_title']; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:description" content="<?php echo $getVacancy['meta_desc']; ?>"/>
    <meta property="og:image" content="<?= $config['website_url'];?>images/og-job.jpg"/>

    <?php include("inc/head-includes.php"); ?>

    <link href="<?= $config['website_url']; ?>css/vacancies.css" rel="stylesheet">

    <style type="text/css">
        .banner .curved-element {
            border: 0px;
        }
    </style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper" itemscope itemtype="http://schema.org/JobPosting" >

    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
                <?php include("inc/header.php"); ?>
            </div>
        </div>
        <div class="content">
            <div class="banner small-banner  about-bn" style="background-image: url(<?= $config['website_url'];?>images/jobs-banner.jpg); background-size: cover; background-position:center;">


                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading" itemprop="title"><?= $getVacancy['title']; ?></span>
                                        <h1 class="heading"><?= $getLocations['name']; ?></h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
                <div class="container">
                    <div class="container" id="vacancysearch">

                        <div class="row">

                            <div class="col-md-4">
                                <?php if ($_GET['applied'] != 1) { ?>
                                    <div id="vacancy-apply">
                                        <h1>Apply Now</h1>
                                        <form method="post" enctype="multipart/form-data">
                                            <input name="name" type="text" id="name" placeholder="Your Name"
                                                   value="<?= $_POST['name']; ?>">
                                            <input name="tel" type="text" id="tel"
                                                   placeholder="Contact Telephone Number"
                                                   value="<?= $_POST['tel']; ?>">
                                            <input name="email" type="text" id="email" placeholder="Email Address"
                                                   value="<?= $_POST['email']; ?>">
                                            <span class="cv-label"><i class="fas fa-cloud-upload-alt"></i>  Upload Your CV</span>
                                            <input name="file" type="file" class="fileInput">
                                            <div class="clearfix"></div>
                                            <label for="gdpr" class="gdprconfirm"
                                                   <?php if ($file['error']['gdpr'] == 1) { ?>style="background: #ffbebe;"<?php } ?>>
                                                <input name="gdpr" type="checkbox" id="gdpr" value="1"
                                                       onchange="document.getElementById('apply_now').disabled = !this.checked;"> <?php if ($file['error']['gdpr'] == 1) { ?>
                                                <strong><?php } ?>I have read and agree with the <a
                                                            href="<?= $config['website_url']; ?>privacy/"
                                                            target="_blank"><strong>Privacy
                                                            Policy</strong></a>.<?php if ($file['error']['gdpr'] == 1) { ?>
                                                </strong><?php } ?>
                                            </label>
                                            <input name="apply_now" type="submit" id="apply_now" class="apply_now"
                                                   value="Apply Now &#x25B8;"
                                                   disabled>
                                        </form>
                                    </div>
                                <?php } else { ?>
                                    <div id="vacancy-applied">
                                        <h1>Application Submitted</h1>
                                        <p>Thank you. Your details have been submitted to our team and will be reviewed
                                            in due
                                            course. We will
                                            be in touch with you to discuss in further details if a suitable vacancy is
                                            available.</p>
                                        <a href="<?= $config['website_url']; ?>vacancies<?= $_SESSION['searchString']; ?>"><i
                                                    class="fas fa-long-arrow-alt-left"></i> Back to Vacancies</a>
                                    </div>
                                <?php } ?>

                            </div>


                            <div class="col-md-8">

                                <div id="vacancy-info">
									<span itemprop="hiringOrganization" style="display:none;">Identify HR</span>
									<span itemprop="datePosted" style="display:none;"><?= date("d/m/Y", strtotime($getVacancy['date'])); ?></span>
                                    <h1><?= $getVacancy['title']; ?></h1>
                                    <div id="vacancy-specifics">
                                        <?php if ($getVacancy['ref'] != "") { ?>
                                            <div class="row" >
                                                <span class="value-label">Ref #:</span>
                                                <span class="value"><?= $getVacancy['ref']; ?></span>
                                            </div>
                                        <?php } ?>
                                        <div class="row" itemprop="jobLocation" itemscope itemtype="http://schema.org/Place">
                                            <span class="value-label">Location:</span>
                                            <span class="value"><?= $getLocation['name']; ?></span>
											
											<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
														<span itemprop="addressLocality" style="display:none;">Edinburgh</span>
														<span itemprop="streetAddress" style="display:none;" >93 George St</span>
														<span itemprop="addressRegion" style="display:none;" >Edinburgh</span>
														<span itemprop="postalCode" style="display:none;" >EH2 3ES</span>
											</span>
                                        </div>
						<span itemprop="validThrough" style=display:none;><?php echo $dateAdded;?></span>
                                        <?php if ($getVacancy['benefits'] != "") { ?>
                                            <div class="row">
                                                <span class="value-label">Benefits:</span>
                                                <span class="value"><?= $getVacancy['benefits']; ?></span>
                                            </div>
                                        <?php } ?>
                                        <?php if ($getVacancy['contract_length'] != "") { ?>
                                            <div class="row">
                                                <span class="value-label">Contract:</span>
                                                <span class="value"><?= $getVacancy['contract_length']; ?></span>
                                            </div>
                                        <?php } ?>
                                        <div class="row">
                                            <span class="value-label">Sector:</span>
                                            <span class="value">
								<?= $getSector['name']; ?>
                             </span>
                                        </div>
                                        <div class="row">
                                            <span class="value-label">Type:</span>
                                            <span class="value"><?= ucwords($getVacancy['contract_type']); ?></span>
                                        </div>
                                    </div>

                                    <div class="jobdesc" itemprop="description"><?= $getVacancy['description']; ?></div>

                                    <a href="<?= $config['website_url']; ?>vacancies" class="back-btn"><i
                                                class="fa fa-arrow-left"></i> Back</a>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>


            </div>
        </div>
        <?php include("inc/footer.php"); ?>

        <?php include("inc/modals.php"); ?>

        <?php include("inc/footer-js.php"); ?>


</body>
</html>