<?php $page_allowed = 1;
include("inc/support/common.php");include("backend/config/config.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php include("inc/head-includes.php"); ?>
	
	<style type="text/css">
		.banner .curved-element { border: 0px; }
	</style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">

    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
            <?php include("inc/header.php"); ?>
        </div>
        </div>
        <div class="content">
            <div class="banner small-banner  about-bn">


                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">For Clients</span>
                                        <h1 class="heading">Identify HR and Resourcing Solutions</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
            <div class="container">
                
            </div>
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="text-content">
                                    <h2>Our Services</h2>
                                    <br><br>
                                    <?= getContentPage(18); ?>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
               
                
            </div>
        </div>
           
		<?php include("inc/footer.php"); ?>

		<?php include("inc/modals.php"); ?>

		<?php include("inc/footer-js.php"); ?>

</body>
</html>