<?php

include_once('../../../prg_admin/inc/support/common.php');

$locations = $db->prepare("SELECT * FROM `ignite_towns` WHERE `deleted` = '0' ORDER BY `name` ASC");
$locations->execute(array('region_id'=>$region['id']));
$locations = $locations->fetchAll();

?>
<!DOCTYPE html>
<html>
	<head>


		<script src="js/jquery.min.js"></script>
		<script src="../dist/js/standalone/selectize.js"></script>
		<script src="js/index.js"></script>
		
		<style type="text/css">
		.selectize-control .option .title {
			display: block;
		}
		.selectize-control .option .url {
			font-size: 12px;
			display: block;
			color: #a0a0a0;
		}
		.selectize-control .item a {
			color: #006ef5;
		}
		.selectize-control .item.active a {
			color: #303030;
		}
			
		pre { display: none; }
			
		.item { background: #eb3158 !important; border-color: #d61b42 !impoortant; }
		.item a { color: #fff !important; }
		.selectize-control.multi .selectize-input > div { border-color: #d61b42 !important; }
			
		</style>
	</head>
   
    <body>
		<div id="wrapper">
			<div class="demo">
				<div class="control-group">
					<input type="text" id="select-links" class="demo-default" value="awesome,neat">
				</div>
				<script>
				// <select id="select-links"></select>

				$('#select-links').selectize({
					theme: 'links',
					maxItems: null,
					valueField: 'id',
					searchField: 'title',
					options: [
						<?php foreach($locations as $location) { ?>
						{id: <?= $location['id']; ?>, title: '<?= $location['name']; ?>'},
						<?php } ?>
					],
					render: {
						option: function(data, escape) {
							return '<div class="option">' +
									'<span class="title">' + escape(data.title) + '</span>' +
								'</div>';
						},
						item: function(data, escape) {
							return '<div class="item"><a href="' + escape(data.url) + '">' + escape(data.title) + '</a></div>';
						}
					},
					create: function(input) {
						return {
							id: 0,
							title: input,
							url: '#'
						};
					}
				});
				</script>

			</div>
		</div>
	</body>
</html>
