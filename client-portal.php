<?php $page_allowed = 1;
include("inc/support/common.php");include("backend/config/config.php"); 

$pageID = 40;

$content = $db->prepare("SELECT * FROM `content_pages` WHERE `content_page_id` = :content_page_id LIMIT 1;");
$content->execute(array(
    "content_page_id" => $pageID
));
$content = $content->fetch();



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo $content['meta_title']; ?></title>
    <link href='<?php echo $content['canonical']; ?>' rel='canonical'>
    <meta content='<?php echo $content['meta_desc']; ?>' name='Description'>
    <meta content='<?php echo $content['meta_keywords']; ?>' name='Keywords'>
    <meta content='<?php echo $content['meta_title']; ?>' name='twitter:title'>
    <meta content='<?php echo $content['meta_desc']; ?>' name='twitter:description'>
    <meta property="og:title" content="<?php echo $content['meta_title']; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:description" content="<?php echo $content['meta_desc']; ?>"/>
    <meta property="og:image" content="<?= $config['website_url'];?>images/og.jpg"/>



    <?php include("inc/head-includes.php"); ?>
	
	<style type="text/css">
		.banner .curved-element { border: 0px; }
	</style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">

    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
            <?php include("inc/header.php"); ?>
        </div>
        </div>
        <div class="content">
            <div class="banner small-banner  about-bn" style="background-image: url(<?= $config['website_url'];?>images/testimonial-banner.jpg); background-size: cover; background-position:center;">


                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">Client Portal</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
            <div class="container">
               
            </div>
                <div class="content">
                    
                    
                    
                    <div style="display: inline-block; background: #CD1761; padding: 6px 12px; border-radius: 4px; color: #FFFFFF; font-size: 14px"><a href="https://identifyhr.vincere.io/careers/client/login" target="_blank" style="font-size: 14px; color: #FFFFFF; font-style: regular; font-weight: initial; line-height: 1.5; text-decoration: none;">Log in</a> | <a href="https://identifyhr.vincere.io/careers/client/register" target="_blank" style="font-size: 14px; color: #FFFFFF; font-style: regular; font-weight: initial; line-height: 1.5; text-decoration: none;">Register</a></div>
                    
                    
                    
                </div>
               
                
            </div>
        </div>
           
		<?php include("inc/footer.php"); ?>

		<?php include("inc/modals.php"); ?>

		<?php include("inc/footer-js.php"); ?>

</body>
</html>