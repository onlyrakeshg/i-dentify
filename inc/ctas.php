<div class="container">
	<div class="row">

	<?php if($file_error == 1) { ?>
		<div class="error-bar">
		<div class="container">
			We only accept the following document formats: <strong>.doc</strong>, <strong>.docx</strong>, <strong>.pdf</strong>, and <strong>.odt</strong> files
		</div>
		</div>
	</div>
	<?php } ?>
	<?php if($_GET['vacancyuploaded'] == 1) { ?>
	<div class="confirm-bar">
		<div class="container">
			Thank you for uploading your vacancy. One of our team will be in touch ASAP to discuss this in further detail.
		</div>
	</div>
	<?php } ?>
	<?php if($_GET['cvuploaded'] == 1) { ?>
	<div class="confirm-bar">
		<div class="container">
			Thank you for uploading your CV. One of our team will be in touch ASAP to discuss this in further detail.
		</div>
	</div>
	<?php } ?>
	<?php if($_GET['applied'] == 1) { ?>
	<div class="confirm-bar">
		<div class="container">
			Thank you for your application. One of our team will be in touch ASAP to discuss this in further detail.
		</div>
	</div>
	<?php } ?>

	<?php if($_GET['sent'] == 1) { ?>
	<div class="confirm-bar">
		<div class="container">
			Thank you for contacting us. One of our team will be in touch ASAP to discuss this in further detail.
		</div>
	</div>
	<?php } ?>

	<?php if($_GET['updated'] == 1) { ?>
	<div class="confirm-bar">
		<div class="container">
			<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Your profile has been updated successfully.
		</div>
	</div>
	<?php } ?>


	<?php if($_GET['interview_requested'] == 1 || $_GET['info_requested'] == 1) { ?>
	<div class="confirm-bar">
		<div class="container">
			<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Thank you for your request. One of our consultants will be in touch to discuss this ASAP.
		</div>
	</div>
	<?php } ?>
</div>