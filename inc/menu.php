              <div class="menu">
                <div class="overlay">
                  <div class="navbar-pop-up">
                    <div class="row menu-container">
                      
                      <!-- Start Text Close -->
                      <div class="col-md-4 col-sm-4 col-xs-3">
                        <a class="text-close" id="close-menu">&nbsp;</a>
                      </div>
                      <!-- End Text Close -->

                      <!-- Start Logo -->
                      <div class="col-md-4 col-sm-4 col-xs-9">
                        <div class="logo">
                          <a href="<?= $config['website_url'];?>"><img src="<?= $config['website_url'];?>images/logo.png" width="180" /></a>
                        </div>
                      </div>
                      <!-- End Logo -->

                      <!-- Start Right Block -->
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="users-block">
                          <a href="<?= $config['website_url'];?>login">Candidate Login/Register</a>
                          <a href="https://identifyhr.vincere.io/careers/client/login">Client Login/Register</a> 
                            <!--<button class="primary">cv drop</button>-->
                        </div>
                      </div>
                      <!-- End Right Block -->

                    </div>  
                  </div>
                  <div class="container">
                    <div class="row burger-pop_up">
                      <div class="col-md-6">

                        <!-- Start Primary Menu -->
                        <nav class="overlayMenu">  
                            <ul role="menu">
                                <li><a href="<?= $config['website_url'];?>about" role="menuitem">About Us</a></li>
                                <li><a href="<?= $config['website_url'];?>team" role="menuitem">Meet The Team</a></li>
                                <li><a href="<?= $config['website_url'];?>candidates" role="menuitem">For Candidates</a></li>
                                <li><a href="<?= $config['website_url'];?>clients" role="menuitem">For Clients</a></li>
                                
                               	<li class="parent_menu_item"><a role="menuitem">Our Sectors</a>
									<ul class="child_menu_list">
									<li><a href="<?= $config['website_url'];?>sectors_hr" role="menuitem">Human Resources</a></li>
									<li><a href="<?= $config['website_url'];?>sectors_tech" role="menuitem">Technology</a></li>
									<li><a href="<?= $config['website_url'];?>sectors_ict" role="menuitem">ICT</a></li>
									<li><a href="<?= $config['website_url'];?>sectors_digital" role="menuitem">Digital</a></li>
									</ul>
								</li>
                                
                               	<li class="parent_menu_item"><a role="menuitem">Our Services</a>
									<ul class="child_menu_list">
									<li><a href="<?= $config['website_url'];?>services_permanent" role="menuitem">Permanent</a></li>
									<li><a href="<?= $config['website_url'];?>services_contract" role="menuitem">Contract</a></li>
									<li><a href="<?= $config['website_url'];?>services_managed" role="menuitem">Managed</a></li>
									<li><a href="<?= $config['website_url'];?>services_hr" role="menuitem">HR</a></li>
									<li><a href="<?= $config['website_url'];?>services_hcm" role="menuitem">HR & HCM Software Advisory</a></li>
									</ul>
								</li>
                                
                               
                                <li><a href="<?= $config['website_url'];?>vacancies" role="menuitem">Jobs</a></li>
                                <li><a href="<?= $config['website_url'];?>testimonials" role="menuitem">Testimonials</a></li>
                                <li><a href="<?= $config['website_url'];?>blog" role="menuitem">Blog</a></li>
                                <li><a href="<?= $config['website_url'];?>contact" role="menuitem">Contact Us</a></li>
                                
								<li>
								<?php if (isset($_SESSION['candidate_logged_in'])) { ?>
                        <a href="<?php echo siteUrl('profile'); ?>" class="login-register">
                            <i class="fal fa-user"></i>&nbsp;&nbsp;Profile
                        </a>
                        &nbsp;&nbsp;
                        <a href="<?php echo siteUrl('logout'); ?>" class="login-register">
                            <i class="fal fa-lock-alt"></i>&nbsp;&nbsp;Log Out
                        </a>
                    <?php } else { ?>
                        <a href="<?php echo siteUrl('login'); ?>" class="login-register">
                            <i class="fal fa-lock-alt"></i>&nbsp;&nbsp;Candidate Login/Register
                        </a>
                    <?php } ?></li>
						<li>		
                        <a href="https://identifyhr.vincere.io/careers/client/login" class="login-register">
                            <i class="fal fa-lock-alt"></i>&nbsp;&nbsp; Client Login/Register
                        </a>
						</li>
                            </ul>
                        </nav>
                        <!-- End Primary Menu -->

                      </div>
                      <div class="col-md-4">

                        <!-- Start Contact Us -->
                        <div class="contact-us">
                          <h5>Contact Us</h5>
                          <ul role="menu">
                              <li><p>Identify HR</p></li>
                              <li><p>93 George St, Edinburgh EH2 3ES</p></li>
                              <li><p>01688 320620</p></li>
                          </ul>
                        </div>
                        <!-- End Contact Us -->

                      </div>
                      <div class="col-md-2">

                        <!-- Start Social Link -->
                        <div class="social-link">
                          <h5>Social</h5>
                          <ul role="menu">
                              <li><a href="https://www.linkedin.com/company/identify-hr-resourcing-solutions-ltd/" role="menuitem" target="_blank">LinkedIn</a></li>
                              <li><a href="https://www.facebook.com/identifyhr/" role="menuitem" target="_blank">Facebook</a></li>
                              <li><a href="https://www.instagram.com/explore/locations/558036051346350/identify-hr-resourcing-solutions-ltd/?hl=en" role="menuitem" target="_blank">Instagram</a></li>
                              <li><a href="https://twitter.com/HrIdentify" role="menuitem" target="_blank">Twitter</a></li>							  
							  
							  
							  
                          </ul>
                        </div>
                        <!-- End Social Link -->

                      </div>
                    </div>
                  </div>
                 <!-- <div class="footer-pop-up">
                    <p>Copyright © 2018 Bold Identities Ltd, registered in England & Wales (09777426)<br>
                        Registered Address: Beech House 15 Margaret Street, Wakefield, WF1 2DH</p>
                  </div>-->
                </div>
	</div>       
