<a href="#" id="navbtn"><i class="fal fa-bars"></i></a>
<div class="row">
    <div class="col-xs-7 col-sm-7 col-md-6">
        <a href="<?php echo siteUrl(); ?>" class="brand"> <img src="<?php echo siteUrl('images/logo.png'); ?>"
                                                               alt="Bold Identities"> </a>
    </div>
    <div class="col-xs-5 col-sm-5 col-md-6">
        <div class="loc-part text-right">
            <div class="row hidediv">
                <div class="col-xs-12">
                    <div class="header-social ">
                        <a href="https://www.instagram.com/explore/locations/558036051346350/identify-hr-resourcing-solutions-ltd/?hl=en"
                           target="_blank">
                            <i class="fab fa-instagram" aria-hidden="true"></i></a>
                        <a href="https://www.facebook.com/identifyhr/" target="_blank"><i class="fab fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://www.linkedin.com/company/identify-hr-resourcing-solutions-ltd/"
                           target="_blank"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
						<a href="https://twitter.com/HrIdentify" role="menuitem" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                    </div>
                    <div class="clearfix"></div>
					<div class="padbot" >
                    <?php if (isset($_SESSION['candidate_logged_in'])) { ?>
                        <a href="<?php echo siteUrl('profile'); ?>" class="login-register">
                            <i class="fal fa-user"></i>&nbsp;&nbsp;Profile
                        </a>
                        &nbsp;&nbsp;
                        <a href="<?php echo siteUrl('logout'); ?>" class="login-register">
                            <i class="fal fa-lock-alt"></i>&nbsp;&nbsp;Log Out
                        </a>
                    <?php } else { ?>
                        <a href="<?php echo siteUrl('login'); ?>" class="login-register">
                            <i class="fal fa-lock-alt"></i>&nbsp;&nbsp;Candidate Login / Register
                        </a>
                    <?php } ?>
                        <a href="https://identifyhr.vincere.io/careers/client/login" class="login-register" style="margin-left: 20px;">
                            <i class="fal fa-lock-alt"></i>&nbsp;&nbsp;Client Login / Register
                        </a>
					</div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="basic-nav">

                        <!--<div class="site-nav">

							<a href="./">Home</a>
							<a href="./about">About Us</a>
							<a href="./clients">Clients</a>
							<a href="./candidates">Candidates</a>
							<a href="./vacancies">Jobs</a>
							<a href="./join-us">Join Us</a>
							<a href="./team">Meet The Team</a>
							<a href="./blog">Blog</a>
							<a href="./contact">Contact Us</a>
							
							<?php if (!isset($_SESSION['candidate_logged_in'])) { ?><a href="./login">Register/Login</a><?php } else { ?><a href="./login">Logout</a><?php } ?>

						</div>-->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>