<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous">
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/wow/0.1.12/wow.min.js"></script>

<script src="<?= $config['website_url']; ?>slick/slick.min.js"></script>
<script src="<?= $config['website_url']; ?>js/drop_uploader.js"></script>

<?php if ($page == "home") { ?>
    <script src="<?= $config['website_url']; ?>js/typed.min.js"></script>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            new Typed('#typed3', {
                strings: ["Effective Resourcing Solutions", "Expert HR Solutions", "Professional, Friendly & Knowledgeable"],
                typeSpeed: 50,
                backSpeed: 30,
                smartBackspace: true,
                loop: true
            });

        });
    </script>
<?php } ?>
<script>

    $(function () {
		
		$('.parent_menu_item').click(function(){
			$(this).children('.child_menu_list').slideToggle();							
									
									
									
		});
		
		
		
        $('#page-content-wrapper').click(function (e) {
            $('#wrapper').removeClass('toggled');
        });

        $('#menu-toggle').on('click', function (e) {
            if ($('#wrapper').hasClass("toggled")) {
                $("body").addClass('active-body');
            } else {
                $("body").removeClass('active-body');
            }
        });

        $('input[type=file]').drop_uploader({
            uploader_text: 'Drag & Drop Here',
            browse_text: 'Browse File',
            browse_css_class: 'button button-primary',
            browse_css_selector: 'file_browse',
            uploader_icon: '<i class="fa fa-upload"></i>',
            file_icon: '<i class="fa fa-paperclip"></i>',
            time_show_errors: 5,
            layout: 'thumbnails'
        });

        $("#navbtn").click(function () {
            $(this).toggleClass("active");
            $(".overlay").toggleClass("open");
            // this line ▼ prevents content scroll-behind
            $("body").toggleClass("locked");
            $("main .about-us-block").toggleClass("hidden");
            $("main .about-us-block .about-text").toggleClass("hidden");
        });

        $("#close-menu").click(function () {
            $(this).toggleClass("active");
            $(".overlay").toggleClass("open");
            // this line ▼ prevents content scroll-behind
            $("body").toggleClass("locked");
            $("main .about-us-block").toggleClass("hidden");
            $("main .about-us-block .about-text").toggleClass("hidden");
        });

        $(".overlay").scroll(function () {
            var scroll = $(".overlay").scrollTop();

            //>=, not <=
            if (scroll >= 10) {
                //clearHeader, not clearheader - caps H
                $("#navToggle").addClass("hide");
            } else {
                $("#navToggle").removeClass("hide");
            }
        }); //missing );

        new WOW().init();

        $('.multiple-items').slick({
            infinite: true,
            dots: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]

        });

        $('.multiple-testimonials1').slick({
            infinite: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 7000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]

        });
        $('.multiple-testimonials2').slick({
            infinite: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 7000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]

        });

        $('#keywords').click(function () {
            $(".search-bar2").addClass("active");
        });
        $('#SideKeywords').click(function () {
            $(".search-bar1").addClass("active");
        });


        if ($('#sub-list1').is(':visible')) {
            $('.page-wrapper').click(function () {
                $('#sub-list1').hide();
            });
        } else {
            $('.toggle-list1').click(function () {
                $('#sub-list1').toggle();
                $("#sub-list1").parent().toggleClass("active");
            });
        }
        if ($('#sub-list2').is(':visible')) {
            $('.page-wrapper').click(function () {
                $('#sub-list2').hide();
            });
        } else {
            $('.toggle-list2').click(function () {
                $('#sub-list2').toggle();
                $("#sub-list2").parent().toggleClass("active");
            });
        }
        if ($('#sub-list3').is(':visible')) {
            $('.page-wrapper').click(function () {
                $('#sub-list3').hide();
            });
        } else {
            $('.toggle-list3').click(function () {
                $('#sub-list3').toggle();
                $("#sub-list3").parent().toggleClass("active");
            });
        }

        if ($('#sub-list4').is(':visible')) {
            $('.page-wrapper').click(function () {
                $('#sub-list4').hide();
            });
        } else {
            $('.toggle-list4').click(function () {
                $('#sub-list4').toggle();
                $("#sub-list4").parent().toggleClass("active");
            });
        }

        if ($('#sub-list5').is(':visible')) {
            $('.page-wrapper').click(function () {
                $('#sub-list5').hide();
            });
        } else {
            $('.toggle-list5').click(function () {
                $('#sub-list5').toggle();
                $("#sub-list5").parent().toggleClass("active");
            });
        }

        $("#about-img > div:gt(0)").hide();

        setInterval(function () {
            $('#about-img > div:first')
                .fadeOut(1000)
                .next()
                .fadeIn(1000)
                .end()
                .appendTo('#about-img');
        }, 5000);
    });
</script>