<div class="footer">
    <div class="site-links">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-3 wow fadeIn">
                    <dl>
                        <dt><a href="<?= $config['website_url']; ?>">Home</a></dt>
                    </dl>
                    <dl>
                        <dt><a href="<?= $config['website_url']; ?>about">About Us</a></dt>
                    </dl>
                    <dl>
                        <dt><a href="<?= $config['website_url']; ?>candidates">For Candidates</a></dt>
                    </dl>
					<dl>
                        <dt><a href="<?= $config['website_url']; ?>clients">For Clients</a></dt>
                    </dl>

                </div>
                <div class="col-xs-12 col-md-3 wow fadeIn">
                    <dl>
                        <dt><a href="<?= $config['website_url']; ?>vacancies">Jobs</a></dt>
                    </dl>
                    <dl>
                        <dt><a href="<?= $config['website_url']; ?>team">Meet the Team</a></dt>
                    </dl>
                    <dl>
                        <dt><a href="<?= $config['website_url']; ?>blog">Blog</a></dt>
                    </dl>
                    <dl>
                        <dt><a href="<?= $config['website_url']; ?>contact">Contact Us</a></dt>
                    </dl>
                </div>
                <div class="col-xs-12 col-md-4 wow fadeIn">
                    <div class="footer-address">
                        <span class="f-location">Where we are</span>
                        <address>
                            <i class="fal fa-map-marker-alt"></i>&nbsp;&nbsp;&nbsp;
                            93 George St, Edinburgh EH2 3ES
                        </address>
                        <span class="f-telephone">
                                <i class="fal fa-phone"></i>&nbsp;&nbsp;&nbsp;
                                01688 320620
                            </span>
                        <span class="f-email">
                                <i class="fal fa-at"></i>&nbsp;&nbsp;&nbsp;
                                <a href="mailto:theteam@i-dentify-solutions.com">theteam@i-dentify-solutions.com</a>
                            </span>
                    </div>


                </div>
                <div class="col-xs-12 col-md-2 wow fadeIn">
                    <div class="social-links">
                        <span class="social-title">Follow Us</span>
                        <a href="https://www.instagram.com/explore/locations/558036051346350/identify-hr-resourcing-solutions-ltd/?hl=en" target="_blank">
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.facebook.com/identifyhr/" target="_blank">
                            <i class="fab fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.linkedin.com/company/identify-hr-resourcing-solutions-ltd/" target="_blank">
                            <i class="fab fa-linkedin" aria-hidden="true"></i>
                        </a>
						<a href="https://twitter.com/HrIdentify" role="menuitem" target="_blank">
							<i class="fab fa-twitter" aria-hidden="true"></i>
						</a>
						<div class="f-logos">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

<div class="signoff">
    <div class="container">
        <div class="row">
            <div class="col col-xs-12">
                <p>
                    <a href="<?= $config['website_url']; ?>privacy/">Privacy Policy</a>
                </p>
                <p>
                    <span><?= date("Y"); ?>
                    Identify HR (SC621267). All Rights Reserved</span>
                </p>
            </div>
        </div>
    </div>
    <div class="curved-element"></div>
</div>
<!-- Begin INDEED conversion code -->
<script type="text/javascript">
    /* <![CDATA[ */
    var indeed_conversion_id = '3163023241206240';
    var indeed_conversion_label = '';
    /* ]]> */
</script>
<script type="text/javascript" src="https://conv.indeed.com/applyconversion.js">
</script>
<noscript>
    <img height=1 width=1 border=0 src="https://conv.indeed.com/pagead/conv/3163023241206240/?script=0"
         style="visibility: hidden">
</noscript>
<!-- End INDEED conversion code -->