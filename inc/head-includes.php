<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Identify | <?php echo $page; ?></title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
      integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.3.1/css/all.css"
      integrity="sha384-9ralMzdK1QYsk4yBY680hmsb4/hJ98xK3w0TIaJ3ll4POWpWUYaA2bRjGGujGT8w" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600,700" rel="stylesheet">

<link rel="stylesheet" href="<?php echo siteUrl('slick/slick.css'); ?>"/>
<link rel="stylesheet" href="<?php echo siteUrl('slick/slick-theme.css'); ?>"/>
<link rel="stylesheet" href="<?php echo siteUrl('css/styles.css'); ?>">
<link rel="stylesheet" href="<?php echo siteUrl('css/responsive.css'); ?>">
<link rel="stylesheet" href="<?php echo siteUrl('css/animate.css'); ?>">
<link rel="stylesheet" href="<?php echo siteUrl('css/drop_uploader.css'); ?>">

<link rel="icon" href="<?php echo siteUrl('images/favicon.png'); ?>" type="image/png"/>
