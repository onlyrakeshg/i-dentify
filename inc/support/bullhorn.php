<?php

define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
define('BASEPATH', SELF);

include_once "backend/config/bullhorn.php";
include_once "backend/libraries/Bullhorn_api.php";

function get_token()
{
    global $db;

    $query = $db->prepare("SELECT `access_token`, `refresh_token`, `expires`, `created` FROM `bullhorn_integration` ORDER BY `created` DESC LIMIT 1");
    $query->execute();
    return $query->fetch(PDO::FETCH_OBJ);
}

function add_token($data)
{
    global $db;

    $query = $db->prepare("INSERT INTO `bullhorn_integration`(`access_token`, `refresh_token`, `expires`, `created`) VALUES (:access_token, :refresh_token, :expires, :created)");
    return $query->execute($data);
}

function refresh_token($access_token)
{
    global $bh;

    $token = $bh->get_refresh_token($access_token->refresh_token);
    if (isset($token->access_token)) {
        $data = array(
            'access_token' => $token->access_token,
            'refresh_token' => $token->refresh_token,
            'expires' => $token->expires_in,
            'created' => time()
        );
        add_token($data);

        return TRUE;
    }

    return FALSE;
}

$bh_params = array(
    "client_id" => $config['bullhorn_client_id'],
    "client_secret" => $config['bullhorn_client_secret'],
    "redirect_uri" => NULL
);
$bh = new Bullhorn_API($bh_params);
