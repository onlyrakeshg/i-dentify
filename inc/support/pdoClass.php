<?php

class MyPDO extends PDO {
    public function shorthand($sql, $bind = NULL) {
        $stmt = $this->prepare($sql);
        $stmt->execute($bind);
        return $stmt;
    }
}

$db = new PDO("mysql:host=".$database_host.";dbname=".$database_name.";charset=utf8", 
			  $database_user, 
			  $database_password);

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

?>