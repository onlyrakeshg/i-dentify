<?php

session_start();
date_default_timezone_set('Europe/London');
ob_start("ob_gzhandler");

header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.

define('ENVIRONMENT', 'development'); // TODO: Change to "production" when installing on client's server
error_reporting(ENVIRONMENT === "production" ? NULL : E_ALL);

require_once "backend/config/config.php";
require_once "backend/config/database.php";

include("simpleImageClass.php");

include("dbSettings.php");

$url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

define('WEBSITE_URL', $config['website_url']);

$global_path = realpath('/home/identifysolution/public_html/');

function chkUserAuth()
{

    global $ignitePath;

    if ($_SESSION['ignite']['logged_in'] != 1) {

        header("Location: $ignitePath");

    }

}


function getContentPage($contentPageID)
{

    global $db;

    $qryGetContentPage = $db->prepare("SELECT * FROM `content_pages` WHERE `content_page_id` = :id");

    $qryGetContentPage->execute(array('id' => $contentPageID));

    $qryGetContentPage = $qryGetContentPage->fetch(PDO::FETCH_ASSOC);

    return $qryGetContentPage['content'];

}


function setNewImageName($imageFileName)
{

    $ext = pathinfo($imageFileName, PATHINFO_EXTENSION);

    $newImageName = str_replace(".", "", microtime(true)) . "-" . date('d-m-Y-H-i') . '.' . $ext;

    return $newImageName;

}


include("pdoClass.php");


function portalUploadCV($file_field = null, $check_image = false, $random_name = false)
{

    $path = '/home/identifysolution/public_html/uploads/cvs/';

    $max_size = 1000000;

    $whitelist_ext = array('doc', 'docx', 'pdf', 'fotd');

    $whitelist_type = array('application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf', 'application/vnd.oasis.
opendocument.text');


    $out = array('error' => null);

    if (!$file_field) {

        $out['error'][] = "Please specify a valid form field name";

    }

    if (!$path) {

        $out['error'][] = "Please specify a valid upload path";

    }

    if (count($out['error']) > 0) {
        return $out;
    }

    if ((!empty($_FILES[$file_field])) && ($_FILES[$file_field]['error'] == 0)) {

        $file_info = pathinfo($_FILES[$file_field]['name']);
        $name = $file_info['filename'];
        $ext = $file_info['extension'];

        if (!in_array($ext, $whitelist_ext)) {
            $out['error'][] = "Invalid file Extension";
        }

        if (!in_array($_FILES[$file_field]["type"], $whitelist_type)) {
            $out['error'][] = "Invalid file Type";
        }

        if ($_FILES[$file_field]["size"] > $max_size) {

            $out['error'][] = "File is too big";

        }

        if ($check_image) {

            if (!getimagesize($_FILES[$file_field]['tmp_name'])) {

                $out['error'][] = "Uploaded file is not a valid file";

            }

        }

        if ($random_name) {

            $tmp = str_replace(array('.', ' '), array('', ''), microtime());

            if (!$tmp || $tmp == '') {

                $out['error'][] = "File must have a name";

            }
            $newname = $tmp . '.' . $ext;

        } else {

            $newname = $name . '.' . $ext;

        }

        if (file_exists($path . $newname)) {

            $out['error'][] = "A file with this name already exists";

        }

        if (count($out['error']) > 0) {

            return $out;

        }

        if (move_uploaded_file($_FILES[$file_field]['tmp_name'], $path . $newname)) {

            $out['filepath'] = $path;

            $out['filename'] = $newname;

            return $out;

        } else {

            $out['error'][] = "Server Error!";

        }

    } else {

        $out['error'][] = "No file uploaded";

        return $out;

    }

}


function uploadFile($file_field = null, $check_image = false, $random_name = false)
{

    $path = '/home/identifysolution/public_html/uploads/cvs/';

    $max_size = 1500000;

    $whitelist_ext = array('doc', 'docx', 'pdf', 'fotd');

    $whitelist_type = array('application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf', 'application/vnd.oasis.
opendocument.text');


    $out = array('error' => null);

    if (!$file_field) {

        $out['error'][] = "Please specify a valid form field name";

    }

    if (!$path) {

        $out['error'][] = "Please specify a valid upload path";

    }

    if (count($out['error']) > 0) {
        return $out;
    }

    if ((!empty($_FILES[$file_field])) && ($_FILES[$file_field]['error'] == 0)) {

        $file_info = pathinfo($_FILES[$file_field]['name']);
        $name = $file_info['filename'];
        $ext = $file_info['extension'];

        if (!in_array($ext, $whitelist_ext)) {
            $out['error'][] = "Invalid file Extension";
        }

        if (!in_array($_FILES[$file_field]["type"], $whitelist_type)) {
            $out['error'][] = "Invalid file Type";
        }

        if ($_FILES[$file_field]["size"] > $max_size) {

            $out['error'][] = "File is too big";

        }

        if ($check_image) {

            if (!getimagesize($_FILES[$file_field]['tmp_name'])) {

                $out['error'][] = "Uploaded file is not a valid file";

            }

        }

        if ($random_name) {

            $tmp = str_replace(array('.', ' '), array('', ''), microtime());

            if (!$tmp || $tmp == '') {

                $out['error'][] = "File must have a name";

            }
            $newname = $tmp . '.' . $ext;

        } else {

            $newname = $name . '.' . $ext;

        }

        if (file_exists($path . $newname)) {

            $out['error'][] = "A file with this name already exists";

        }

        if (count($out['error']) > 0) {

            return $out;

        }

        if (move_uploaded_file($_FILES[$file_field]['tmp_name'], $path . $newname)) {

            $out['filepath'] = $path;

            $out['filename'] = $newname;

            return $out;

        } else {

            $out['error'][] = "Server Error!";

        }

    } else {

        $out['error'][] = "No file uploaded";

        return $out;

    }

}


function uploadVacancy($file_field = null, $check_image = false, $random_name = false)
{

    $path = '/home/identifysolution/public_html/uploads/vacancies/';

    $max_size = 1500000;

    $whitelist_ext = array('doc', 'docx', 'pdf', 'odt', 'fotd');

    $whitelist_type = array('application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf', 'application/vnd.oasis.
opendocument.text');

    $out = array('error' => null);

    if (!$file_field) {
        $out['error'][] = "Please specify a valid form field name";
    }

    if (!$path) {
        $out['error'][] = "Please specify a valid upload path";
    }

    if (count($out['error']) > 0) {
        return $out;
    }


    if ((!empty($_FILES[$file_field])) && ($_FILES[$file_field]['error'] == 0)) {

        $file_info = pathinfo($_FILES[$file_field]['name']);
        $name = $file_info['filename'];
        $ext = $file_info['extension'];

        if (!in_array($ext, $whitelist_ext)) {
            $out['error'][] = "Invalid file Extension";
        }

        if (!in_array($_FILES[$file_field]["type"], $whitelist_type)) {
            $out['error'][] = "Invalid file Type";
        }

        if ($_FILES[$file_field]["size"] > $max_size) {
            $out['error'][] = "File is too big";
        }

        if ($check_image) {
            if (!getimagesize($_FILES[$file_field]['tmp_name'])) {
                $out['error'][] = "Uploaded file is not a valid file";
            }
        }

        if ($random_name) {
            $tmp = str_replace(array('.', ' '), array('', ''), microtime());

            if (!$tmp || $tmp == '') {
                $out['error'][] = "File must have a name";
            }
            $newname = $tmp . '.' . $ext;
        } else {
            $newname = $name . '.' . $ext;
        }

        if (file_exists($path . $newname)) {
            $out['error'][] = "A file with this name already exists";
        }

        if (count($out['error']) > 0) {
            return $out;
        }

        if (move_uploaded_file($_FILES[$file_field]['tmp_name'], $path . $newname)) {

            $out['filepath'] = $path;
            $out['filename'] = $newname;
            return $out;
        } else {
            $out['error'][] = "Server Error!";
        }

    } else {
        $out['error'][] = "No file uploaded";
        return $out;
    }
}


function gen_slug($str)
{
    # special accents
    $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?');
    $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
    return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), str_replace($a, $b, $str)));
}




function defaultValue($value, $default = false)
{
    if (!$value) return $default; else return $value;
}

function post($name, $mode = TRUE)
{
    if (isset($_POST[$name]))
        return filter($_POST[$name], $mode);
    else
        return NULL;
}

function get($name, $mode = TRUE)
{
    if (isset($_GET[$name]))
        return filter($_GET[$name], $mode);
    else
        return NULL;
}

function filter($data, $mode = TRUE)
{
    if ($mode === FALSE)
        return $data;

    if (!is_array($data)) {
        if ($mode === 'int') {
            return intval($data);
        } elseif ($mode === 'float') {
            return floatval($data);
        } else {
            $data = str_replace("\0", "", trim($data));
            $data = htmlentities($data, ENT_QUOTES, "UTF-8");
            return $data;
        }
    } else {
        foreach ($data as $key => $value)
            $data[$key] = filter($value, $mode);

        return $data;
    }
}

function print_data($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function siteUrl($url = '')
{
    return WEBSITE_URL . $url;
}

function redirect($url)
{
    header('Location: ' . $url);
    exit;
}