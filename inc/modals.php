<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog cv-modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close close-icon" data-dismiss="modal">&times;</button>
          <h3 class="heading cv-upload-text"><span>CV Upload</span></h3>
        </div>
        <div class="modal-body">
            <form action="" id="submit-cv-form" method="">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-field text">
                            <input placeholder="First Name" id="firstname" name="firstname" type="text" data-val="true" data-val-required="This is a required field">
                            <span class="field-validation-valid" data-valmsg-for="firstname" data-valmsg-replace="true"></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-field text">
                            <input placeholder="Last name" id="lastname" name="lastname" type="text" data-val="true" data-val-required="This is a required field">
                            <span class="field-validation-valid" data-valmsg-for="lastname" data-valmsg-replace="true"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="container-check">
                            <div class="round">
                                <input type="checkbox" id="checkbox1" />
                                <label for="checkbox1"></label>
                                <a href="" class="checkbox-text1">Terms & conditions</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="container-check">
                            <div class="round">
                                <input type="checkbox" id="checkbox2" />
                                <label for="checkbox2"></label>
                                <span class="checkbox-text2">Are you elibigible to work in the UK?</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-field file">
                            <div class="upload-cv-btn">
                                <div class="btn i-btn toggle-j-modal modal-cv">
                                    <i class="fa fa-cloud-upload toggle-j-modal" data-id="cv-upload-form"></i>
                                    <span>CV Upload</span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="btn btn-ghost-primary modal-send">
                            <span>Send</span>
                            <input type="submit" value="Send">
                        </div>
                    </div>
                </div>
            </form>


        </div>

        </div>
      </div>
      
    </div>
  </div>