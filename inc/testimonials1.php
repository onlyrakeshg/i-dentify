<?php
$getTestimonials = $db->prepare("SELECT * FROM `testimonials`");
$getTestimonials->execute();
$getTestimonials = $getTestimonials->fetchAll();
?>

<div class="for-slider wow fadeInLeft">
    <div class="container">
        <div class="row">
            <div class=" col-xs-12">
                <h3 class="heading largeheading"><span>Testimonials</span></h3>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="slider single-item multiple-testimonials1">

            <?php foreach ($getTestimonials as $row) { ?>
                <div class="slider-inner-block3">
                    <div class="testimonial-block">
                        <?= $row['text']; ?>
                        <p style="margin-top:0">
                            <?php echo $row['author'] ? $row['author'] . ", " : ""; ?>
                            <?php echo $row['position'] ? $row['position'] : ""; ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
            