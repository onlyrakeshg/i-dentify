<?php $page_allowed = 1;
$page = "Micro RPO";
include("inc/support/common.php");include("backend/config/config.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php include("inc/head-includes.php"); ?>
	
	<style type="text/css">
		.banner .curved-element { border: 0px; }
	</style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">

    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
            <?php include("inc/header.php"); ?>
        </div>
        </div>
        <div class="content">
            <div class="banner small-banner  rpo-bn">


                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">Micro RPO </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="text-content rte wow fadeInLeft">
							
                            <?= getContentPage(30); ?>

							
                        </div>
                    </div>
                </div>
            </div>
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="text-content">
                                    <?= getContentPage(31); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="image-content">
                                    <div class="about-img">
			
										<div>
											<img src="images/rpo-slide.jpg" alt=""/>
										</div>


									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
	<?php 
		$getClient = $db->prepare("SELECT * FROM `clients` ORDER BY `client_id` DESC");
		$getClient->execute();
		$newsCount = $getClient->rowCount();
		$getClient = $getClient->fetchAll();
?>
		<div class="container">
			
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="text-content rte wow fadeInLeft">
							
							<h2 class="heading text-center small-text-section">Active Micro RPO's</h2>

							
                        </div>
                    </div>
                </div>
			
                <div class="row">
			<?php $count = 1; foreach ($getClient as $row) { ?>
			
		<div class="col-md-4 col-sm-4">
			<article class="postbox">
				<figure><a href="<?php echo $config['website_url'];?>microsite/<?php echo $row['ref']; ?>">
					<div class="rpo-image" style="background:url('<?= $config['website_url']; ?>uploads/images/<?= $row['key_image']; ?>') center center; background-size:cover;"></div>
					</a></figure>
				<h4><a href="<?php echo $config['website_url'];?>microsite/<?php echo $row['ref']; ?>"><?php echo $row['client_name']; ?></a></h4>
				<p><?php echo substr(strip_tags($row['content']), 0, 100); ?>...</p>
			</article>
		</div>
			
			
			
			<?php if($count % 3 == 0):?><div class="clearfix"></div><?php endif; ?>
			<?php $count++; } ?>
		</div>	
		</div>	
				
				
                
            </div>
        </div>
           
		<?php include("inc/footer.php"); ?>

		<?php include("inc/modals.php"); ?>

		<?php include("inc/footer-js.php"); ?>

</body>
</html>