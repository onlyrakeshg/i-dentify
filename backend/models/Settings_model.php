<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `settings` ( `name` VARCHAR(150) NOT NULL , `title` VARCHAR(150) NOT NULL, `value` TEXT NOT NULL , PRIMARY KEY (`name`)) ENGINE = InnoDB;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->get('settings')->row();
    }

    public function get($params = array(), $per_page = NULL, $offset = NULL, $order_by = NULL, $order_direction = NULL)
    {
        if ($per_page !== NULL && $offset !== NULL)
            $this->db->limit($per_page, $offset);

        if ($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);

        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->get('settings')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('settings');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('settings', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('settings', $data);
    }

}