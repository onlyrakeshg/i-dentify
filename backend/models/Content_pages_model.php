<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Content_pages_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `content_pages` (
  `content_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `content` text NOT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_desc` varchar(255) DEFAULT NULL,
  `canonical` varchar(100) DEFAULT NULL,
  `date` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`content_page_id`)
) ENGINE=InnoDB;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->get('content_pages')->row();
    }

    public function get($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->get('content_pages')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('content_pages');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('content_pages', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('content_pages', $data);
    }

}