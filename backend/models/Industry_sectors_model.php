<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Industry_sectors_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array("CREATE TABLE `industry_sectors` (
  `industry_sector_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'Industry Sector',
  PRIMARY KEY (`industry_sector_id`)
) ENGINE=InnoDB;");

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->get('industry_sectors')->row();
    }

    public function get($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->get('industry_sectors')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('industry_sectors');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('industry_sectors', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('industry_sectors', $data);
    }

    public function delete_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->delete('industry_sectors');
    }

    public function insert_id()
    {
        return $this->db->insert_id();
    }

}
