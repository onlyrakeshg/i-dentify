<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vincere_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array("CREATE TABLE `vincere_integration` (
  `vincere_integration_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(200) NOT NULL,
  `refresh_token` varchar(200) NOT NULL,
  `id_token` varchar(200) NOT NULL,
  `expires_in` bigint NOT NULL,
  `time` bigint NOT NULL,
  PRIMARY KEY (`vincere_integration_id`)
) ENGINE=InnoDB;",
            "ALTER TABLE `vincere_integration` CHANGE `access_token` `access_token` TEXT NOT NULL, CHANGE `refresh_token` `refresh_token` TEXT NOT NULL, CHANGE `id_token` `id_token` TEXT NOT NULL; ",
            "ALTER TABLE `vincere_integration` CHANGE `refresh_token` `refresh_token` TEXT NULL DEFAULT NULL; ");

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array(), $order_by = NULL, $order_direction = NULL)
    {
        if($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);

        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->get('vincere_integration')->row();
    }

    public function get($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->get('vincere_integration')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('vincere_integration');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('vincere_integration', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('vincere_integration', $data);
    }

    public function delete_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->delete('vincere_integration');
    }

}