<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bullhorn_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `bullhorn_integration` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `access_token` VARCHAR(200) NOT NULL ,
  `refresh_token` VARCHAR(200) NOT NULL ,
  `expires` INT NOT NULL ,
  `created` BIGINT NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array(), $order_by = NULL, $order_direction = NULL)
    {
		if($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);	
		
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->get('bullhorn_integration')->row();
    }

    public function get($params = array(), $per_page = NULL, $offset = NULL, $order_by = NULL, $order_direction = NULL)
    {
        if($per_page !== NULL && $offset !== NULL)
            $this->db->limit($per_page, $offset);

        if($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);

        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->get('bullhorn_integration')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('bullhorn_integration');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('bullhorn_integration', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('bullhorn_integration', $data);
    }

    public function delete($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->delete('bullhorn_integration');
    }
}
