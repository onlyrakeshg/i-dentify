<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_testimonials_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `clients_testimonials` (
  `clients_testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `date` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`clients_testimonial_id`)
) ENGINE=InnoDB;",
            "ALTER TABLE `clients_testimonials` ADD `vimeo_video_id` VARCHAR(150) NULL DEFAULT NULL AFTER `content`, ADD `video_image` VARCHAR(150) NULL DEFAULT NULL AFTER `vimeo_video_id`;",
            "ALTER TABLE `clients_testimonials` DROP `video_image`;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where(strpos('.', $column) === FALSE ? 'clients_testimonials.' . $column : $column, $value);
        }

        $this->db->join('clients', 'clients_testimonials.client_id = clients.client_id', 'left');
        $this->db->select('clients_testimonials.*, clients.client_name');

        $this->db->limit(1);
        return $this->db->get('clients_testimonials')->row();
    }

    public function get($params = array(), $per_page = NULL, $offset = NULL, $order_by = NULL, $order_direction = NULL)
    {
        if ($per_page !== NULL && $offset !== NULL)
            $this->db->limit($per_page, $offset);

        if ($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);

        foreach ($params as $column => $value) {
            $this->db->where(strpos('.', $column) === FALSE ? 'clients_testimonials.' . $column : $column, $value);
        }

        $this->db->join('clients', 'clients_testimonials.client_id = clients.client_id', 'left');
        $this->db->select('clients_testimonials.*, clients.client_name');

        return $this->db->get('clients_testimonials')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('clients_testimonials');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('clients_testimonials', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('clients_testimonials', $data);
    }

}