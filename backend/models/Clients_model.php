<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` varchar(50) DEFAULT NULL,
  `client_name` varchar(100) NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `logo` varchar(50) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `company_segment` varchar(150) NOT NULL,
  `company_size` int(11) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created_date` datetime DEFAULT NULL,
 PRIMARY KEY (`client_id`)
) ENGINE=InnoDB;",
            "CREATE TABLE `clients_sectors` (
  `clients_sector_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `industry_sector_id` int(11) NOT NULL,
  PRIMARY KEY (`clients_sector_id`)
) ENGINE=InnoDB;",
            "CREATE TABLE `clients_locations` (
  `clients_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`clients_location_id`)
) ENGINE=InnoDB;",
            "ALTER TABLE `clients` ADD `matterport_id` VARCHAR(100) NULL DEFAULT NULL AFTER `content`;",
            "ALTER TABLE `clients` ADD `facebook_feed` TEXT NULL DEFAULT NULL AFTER `created_date`;",
            "ALTER TABLE `clients` ADD `twitter_feed` TEXT NULL DEFAULT NULL AFTER `facebook_feed`;",
            "ALTER TABLE `clients` ADD `key_image` VARCHAR(100) NULL DEFAULT NULL AFTER `twitter_feed`, ADD `video` VARCHAR(100) NULL DEFAULT NULL AFTER `key_image`;",
            "ALTER TABLE `clients` ADD `tags` TEXT NULL DEFAULT NULL AFTER `video`;",
            "ALTER TABLE `clients` DROP `company_segment`;",
            "ALTER TABLE `clients` ADD `overview_image` VARCHAR(150) NULL DEFAULT NULL AFTER `key_image`;",
            "ALTER TABLE `clients` ADD `opportunities_image` VARCHAR(150) NULL DEFAULT NULL AFTER `overview_image`;",
            "ALTER TABLE `clients` ADD `awards_title` VARCHAR(300) NULL DEFAULT NULL AFTER `tags`;",
            "ALTER TABLE `clients` ADD `meta_title` VARCHAR(150) NULL DEFAULT NULL AFTER `awards_title`, ADD `meta_keywords` TEXT NULL DEFAULT NULL AFTER `meta_title`, ADD `meta_desc` TEXT NULL DEFAULT NULL AFTER `meta_keywords`, ADD `open_graph_image` VARCHAR(150) NULL DEFAULT NULL AFTER `meta_desc`;",
            "ALTER TABLE `clients` DROP `email`, DROP `password`;",
            "ALTER TABLE `clients`
  DROP `matterport_id`,
  DROP `facebook_feed`,
  DROP `twitter_feed`,
  DROP `awards_title`;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function insert_id()
    {
        return $this->db->insert_id();
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        $client = $this->db->get('clients')->row();

        if ($client) {
            $client->sector_ids = array();
            $client->sectors = array();
            $sectors = $this->sectors_get(array(
                'client_id' => $client->client_id
            ));
            if (is_array($sectors) && count($sectors)) {
                foreach ($sectors as $sector) {
                    $client->sector_ids[] = $sector->industry_sector_id;
                    $client->sectors[] = $sector;
                }
            }

            $client->location_ids = array();
            $client->locations = array();
            $locations = $this->locations_get(array(
                'client_id' => $client->client_id
            ));
            if (is_array($locations) && count($locations)) {
                foreach ($locations as $location) {
                    $client->location_ids[] = $location->location_id;
                    $client->locations[] = $location;
                }
            }
        }

        return $client;
    }

    public function get($params = array(), $per_page = NULL, $offset = NULL, $order_by = NULL, $order_direction = NULL)
    {
        if ($per_page !== NULL && $offset !== NULL)
            $this->db->limit($per_page, $offset);

        if ($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);

        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $clients = $this->db->get('clients')->result();

        if (is_array($clients) && count($clients)) {
            foreach ($clients as $client) {
                $client->sector_ids = array();
                $client->sectors = array();
                $sectors = $this->sectors_get(array(
                    'client_id' => $client->client_id
                ));
                if (is_array($sectors) && count($sectors)) {
                    foreach ($sectors as $sector) {
                        $client->sector_ids[] = $sector->industry_sector_id;
                        $client->sectors[] = $sector;
                    }
                }
            }

            foreach ($clients as $client) {
                $client->location_ids = array();
                $client->locations = array();
                $locations = $this->locations_get(array(
                    'client_id' => $client->client_id
                ));
                if (is_array($locations) && count($locations)) {
                    foreach ($locations as $location) {
                        $client->location_ids[] = $location->location_id;
                        $client->locations[] = $location;
                    }
                }
            }
        }

        return $clients;
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('clients');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('clients', $data);
    }

    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('clients', $data);
    }

    public function delete_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->delete('clients');
    }

    public function delete($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->delete('clients');
    }

    public function sectors_get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where('clients_sectors.' . $column, $value);
        }

        $this->db->limit(1);
        $this->db->join('industry_sectors', 'industry_sectors.industry_sector_id = clients_sectors.industry_sector_id', 'left');
        $this->db->select('clients_sectors.*, industry_sectors.name as industry_sector_name');
        return $this->db->get('clients_sectors')->row();
    }

    public function sectors_get($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where('clients_sectors.' . $column, $value);
        }

        $this->db->join('industry_sectors', 'industry_sectors.industry_sector_id = clients_sectors.industry_sector_id', 'left');
        $this->db->select('clients_sectors.*, industry_sectors.name as industry_sector_name');
        return $this->db->get('clients_sectors')->result();
    }

    public function sectors_get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('clients_sectors');
    }

    public function sectors_add_row($data = array())
    {
        return $this->db->insert('clients_sectors', $data);
    }

    public function sectors_add($data = array())
    {
        return $this->db->insert_batch('clients_sectors', $data);
    }

    public function sectors_update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('clients_sectors', $data);
    }

    public function sectors_delete_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->delete('clients_sectors');
    }

    public function sectors_delete($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->delete('clients_sectors');
    }


    public function locations_get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where('clients_locations.' . $column, $value);
        }

        $this->db->limit(1);
        $this->db->join('locations', 'locations.location_id = clients_locations.location_id', 'left');
        $this->db->select('clients_locations.*, locations.name as location_name');
        return $this->db->get('clients_locations')->row();
    }

    public function locations_get($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where('clients_locations.' . $column, $value);
        }

        $this->db->join('locations', 'locations.location_id = clients_locations.location_id', 'left');
        $this->db->select('clients_locations.*, locations.name as location_name');
        return $this->db->get('clients_locations')->result();
    }

    public function locations_get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('clients_locations');
    }

    public function locations_add_row($data = array())
    {
        return $this->db->insert('clients_locations', $data);
    }

    public function locations_add($data = array())
    {
        return $this->db->insert_batch('clients_locations', $data);
    }

    public function locations_update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('clients_locations', $data);
    }

    public function locations_delete_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->delete('clients_locations');
    }

    public function locations_delete($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->delete('clients_locations');
    }

}
