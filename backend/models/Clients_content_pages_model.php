<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_content_pages_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `clients_content_pages` (
  `clients_content_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `content` text NOT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_desc` varchar(255) DEFAULT NULL,
  `canonical` varchar(100) DEFAULT NULL,
  `date` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`clients_content_page_id`)
) ENGINE=InnoDB;",
            "ALTER TABLE `clients_content_pages` ADD `client_id` INT NULL DEFAULT NULL AFTER `clients_content_page_id`;",
            "ALTER TABLE `clients_content_pages` ADD `content_image` VARCHAR(150) NULL DEFAULT NULL AFTER `image`, ADD `thumb` VARCHAR(150) NULL DEFAULT NULL AFTER `content_image`;",
            "ALTER TABLE `clients_content_pages` ADD `sort` INT NULL DEFAULT NULL AFTER `slug`;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array(), $order_by = NULL, $order_direction = NULL)
    {
        if ($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);

        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->get('clients_content_pages')->row();
    }

    public function get($params = array(), $order_by = NULL, $order_direction = NULL)
    {
        if ($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);

        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->get('clients_content_pages')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('clients_content_pages');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('clients_content_pages', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('clients_content_pages', $data);
    }

    public function get_max($col, $params = array())
    {
        foreach ($params as $column => $value)
            $this->db->where($column, $value);

        $this->db->select_max($col);
        return $this->db->get('clients_content_pages')->row()->{$col};
    }

    public function get_min($col, $params = array())
    {
        foreach ($params as $column => $value)
            $this->db->where($column, $value);

        $this->db->select_min($col);
        return $this->db->get('clients_content_pages')->row()->{$col};
    }

}