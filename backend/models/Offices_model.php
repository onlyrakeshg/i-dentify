<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Offices_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `offices` (
  `office_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `coordinates` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`office_id`)
) ENGINE=InnoDB;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where(strpos('.', $column) === FALSE ? 'offices.' . $column : $column, $value);
        }

        $this->db->join('clients', 'offices.client_id = clients.client_id', 'left');
        $this->db->select('offices.*, clients.client_name');

        $this->db->limit(1);
        return $this->db->get('offices')->row();
    }

    public function get($params = array(), $per_page = NULL, $offset = NULL, $order_by = NULL, $order_direction = NULL)
    {
        if ($per_page !== NULL && $offset !== NULL)
            $this->db->limit($per_page, $offset);

        if ($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);

        foreach ($params as $column => $value) {
            $this->db->where(strpos('.', $column) === FALSE ? 'offices.' . $column : $column, $value);
        }

        $this->db->join('clients', 'offices.client_id = clients.client_id', 'left');
        $this->db->select('offices.*, clients.client_name');

        return $this->db->get('offices')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('offices');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('offices', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('offices', $data);
    }

}