<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Videos_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `videos` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `vimeo_video_id` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where(strpos('.', $column) === FALSE ? 'videos.' . $column : $column, $value);
        }

        $this->db->join('clients', 'videos.client_id = clients.client_id', 'left');
        $this->db->select('videos.*, clients.client_name');

        $this->db->limit(1);
        return $this->db->get('videos')->row();
    }

    public function get($params = array(), $per_page = NULL, $offset = NULL, $order_by = NULL, $order_direction = NULL)
    {
        if ($per_page !== NULL && $offset !== NULL)
            $this->db->limit($per_page, $offset);

        if ($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);

        foreach ($params as $column => $value) {
            $this->db->where(strpos('.', $column) === FALSE ? 'videos.' . $column : $column, $value);
        }

        $this->db->join('clients', 'videos.client_id = clients.client_id', 'left');
        $this->db->select('videos.*, clients.client_name');

        return $this->db->get('videos')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('videos');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('videos', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('videos', $data);
    }

}