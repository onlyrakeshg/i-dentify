<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Library_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `cv_library` (
  `library_id` int(11) NOT NULL AUTO_INCREMENT,
  `vacancy_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `tel` varchar(150) NOT NULL,
  `date` datetime NOT NULL,
  `filename` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`library_id`)
) ENGINE=InnoDB;",
            "ALTER TABLE `cv_library` ADD `candidate_id` INT NULL DEFAULT NULL AFTER `vacancy_id`;",
            "ALTER TABLE `cv_library` ADD `linkedin` BOOLEAN NOT NULL DEFAULT FALSE AFTER `filename`;",
            "ALTER TABLE `cv_library` CHANGE `tel` `tel` VARCHAR(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;",
            "ALTER TABLE `cv_library` ADD `bh_candidate_id` INT NULL DEFAULT NULL AFTER `candidate_id`, ADD `application_id` INT NULL DEFAULT NULL AFTER `bh_candidate_id`;",
            "ALTER TABLE `cv_library` ADD `bh_notes` TEXT NULL DEFAULT NULL AFTER `linkedin`;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->get('cv_library')->row();
    }

    public function get($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->get('cv_library')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('cv_library');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('cv_library', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('cv_library', $data);
    }


    public function delete_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->delete('cv_library');
    }

}