<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vacancies_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `vacancies` (
  `vacancy_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `ref` varchar(100) NOT NULL,
  `contract_type` enum('permanent','temporary','fixed') NOT NULL DEFAULT 'permanent',
  `contract_length` varchar(100) DEFAULT NULL,
  `salary_type` enum('salary','hourly','daily') NOT NULL DEFAULT 'salary',
  `salary_value` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `consultant_id` int(11) NOT NULL,
  `client_name` varchar(100) DEFAULT NULL,
  `client_email` varchar(150) DEFAULT NULL,
  `client_tel` varchar(25) DEFAULT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_desc` varchar(255) DEFAULT NULL,
  `canonical` varchar(100) DEFAULT NULL,
  `date` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `expiry_reason` varchar(100) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`vacancy_id`)
) ENGINE=InnoDB;",
            "CREATE TABLE `vacancies_locations` (
  `vacancies_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `vacancy_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`vacancies_location_id`)
) ENGINE=InnoDB;",
            "CREATE TABLE `vacancies_sectors` (
  `vacancies_sector_id` int(11) NOT NULL AUTO_INCREMENT,
  `vacancy_id` int(11) NOT NULL,
  `industry_sector_id` int(11) NOT NULL,
  PRIMARY KEY (`vacancies_sector_id`)
) ENGINE=InnoDB;",
            "ALTER TABLE `vacancies` ADD `postcode` VARCHAR(150) NULL DEFAULT NULL AFTER `client_tel`;",
            "ALTER TABLE `vacancies` ADD `client_id` INT NULL DEFAULT NULL AFTER `vacancy_id`;",
            "ALTER TABLE `vacancies` ADD `allow_on_portal` BOOLEAN NOT NULL DEFAULT FALSE AFTER `slug`;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function insert_id()
    {
        return $this->db->insert_id();
    }

    public function get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        $vacancy = $this->db->get('vacancies')->row();

        if ($vacancy) {
            $vacancy->sector_ids = array();
            $vacancy->sectors = array();
            $sectors = $this->sectors_get(array(
                'vacancy_id' => $vacancy->vacancy_id
            ));
            if (is_array($sectors) && count($sectors)) {
                foreach ($sectors as $sector) {
                    $vacancy->sector_ids[] = $sector->industry_sector_id;
                    $vacancy->sectors[] = $sector;
                }
            }

            $vacancy->location_ids = array();
            $vacancy->locations = array();
            $locations = $this->locations_get(array(
                'vacancy_id' => $vacancy->vacancy_id
            ));
            if (is_array($locations) && count($locations)) {
                foreach ($locations as $location) {
                    $vacancy->location_ids[] = $location->location_id;
                    $vacancy->locations[] = $location;
                }
            }
        }

        return $vacancy;
    }

    public function get($params = array(), $per_page = NULL, $offset = NULL, $order_by = NULL, $order_direction = NULL)
    {
        if ($per_page !== NULL && $offset !== NULL)
            $this->db->limit($per_page, $offset);

        if ($order_by !== NULL && $order_direction !== NULL)
            $this->db->order_by($order_by, $order_direction);

        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $vacancies = $this->db->get('vacancies')->result();

        if (is_array($vacancies) && count($vacancies)) {
            foreach ($vacancies as $vacancy) {
                $vacancy->sector_ids = array();
                $vacancy->sectors = array();
                $sectors = $this->sectors_get(array(
                    'vacancy_id' => $vacancy->vacancy_id
                ));
                if (is_array($sectors) && count($sectors)) {
                    foreach ($sectors as $sector) {
                        $vacancy->sector_ids[] = $sector->industry_sector_id;
                        $vacancy->sectors[] = $sector;
                    }
                }

                $vacancy->location_ids = array();
                $vacancy->locations = array();
                $locations = $this->locations_get(array(
                    'vacancy_id' => $vacancy->vacancy_id
                ));
                if (is_array($locations) && count($locations)) {
                    foreach ($locations as $location) {
                        $vacancy->location_ids[] = $location->location_id;
                        $vacancy->locations[] = $location;
                    }
                }
            }
        }

        return $vacancies;
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('vacancies');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('vacancies', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('vacancies', $data);
    }

    public function update($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }
        
        return $this->db->update('vacancies', $data);
    }

    public function delete_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->delete('vacancies');
    }

    public function sectors_get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where('vacancies_sectors.' . $column, $value);
        }

        $this->db->limit(1);
        $this->db->join('industry_sectors', 'industry_sectors.industry_sector_id = vacancies_sectors.industry_sector_id', 'left');
        $this->db->select('vacancies_sectors.*, industry_sectors.name as industry_sector_name');
        return $this->db->get('vacancies_sectors')->row();
    }

    public function sectors_get($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where('vacancies_sectors.' . $column, $value);
        }

        $this->db->join('industry_sectors', 'industry_sectors.industry_sector_id = vacancies_sectors.industry_sector_id', 'left');
        $this->db->select('vacancies_sectors.*, industry_sectors.name as industry_sector_name');
        return $this->db->get('vacancies_sectors')->result();
    }

    public function sectors_get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('vacancies_sectors');
    }

    public function sectors_add_row($data = array())
    {
        return $this->db->insert('vacancies_sectors', $data);
    }

    public function sectors_add($data = array())
    {
        return $this->db->insert_batch('vacancies_sectors', $data);
    }

    public function sectors_update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('vacancies_sectors', $data);
    }

    public function sectors_delete_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->delete('vacancies_sectors');
    }

    public function sectors_delete($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->delete('vacancies_sectors');
    }

    public function locations_get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where('vacancies_locations.' . $column, $value);
        }

        $this->db->limit(1);
        $this->db->join('locations', 'locations.location_id = vacancies_locations.location_id', 'left');
        $this->db->select('vacancies_locations.*, locations.name as location_name');
        return $this->db->get('vacancies_locations')->row();
    }

    public function locations_get($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where('vacancies_locations.' . $column, $value);
        }

        $this->db->join('locations', 'locations.location_id = vacancies_locations.location_id', 'left');
        $this->db->select('vacancies_locations.*, locations.name as location_name');
        return $this->db->get('vacancies_locations')->result();
    }

    public function locations_get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('vacancies_locations');
    }

    public function locations_add_row($data = array())
    {
        return $this->db->insert('vacancies_locations', $data);
    }

    public function locations_add($data = array())
    {
        return $this->db->insert_batch('vacancies_locations', $data);
    }

    public function locations_update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('vacancies_locations', $data);
    }

    public function locations_delete_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->delete('vacancies_locations');
    }

    public function locations_delete($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->delete('vacancies_locations');
    }

}
