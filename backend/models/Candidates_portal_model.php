<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Candidates_portal_model extends CI_Model
{
    public function plugin_install()
    {
        /* Queries must be performed while installation */
        $queries = array(
            "CREATE TABLE `candidates_portal` (
  `candidate_id` INT NOT NULL AUTO_INCREMENT ,
  `firstname` VARCHAR(150) NOT NULL ,
  `lastname` VARCHAR(150) NOT NULL ,
  `email` VARCHAR(150) NOT NULL ,
  `tel` VARCHAR(50) NULL DEFAULT NULL ,
  `cv` VARCHAR(100) NULL DEFAULT NULL ,
  `password` VARCHAR(200) NOT NULL ,
  `photo` VARCHAR(100) NULL DEFAULT NULL ,
  `title` VARCHAR(250) NULL DEFAULT NULL ,
  `content` TEXT NULL DEFAULT NULL ,
  `date` DATETIME NOT NULL ,
  `deleted` BOOLEAN NOT NULL DEFAULT FALSE ,
  PRIMARY KEY (`candidate_id`)
) ENGINE = InnoDB;",
            "ALTER TABLE `candidates_portal` ADD `location` VARCHAR(150) NULL DEFAULT NULL AFTER `content`, ADD `job_title` VARCHAR(150) NULL DEFAULT NULL AFTER `location`, ADD `status` VARCHAR(150) NULL DEFAULT NULL AFTER `job_title`, ADD `interview_availability` VARCHAR(150) NULL DEFAULT NULL AFTER `status`;",
            "ALTER TABLE `candidates_portal` ADD `alerts_locations` TEXT NULL DEFAULT NULL AFTER `content`, ADD `alerts_sectors` TEXT NULL DEFAULT NULL AFTER `alerts_locations`;",
            "ALTER TABLE `candidates_portal` ADD `ref` VARCHAR(20) NULL DEFAULT NULL AFTER `candidate_id`;",
            "ALTER TABLE `candidates_portal` CHANGE `ref` `bh_candidate_id` VARCHAR(100) NULL DEFAULT NULL;".
            "ALTER TABLE `candidates_portal` ADD `bh_notes` TEXT NULL DEFAULT NULL AFTER `bh_candidate_id`;"
        );

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function plugin_upgrade($version)
    {
        $queries = array();

        /* Queries must be performed while upgrading. Use case to check actual version */
        switch ($version) {

        }

        foreach ($queries as $query)
            $this->db->query($query);
    }

    public function get_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->get('candidates_portal')->row();
    }

    public function get($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->get('candidates_portal')->result();
    }

    public function get_count($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        return $this->db->count_all_results('candidates_portal');
    }

    public function add_row($data = array())
    {
        return $this->db->insert('candidates_portal', $data);
    }


    public function update_row($params = array(), $data = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->update('candidates_portal', $data);
    }

    public function delete_row($params = array())
    {
        foreach ($params as $column => $value) {
            $this->db->where($column, $value);
        }

        $this->db->limit(1);
        return $this->db->delete('candidates_portal');
    }
}