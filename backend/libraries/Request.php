<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Request
{
    private $handler;
    private $info = NULL;
    private $headers = NULL;

    public function get_contents($url, $get = NULL, $post = NULL, $headers = FALSE, $custom_method = NULL)
    {
        $this->handler = curl_init();
        curl_setopt($this->handler, CURLOPT_URL, $url . ($get ? "?" . http_build_query($get) : ""));
        curl_setopt($this->handler, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($this->handler, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($this->handler, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($this->handler, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->handler, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0');
        curl_setopt($this->handler, CURLINFO_HEADER_OUT, TRUE);

        if ($headers && is_array($headers))
            curl_setopt($this->handler, CURLOPT_HTTPHEADER, $headers);

        if ($post) {
            curl_setopt($this->handler, CURLOPT_POST, TRUE);
            if (is_array($post))
                curl_setopt($this->handler, CURLOPT_POSTFIELDS, http_build_query($post));
            else
                curl_setopt($this->handler, CURLOPT_POSTFIELDS, $post);
        }

        if ($custom_method)
            curl_setopt($this->handler, CURLOPT_CUSTOMREQUEST, $custom_method);

        $output = curl_exec($this->handler);
        $this->info = curl_getinfo($this->handler);
        $this->headers = curl_getinfo($this->handler, CURLINFO_HEADER_OUT);
        if ($this->handler) curl_close($this->handler);

        if ($this->info['http_code'] == 301 || $this->info['http_code'] == 302) {
            $url = $this->info['redirect_url'];
            $url_parsed = parse_url($url);
            return (isset($url_parsed)) ? $this->get_contents($url, $get, $post, $headers, $custom_method) : '';
        }

        return $output;
    }

    public function get_last_info()
    {
        return $this->info;
    }

    public function get_last_headers()
    {
        return $this->headers;
    }
}