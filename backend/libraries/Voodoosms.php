<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Voodoosms
{

    public function sendSMS($dest, $orig, $msg, $uid, $pass, $validity = 1, $format = 'json')
    {
        return $this->get_contents("https://www.voodoosms.com/vapi/server/sendSMS", NULL, array(
            "uid" => $uid,
            "pass" => $pass,
            "dest" => $dest,
            "orig" => $orig,
            "msg" => $msg,
            "validity" => $validity,
            "format" => $format
        ), FALSE);
    }

    public function get_contents($url, $get = NULL, $post = NULL, $headers = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . ($get ? "?" . http_build_query($get) : ""));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0');
        if ($headers && is_array($headers))
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if ($post) {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        }

        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        if ($ch) curl_close($ch);

        if ($info['http_code'] == 301 || $info['http_code'] == 302) {
            $url = $info['redirect_url'];
            $url_parsed = parse_url($url);
            return (isset($url_parsed)) ? $this->get_contents($url, $get, $post, $headers) : '';
        }

        return $output;
    }
}