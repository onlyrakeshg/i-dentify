<?php
class File
{
    /**
     * @param $path
     * @return bool
     */
    static public function exist($path)
    {
        if (file_exists($path))
            return true;
        else
            return false;
    }

    /**
     * @param $path
     * @return bool
     */
    static public function remove($path)
    {
        return @unlink($path);
    }

    /**
     * @param $filePath
     * @param $copyPath
     * @return bool
     */
    static public function copy($filePath, $copyPath)
    {
        return @copy($filePath, $copyPath);
    }


    /**
     * function mkdirRecursive
     * @param $path
     * @param string $chmod
     * @return string
     */
    static public function mkdir($path, $chmod = '0755')
    {
        $path = trim($path, '/');
        $array = explode('/', $path);
        $fullPath = '';

        foreach ($array as $value) {
            $fullPath .= $value.'/';
            @mkdir($fullPath, $chmod);
        }

        return $fullPath;
    }


    /**
     * @var array
     */
    static public $allowedImageFormats = array(
        'gif'   => true,
        'png'   => true,
        'jpg'   => true,
        'jpeg'   => true
    );

    /**
     * @var int
     */
    static public $allowedFileSize = 209715200; //200M

    /**
     * @var
     */
    static public $error;


    static public function remkdir($path, $chmod = '0777') {
        $array = explode('/', trim($path, '/'));
        $s = '';

        foreach ($array as $value) {
            $s .= $value.'/';
            if (!is_dir($s)) {
                @mkdir($s, 0777);
                @chmod($s, 0777);
            }
        }

        return true;
    }


    /**
     * Function ImageResize
     * @param $src - file path
     * @param $dst - file out
     * @param $width - new width
     * @param $height - new height
     * @param int $crop - crop image
     * @return bool|string
     */
    static public function ImageResize($src, $dst, $width, $height, $crop = 0) {

//        $tmp=tmpfile();
//        $file=stream_get_meta_data($tmp)['uri'];
//        $ch=curl_init($link);
//
//        curl_setopt_array($ch,array(
//            CURLOPT_FILE=>$tmp,
//            CURLOPT_ENCODING=>''
//        ));
//
//        curl_exec($ch);
//        curl_close($ch);
//
//        list($width, $height) = getimagesize($file);
//        fclose($tmp); // << explicitly deletes the file, freeing up disk space etc~ - though php would do this automatically at the end of script execution anyway.


//        if (!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";

        $type = strtolower(substr(strrchr($src, "."), 1));
        if ($type == 'jpeg') $type = 'jpg';

        switch ($type) {
            case 'bmp':
                $img = imagecreatefromwbmp($src);
                break;
            case 'gif':
                $img = imagecreatefromgif($src);
                break;
            case 'jpg':
                $img = imagecreatefromjpeg($src);
                break;
            case 'png':
                $img = imagecreatefrompng($src);
                break;
            default :
                return "Unsupported picture type!";
        }

        $w = imagesx($img);
        $h = imagesy($img);

        // resize
        if ($crop) {
            if ($w < $width or $h < $height) return "Picture is too small!";
            $ratio = max($width / $w, $height / $h);
            $h = $height / $ratio;
            $x = ($w - $width / $ratio) / 2;
            $w = $width / $ratio;
        } else {
            if ($w < $width and $h < $height) return "Picture is too small!";
            $ratio = min($width / $w, $height / $h);
            $width = $w * $ratio;
            $height = $h * $ratio;
            $x = 0;
        }

        $new = imagecreatetruecolor($width, $height);

        // preserve transparency
        if ($type == "gif" or $type == "png") {
            imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
            imagealphablending($new, false);
            imagesavealpha($new, true);
        }

        imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

        switch ($type) {
            case 'bmp':
                imagewbmp($new, $dst);
                break;
            case 'gif':
                imagegif($new, $dst);
                break;
            case 'jpg':
                imagejpeg($new, $dst);
                break;
            case 'png':
                imagepng($new, $dst);
                break;
        }

        imageDestroy($new);

        return true;
    }


    /**
     * Function LoadImg
     * @param $file
     * @param array $data
     * @return array
     */
    static public function LoadImg($file, $data = array())
    {
        /*
        "+" - (нужно указывать)
        "!" - (необязательно указывать)
        "-" - (не нужно указывать)

        $file - файл(+) / $_FILES['name']
        $data['path'] - путь загрузки(+) / 'app/public/'

        $data['new_name'] - новое имя(!) / 'name' else random md5 hash
        $data['new_format'] - новый формат загружаймого файла(! по умолчанию jpg) / 'png'
        $data['resize'] - resize картинки(! по умолчанию 0) / 0 - no resize(сжать), 1 - обрезать не изменяя размеров, 2 - обрезать симетрически уменьшив
        $data['allowed_formats'] - разрешаемые форматы(!) / array('jpg' => true, 'gif' => false)
        $data['mkdir'] - создание пути(!) / true, false
        $data['min_size'] - min размер(!)
        $data['max_size'] - max размер(!)
        $data['new_width'] - новая ширина(!)
        $data['new_height'] - новая высота(!)
        $data['min_width'] - min ширина(!)
        $data['min_height'] - min высота(!)
        $data['max_width'] - max ширина(!)
        $data['max_height'] - max высота(!)
        $data['ratio'] - коэффициент(!)

        $data['format'] - формат загружаймого файла(-)
        $data['tmp_name'] - хранение tmp(-)
        $data['size'] - размер файла(-)
        $data['type'] - тип файла(-)
        $data['name'] - имя файла(-)
        $data['width'] - ширина картинки(-)
        $data['height'] - высота картинки(-)
        $data['error'] - код ошибки(-)
        */

        $data['error'] = 0;
        $data['format'] = mb_strtolower(mb_substr($file['name'], mb_strrpos($file['name'], '.')+1));
        $data['tmp_name'] = $file['tmp_name'];
        $data['size'] = $file['size'];
        $data['type'] = $file['type'];
        $data['name'] = $file['name'];
        $data['originalPath'] = trim($data['path'], '/').'/';
        $data['path'] = trim($data['path'], '/').'/';

        if (!$data['new_name'])
            $data['new_name'] = uniqid('', true);
        if (!$data['new_format'])
            $data['new_format'] = 'jpg';
        if (!$data['resize'])
            $data['resize'] = 0;
        if (!$data['allowed_formats'])
            $data['allowed_formats'] = self::$allowedImageFormats;
        if (!$data['mkdir'])
            $data['mkdir'] = true;
        if ($data['mkdir'] === true)
            self::remkdir($data['originalPath']);

        if ($data['allowed_formats'][$data['format']] !== true)
            $data['error'] = 10;

        if ($data['min_size'] && intval($data['min_size']) < $data['size'])
            $data['error'] = 20;

        if ($data['max_size'] && intval($data['max_size']) > $data['size'])
            $data['error'] = 30;

        if ($data['format'] == 'jpg')
            $imageCreateFrom = 'imagecreatefromjpeg';
        else
            $imageCreateFrom = 'imagecreatefrom'.$data['format'];

        if ($data['new_format'] == 'jpg')
            $imagePrint = 'imageJpeg';
        else
            $imagePrint = 'image'.$data['new_format'];

        // Create resource image
        $image = $imageCreateFrom($data['tmp_name']);

        $data['width'] = imagesx($image);
        $data['height'] = imagesy($image);

        // Min/Max resizing
        $minWidth = 0;
        $minHeight = 0;
        $maxWidth = 0;
        $maxHeight = 0;

        if ($data['min_width']){
            if ($data['min_width'] <= $data['width'])
                $minWidth = $data['width'];
            else
                $data['error'] = 40;
        }

        if ($data['min_height']) {
            if ($data['min_height'] <= $data['height'])
                $minHeight = $data['height'];
            else
                $data['error'] = 50;
        }

        if ($data['max_width']) {
            if ($data['max_width'] > $data['width'])
                $maxWidth = $data['width'];
            else
                $maxWidth = $data['max_width'];
        }

        if ($data['max_height']) {
            if ($data['max_height'] > $data['height'])
                $maxHeight = $data['height'];
            else
                $maxHeight = $data['max_height'];
        }

        // Приоритеты
        if (!$data['new_width']) {
            if ($maxWidth)
                $data['new_width'] = $maxWidth;
            else
                $data['new_width'] = $data['width'];
        }

        if (!$data['new_height']) {
            if ($maxHeight)
                $data['new_height'] = $maxHeight;
            else
                $data['new_height'] = $data['height'];
        }

        // Resizing
        if ($data['new_width'] == 0 && $data['new_height'] == 0) {
            $data['new_width'] = $data['width'];
            $data['new_height'] = $data['height'];
        } else if ($data['new_width'] != 0 && $data['new_height'] == 0) {
            $hw = round($data['height'] / $data['width'], 6);
            $data['new_height'] = round($hw * $data['new_width'], 0);
        } else if ($data['new_width'] == 0 && $data['new_height'] != 0) {
            $hw = round($data['width'] / $data['height'], 6);
            $data['new_width'] = round($hw * $data['new_height'], 0);
        } else if ($data['new_width'] != 0 && $data['new_height'] != 0) {

        }

        if ($data['resize'] == 1) {
            $data['height'] = $data['new_height'];
            $data['width'] = $data['new_width'];
        }

        if ($data['resize'] == 2) {
            if ($data['new_width'] > $data['new_height']) {
                $hw = round($data['new_height'] / $data['new_width'], 6);
                $data['height'] = round($hw * $data['width'], 0);
            } elseif ($data['new_width'] < $data['new_height']) {
                $hw = round($data['new_width'] / $data['new_height'], 6);
                $data['width'] = round($hw * $data['height'], 0);
            } else {
                if ($data['width'] > $data['height']) {
                    $data['width'] = $data['height'];
                } else {
                    $data['height'] = $data['width'];
                }
            }
        }

        if ($data['error'] != 0)
            return $data;


        if (!$data['cropPosX'] OR $data['cropPosX'] <= 0)
            $data['cropPosX'] = 0;

        if (!$data['cropPosY'] OR $data['cropPosY'] <= 0)
            $data['cropPosY'] = 0;

        if (!$data['crop_width'] OR $data['crop_width'] <= 0) {
            $imageWidth = $data['width'];
        } else {
            $imageWidth = $data['crop_width']-1;
            if ($data['make_bigger_quality'] == true) {
                if ($imageWidth > $data['new_width'] && $imageWidth <= 500)
                    $data['new_width'] = $imageWidth;
                elseif ($imageWidth > $data['new_width'] && $imageWidth > 500)
                    $data['new_width'] = 500;
            }
        }

        if (!$data['crop_height'] OR $data['crop_height'] <= 0) {
            $imageHeight = $data['height'];
        } else {
            $imageHeight = $data['crop_height']-1;
            if ($data['make_bigger_quality'] == true) {
                if ($imageHeight > $data['new_height'] && $imageHeight <= 500)
                    $data['new_height'] = $imageHeight;
                elseif ($imageHeight > $data['new_height'] && $imageHeight > 500)
                    $data['new_height'] = 500;
            }
        }

        $screen = imageCreateTrueColor($data['new_width'], $data['new_height']);

        if ($data['format'] == 'png') {
            imagealphablending($screen, false); // Disable pairing colors
            imagesavealpha($screen, true); // Including the preservation of the alpha channel
        }

        imageCopyResampled($screen, $image, 0, 0, $data['cropPosX'], $data['cropPosY'], $data['new_width'], $data['new_height'], $imageWidth, $imageHeight);
        $imagePrint($screen, $data['path'].$data['new_name'].'.'.$data['new_format']);
        imageDestroy($image);

        $data['file_name'] = $data['new_name'].'.'.$data['new_format'];
        return $data;
    }

    /**
     * Function LoadImage
     * @param array $file ex. $_FILES['name']
     * @param string $path ex. 'app/public/'
     * @param null $name ex. 'name'
     * @param string $format ex. 'jpg'
     * @param array $allowedFormats ex. array('jpg' => true, 'gif' => false)
     * @param int $size - max file size
     * @param int $resize ex. 0 - no resize(сжать), 1 - обрезать не изменяя размеров, 2 - обрезать симетрически уменьшив
     * @param int $minHeight
     * @param int $minWidth
     * @param int $maxHeight
     * @param int $maxWidth
     * @return mixed
     */
    static public function LoadImage($file, $path, $name = null, $format = 'jpg', $allowedFormats = array(), $size = 0, $resize = 0, $minHeight = 0, $minWidth = 0, $maxHeight = 0, $maxWidth = 0)
    {
        $data = array('error' => 0);
        $data['format'] = mb_strtolower(mb_substr($file['name'], mb_strrpos($file['name'], '.')+1));
        $data['new_format'] = $format;
        $data['path'] = trim($path, '/').'/';
        $data['tmp_name'] = $file['tmp_name'];
        $data['size'] = $file['size'];
        $data['type'] = $file['type'];
        $data['name'] = $file['name'];

        // Recursive mkdir
        self::remkdir($path);

        if (!$name)
            $data['new_name'] = uniqid('', true);
        else
            $data['new_name'] = $name;

        if (!is_array($allowedFormats) OR empty($allowedFormats))
            $allowedFormats = self::$allowedImageFormats;

        if ($allowedFormats[$data['format']] !== true) {
            $data['error'] = 1;
            $data['error_msg'] = 'Incorrect file format';
            return $data;
        }

        if (intval($size) > 0 && $data['size'] > $size) {
            $data['error'] = 2;
            $data['error_msg'] = 'File size is too large';
            return $data;
        }

        if ($data['format'] == 'jpg')
            $imageCreateFrom = 'ImageCreateFromJpeg';
        else
            $imageCreateFrom = 'ImageCreateFrom'.$data['format'];

        if ($data['new_format'] == 'jpg')
            $imagePrint = 'imageJpeg';
        else
            $imagePrint = 'image'.$data['new_format'];

        // Create resource image
        $img = $imageCreateFrom($file['tmp_name']);

        $data['height'] = imagesy($img);
        $data['width'] = imagesx($img);

        // Min resizing
        if ($minHeight == 0 && $minWidth == 0) {
            $data['new_height'] = $data['height'];
            $data['new_width'] = $data['width'];
        } else if ($minHeight != 0 && $minWidth == 0) {
            $data['new_height'] = $minHeight;
            $hw = round($data['width'] / $data['height'], 6);
            $data['new_width'] = round($hw * $minHeight,0);
        } else if ($minHeight == 0 && $minWidth != 0) {
            $data['new_width'] = $minWidth;
            $hw = round($data['height'] / $data['width'], 6);
            $data['new_height'] = round($hw * $minWidth, 0);
        } else if ($minHeight != 0 && $minWidth != 0) {
            $data['new_height'] = $minHeight;
            $data['new_width'] = $minWidth;
        }

        // Max resizing
        if ($maxHeight != 0 && $maxWidth == 0 && $maxHeight < $data['height']) {
            $data['new_height'] = $maxHeight;
            $hw = round($data['width'] / $data['height'], 6);
            $data['new_width'] = round($hw * $maxHeight,0);
        } else if ($maxHeight == 0 && $maxWidth != 0 && $maxWidth < $data['width']) {
            $data['new_width'] = $maxWidth;
            $hw = round($data['height'] / $data['width'], 6);
            $data['new_height'] = round($hw * $maxWidth, 0);
        } else if ($maxHeight != 0 && $maxWidth != 0 && ($maxHeight < $data['height'] OR $maxWidth < $data['width'])) {
            if ($data['height'] > $data['width']) {
                $data['new_height'] = $maxHeight;
                $hw = round($data['width'] / $data['height'], 6);
                $data['new_width'] = round($hw * $maxHeight,0);
            } elseif ($data['height'] < $data['width']) {
                $data['new_width'] = $maxWidth;
                $hw = round($data['height'] / $data['width'], 6);
                $data['new_height'] = round($hw * $maxWidth, 0);
            }
        }

        if ($resize == 1) {
            $data['height'] = $data['new_height'];
            $data['width'] = $data['new_width'];
        }

        if ($resize == 2) {
            if ($data['new_width'] > $data['new_height']) {
                $hw = round($data['new_height'] / $data['new_width'], 6);
                $data['height'] = round($hw * $data['width'], 0);
            } elseif ($data['new_width'] < $data['new_height']) {
                $hw = round($data['new_width'] / $data['new_height'], 6);
                $data['width'] = round($hw * $data['height'], 0);
            } else {
                if ($data['width'] > $data['height']) {
                    $data['width'] = $data['height'];
                } else {
                    $data['height'] = $data['width'];
                }
            }
        }

        $screen = imageCreateTrueColor($data['new_width'], $data['new_height']);

        if ($data['format'] == 'png') {
            imagealphablending($screen, false); // Disable pairing colors
            imagesavealpha($screen, true); // Including the preservation of the alpha channel
        }

        imageCopyResampled($screen, $img, 0, 0, 0, 0, $data['new_width'], $data['new_height'], $data['width'], $data['height']);
        $imagePrint($screen, $data['path'].$data['new_name'].'.'.$data['new_format']);
        imageDestroy($img);

        $data['file_name'] = $data['new_name'].'.'.$data['new_format'];
        return $data;
    }


    static public function print_date($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
}