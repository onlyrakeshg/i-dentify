<!--

/*
  ____        _     _    ____ __  __ ____
 | __ )  ___ | | __| |  / ___|  \/  / ___|
 |  _ \ / _ \| |/ _` | | |   | |\/| \___ \
 | |_) | (_) | | (_| | | |___| |  | |___) |
 |____/ \___/|_|\__,_|  \____|_|  |_|____/   .v2 2018

 Copyright Bold Identities Ltd - All Rights Reserved

 Author: Bold Identities Ltd

*/

-->
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        <?php echo isset($title) && $title ? $title . ' / ' : ''; ?>
        BoldCMS2
    </title>

    <link rel="stylesheet" href="<?php echo ms_base_url('css/backend/main.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo ms_base_url('css/backend/jquery-ui.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo ms_base_url('css/backend/all.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo ms_base_url('plugins/data-tables/jquery.datatables.css'); ?>"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo ms_base_url('plugins/tooltipster-master/'); ?>dist/css/tooltipster.bundle.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo ms_base_url('plugins/tooltipster-master/'); ?>dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <link rel="shortcut icon" href="<?php echo ms_base_url('images/logo.png'); ?>" type="image/png"/>

    <script src="<?php echo ms_base_url('js/backend/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?php echo ms_base_url('js/backend/jquery-ui.min.js'); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-flash-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
    <script src="<?php echo ms_base_url('plugins/tooltipster-master/dist/js/tooltipster.bundle.min.js'); ?>"></script>

    <script type="text/javascript">
        $(function () {
            $('.success').delay(10000).fadeOut('fast');
            $('.error').delay(10000).fadeOut('fast');

            var inputs = document.querySelectorAll('.inputfile');
            Array.prototype.forEach.call(inputs, function (input) {
                var label = input.nextElementSibling, labelVal;

                if (label) labelVal = label.innerHTML;

                input.addEventListener('change', function (e) {
                    var fileName = '';
                    if (this.files && this.files.length > 1)
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                    else
                        fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        label.querySelector('span').innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });
            });

        });
    </script>
</head>

<div id="top-bar">
    <?php if (isset($notification) && $notification) { ?>
        <div class="cancel" style="padding-left: 10px;padding-right: 10px; float: left;">
            <?php echo $notification; ?>
        </div>
    <?php } ?>
    <a href="<?php echo site_url('team/logout'); ?>" class="logout">
        <i class="fas fa-sign-out"></i>
        Logout
    </a>
</div>

<div id="left-column-nav">
    <div class="logo">
        <a href="<?php echo ms_site_url('home'); ?>" target="_blank" class="bold">&nbsp;</a>
    </div>

    <div class="nav">
        <ul>
            <li class="<?php echo isset($slug) && $slug === "dashboard" ? 'active' : ''; ?>">
                <a href="<?php echo site_url("dashboard"); ?>">
                    <i class="fab fa fa-tachometer-alt"></i>
                    Dashboard
                </a>
            </li>

            <?php

            foreach ($plugin_groups as $group_title) {

                $group_plugins = array_filter($plugins, function ($plugin) use ($group_title) {
                    return $plugin['group'] === $group_title && $plugin['enabled'] === TRUE;
                });

                if (count($group_plugins) > 0) {
                    ?>
                    <li class="subheader">
                        <?php echo $group_title; ?>
                    </li>
                    <?php

                    foreach ($group_plugins as $plugin) {

                        if (!isset($plugin['hidden']) || (isset($plugin['hidden']) && $plugin['hidden'] === FALSE)) {
                            ?>
                            <li class="<?php echo isset($slug) && $slug === $plugin['slug'] ? 'active' : ''; ?>">
                                <a href="<?php echo site_url($plugin['slug']); ?>">
                                    <i class="<?php echo $plugin['icon']; ?>"></i>
                                    <?php echo $plugin['title']; ?>
                                </a>
                            </li>
                            <?php
                        }
                    }

                    ?>
                    <?php
                }
            }

            ?>
        </ul>

    </div>

</div>