<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('vacancies'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-clipboard"></i>Manage Vacancies <i class="fas fa-caret-right"></i>Edit
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('vacancies/edit/' . $vacancy->vacancy_id); ?>" method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">General</span>
            <div class="col half_column_left">
                <label for="title">
                    Job Title
                </label>
                <input type="text" name="title" id="title"
                       value="<?php echo set_value('title') ? set_value('title') : ($vacancy->title ? $vacancy->title : ""); ?>"
                       required>
            </div>
            <div class="col half_column_right">
                <label for="ref">
                    Job Ref (Letters, numbers and hyphens (-) only)
                </label>
                <input type="text" name="ref" id="ref"
                       value="<?php echo set_value('ref') ? set_value('ref') : ($vacancy->ref ? $vacancy->ref : ""); ?>"
                       required>
            </div>
            <div class="clr"></div>
            <div class="col half_column_left">
                <label>
                    Industries/Sectors
                </label>
                <div class="multiple_checkboxes_box">
                    <?php if (isset($industry_sectors) && is_array($industry_sectors) && count($industry_sectors) > 0) { ?>
                        <?php foreach ($industry_sectors as $industry_sector) { ?>
                            <label>
                                <input type="checkbox" name="sector_ids[]"
                                    <?php echo set_value('sector_ids[]') && is_array(set_value('sector_ids[]')) && in_array($industry_sector->industry_sector_id, set_value('sector_ids[]')) ? 'checked' : ($vacancy->sector_ids && is_array($vacancy->sector_ids) && in_array($industry_sector->industry_sector_id, $vacancy->sector_ids) ? 'checked' : ''); ?>
                                       value="<?php echo $industry_sector->industry_sector_id; ?>">
                                <?php echo $industry_sector->name; ?>
                            </label>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="col half_column_right">
                <label>
                    Locations
                </label>
                <div class="multiple_checkboxes_box">
                    <?php if (isset($locations) && is_array($locations) && count($locations) > 0) { ?>
                        <?php foreach ($locations as $location) { ?>
                            <label>
                                <input type="checkbox" name="location_ids[]"
                                    <?php echo set_value('location_ids[]') && is_array(set_value('location_ids[]')) && in_array($location->location_id, set_value('location_ids[]')) ? 'checked' : ($vacancy->location_ids && is_array($vacancy->location_ids) && in_array($location->location_id, $vacancy->location_ids) ? 'checked' : ''); ?>
                                       value="<?php echo $location->location_id; ?>">
                                <?php echo $location->name; ?>
                            </label>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="clr"></div>
            <div class="col half_column_left">
                <label for="contract_type">
                    Contract Type
                </label>
                <select name="contract_type" id="contract_type" required>
                    <option
                        <?php echo set_value('contract_type') && set_value('contract_type') === 'permanent' ? 'selected' : ($vacancy->contract_type && $vacancy->contract_type === 'permanent' ? 'selected' : ''); ?>
                            value="permanent">Permanent
                    </option>
                    <option
                        <?php echo set_value('contract_type') && set_value('contract_type') === 'temporary' ? 'selected' : ($vacancy->contract_type && $vacancy->contract_type === 'temporary' ? 'selected' : ''); ?>
                            value="temporary">Temporary
                    </option>
                    <option
                        <?php echo set_value('contract_type') && set_value('contract_type') === 'fixed' ? 'selected' : ($vacancy->contract_type && $vacancy->contract_type === 'fixed' ? 'selected' : ''); ?>
                            value="fixed">Fixed Term Contract
                    </option>
                </select>
            </div>
            <div class="col half_column_right" style="display: none">
                <label for="contract_length">
                    If temporary, how long for?
                </label>
                <input type="text" name="contract_length" id="contract_length"
                       value="<?php echo set_value('contract_length') ? set_value('contract_length') : ($vacancy->contract_length ? $vacancy->contract_length : ""); ?>">
            </div>
            <div class="clr"></div>
            <div class="col half_column_left">
                <label for="salary_type">
                    Pay Frequency
                </label>
                <select name="salary_type" id="salary_type" required>
                    <option
                        <?php echo set_value('salary_type') && set_value('salary_type') === 'salary' ? 'selected' : ($vacancy->salary_type && $vacancy->salary_type === 'salary' ? 'selected' : ''); ?>
                            value="salary">Salary
                    </option>
                    <option
                        <?php echo set_value('salary_type') && set_value('salary_type') === 'hourly' ? 'selected' : ($vacancy->salary_type && $vacancy->salary_type === 'hourly' ? 'selected' : ''); ?>
                            value="hourly">Hourly
                    </option>
                    <option
                        <?php echo set_value('salary_type') && set_value('salary_type') === 'daily' ? 'selected' : ($vacancy->salary_type && $vacancy->salary_type === 'daily' ? 'selected' : ''); ?>
                            value="daily">Daily
                    </option>
                </select>
            </div>
            <div class="col half_column_right">
                <label for="salary_value">
                    Rate of pay
                </label>
                <input type="text" name="salary_value" id="salary_value"
                       value="<?php echo set_value('salary_value') ? set_value('salary_value') : ($vacancy->salary_value ? $vacancy->salary_value : ""); ?>"
                       required>
            </div>

            <div class="col half_column_left">
                <label for="postcode">
                    Postcode
                </label>
                <input type="text" name="postcode" id="postcode"
                       value="<?php echo set_value('postcode') ? set_value('postcode') : ($vacancy->postcode ? $vacancy->postcode : ""); ?>">
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <span class="heading">Description</span>
            <div class="col full_column">
                <textarea name="description" id="description"
                          rows="20"><?php echo set_value('description') ? set_value('description') : ($vacancy->description ? $vacancy->description : ""); ?></textarea>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <span class="heading">Consultant</span>
            <div class="col full_column">
                <select name="consultant_id" id="consultant_id" required>
                    <?php if (isset($team) && is_array($team) && count($team) > 0) { ?>
                        <?php foreach ($team as $member) { ?>
                            <option
                                <?php echo set_value('consultant_id') && set_value('consultant_id') === $member->user_id ? 'selected' : ($vacancy->consultant_id && $vacancy->consultant_id === $member->user_id ? 'selected' : ''); ?>
                                    value="<?php echo $member->user_id; ?>"><?php echo $member->firstname . ' ' . $member->lastname; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-section">
            <span class="heading">Client</span>
            <div class="col full_column">
                <label for="client_name">
                    Name
                </label>
                <input type="text" name="client_name" id="client_name"
                       value="<?php echo set_value('client_name') ? set_value('client_name') : ($vacancy->client_name ? $vacancy->client_name : ""); ?>">
            </div>
            <div class="clr"></div>
            <div class="col half_column_left">
                <label for="client_email">
                    Email
                </label>
                <input type="email" name="client_email" id="client_email"
                       value="<?php echo set_value('client_email') ? set_value('client_email') : ($vacancy->client_email ? $vacancy->client_email : ""); ?>">
            </div>
            <div class="col half_column_right">
                <label for="client_tel">
                    Telephone Number
                </label>
                <input type="text" name="client_tel" id="client_tel"
                       value="<?php echo set_value('client_tel') ? set_value('client_tel') : ($vacancy->client_tel ? $vacancy->client_tel : ""); ?>">
            </div>
            <div class="clr"></div>
        </div>

        <?php if (isset($vacancy->client_id) && $vacancy->client_id) { ?>
            <div class="form-section">
                <span class="heading">Additional Options</span>
                <div class="col half_column_left">
                    <label for="allow_on_portal">
                        Display on Portal
                    </label>
                    <input type="checkbox" name="allow_on_portal" id="allow_on_portal"
                           value="1" <?php echo set_value('allow_on_portal') == '1' ? 'checked' : ($vacancy->allow_on_portal ? 'checked' : ''); ?>/>
                </div>
                <div class="clr"></div>
            </div>
        <?php } ?>

        <div class="form-section">
            <span class="heading">On-page SEO</span>
            <div class="col half_column_left">
                <label for="meta_title">
                    Meta Title<a href="https://moz.com/learn/seo/title-tag" target="_blank"><i
                                class="fas fa-info-circle"></i></a>
                </label>
                <input type="text" name="meta_title" id="meta_title"
                       value="<?php echo set_value('meta_title') ? set_value('meta_title') : ($vacancy->meta_title ? $vacancy->meta_title : ""); ?>">
            </div>
            <div class="col half_column_right">
                <label for="meta_keywords">
                    Meta Keywords<a href="https://moz.com/learn/seo/what-are-keywords"
                                    target="_blank"><i class="fas fa-info-circle"></i></a>
                </label>
                <input type="text" name="meta_keywords" id="meta_keywords"
                       value="<?php echo set_value('meta_keywords') ? set_value('meta_keywords') : ($vacancy->meta_keywords ? $vacancy->meta_keywords : ""); ?>">
            </div>
            <div class="col half_column_left">
                <label for="meta_desc">
                    Meta Description<a href="https://moz.com/learn/seo/meta-description"
                                       target="_blank"><i
                                class="fas fa-info-circle"></i></a>
                </label>
                <input type="text" name="meta_desc" id="meta_desc"
                       value="<?php echo set_value('meta_desc') ? set_value('meta_desc') : ($vacancy->meta_desc ? $vacancy->meta_desc : ""); ?>">
            </div>
            <div class="col half_column_right">
                <label for="canonical">
                    Canonical<a href="https://moz.com/learn/seo/canonicalization" target="_blank"><i
                                class="fas fa-info-circle"></i></a>
                </label>
                <input type="text" name="canonical" id="canonical"
                       value="<?php echo set_value('canonical') ? set_value('canonical') : ($vacancy->canonical ? $vacancy->canonical : ""); ?>">
            </div>
            <div class="col half_column_right">
                <label for="slug">
                    URL Slug<a href="https://moz.com/blog/15-seo-best-practices-for-structuring-urls"
                               target="_blank"><i class="fas fa-info-circle"></i></a>
                </label>
                <input type="text" name="slug" id="slug"
                       value="<?php echo set_value('slug') ? set_value('slug') : ($vacancy->slug ? $vacancy->slug : ""); ?>">
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('vacancies'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>

<link rel="stylesheet"
      href="<?php echo ms_base_url('plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') ?>">
<script src="<?php echo ms_base_url('plugins/ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo ms_base_url('plugins/ckeditor/samples/js/sample.js'); ?>"></script>
<script>
    var company_name = '<?php echo isset($company_name) ? $company_name : ''; ?>';
    var website_url = '<?php echo ms_site_url(); ?>';

    $(function () {
        if ($('#contract_type').val() === 'permanent')
            $('#contract_length').parent().hide();
        else
            $('#contract_length').parent().show();

        $('#contract_type').change(function () {
            if ($(this).val() === 'permanent')
                $('#contract_length').parent().hide();
            else
                $('#contract_length').parent().show();
        });

        $("#title").keyup(function () {
            if ($(this).val().length > 0) {
                $("#meta_title").val(company_name + " Blog | " + $(this).val());
                $("#slug").val($(this).val().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase());
                $("#canonical").val(website_url + "article/" + $(this).val().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase());
            } else {
                $("#meta_title").val("");
                $("#slug").val("");
                $("#canonical").val("");
            }
        });

        var editor = CKEDITOR.replace('description', {
            htmlEncodeOutput: false,
            wordcount: {
                showWordCount: true,
                showCharCount: true,
                countSpacesAsChars: true,
                countHTML: false,
            },
            removePlugins: 'zsuploader',

            filebrowserBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=images'); ?>',
            filebrowserUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=images'); ?>'
        });
    });
</script>