<div id="control-container">
    <div id="button-holder">
		<?php if(!is_numeric($show)) { ?>
			<?php if ($show === 'old') { ?>
				<a href="<?php echo site_url('vacancies'); ?>" class="btn add">
					<i class="fas fa-clipboard"></i> Active Vacancies
				</a>
			<?php } else { ?>
				<a href="<?php echo site_url('vacancies/add/' . $show); ?>" class="btn add">
					<i class="fas fa-plus-circle"></i> Add New Vacancy
				</a>	
				<a href="<?php echo site_url('vacancies/index/old'); ?>" class="btn add">
					<i class="fas fa-clipboard"></i> Archived Vacancies
				</a>
			<?php } ?>
		<?php } else { ?>
			<a href="<?php echo site_url('vacancies/add/' . $show); ?>" class="btn add">
				<i class="fas fa-plus-circle"></i> Add New Vacancy
			</a>	
        <?php } ?>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-clipboard"></i>Manage Vacancies <?php echo isset($client) && $client && isset($client->client_name) ? "for " . $client->client_name : ""; ?>
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="vacancies">
        <thead>
        <tr>
            <th align="center">Ref</th>
            <th>Job Title</th>
            <th>Sector(s)</th>
            <th>Location(s)</th>
            <?php if ($show === 'old') { ?>
                <th>Expiry Reason</th>
            <?php } ?>
            <th>Date Posted</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Ref</th>
            <th>Job Title</th>
            <th>Sector(s)</th>
            <th>Location(s)</th>
            <?php if ($show === 'old') { ?>
                <th>Expiry Reason</th>
            <?php } ?>
            <th>Date Posted</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($vacancies) && is_array($vacancies) && count($vacancies)) { ?>
            <?php foreach ($vacancies as $vacancy) { ?>
                <tr>
                    <td align="center">
                        <?php echo $vacancy->ref; ?>
                    </td>
                    <td>
                        <a href="<?php echo site_url('vacancies/view/' . $vacancy->vacancy_id); ?>">
                            <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                            <?php echo $vacancy->title; ?>
                        </a>
                    </td>
                    <td>
                        <?php echo implode(", ", array_map(function ($sector) {
                            return $sector->industry_sector_name;
                        }, $vacancy->sectors)); ?>
                    </td>
                    <td>
                        <?php echo implode(", ", array_map(function ($location) {
                            return $location->location_name;
                        }, $vacancy->locations)); ?>
                    </td>
                    <?php if ($show === 'old') { ?>
                        <td>
                            <?php echo $vacancy->expiry_reason ? $vacancy->expiry_reason : 'N/A'; ?>
                        </td>
                    <?php } ?>
                    <td>
                        <?php echo date("d/m/Y", strtotime($vacancy->date)); ?>
                    </td>
                    <td align="center">
                        <?php if ($show !== 'old') { ?>
                            <a href="<?php echo site_url('vacancies/edit/' . $vacancy->vacancy_id); ?>"
                               class="icon fa fa-fw fa-pencil-alt" title="Edit"></a>
                            <a href="<?php echo site_url('vacancies/expire/' . $vacancy->vacancy_id); ?>"
                               class="icon fa fa-fw fa-business-time" title="Expire"></a>
                            <a href="<?php echo site_url('vacancies/delete/' . $vacancy->vacancy_id); ?>"
                               onclick="return confirm('Are you sure you wish to delete this vacancy?');"
                               class="icon fa fa-fw fa-trash" title="Delete"></a>
                        <?php } else { ?>
                            N/A
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#vacancies').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#vacancies tfoot th').each(function () {
            var title = $('#vacancies tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>