<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('vacancies'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Back</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-clipboard"></i>Manage Vacancies <i class="fas fa-caret-right"></i>Applications
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="applications">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Tel</th>
            <th>Date Submitted</th>
            <th align="center">CV File</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Tel</th>
            <th>Date Submitted</th>
            <th>CV File</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (is_array($library) && count($library) > 0) { ?>
            <?php foreach ($library as $cv) { ?>
                <tr>
                    <td>
                        <?php echo $cv->name; ?>
                    </td>
                    <td>
                        <?php echo $cv->email; ?>

                    <td>
                        <?php echo $cv->tel; ?>
                    </td>
                    <td>
                        <?php echo date('d/m/Y', strtotime($cv->date)); ?>
                    </td>
                    <td>
                        <?php if ($cv->filename) { ?>
                            <a href="<?php echo ms_base_url('uploads/cvs/' . $cv->filename); ?>"
                               class="icon fa fa-fw fa-download" title="Download"></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="5">
                    No submitted applications found.
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#applications').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#applications tfoot th').each(function () {
            var title = $('#applications tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>