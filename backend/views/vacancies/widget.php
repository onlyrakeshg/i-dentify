<div class="column">

    <h1>Latest Vacancies</h1>

    <?php foreach ($vacancies as $vacancy) { ?>
        <div class="widget">

            <span class="ref">
                <strong>#<?php echo $vacancy->ref; ?></strong>
            </span>&nbsp;&nbsp;&nbsp;&nbsp;

            <span class="title">
                <?php echo $vacancy->title; ?>
            </span>&nbsp;&nbsp;&nbsp;&nbsp;

            <span class="date">
                <?php echo date("d/m/Y", strtotime($vacancy->date)); ?>
            </span>

            <div class="actions">
                <a href="<?php echo site_url('vacancies/edit/' . $vacancy->vacancy_id); ?>" target="_blank">
                    <i class="fas fa-edit" aria-hidden="true"></i>
                </a>
            </div>

        </div>

    <?php } ?>

</div>