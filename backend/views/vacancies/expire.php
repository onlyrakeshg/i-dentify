<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('vacancies'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-clipboard"></i>Manage Vacancies <i class="fas fa-caret-right"></i>Expire
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('vacancies/expire/' . $vacancy->vacancy_id); ?>" method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">Reason</span>
            <div class="col full_column">
                <input type="text" name="expiry_reason" id="expiry_reason"
                       value="<?php echo set_value('expiry_reason', $vacancy->expiry_reason); ?>"
                       required>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('vacancies'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>