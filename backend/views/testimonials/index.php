<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('testimonials/add'); ?>" class="btn add">
            <i class="fas fa-plus-circle"></i> Add New Testimonial
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-home"></i>Testimonials
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="testimonials">
        <thead>
        <tr>
            <th align="center">ID</th>
            <th>Author</th>
            <th>Position</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Author</th>
            <th>Position</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($testimonials) && is_array($testimonials) && count($testimonials)) { ?>
            <?php foreach ($testimonials as $testimonial) { ?>
                <tr>
                    <td align="center">
                        <?php echo $testimonial->testimonial_id; ?>
                    </td>
                    <td>
                        <?php echo $testimonial->author; ?>
                    </td>
                    <td>
                        <?php echo $testimonial->position; ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('testimonials/edit/' . $testimonial->testimonial_id); ?>"
                           class="icon fa fa-fw fa-pencil-alt" title="Edit"></a>
                        <a href="<?php echo site_url('testimonials/delete/' . $testimonial->testimonial_id); ?>"
                           onclick="return confirm('Are you sure you wish to delete this testimonial?');"
                           class="icon fa fa-fw fa-trash" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#testimonials').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[3, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [3]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#testimonials tfoot th').each(function () {
            var title = $('#testimonials tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>