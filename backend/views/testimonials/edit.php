<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('testimonials'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-home"></i>Testimonials <i class="fas fa-caret-right"></i>Edit
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('testimonials/edit/' . $testimonial->testimonial_id); ?>"
          method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">General</span>
            <div class="col half_column_left">
                <label for="author">
                    Author
                </label>
                <input type="text" name="author" id="author" required
                       value="<?php echo set_value('author') ? set_value('author') : $testimonial->author; ?>">
            </div>
            <div class="col half_column_right">
                <label for="position">
                    Position
                </label>
                <input type="text" name="position" id="position" required
                       value="<?php echo set_value('position') ? set_value('position') : $testimonial->position; ?>">
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label for="text">
                    Text
                </label>
                <textarea name="text" id="text" required><?php echo set_value('text') ? set_value('text', '', FALSE) : $testimonial->text; ?></textarea>
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label for="date">
                    Date
                </label>
                <input type="text" name="date" id="date" required
                       value="<?php echo set_value('date') ? set_value('date') : $testimonial->date; ?>">
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('testimonials'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>

<link rel="stylesheet"
            href="<?php echo ms_base_url('plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') ?>">
<script src="<?php echo ms_base_url('plugins/ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo ms_base_url('plugins/ckeditor/samples/js/sample.js'); ?>"></script>
<script>
    $(function () {
        var editor = CKEDITOR.replace('text', {
            htmlEncodeOutput: false,
            wordcount: {
                showWordCount: true,
                showCharCount: true,
                countSpacesAsChars: true,
                countHTML: false,
            },
            removePlugins: 'zsuploader',

            filebrowserBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=images'); ?>',
            filebrowserUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=images'); ?>'
        });
    });
</script>