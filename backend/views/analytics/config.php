<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('analytics'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Back to Charts</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fab fa-google"></i>Google Analytics <i class="fas fa-caret-right"></i>Config
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-exclamation-triangle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('analytics/config'); ?>" method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">System Configuration</span>
            <div class="col half_column_left">
                <label for="view_id">
                    View ID <a href="https://ga-dev-tools.appspot.com/account-explorer/" target="_blank">Obtain</a>
                </label>
                <input type="text" name="view_id" id="view_id" required
                       value="<?php echo set_value('view_id', $view_id); ?>">
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label for="credentials_json">
                    Credentials JSON <a
                            href="https://developers.google.com/analytics/devguides/reporting/core/v4/quickstart/service-php"
                            target="_blank">Instructions</a>
                </label>
                <textarea name="credentials_json" id="credentials_json" required style="width: 100%"
                          rows="20"><?php echo set_value('credentials_json', $credentials_json); ?></textarea>
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label for="include_code">
                    Include JS Code
                </label>
                <textarea name="include_code" id="include_code" required style="width: 100%"
                          rows="20"><?php echo set_value('include_code', $include_code); ?></textarea>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('analytics'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>