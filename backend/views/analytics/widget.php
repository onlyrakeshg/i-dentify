<style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }

    #dashboard-stats .height-auto {
        height: auto !important;
    }

    #dashboard-stats .column-3 {
        width: 32.3% !important;
    }

    #top_div_column {
        height: auto !important;
    }

    #top_div {
        overflow-y: scroll;
        max-height: 190px;
    }

    .row {
        float: left;
        width: 100%;
        height: auto;
    }
</style>

<div class="clr"></div>
<div class="row">
    <div class="full-column height-auto">

        <h1>Website Visitor Statistics</h1>

        <div id="new_users_chart" class="chart">
            <i class="fas fa-spinner fa-spin"></i>
            <canvas id="new_users_canvas" style="display: none;"></canvas>
        </div>

    </div>
	
</div>

<div class="clr"></div>

<div class="row">
    <div class="column column-3 height-auto">

        <h1>Devices</h1>

        <div id="devices_chart" class="chart">
            <i class="fas fa-spinner fa-spin"></i>
            <canvas id="devices_canvas" style="display: none;"></canvas>
        </div>

    </div>

    <div class="column column-3 height-auto">

        <h1>Countries</h1>

        <div id="country_chart" class="chart">
            <i class="fas fa-spinner fa-spin"></i>
            <canvas id="country_canvas" style="display: none;"></canvas>
        </div>

    </div>
	
	<div class="column column-3 height-auto">

        <h1>Traffic Sources</h1>

        <div id="sources_chart" class="chart">
            <i class="fas fa-spinner fa-spin"></i>
            <canvas id="sources_canvas" style="display: none;"></canvas>
        </div>

    </div>

	
    <div class="column" id="top_div_column">

        <h1>Top Visited Pages</h1>

        <div id="top_div">
            <i class="fas fa-spinner fa-spin"></i>
        </div>
    </div>
	
    
</div>
<div class="clr"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>

<script>
    var colors = ['#5899DA', '#E8743B', '#19A979', '#ED4A7B', '#945ECF', '#13A4B4', '#525DF4', '#BF399E', '#6C8893', '#EE6868', '#2F6497', '#0e8c62', '#E8743B', '#367dc4', '#3fa45b', '#de890d', '#dc0d0e'];

    function top_list() {
        $('#top_div').html('<i class="fas fa-spinner fa-spin"></i>');

        $.get('<?php echo site_url('analytics/ajax/top'); ?>', {}, function (json) {
            $('#top_div').empty();
            if (!json.error) {
                $(json).each(function (i, row) {
                    $('#top_div').append('<div class="widget">' +
                        '<span class="title">' +
                        row.path +
                        '</span>&nbsp;&nbsp;&nbsp;&nbsp;' +
                        '<span class="date">' +
                        row.pageviews +
                        '</span>' +
                        '</div>');
                });
            } else {
                $('#top_div').text(json.error);
            }
        });
    }

    function sources_chart() {
        pie_chart('sources', 'Traffic Sources');
    }

    function country_chart() {
        pie_chart('country', 'Countries');
    }

    function devices_chart() {
        pie_chart('devices', 'Devices');
    }

    function pie_chart(source, title) {
        $.get('<?php echo site_url('analytics/ajax/'); ?>' + source, {}, function (json) {
            $('#' + source + '_chart').find('i').hide();
            $('#' + source + '_chart').find('#' + source + '_canvas').show();

            if (!json.error) {
                var ctx = document.getElementById(source + '_canvas').getContext('2d');
                var data = [];
                var labels = [];
                $(json).each(function (i, row) {
                    labels.push(row['base']);
                    data.push(row['ga:visits']);
                });

                var chart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: labels,
                        datasets: [{
                            data: data,
                            label: title,
                            backgroundColor: colors
                        }]
                    },
                    options: {
                        title: {
                            display: false
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        }
                    }
                });
            } else {
                $('#chart').text(json.error);
            }
        });
    }

    function new_users_chart() {
        $.get('<?php echo site_url('analytics/ajax/'); ?>new_users', {}, function (json) {
            $('#new_users_chart').find('i').hide();
            $('#new_users_chart').find('#new_users_canvas').show();

            if (!json.error) {
                var ctx = document.getElementById('new_users_canvas').getContext('2d');
                var data_new_users = [];
                var data_returning_users = [];
                $(json).each(function (i, row) {
                    if (row['user_type'] === "New Visitor") {
                        data_new_users.push({
                            t: moment(row.date, "YYYYMMDD").toDate(),
                            y: row['new_users']
                        });
                        data_returning_users.push({
                            t: moment(row.date, "YYYYMMDD").toDate(),
                            y: 0
                        });
                    } else if (row['user_type'] === "Returning Visitor") {
                        data_returning_users.push({
                            t: moment(row.date, "YYYYMMDD").toDate(),
                            y: row['users']
                        });
                    }
                });

                var chart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        datasets: [{
                            label: 'New Users',
                            backgroundColor: colors[0],
                            borderColor: colors[0],
                            data: data_new_users,
                            fill: false,
                        }, {
                            label: 'Returning Users',
                            backgroundColor: colors[1],
                            borderColor: colors[1],
                            data: data_returning_users,
                            fill: false,
                        }]
                    },
                    options: {
                        title: {
                            display: false
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                type: 'time'
                            }],
                            yAxes: [{}]
                        }
                    }
                });
            } else {
                $('#chart').text(json.error);
            }
        });
    }

    $(function () {
        new_users_chart();
        top_list();
        devices_chart();
        country_chart();
        sources_chart();
    });
</script>
