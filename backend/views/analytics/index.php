<style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }

    .active-btn {
        background-color: #0b2e13;
    }
</style>

<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('analytics/config'); ?>" class="btn add">
            <i class="fas fa-cogs"></i> Configure
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fab fa-google"></i>Google Analytics
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>

    <div class="clr"></div>

    <div id="button-holder">
        <button class="btn add view-btn" id="sources_btn" onclick="sources_chart();">
            <i class="fas fa-chart-pie"></i> Traffic Sources
        </button>
        <button class="btn add view-btn" id="country_btn" onclick="country_chart();">
            <i class="fas fa-chart-pie"></i> Countries
        </button>
        <button class="btn add view-btn" id="devices_btn" onclick="devices_chart();">
            <i class="fas fa-chart-pie"></i> Devices
        </button>
        <button class="btn add view-btn" id="new_users_btn" onclick="new_users_chart();">
            <i class="fas fa-chart-line"></i> New Users Per Day
        </button>
        <button class="btn add view-btn" id="pageviews_btn" onclick="pageviews_chart();">
            <i class="fas fa-chart-line"></i> Page Views Per Day
        </button>
        <button class="btn add view-btn" id="users_btn" onclick="users_chart();">
            <i class="fas fa-chart-line"></i> Users Per Day
        </button>
        <button class="btn add view-btn" id="sessions_btn" onclick="sessions_chart();">
            <i class="fas fa-chart-line"></i> Sessions Per Day
        </button>
        <div class="clr"></div>
    </div>

    <h1>
        View:
    </h1>

    <div class="form-section">
        <div class="full_column">
            <div id="chart">
                <i class="fas fa-spinner fa-spin"></i>
                <canvas id="canvas" style="display: none;"></canvas>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>

<script>
    var colors = ['#5899DA', '#E8743B', '#19A979', '#ED4A7B', '#945ECF', '#13A4B4', '#525DF4', '#BF399E', '#6C8893', '#EE6868', '#2F6497', '#0e8c62', '#E8743B', '#367dc4', '#3fa45b', '#de890d', '#dc0d0e'];

    function sources_chart() {
        $('.view-btn').removeClass('active-btn');
        $('#sources_btn').addClass('active-btn');
        pie_chart('sources', 'Traffic Sources');
    }

    function country_chart() {
        $('.view-btn').removeClass('active-btn');
        $('#country_btn').addClass('active-btn');
        pie_chart('country', 'Countries');
    }

    function devices_chart() {
        $('.view-btn').removeClass('active-btn');
        $('#devices_btn').addClass('active-btn');
        pie_chart('devices', 'Devices');
    }

    function pie_chart(source, title) {
        $('#chart').html(
            '<i class="fas fa-spinner fa-spin"></i>' +
            '<canvas id="canvas" style="display: none;"></canvas>'
        );

        $.get('<?php echo site_url('analytics/ajax/'); ?>' + source, {}, function (json) {
            $('#chart').find('i').hide();
            $('#chart').find('#canvas').show();

            if (!json.error) {
                var ctx = document.getElementById('canvas').getContext('2d');
                var data = [];
                var labels = [];
                $(json).each(function (i, row) {
                    labels.push(row['base']);
                    data.push(row['ga:visits']);
                });

                var chart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: labels,
                        datasets: [{
                            data: data,
                            label: title,
                            backgroundColor: colors
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: false
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        }
                    }
                });
            } else {
                $('#chart').text(json.error);
            }
        });
    }

    function sessions_chart() {
        $('.view-btn').removeClass('active-btn');
        $('#sessions_btn').addClass('active-btn');
        line_chart('sessions', 'Sessions');
    }

    function users_chart() {
        $('.view-btn').removeClass('active-btn');
        $('#users_btn').addClass('active-btn');
        line_chart('users', 'Users');
    }

    function new_users_chart() {
        $('.view-btn').removeClass('active-btn');
        $('#new_users_btn').addClass('active-btn');

        $.get('<?php echo site_url('analytics/ajax/'); ?>new_users', {}, function (json) {
            $('#new_users_chart').find('i').hide();
            $('#new_users_chart').find('#new_users_canvas').show();

            if (!json.error) {
                var color = colors[Math.floor(Math.random() * colors.length)];
                var color2 = colors[Math.floor(Math.random() * colors.length)];
                var ctx = document.getElementById('canvas').getContext('2d');
                var data_new_users = [];
                var data_returning_users = [];
                $(json).each(function (i, row) {
                    if (row['user_type'] === "New Visitor") {
                        data_new_users.push({
                            t: moment(row.date, "YYYYMMDD").toDate(),
                            y: row['new_users']
                        });
                        data_returning_users.push({
                            t: moment(row.date, "YYYYMMDD").toDate(),
                            y: 0
                        });
                    } else if (row['user_type'] === "Returning Visitor") {
                        data_returning_users.push({
                            t: moment(row.date, "YYYYMMDD").toDate(),
                            y: row['users']
                        });
                    }
                });

                var chart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        datasets: [{
                            label: 'New Users',
                            backgroundColor: color,
                            borderColor: color,
                            data: data_new_users,
                            fill: false,
                        }, {
                            label: 'Returning Users',
                            backgroundColor: color2,
                            borderColor: color2,
                            data: data_returning_users,
                            fill: false,
                        }]
                    },
                    options: {
                        title: {
                            display: false
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                type: 'time'
                            }],
                            yAxes: [{}]
                        }
                    }
                });
            } else {
                $('#chart').text(json.error);
            }
        });
    }

    function pageviews_chart() {
        $('.view-btn').removeClass('active-btn');
        $('#pageviews_btn').addClass('active-btn');
        line_chart('pageviews', 'Page Views');
    }

    function line_chart(source, title) {
        $('#chart').html(
            '<i class="fas fa-spinner fa-spin"></i>' +
            '<canvas id="canvas" style="display: none;"></canvas>'
        );

        $.get('<?php echo site_url('analytics/ajax/'); ?>' + source, {}, function (json) {
            $('#chart').find('i').hide();
            $('#chart').find('#canvas').show();

            if (!json.error) {
                var color = colors[Math.floor(Math.random() * colors.length)];
                var ctx = document.getElementById('canvas').getContext('2d');
                var data = [];
                $(json).each(function (i, row) {
                    data.push({
                        t: moment(row.date, "YYYYMMDD").toDate(),
                        y: row[source]
                    })
                });

                var chart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        datasets: [{
                            label: title,
                            backgroundColor: color,
                            borderColor: color,
                            data: data,
                            fill: false,
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: false
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                type: 'time'
                            }],
                            yAxes: [{}]
                        }
                    }
                });
            } else {
                $('#chart').text(json.error);
            }
        });
    }

    $(function () {
        sessions_chart();
    });
</script>
