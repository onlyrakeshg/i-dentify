<div id="control-container">
    <h1>
        <i class="fas fa-book-reader"></i>Applicants
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="library">
        <thead>
        <tr>
            <th align="center">Vacancy ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Tel</th>
            <th>Date</th>
            <th>BullHorn Candidate ID</th>
            <th>BullHorn Application ID</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Vacancy ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Tel</th>
            <th>Date</th>
            <th>BullHorn Candidate ID</th>
            <th>BullHorn Application ID</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($library) && is_array($library) && count($library)) { ?>
            <?php foreach ($library as $cv) { ?>
                <tr>
                    <td>
                        <?php echo $cv->vacancy_id; ?>
                    </td>
                    <td>
                        <?php echo $cv->linkedin ? '<i class="fab fa-linkedin"></i>' : ''; ?>
                        <?php echo $cv->name; ?>
                    </td>
                    <td>
                        <?php echo $cv->email; ?>
                    </td>
                    <td>
                        <?php echo $cv->tel; ?>
                    </td>
                    <td>
                        <?php echo date('d/m/Y', strtotime($cv->date)); ?>
                    </td>
                    <td>
                        <?php echo $cv->bh_candidate_id ? $cv->bh_candidate_id : "Not Synced"; ?>
                    </td>
                    <td>
                        <?php echo $cv->application_id ? $cv->application_id : "Not Synced"; ?>
                    </td>
                    <td align="center">
                        <?php if ($cv->filename) { ?>
                            <a href="<?php echo ms_base_url('uploads/cvs/' . $cv->filename); ?>"
                               class="icon fa fa-fw fa-download" title="Download"></a>
                        <?php } ?>
                        <?php if ($cv->bh_notes) { ?>
                            <a onclick="show_notes(this);"
                               data-notes="<?php echo $cv->bh_notes ? $cv->bh_notes : "No Data"; ?>"
                               class="icon fa fa-fw fa-info-circle" title="Notes"></a>
                        <?php } ?>
                        <a href="<?php echo site_url('library/delete/' . $cv->library_id); ?>"
                           onclick="return confirm('Are you sure you wish to delete this application?');"
                           class="icon fa fa-fw fa-trash" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>

<div id="dialog" style="display: none;" title="Notes">
    <p id="dialog-text"></p>
</div>

<script>
    function show_notes(e) {
        $("#dialog-text").html($(e).data('notes'));
        $("#dialog").dialog({
            width: 500,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }

    $(function () {
        var table = $('#library').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#library tfoot th').each(function () {
            var title = $('#library tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>