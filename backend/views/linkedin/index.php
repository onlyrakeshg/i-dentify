<div id="control-container">
    <h1>
        <i class="fab fa-linkedin"></i>LinkedIn
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-exclamation-triangle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('linkedin'); ?>" method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">Configuration</span>
            <div class="col half_column_left">
                <label for="linkedin_id">
                    App Client ID <a href="https://www.linkedin.com/secure/developer?newapp=" target="_blank">Obtain</a>
                </label>
                <input type="text" name="linkedin_id" id="linkedin_id" required
                       value="<?php echo set_value('linkedin_id', $linkedin_id); ?>">
            </div>
            <div class="col half_column_right">
                <label for="linkedin_secret">
                    App Secret
                </label>
                <input type="text" name="linkedin_secret" id="linkedin_secret" required
                       value="<?php echo set_value('linkedin_secret', $linkedin_secret); ?>">
            </div>
            <div class="clr"></div>
            <div>
                <small>
                    <i>Please check r_basicprofile and r_emailaddress at LinkedIn app Default Application
                        Permissions.</i>
                </small>
            </div>
            <div class="col half_column_left">
                <label for="login_callback_url">
                    Authorized Redirect URLs: Login
                </label>
                <input type="text" readonly id="login_callback_url" required
                       value="<?php echo ms_site_url('linkedin/login'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="apply_callback_url">
                    Authorized Redirect URLs: Apply to Vacancy
                </label>
                <input type="text" readonly id="apply_callback_url" required
                       value="<?php echo ms_site_url('linkedin/apply'); ?>">
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Configuration
            </button>
            <div class="clr"></div>
        </div>
    </form>
</div>