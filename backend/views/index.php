<style>
    #dashboard-stats .column {
        width: 49%;
        margin: 0.5%;
        float: left;
        padding: 20px;
        box-sizing: border-box;
        background: #fff;
        border-radius: 3px;
        border: #ccc 1px solid;
        height: 250px;
    }

    #dashboard-stats .column .block {
        width: 33%;
        float: left;
        padding-top: 20px;
        padding-bottom: 20px;
    }

    #dashboard-stats .column .block span {
        display: block;
    }

    #dashboard-stats .column .block .number {
        font-size: 22px;
        color: #000;
    }

    #dashboard-stats .column .block .label {
        font-size: 12px;
        color: #999;
    }

    #dashboard-stats .column .block .number i {
        color: #1e4a99;
    }

    #dashboard-stats .column h1 {
        font-weight: bolder !important;
        margin-left: 0px !important;
        font-size: 20px;
    }

    #dashboard-stats .column .widget {
        font-size: 12px;
        padding: 5px 10px 5px 10px;
        background: #f9f9f9;
        border-bottom: #fff 1px solid;
        box-sizing: border-box;
        border-radius: 2px;
        transition: all ease 0.5s;
    }

    #dashboard-stats .column .widget:hover {
        background: #f1f1f1;
        border-color: #ccc;
    }

    #dashboard-stats .column .widget .actions {
        float: right;
    }

    #dashboard-stats .column .widget .date {
        color: #999;
    }

    #dashboard-stats .column .widget .date {
        color: #999;
    }

    #dashboard-stats .column .widget .ref {
        color: #1e4a99;
        width: 50px;
        display: block;
        float: left;
    }
	
	#dashboard-stats .full-column {
        width: 99%;
        margin: 0.5%;
        float: left;
        padding: 20px;
        box-sizing: border-box;
        background: #fff;
        border-radius: 3px;
        border: #ccc 1px solid;
        height: 200px;
    }
	
	#dashboard-stats .full-column h1 { font-weight: bolder !important;
    margin-left: 0px !important;
    font-size: 20px; }
	
</style>

<div id="control-container">
    <h1>
        <i class="fas fa-tachometer-alt"></i>
        Dashboard
    </h1>

    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-exclamation-triangle"></i><?php echo $error; ?>
        </div>
    <?php } ?>

    <div id="dashboard-stats">

        <div class="column">
            <h1>Stats Since Launch</h1>
            <?php foreach ($statistics as $statistic) { ?>
                <div class="block">
                    <span class="number"><i class="<?php echo $statistic['icon']; ?>" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo $statistic['count']; ?></span>
                    <span class="label"><?php echo $statistic['title']; ?></span>
                </div>
            <?php } ?>
        </div>

        <?php foreach ($widgets as $widget) { ?>
            <?php echo $widget; ?>
        <?php } ?>
        <div class="clr"></div>

    </div>
</div>