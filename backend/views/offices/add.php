<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('offices/index/' . $client_id); ?>"
           class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-map-pin"></i>Offices <i class="fas fa-caret-right"></i>New
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('offices/add/' . $client_id); ?>"
          method="post" enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">General</span>
            <div class="col full_column">
                <label for="title">
                    Title
                </label>
                <input type="text" name="title" required id="title" value="<?php echo set_value('title'); ?>">
            </div>
            <div class="clr"></div>
            <div class="col half_column_left">
                <label for="address">
                    Address
                    <small>(Start typing to find exact location on map. Drag pin to adjust)</small>
                </label>
                <input type="text" name="address" required id="address" value="<?php echo set_value('address'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="address">
                    Coordinates
                    <small>(This field will be auto populated with coordinates of pin from map)
                    </small>
                </label>
                <input type="text" name="coordinates" id="coordinates" value="<?php echo set_value('coordinates'); ?>"/>
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <div id="map" style="height: 300px; width: 100%"></div>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('offices/index/' . $client_id); ?>"
               class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=<?php echo isset($maps_api_key) && $maps_api_key ? $maps_api_key : ''; ?>&callback=init_geocoder"></script>

<script>
    var map, geocoder, marker = null;
    var default_coordinates = {lat: 51.5095146286, lng: -0.1244828354};

    function init_geocoder() {
        geocoder = new google.maps.Geocoder();

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 9,
            center: default_coordinates
        });
    }

    function edit_marker(lat, lng, address) {
        if (marker !== null) {
            marker.setMap(null);
            marker = null;
        }

        marker = new google.maps.Marker({
            position: {
                lat: lat,
                lng: lng
            },
            map: map,
            title: address,
            draggable: true
        });

        $('#coordinates').val(marker.getPosition().lat() + "," + marker.getPosition().lng());

        marker.addListener('mouseup', function () {
            $('#coordinates').val(marker.getPosition().lat() + "," + marker.getPosition().lng());
        });


        map.panTo({
            lat: lat,
            lng: lng
        });
    }

    $(function () {
        $('#address').keyup(function () {
            var address = $(this).val().trim();
            if (address.length <= 2) return;

            geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                    edit_marker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), address);
                } else {
                    var title = '';
                    if (status === 'ZERO_RESULTS') {
                        title = 'Address not found on map';
                    }
                    if (status === 'OVER_QUERY_LIMIT') {
                        title = 'Geocoder Limit exceed, wait few seconds and try again';
                    }
                }
            });
        });
    })
</script>