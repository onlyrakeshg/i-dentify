<!--

/*
  ____        _     _    ____ __  __ ____
 | __ )  ___ | | __| |  / ___|  \/  / ___|
 |  _ \ / _ \| |/ _` | | |   | |\/| \___ \
 | |_) | (_) | | (_| | | |___| |  | |___) |
 |____/ \___/|_|\__,_|  \____|_|  |_|____/   .v2 2018

 Copyright Bold Identities Ltd - All Rights Reserved

 Author: Bold Identities Ltd

*/

-->
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        <?php echo isset($title) && $title ? $title . ' / ' : ''; ?>
        BoldCMS2
    </title>

    <link rel="stylesheet" href="<?php echo ms_base_url('css/backend/login.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo ms_base_url('css/backend/all.css'); ?>"/>

    <script src="<?php echo ms_base_url('js/backend/jquery-3.3.1.min.js'); ?>"></script>
</head>
<body>
<div class="login-holder">
    <div class="login" id="login">
        <i class="fal fa-lock-alt"></i>
        
        <img src="<?php echo ms_base_url('images/backend/logo.png'); ?>" width="150">

        <h1>Log In</h1>

        <?php echo validation_errors('<div class="login-error">', "</div>"); ?>

        <?php if (isset($errors) && $errors) { ?>
            <div class="login-error">
                <?php echo is_array($errors) ? implode("<br/>", $errors) : $errors; ?>
            </div>
        <?php } ?>

        <form method="post" action="">
            <input type="email" name="email" placeholder="Email Address" required autofocus
                   value="<?php echo set_value('email'); ?>"/>
            <input type="password" name="password" placeholder="Password" required
                   value=""/>
            <input type="submit" name="login" value="Log In">
        </form>

    </div>
</div>


<script>
    function shakeForm() {
        var l = 20;

        for (var i = 0; i <= 10; i++) {
            $('#login').animate({
                'margin-left': '+=' + (l = -l) + 'px',
                'margin-right': '-=' + l + 'px'
            }, 50);
        }
    }

    <?php if (validation_errors()) { ?>
    $(function () {
        shakeForm();
    });
    <?php } ?>
</script>
</body>
</html>