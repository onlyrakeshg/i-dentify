<div class="column">

    <h1>Latest Blog Posts</h1>

    <?php foreach ($blogs as $blog) { ?>
        <div class="widget">

            <span class="ref">
                <strong>#<?php echo $blog->blog_id; ?></strong>
            </span>&nbsp;&nbsp;&nbsp;&nbsp;

            <span class="title">
                <?php echo $blog->title; ?>
            </span>&nbsp;&nbsp;&nbsp;&nbsp;

            <span class="date">
                <?php echo date("d/m/Y", strtotime($blog->date)); ?>
            </span>

            <div class="actions">
                <a href="<?php echo site_url('blog/edit/' . $blog->blog_id); ?>" target="_blank">
                    <i class="fas fa-edit" aria-hidden="true"></i>
                </a>
            </div>

        </div>

    <?php } ?>

</div>