<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('locations/add'); ?>" class="btn add">
            <i class="fas fa-plus-circle"></i> Add New Location
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-map-marker"></i>Locations
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="locations">
        <thead>
        <tr>
            <th align="center">ID</th>
            <th>Name</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($locations) && is_array($locations) && count($locations)) { ?>
            <?php foreach ($locations as $location) { ?>
                <tr>
                    <td align="center">
                        <?php echo $location->location_id; ?>
                    </td>
                    <td>
                        <?php echo $location->name; ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('locations/edit/' . $location->location_id); ?>"
                           class="icon fa fa-fw fa-pencil-alt" title="Edit"></a>
                        <a href="<?php echo site_url('locations/delete/' . $location->location_id); ?>"
                           onclick="return confirm('Are you sure you wish to delete this location?');"
                           class="icon fa fa-fw fa-trash" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#locations').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#locations tfoot th').each(function () {
            var title = $('#locations tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>