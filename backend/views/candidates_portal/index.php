<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('candidates_portal/add'); ?>" class="btn add">
            <i class="fas fa-plus-circle"></i> Add New Candidate
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-user-tag"></i>Candidate Registrations
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="candidates_portal">
        <thead>
        <tr>
            <th>Full Name</th>
            <th>Email</th>
            <th>Profile Title</th>
            <th>BullHorn ID</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Full Name</th>
            <th>Email</th>
            <th>Profile Title</th>
            <th>BullHorn ID</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($candidates) && is_array($candidates) && count($candidates)) { ?>
            <?php foreach ($candidates as $candidate) { ?>
                <tr>
                    <td>
                        <?php echo $candidate->firstname . ' ' . $candidate->lastname; ?>
                    </td>
                    <td>
                        <?php echo $candidate->email; ?>
                    </td>
                    <td>
                        <?php echo $candidate->title ? $candidate->title : "-"; ?>
                    </td>
                    <td>
                        <?php echo $candidate->bh_candidate_id ? $candidate->bh_candidate_id : "<i>Not Synced</i>"; ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('candidates_portal/edit/' . $candidate->candidate_id); ?>"
                           class="icon fa fa-fw fa-pencil-alt" title="Edit"></a>
                        <?php if ($candidate->bh_notes) { ?>
                            <a href="#" onclick="show_notes(this);"
                               data-notes="<?php echo $candidate->bh_notes ? $candidate->bh_notes : "No Data"; ?>"
                               class="icon fa fa-fw fa-info-circle" title="Notes"></a>
                        <?php } ?>
                        <a href="<?php echo site_url('candidates_portal/delete/' . $candidate->candidate_id); ?>"
                           onclick="return confirm('Are you sure you wish to delete this candidate?');"
                           class="icon fa fa-fw fa-trash" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>

<div id="dialog" style="display: none;" title="Notes">
    <p id="dialog-text"></p>
</div>

<script>
    function show_notes(e) {
        $("#dialog-text").html($(e).data('notes'));
        $("#dialog").dialog({
            width: 500,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }

    $(function () {
        var table = $('#candidates_portal').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#candidates_portal tfoot th').each(function () {
            var title = $('#candidates_portal tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>