<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('candidates_portal'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-user-tag"></i>Candidate Registrations <i class="fas fa-caret-right"></i>New
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success mt-2">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-exclamation-triangle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('candidates_portal/add'); ?>" method="post" enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">Personal Info</span>
            <div class="col half_column_left">
                <label for="firstname">
                    Firstname
                </label>
                <input type="text" name="firstname" id="firstname" required
                       value="<?php echo set_value('firstname'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="lastname">
                    Lastname
                </label>
                <input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname'); ?>">
            </div>
            <div class="col half_column_left">
                <label for="photo">
                    Photo
                </label>
                <input type="file" name="photo" id="image" class="inputfile"/>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <span class="heading">Account Info</span>
            <div class="col half_column_left">
                <label for="tel">
                    Telephone Number
                </label>
                <input type="text" name="tel" id="tel" value="<?php echo set_value('tel'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="email">
                    Email
                </label>
                <input type="email" name="email" id="email" required
                       value="<?php echo set_value('email'); ?>">
            </div>
            <div class="col half_column_left">
                <label for="password">
                    Password
                </label>
                <input type="password" name="password" id="password" required
                       value="">
            </div>
            <div class="col half_column_right">
                <label for="cv">
                    CV file
                </label>
                <input type="file" name="cv" id="cv" class="inputfile"/>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <span class="heading">Candidate Profile</span>
            <div class="col half_column_left">
                <label for="title">
                    Opening Line
                </label>
                <input type="text" name="title" id="title"
                       value="<?php echo set_value('title'); ?>">
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label for="content">
                    Candidate Summary
                </label>
                <textarea name="content" id="content" rows="20"><?php echo set_value('content'); ?></textarea>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('candidates_portal'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>

<link rel="stylesheet"
      href="<?php echo ms_base_url('plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') ?>">
<script src="<?php echo ms_base_url('plugins/ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo ms_base_url('plugins/ckeditor/samples/js/sample.js'); ?>"></script>
<script>
    $(function () {
        CKEDITOR.replace('content', {

            htmlEncodeOutput: false,
            wordcount: {
                showWordCount: true,
                showCharCount: true,
                countSpacesAsChars: true,
                countHTML: false,
            },
            removePlugins: 'zsuploader',

            filebrowserBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=images'); ?>',
            filebrowserUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=images'); ?>'
        });
    });
</script>