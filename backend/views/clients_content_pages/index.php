<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('clients_content_pages/add/' . (isset($client_id) && $client_id ? $client_id : "")); ?>"
           class="btn add">
            <i class="fas fa-plus-circle"></i> Add New Content Page
        </a>
        <a href="<?php echo site_url('clients'); ?>" class="btn cancel">
            <i class="fas fa-arrow-left"></i> Back
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-align-left"></i>Content Pages
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="clients_content_pages">
        <thead>
        <tr>
            <th align="center">ID</th>
            <th>Title</th>
            <th>Sort</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Sort</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($clients_content_pages) && is_array($clients_content_pages) && count($clients_content_pages)) { ?>
            <?php foreach ($clients_content_pages as $clients_content_page) { ?>
                <tr>
                    <td align="center">
                        <?php echo $clients_content_page->clients_content_page_id; ?>
                    </td>
                    <td>
                        <?php echo $clients_content_page->title; ?>
                    </td>
                    <td>
                        <?php echo $clients_content_page->sort; ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('clients_content_pages/edit/' . $clients_content_page->clients_content_page_id); ?>"
                           class="icon fa fa-fw fa-pencil-alt tooltip" title="Edit"></a>
                        <a href="<?php echo site_url('clients_content_pages/duplicate/' . $clients_content_page->clients_content_page_id); ?>"
                           class="icon fa fa-fw fa-copy tooltip" title="Duplicate"></a>
                        <a href="<?php echo site_url('clients_content_pages/sort/up/' . $clients_content_page->clients_content_page_id); ?>"
                           class="icon fa fa-fw fa-arrow-up tooltip" title="Move Up"></a>
                        <a href="<?php echo site_url('clients_content_pages/sort/down/' . $clients_content_page->clients_content_page_id); ?>"
                           class="icon fa fa-fw fa-arrow-down tooltip" title="Move Down"></a>
                        <a href="<?php echo site_url('clients_content_pages/delete/' . $clients_content_page->clients_content_page_id); ?>"
                           class="icon fa fa-fw fa-trash tooltip" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#clients_content_pages').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'desc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [3]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#clients_content_pages tfoot th').each(function () {
            var title = $('#clients_content_pages tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>