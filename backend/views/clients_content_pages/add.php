<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('clients_content_pages' . (isset($client_id) && $client_id ? '/index/' . $client_id : "")); ?>"
           class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-align-left"></i>Content Pages <i class="fas fa-caret-right"></i>New
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('clients_content_pages/add/' . (isset($client_id) && $client_id ? $client_id : "")); ?>"
          method="post" enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">General</span>
            <div class="col half_column_left">
                <label for="title">
                    Content Page Title
                </label>
                <input type="text" name="title" id="title" value="<?php echo set_value('title'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="image">
                    Main Image <small><i>(Image files must be under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or PNG format)</i></small>
                </label>
                <input type="file" name="image" id="image" class="inputfile"/>
            </div>
            <div class="col half_column_left">
                <label for="content_image">
                    Content Image <small><i>(Image files must be under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or PNG format)</i></small>
                </label>
                <input type="file" name="content_image" id="content_image" class="inputfile"/>
            </div>
            <div class="col half_column_right">
                <label for="thumb">
                    Thumbnail <small><i>(Image files must be under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or PNG format)</i></small>
                </label>
                <input type="file" name="thumb" id="thumb" class="inputfile"/>
            </div>
            <div class="clr"></div>
        </div>
        
        <div class="form-section">
            <span class="heading">Page Content (SHIFT + CTRL + V to paste from Word)</span>
            <div class="col full_column">
                <textarea name="content" id="content" rows="20"><?php echo set_value('content'); ?></textarea>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('clients_content_pages' . (isset($client_id) && $client_id ? '/index/' . $client_id : "")); ?>"
               class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>

<link rel="stylesheet"
      href="<?php echo ms_base_url('plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') ?>">
<script src="<?php echo ms_base_url('plugins/ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo ms_base_url('plugins/ckeditor/samples/js/sample.js'); ?>"></script>
<script>
    var company_name = '<?php echo isset($company_name) ? $company_name : ''; ?>';
    var website_url = '<?php echo ms_site_url(); ?>';

    $(function () {
        $("#title").keyup(function () {
            if ($(this).val().length > 0) {
                $("#meta_title").val(company_name + " Page | " + $(this).val());
                $("#slug").val("/" + $(this).val().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase());
                $("#canonical").val(website_url + "article/" + $(this).val().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase());
            } else {
                $("#meta_title").val("");
                $("#slug").val("");
                $("#canonical").val("");
            }
        });

        var editor = CKEDITOR.replace('content', {
            htmlEncodeOutput: false,
            wordcount: {
                showWordCount: true,
                showCharCount: true,
                countSpacesAsChars: true,
                countHTML: false,
            },
            removePlugins: 'zsuploader',

            filebrowserBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=images'); ?>',
            filebrowserUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=images'); ?>'
        });
    });
</script>