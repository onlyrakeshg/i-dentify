<div id="control-container">
    <h1>
        <i class="fab fa-google"></i>Maps
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-exclamation-triangle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('maps'); ?>" method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">Configuration</span>
            <div class="col half_column_left">
                <label for="maps_api_key">
                    API Key <a href="https://developers.google.com/maps/documentation/javascript/get-api-key"
                               target="_blank">Obtain</a>
                </label>
                <input type="text" name="maps_api_key" id="maps_api_key" required
                       value="<?php echo set_value('maps_api_key', $maps_api_key); ?>">
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Configuration
            </button>
            <div class="clr"></div>
        </div>
    </form>
</div>