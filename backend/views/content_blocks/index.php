<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('content_blocks/add'); ?>" class="btn add">
            <i class="fas fa-plus-circle"></i> Add New Content Block
        </a>
        <?php if ($show === 'old') { ?>
            <a href="<?php echo site_url('content_blocks'); ?>" class="btn add">
                <i class="fas fa-text-width"></i> Active Blocks
            </a>
        <?php } else { ?>
            <a href="<?php echo site_url('content_blocks/index/old'); ?>" class="btn add">
                <i class="fas fa-text-width"></i> Archived Blocks
            </a>
        <?php } ?>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-text-width"></i>Content Blocks
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="content_blocks">
        <thead>
        <tr>
            <th align="center">ID</th>
            <th>Identifier</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Identifier</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($content_blocks) && is_array($content_blocks) && count($content_blocks)) { ?>
            <?php foreach ($content_blocks as $content_block) { ?>
                <tr>
                    <td align="center">
                        <?php echo $content_block->block_id; ?>
                    </td>
                    <td>
                        <?php echo $content_block->identifier; ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('content_blocks/edit/' . $content_block->block_id); ?>"
                           class="icon fa fa-fw fa-pencil-alt" title="Edit"></a>
                        <?php if ($show !== 'old') { ?>
                            <a href="<?php echo site_url('content_blocks/delete/' . $content_block->block_id); ?>"
                               onclick="return confirm('Are you sure you wish to delete this content block?');"
                               class="icon fa fa-fw fa-trash" title="Delete"></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#content_blocks').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#content_blocks tfoot th').each(function () {
            var title = $('#content_blocks tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>