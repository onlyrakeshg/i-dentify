<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('videos/add/' . $client_id); ?>" class="btn add">
            <i class="fas fa-plus-circle"></i> Add New Videos
        </a>
        <a href="<?php echo site_url('clients'); ?>" class="btn cancel">
            <i class="fas fa-arrow-left"></i> Back
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-video"></i>Videos
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="videos">
        <thead>
        <tr>
            <th align="center">ID</th>
            <th>Title</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($videos) && is_array($videos) && count($videos)) { ?>
            <?php foreach ($videos as $video) { ?>
                <tr>
                    <td align="center">
                        <?php echo $video->video_id; ?>
                    </td>
                    <td>
                        <?php echo $video->title; ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('videos/edit/' . $video->video_id); ?>"
                           class="icon fa fa-fw fa-pencil-alt tooltip" title="Edit"></a>
                        <a href="<?php echo site_url('videos/delete/' . $video->video_id); ?>"
                           class="icon fa fa-fw fa-trash tooltip" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#videos').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#videos tfoot th').each(function () {
            var title = $('#videos tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>