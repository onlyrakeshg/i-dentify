<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('settings'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-cogs"></i>Settings <i class="fas fa-caret-right"></i>Edit
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('settings/edit/' . $setting->name); ?>" method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <div class="col full_column">
                <label for="value">
                    Value
                </label>
                <textarea name="value" id="value" required style="width: 100%"
                          rows="20"><?php echo set_value('value', $setting->value); ?></textarea>
            </div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('settings'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>