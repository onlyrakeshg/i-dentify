<div id="control-container">
    <h1>
        <i class="fas fa-cogs"></i>Settings
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="settings">
        <thead>
        <tr>
            <th align="center">Title</th>
            <th>Value</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Title</th>
            <th>Value</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($settings) && is_array($settings) && count($settings)) { ?>
            <?php foreach ($settings as $setting) { ?>
                <tr>
                    <td>
                        <?php echo $setting->title; ?>
                    </td>
                    <td>
                        <?php echo htmlspecialchars($setting->value); ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('settings/edit/' . $setting->name); ?>"
                           class="icon fa fa-fw fa-pencil-alt" title="Edit"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#settings').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#settings tfoot th').each(function () {
            var title = $('#settings tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>