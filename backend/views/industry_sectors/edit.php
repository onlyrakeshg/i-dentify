<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('industry_sectors'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fab fa-buromobelexperte"></i>Industry Sectors <i class="fas fa-caret-right"></i>Edit
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('industry_sectors/edit/' . $industry_sector->industry_sector_id); ?>"
          method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">General</span>
            <div class="col full_column">
                <label for="name">
                    Name
                </label>
                <input type="text" name="name" id="name"
                       value="<?php echo set_value('name', $industry_sector->name); ?>">
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('industry_sectors'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>