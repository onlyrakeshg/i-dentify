<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('industry_sectors/add'); ?>" class="btn add">
            <i class="fas fa-plus-circle"></i> Add New Industry Sector
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fab fa-buromobelexperte"></i>Industry Sectors
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="industry_sectors">
        <thead>
        <tr>
            <th align="center">ID</th>
            <th>Name</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($industry_sectors) && is_array($industry_sectors) && count($industry_sectors)) { ?>
            <?php foreach ($industry_sectors as $industry_sector) { ?>
                <tr>
                    <td align="center">
                        <?php echo $industry_sector->industry_sector_id; ?>
                    </td>
                    <td>
                        <?php echo $industry_sector->name; ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('industry_sectors/edit/' . $industry_sector->industry_sector_id); ?>"
                           class="icon fa fa-fw fa-pencil-alt" title="Edit"></a>
                        <a href="<?php echo site_url('industry_sectors/delete/' . $industry_sector->industry_sector_id); ?>"
                           onclick="return confirm('Are you sure you wish to delete this industry sector?');"
                           class="icon fa fa-fw fa-trash" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#industry_sectors').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#industry_sectors tfoot th').each(function () {
            var title = $('#industry_sectors tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>