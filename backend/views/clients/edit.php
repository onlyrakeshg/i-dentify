<link rel="stylesheet" href="<?php echo ms_base_url('css/backend/jquery.tagit.css'); ?>"/>
<style>
    .no-bg {
        background: none !important;
        padding: 0px !important;
    }

    .hidden {
        display: none !important;
    }
</style>

<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('clients'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-users"></i>Manage Microsites <i class="fas fa-caret-right"></i>Edit
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success mt-2">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-exclamation-triangle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('clients/edit/' . $client->client_id); ?>" method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">Account Info</span>
            <div class="col full_column">
                <label for="client_name">
                    Client Name <span class="text-danger">*</span>
                </label>
                <input type="text" name="client_name" id="client_name" required
                       value="<?php echo set_value('client_name') ? set_value('client_name') : ($client->client_name ? $client->client_name : "") ?>">
            </div>
            <div class="clr"></div>
            <div class="col half_column_left">
                <label for="ref">
                    Ref (Only numbers, letters and dash accepted)
                </label>
                <input type="text" name="ref" id="ref" pattern="[a-zA-Z0-9-]+"
                       value="<?php echo set_value('ref') ? set_value('ref') : ($client->ref ? $client->ref : "") ?>">
            </div>
            <div class="col half_column_right">
                <label for="logo">
                    Logo <small><i>(Image files must be under <?php echo file_upload_max_size_format() ?>, and JPG, GIF
                            or PNG format)</i></small>
                </label>
                <input type="file" name="logo" id="logo" class="inputfile"/>
                <?php if ($client->logo) { ?>
                    <div id="edit_bg" class="preview-img">
                        <a href="<?php echo ms_base_url('uploads/images/' . $client->logo); ?>" target="_blank"
                           title="Click to Open in new tab" class="title">
                            <img src="<?php echo ms_base_url('uploads/images/' . $client->logo); ?>"
                                 alt="Image" height="100">
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <span class="heading">Key Information</span>
            <div class="col half_column_left">
                <label for="website">
                    Website URL
                </label>
                <input type="text" name="website" id="website"
                       value="<?php echo set_value('website') ? set_value('website') : ($client->website ? $client->website : "") ?>">
            </div>
            <div class="col half_column_right">
                <label for="tags">
                    Top 5 Sectors
                </label>
                <input type="text" name="tags" id="tags"
                       value="<?php echo set_value('tags') ? set_value('tags') : ($client->tags ? $client->tags : "") ?>">
            </div>
            <div class="col half_column_left">
                <label for="company_size">
                    Company Size <span class="text-danger">*</span>
                </label>
                <input type="number" name="company_size" id="company_size" required
                       value="<?php echo set_value('company_size') ? set_value('company_size') : ($client->company_size ? $client->company_size : "") ?>">
            </div>
            <div class="col half_column_right">
                <label for="image">
                    Landing Image <small><i>(Image files must be under <?php echo file_upload_max_size_format() ?>, and
                            JPG, GIF or PNG format)</i></small> / Video (Videos must be MP4 format)
                </label>
                <input type="file" name="image" id="image" class="inputfile"/>
                <?php if ($client->image) { ?>
                    <div id="edit_bg" class="preview-img">
                        <a href="<?php echo ms_base_url('uploads/images/' . $client->image); ?>" target="_blank"
                           title="Click to Open in new tab" class="title">
                            <img src="<?php echo ms_base_url('uploads/images/' . $client->image); ?>"
                                 alt="Image" height="100">
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="col half_column_left">
                <label for="key_image">
                    Key Information Section Image <small><i>(Image files must be
                            under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or PNG format)</i></small>
                </label>
                <input type="file" name="key_image" id="key_image" class="inputfile"/>
                <?php if ($client->key_image) { ?>
                    <div id="edit_bg" class="preview-img">
                        <a href="<?php echo ms_base_url('uploads/images/' . $client->key_image); ?>" target="_blank"
                           title="Click to Open in new tab" class="title">
                            <img src="<?php echo ms_base_url('uploads/images/' . $client->key_image); ?>"
                                 alt="Image" height="100">
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="col half_column_right">
                <label for="overview_image">
                    Overview Section Image <small><i>(Image files must be
                            under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or PNG format)</i></small>
                </label>
                <input type="file" name="overview_image" id="overview_image" class="inputfile"/>
                <?php if ($client->overview_image) { ?>
                    <div id="edit_bg" class="preview-img">
                        <a href="<?php echo ms_base_url('uploads/images/' . $client->overview_image); ?>" target="_blank"
                           title="Click to Open in new tab" class="title">
                            <img src="<?php echo ms_base_url('uploads/images/' . $client->overview_image); ?>"
                                 alt="Image" height="100">
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="col half_column_left">
                <label for="opportunities_image">
                    Job Opportunities Image <small><i>(Image files must be
                            under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or PNG format)</i></small>
                </label>
                <input type="file" name="opportunities_image" id="opportunities_image" class="inputfile"/>
                <?php if ($client->opportunities_image) { ?>
                    <div id="edit_bg" class="preview-img">
                        <a href="<?php echo ms_base_url('uploads/images/' . $client->opportunities_image); ?>" target="_blank"
                           title="Click to Open in new tab" class="title">
                            <img src="<?php echo ms_base_url('uploads/images/' . $client->opportunities_image); ?>"
                                 alt="Image" height="100">
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label>
                    Industry
                </label>
                <input type="text" id="sector" value="" autocomplete="off"
                       placeholder="Start typing to filter industries below">
                <div class="multiple_checkboxes_box">
                    <?php if (isset($industry_sectors) && is_array($industry_sectors) && count($industry_sectors) > 0) { ?>
                        <?php foreach ($industry_sectors as $industry_sector) { ?>
                            <label>
                                <input type="checkbox" name="sector_ids[]" class="sectors"
                                    <?php echo set_value('sector_ids[]', $client->sector_ids) && is_array(set_value('sector_ids[]', $client->sector_ids)) && in_array($industry_sector->industry_sector_id, set_value('sector_ids[]', $client->sector_ids)) ? 'checked' : ''; ?>
                                       value="<?php echo $industry_sector->industry_sector_id; ?>">
                                <?php echo $industry_sector->name; ?>
                            </label>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label>
                    Headquarters
                </label>
                <input type="text" id="location" value="" autocomplete="off"
                       placeholder="Start typing to filter headquarters below">
                <div class="multiple_checkboxes_box">
                    <?php if (isset($locations) && is_array($locations) && count($locations) > 0) { ?>
                        <?php foreach ($locations as $location) { ?>
                            <label>
                                <input type="checkbox" name="location_ids[]" class="locations"
                                    <?php echo set_value('location_ids[]', $client->location_ids) && is_array(set_value('location_ids[]', $client->location_ids)) && in_array($location->location_id, set_value('location_ids[]', $client->location_ids)) ? 'checked' : ''; ?>
                                       value="<?php echo $location->location_id; ?>">
                                <?php echo $location->name; ?>
                            </label>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <span class="heading">Overview</span>
            <div class="col full_column">
                <textarea name="content" id="content"
                          rows="20"><?php echo set_value('content') ? set_value('content') : ($client->content ? $client->content : "") ?></textarea>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <span class="heading">Vacancies</span>
            <div class="col full_column">
                <input type="text" id="vacancy" value="" autocomplete="off"
                       placeholder="Start typing to filter vacancies below">
                <div class="multiple_checkboxes_box">
                    <?php if (isset($vacancies) && is_array($vacancies) && count($vacancies) > 0) { ?>
                        <?php foreach ($vacancies as $vacancy) { ?>
                            <label>
                                <input type="checkbox" name="vacancies_ids[]" class="vacancies"
                                    <?php echo set_value('vacancies_ids[]') && is_array(set_value('vacancies_ids[]')) && in_array($vacancy->vacancy_id, set_value('vacancies_ids[]')) ? 'checked' : ($vacancy->client_id == $client->client_id ? 'checked' : ''); ?>
                                       value="<?php echo $vacancy->vacancy_id; ?>">
                                <?php echo $vacancy->title; ?> (<?php echo $vacancy->ref; ?>)
                            </label>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <span class="heading">On-page SEO</span>
            <div class="col half_column_left">
                <label for="meta_title">Meta Title<a href="https://moz.com/learn/seo/title-tag" target="_blank"><i
                                class="fas fa-info-circle"></i></a></label>
                <input type="text" name="meta_title" id="meta_title"
                       value="<?php echo set_value('meta_title') ? set_value('meta_title') : ($client->meta_title ? $client->meta_title : ""); ?>">
            </div>
            <div class="col half_column_right">
                <label for="meta_keywords">
                    Meta Keywords<a href="https://moz.com/learn/seo/what-are-keywords"
                                    target="_blank"><i class="fas fa-info-circle"></i></a>
                </label>
                <input type="text" name="meta_keywords" id="meta_keywords"
                       value="<?php echo set_value('meta_keywords') ? set_value('meta_keywords') : ($client->meta_keywords ? $client->meta_keywords : ""); ?>">
            </div>
            <div class="col half_column_left">
                <label for="meta_desc">
                    Meta Description<a href="https://moz.com/learn/seo/meta-description"
                                       target="_blank"><i
                                class="fas fa-info-circle"></i></a>
                </label>
                <input type="text" name="meta_desc" id="meta_desc"
                       value="<?php echo set_value('meta_desc') ? set_value('meta_desc') : ($client->meta_desc ? $client->meta_desc : ""); ?>">
            </div>
            <div class="col half_column_right">
                <label for="open_graph_image">
                    Open Graph Image
                    <small><i>(Image files must be under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or
                            PNG format)</i></small>
                </label>
                <input type="file" name="open_graph_image" id="open_graph_image" class="inputfile"/>
                <?php if ($client->open_graph_image) { ?>
                    <div id="edit_bg" class="preview-img">
                        <a href="<?php echo ms_base_url('uploads/images/' . $client->open_graph_image); ?>" target="_blank"
                           title="Click to Open in new tab" class="title">
                            <img src="<?php echo ms_base_url('uploads/images/' . $client->open_graph_image); ?>"
                                 alt="Image" height="100">
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="clr"></div>
        </div>

        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('clients'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>

<link rel="stylesheet"
      href="<?php echo ms_base_url('plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') ?>">
<script src="<?php echo ms_base_url('plugins/ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo ms_base_url('plugins/ckeditor/samples/js/sample.js'); ?>"></script>
<script src="<?php echo ms_base_url('js/backend/tag-it.min.js'); ?>"></script>
<script>
    var available_tags = [];

    $(function () {
        $("#client_name").keyup(function () {
            if ($(this).val().length > 0) {
                $("#ref").val($(this).val().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase());
            } else {
                $("#ref").val("");
            }
        });

        $('#location').keyup(function () {
            var q = $(this).val().toLowerCase().trim();

            if (q.length > 0) {
                $('.locations').each(function (i, location) {
                    if ($(location).parent().text().trim().toLowerCase().indexOf(q) === -1) {
                        $(location).parent().addClass('hidden')
                    } else {
                        $(location).parent().removeClass('hidden')
                    }
                });
            } else {
                $('.locations').each(function (i, location) {
                    $(location).parent().removeClass('hidden')
                });
            }
        });

        $('#sector').keyup(function () {
            var q = $(this).val().toLowerCase().trim();

            if (q.length > 0) {
                $('.sectors').each(function (i, sector) {
                    if ($(sector).parent().text().trim().toLowerCase().indexOf(q) === -1) {
                        $(sector).parent().addClass('hidden')
                    } else {
                        $(sector).parent().removeClass('hidden')
                    }
                });
            } else {
                $('.sectors').each(function (i, sector) {
                    $(sector).parent().removeClass('hidden')
                });
            }
        });
        $('#vacancy').keyup(function () {
            var q = $(this).val().toLowerCase().trim();

            if (q.length > 0) {
                $('.vacancies').each(function (i, vacancy) {
                    if ($(vacancy).parent().text().trim().toLowerCase().indexOf(q) === -1) {
                        $(vacancy).parent().addClass('hidden')
                    } else {
                        $(vacancy).parent().removeClass('hidden')
                    }
                });
            } else {
                $('.vacancies').each(function (i, vacancy) {
                    $(vacancy).parent().removeClass('hidden')
                });
            }
        });

        $("#tags").tagit({
            availableTags: available_tags,
            autocomplete: {delay: 0, minLength: 1}
        });
        $("#tags").data("ui-tagit").tagInput.addClass("no-bg");

        CKEDITOR.replace('content', {

            htmlEncodeOutput: false,
            wordcount: {
                showWordCount: true,
                showCharCount: true,
                countSpacesAsChars: true,
                countHTML: false,
            },
            removePlugins: 'zsuploader',

            filebrowserBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=images'); ?>',
            filebrowserUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=images'); ?>'
        });
    });
</script>
