<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('clients/add'); ?>" class="btn add">
            <i class="fas fa-plus-circle"></i> Create New Microsite
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-users"></i>Manage Microsites
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="clients">
        <thead>
        <tr>
            <th>Ref</th>
            <th>Client Name</th>
            <th>Date Created</th>
            <th align="center">Share</th>
            <th align="center">Content Management</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Ref</th>
            <th>Client Name</th>
            <th>Date Created</th>
            <th align="center">Share</th>
            <th align="center">Content Management</th>
            <th align="center">Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($clients) && is_array($clients) && count($clients)) { ?>
            <?php foreach ($clients as $client) { ?>
                <tr>
                    <td>
                        <?php echo $client->ref; ?>
                    </td>
                    <td>
                        <?php echo $client->client_name; ?>
                    </td>
                    <td>
                        <?php echo date("d/m/Y", strtotime($client->created_date)); ?>
                    </td>
                    <td align="center">
                        <a href="#" onclick="share_linkedin(this);"
                           data-url="<?php echo ms_site_url('microsite/' . $client->ref); ?>"
                           class="icon fab fa-fw fa-linkedin tooltip" title="Share To LinkedIn"></a>
                        <a href="#" onclick="share_facebook(this);"
                           data-url="<?php echo ms_site_url('microsite/' . $client->ref); ?>"
                           class="icon fab fa-fw fa-facebook tooltip" title="Share To Facebook"></a>
                        <a href="#" onclick="share_twitter(this);"
                           data-url="<?php echo ms_site_url('microsite/' . $client->ref); ?>"
                           class="icon fab fa-fw fa-twitter tooltip" title="Share To Twitter"></a>
                        <a href="#" onclick="copy_link_modal(this);"
                           data-url="<?php echo ms_site_url('microsite/' . $client->ref); ?>"
                           class="icon fa fa-fw fa-link tooltip" title="Copy Link"></a>
                    </td>
                    <td align="center">
						 <a href="<?php echo site_url('vacancies/index/' . $client->client_id); ?>"
                           class="icon fas fa-clipboard tooltip" title="Vacancies"></a>
                        <a href="<?php echo site_url('clients_content_pages/index/' . $client->client_id); ?>"
                           class="icon fas fa-align-left tooltip" title="Content Pages"></a>
                        <a href="<?php echo site_url('photos/index/' . $client->client_id); ?>"
                           class="icon fas fa-image tooltip" title="Photos"></a>
                        <a href="<?php echo site_url('clients_testimonials/index/' . $client->client_id); ?>"
                           class="icon fas fa-quote-right tooltip" title="Testimonials"></a>
                        <a href="<?php echo site_url('videos/index/' . $client->client_id); ?>"
                           class="icon fas fa-video tooltip" title="Videos"></a>
                        <a href="<?php echo site_url('offices/index/' . $client->client_id); ?>"
                           class="icon fas fa-map-pin tooltip" title="Offices"></a>
                    </td>
                    <td align="center">
                        <a href="<?php echo ms_site_url('microsite/' . $client->ref); ?>" target="_blank"
                           class="icon fas fa-eye tooltip" title="View"></a>
                        <a href="<?php echo site_url('clients/edit/' . $client->client_id); ?>"
                           class="icon fa fa-fw fa-pencil-alt tooltip" title="Edit"></a>
                        <a href="<?php echo site_url('clients/delete/' . $client->client_id); ?>"
                           class="icon fa fa-fw fa-trash tooltip" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>

<div id="copy-link-dialog" class="copy_link_modal" title="Copy Link">
    <input type="text" id="copy_link_profile_url" class="copy_link_modal_input" title="Copy Link" readonly
           value="">
</div>

<script>
    function share_linkedin(e) {
        window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + $(e).data('url'), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
        return false;
    }

    function share_twitter(e) {
        window.open('https://twitter.com/intent/tweet?url=' + $(e).data('url'), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
        return false;
    }

    function share_facebook(e) {
        window.open('https://www.facebook.com/sharer/sharer.php?u=' + $(e).data('url'), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
        return false;
    }

    function copy_link_modal(el) {
        $('#copy_link_profile_url').val($(el).data('url'));
        $('#copy_link_profile_url').tooltipster('content', 'Copy Link');

        $("#copy-link-dialog").dialog({
            resizable: false,
            height: "auto",
            width: 600,
            modal: true,
            buttons: {
                "Copy": function () {
                    var copyText = document.getElementById("copy_link_profile_url");
                    copyText.select();
                    document.execCommand("copy");

                    $('#copy_link_profile_url').tooltipster('content', 'Copied');
                    $('#copy_link_profile_url').tooltipster('open');
                },
                Cancel: function () {
                    $('#copy_link_profile_url').tooltipster('close');
                    $(this).dialog("close");
                }
            }
        });
    }

    $(function () {
        $('#copy_link_profile_url').tooltipster({
            selfDestruction: false
        });

        var table = $('#clients').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, 'asc']],
            "aoColumnDefs": [],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#clients tfoot th').each(function () {
            var title = $('#clients tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>
