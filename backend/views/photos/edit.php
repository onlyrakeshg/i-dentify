<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('photos/index/' . $photo->client_id); ?>" class="btn cancel"><i
                    class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-image"></i>Photos <i class="fas fa-caret-right"></i>Edit
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('photos/edit/' . $photo->photo_id); ?>" method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">General</span>
            <div class="col half_column_left">
                <label for="title">
                    Title
                </label>
                <input type="text" name="title" id="title"
                       value="<?php echo set_value('title') ? set_value('title') : ($photo->title ? $photo->title : "") ?>">
            </div>
            <div class="col half_column_right">
                <label for="image">
                    Photo
                    <small><i>(Image files must be under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or PNG format)</i></small>
                </label>
                <input type="file" name="image" id="image" class="inputfile"/>
                <?php if ($photo->image) { ?>
                    <div id="edit_bg" class="preview-img">
                        <a href="<?php echo ms_base_url('uploads/images/' . $photo->image); ?>" target="_blank"
                           title="Click to Open in new tab" class="title">
                            <img src="<?php echo ms_base_url('uploads/images/' . $photo->image); ?>"
                                 alt="Image" height="100">
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('photos/index/' . $photo->client_id); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>