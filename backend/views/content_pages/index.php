<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('content_pages/add'); ?>" class="btn add">
            <i class="fas fa-plus-circle"></i> Add New Content Page
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-align-left"></i>Content Pages
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="content_pages">
        <thead>
        <tr>
            <th align="center">ID</th>
            <th>Title</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($content_pages) && is_array($content_pages) && count($content_pages)) { ?>
            <?php foreach ($content_pages as $content_page) { ?>
                <tr>
                    <td align="center">
                        <?php echo $content_page->content_page_id; ?>
                    </td>
                    <td>
                        <?php echo $content_page->title; ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('content_pages/edit/' . $content_page->content_page_id); ?>"
                           class="icon fa fa-fw fa-pencil-alt" title="Edit"></a>
                        <a href="<?php echo site_url('content_pages/delete/' . $content_page->content_page_id); ?>"
                           onclick="return confirm('Are you sure you wish to delete this content page?');"
                           class="icon fa fa-fw fa-trash" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#content_pages').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#content_pages tfoot th').each(function () {
            var title = $('#content_pages tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>