<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('team'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-users"></i>Team <i class="fas fa-caret-right"></i>New
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('team/add'); ?>" method="post" enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">General</span>
            <div class="col half_column_left">
                <label for="firstname">
                    First Name
                </label>
                <input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname'); ?>"
                       required>
            </div>
            <div class="col half_column_right">
                <label for="lastname">
                    Last Name
                </label>
                <input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname'); ?>" required>
            </div>
            <div class="col half_column_left">
                <label for="email">
                    Email
                </label>
                <input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>"
                       required>
            </div>
            <div class="col half_column_right">
                <label for="tel">
                    Telephone Number
                </label>
                <input type="tel" name="tel" id="tel" value="<?php echo set_value('tel'); ?>">
            </div>
            <div class="col half_column_left">
                <label for="password">
                    Password
                </label>
                <input type="password" name="password" id="password" value="<?php echo set_value('password'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="image">
                    Image
                </label>
                <input type="file" name="image" id="image" class="inputfile"/>
            </div>
            <div class="col half_column_left">
                <label for="slug">
                    Slug
                </label>
                <input type="text" name="slug" id="slug" value="<?php echo set_value('slug'); ?>">
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <span class="heading">Details</span>
            <div class="col half_column_left">
                <label for="title">
                    Title
                </label>
                <input type="text" name="title" id="title" value="<?php echo set_value('title'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="linkedin">
                    LinkedIn URL
                </label>
                <input type="text" name="linkedin" id="linkedin"
                       value="<?php echo set_value('linkedin'); ?>">
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label for="description">
                    Description
                </label>
                <textarea name="description" id="description"
                          rows="20"><?php echo set_value('description'); ?></textarea>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('team'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>

<link rel="stylesheet"
      href="<?php echo ms_base_url('plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') ?>">
<script src="<?php echo ms_base_url('plugins/ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo ms_base_url('plugins/ckeditor/samples/js/sample.js'); ?>"></script>
<script>
    $(function () {
        $("#firstname").keyup(function () {
            if ($(this).val().length > 0) {
                $("#slug").val(
                    $(this).val().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase() +
                    "-" +
                    $("#lastname").val().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase()
                );
            } else {
                $("#slug").val("");
            }
        });

        $("#lastname").keyup(function () {
            if ($(this).val().length > 0) {
                $("#slug").val(
					
					$("#firstname").val().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase()
					+
                    "-" +
                    $(this).val().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase() 
                    
                );
            } else {
                $("#slug").val("");
            }
        });

        var editor = CKEDITOR.replace('description', {
            htmlEncodeOutput: false,
            wordcount: {
                showWordCount: true,
                showCharCount: true,
                countSpacesAsChars: true,
                countHTML: false,
            },
            removePlugins: 'zsuploader',

            filebrowserBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageBrowseUrl: '<?php echo ms_base_url('plugins/kcfinder/browse.php?opener=ckeditor&type=images'); ?>',
            filebrowserUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageUploadUrl: '<?php echo ms_base_url('plugins/kcfinder/upload.php?opener=ckeditor&type=images'); ?>'
        });
    });
</script>