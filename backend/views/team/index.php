<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('team/add'); ?>" class="btn add">
            <i class="fas fa-plus-circle"></i> Add New Team Member
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-users"></i>Team
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="team">
        <thead>
        <tr>
            <th align="center">ID</th>
            <th>Full Name</th>
            <th>Email</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Ful Name</th>
            <th>Email</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($team) && is_array($team) && count($team)) { ?>
            <?php foreach ($team as $team_row) { ?>
                <tr>
                    <td align="center">
                        <?php echo $team_row->user_id; ?>
                    </td>
                    <td>
                        <?php echo $team_row->firstname . ' ' . $team_row->lastname; ?>
                    </td>
                    <td>
                        <?php echo $team_row->email; ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('team/edit/' . $team_row->user_id); ?>"
                           class="icon fa fa-fw fa-pencil-alt" title="Edit"></a>
                        <?php if ($active_user->user_id !== $team_row->user_id) { ?>
                            <a href="<?php echo site_url('team/delete/' . $team_row->user_id); ?>"
                               onclick="return confirm('Are you sure you wish to delete this team member?');"
                               class="icon fa fa-fw fa-trash" title="Delete"></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#team').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#team tfoot th').each(function () {
            var title = $('#team tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>