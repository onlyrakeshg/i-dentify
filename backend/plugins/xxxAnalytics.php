<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'analytics',
    'title' => 'Google Analytics',
    'enabled' => TRUE,
    'icon' => 'fab fa-google',
    'group' => 'Integrations',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_sort' => 10,
    'widget_block_enabled' => TRUE,
    'widget_block_view_data' => function ($CI) {
        $view_data = array();
        return $view_data;
    }
);