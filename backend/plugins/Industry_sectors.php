<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'industry_sectors',
    'title' => 'Sectors / Industries',
    'enabled' => TRUE,
    'icon' => 'fab fa-buromobelexperte',
    'group' => 'Vacancy Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);