<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'blog',
    'title' => 'Blog / News',
    'enabled' => TRUE,
    'icon' => 'fas fa-rss',
    'group' => 'Content Management',
    'widget_statistic_enabled' => TRUE,
    'widget_statistic_title' => "Blog Posts",
    'widget_statistic_override_number' => 96,
    'widget_block_enabled' => FALSE,
    'widget_block_sort' => 5,
    'widget_block_view_data' => function ($CI) {
        $view_data = array();

        $CI->load->model('blog_model');
        $view_data['blogs'] = $CI->blog_model->get(array(), 7, 0, 'date', 'DESC');

        return $view_data;
    }
);