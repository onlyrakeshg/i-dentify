<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'locations',
    'title' => 'Locations',
    'enabled' => TRUE,
    'icon' => 'fas fa-map-marker',
    'group' => 'Vacancy Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);