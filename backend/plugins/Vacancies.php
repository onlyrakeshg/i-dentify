<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'vacancies',
    'title' => 'Vacancy Manager',
    'enabled' => TRUE,
    'icon' => 'fas fa-clipboard',
    'group' => 'Vacancy Management',
    'widget_statistic_enabled' => TRUE,
    'widget_statistic_title' => "Jobs Listed",
    //'widget_statistic_override_number' => 132,
    'widget_block_enabled' => TRUE,
    'widget_block_sort' => 1,
    'widget_block_view_data' => function ($CI) {
        $view_data = array();

        $CI->load->model('vacancies_model');
        $view_data['vacancies'] = $CI->vacancies_model->get(array(), 7, 0, 'date', 'DESC');

        return $view_data;
    }
);