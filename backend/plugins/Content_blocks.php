<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'content_blocks',
    'title' => 'Content Blocks',
    'enabled' => TRUE,
    'icon' => 'fas fa-text-width',
    'group' => 'Content Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block' => NULL
);