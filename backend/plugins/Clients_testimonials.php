<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'clients_testimonials',
    'title' => 'Testimonials',
    'enabled' => TRUE,
    'hidden' => TRUE,
    'icon' => 'fas fa-quote-right',
    'group' => 'Client Management',
    'widget_statistic_enabled' => FALSE,
    'widget_block_enabled' => FALSE,
);