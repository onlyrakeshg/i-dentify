<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'clients_content_pages',
    'title' => 'Content Pages',
    'enabled' => TRUE,
    'hidden' => TRUE,
    'icon' => 'fas fa-align-left',
    'group' => 'Client Management',
    'widget_statistic_enabled' => FALSE,
    'widget_block_enabled' => FALSE
);