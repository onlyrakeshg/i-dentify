<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'clients',
    'title' => 'Manage Microsites',
    'enabled' => TRUE,
    'hidden' => FALSE,
    'icon' => 'fas fa-users',
    'group' => 'Client Management',
    'widget_statistic_enabled' => FALSE,
    'widget_block_enabled' => FALSE,
);