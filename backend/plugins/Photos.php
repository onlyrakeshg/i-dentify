<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'photos',
    'title' => 'Photos',
    'enabled' => TRUE,
    'hidden' => TRUE,
    'icon' => 'fas fa-image',
    'group' => 'Client Management',
    'widget_statistic_enabled' => FALSE,
    'widget_block_enabled' => FALSE,
);