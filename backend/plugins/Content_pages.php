<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'content_pages',
    'title' => 'Content Pages',
    'enabled' => TRUE,
    'icon' => 'fas fa-align-left',
    'group' => 'Content Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);