<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'testimonials',
    'title' => 'Testimonials',
    'enabled' => TRUE,
    'icon' => 'fas fa-home',
    'group' => 'Content Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);