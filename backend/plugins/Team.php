<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'team',
    'title' => 'Team Manager',
    'enabled' => TRUE,
    'icon' => 'fas fa-users',
    'group' => 'Team Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);