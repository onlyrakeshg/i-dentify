<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'linkedin',
    'title' => 'LinkedIn',
    'enabled' => TRUE,
    'icon' => 'fab fa-linkedin',
    'group' => 'Integrations',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);