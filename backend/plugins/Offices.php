<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'offices',
    'title' => 'Offices',
    'enabled' => TRUE,
    'hidden' => TRUE,
    'icon' => 'fas fa-map-pin',
    'group' => 'Client Management',
    'widget_statistic_enabled' => FALSE,
    'widget_block_enabled' => FALSE,
);