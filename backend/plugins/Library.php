<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'library',
    'title' => 'Applicants',
    'enabled' => TRUE,
    'icon' => 'fas fa-book-reader',
    'group' => 'Talent Pool',
    'widget_statistic_enabled' => TRUE,
    'widget_statistic_title' => "Job Applications",
	//'widget_statistic_override_number' => 173,
    'widget_block_enabled' => FALSE
);