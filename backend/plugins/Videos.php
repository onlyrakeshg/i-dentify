<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'videos',
    'title' => 'Videos',
    'enabled' => TRUE,
    'hidden' => TRUE,
    'icon' => 'fas fa-video',
    'group' => 'Client Management',
    'widget_statistic_enabled' => FALSE,
    'widget_block_enabled' => FALSE,
);