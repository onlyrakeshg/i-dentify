<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'maps',
    'title' => 'Google Maps',
    'enabled' => TRUE,
    'icon' => 'fab fa-google',
    'group' => 'Integrations',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);