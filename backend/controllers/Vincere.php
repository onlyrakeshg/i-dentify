<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vincere extends BOLD_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->config->load('vincere');
        $this->load->library('request');
        $this->load->model('vincere_model');
    }

    public function auth()
    {
        if (!$this->input->get('code')) {
            $params = array(
                "client_id" => $this->config->item('vincere_client_id'),
                "state" => substr(md5(time()), 0, 10),
                "redirect_uri" => site_url('vincere/auth'),
                "response_type" => "code"
            );
            $base = "https://id.vincere.io/oauth2/authorize";

            redirect($base . "?" . http_build_query($params));
        } else {
            $code = $this->input->get('code');

            $params = array(
                "client_id" => $this->config->item('vincere_client_id'),
                "code" => $code,
                "grant_type" => "authorization_code"
            );

            $base = "https://id.vincere.io/oauth2/token";
            $access_token = $this->request->get_contents($base, NULL, $params);

            if ($access_token) {
                $json = json_decode($access_token);
                if ($json) {
                    if ($this->vincere_model->add_row(array(
                        "access_token" => $json->access_token,
                        "refresh_token" => $json->refresh_token,
                        "id_token" => $json->id_token,
                        "expires_in" => $json->expires_in,
                        "time" => time()
                    ))) {
                        echo "Website authorized, you can close this window";
                    }
                }
            }

        }
    }

    public function cronjob()
    {
        header("Content-type:text/plain");

        $access_token = $this->vincere_model->get_row(array(), "time", "DESC");
        if ($access_token && (time() > ($access_token->expires_in + $access_token->time))) {
            echo "Access to Remote API expired (" . date("Y-m-d H:i:s", ($access_token->expires_in + $access_token->time)) . "), trying to refresh token." . "\n";
            $params = array(
                "client_id" => $this->config->item('vincere_client_id'),
                "grant_type" => "refresh_token",
                "refresh_token" => $access_token->refresh_token
            );

            $base = "https://id.vincere.io/oauth2/token";
            $access_token_raw = $this->request->get_contents($base, NULL, $params);

            if ($access_token_raw) {
                $access_token_json = json_decode($access_token_raw);
                if ($access_token_json) {
                    echo "New access token received." . "\n";

                    if ($this->vincere_model->add_row(array(
                        "access_token" => $access_token_json->access_token,
                        "refresh_token" => isset($access_token_json->refresh_token) && $access_token_json->refresh_token ? $access_token_json->refresh_token : $access_token->refresh_token,
                        "id_token" => $access_token_json->id_token,
                        "expires_in" => $access_token_json->expires_in,
                        "time" => time()
                    ))) {
                        $access_token = $this->vincere_model->get_row(array(), "time", "DESC");
                    }
                }
            }
        }

        if ($access_token && (time() < ($access_token->expires_in + $access_token->time))) {
            /*
            echo "Loading user services." . "\n";
            $user = $this->request->get_contents("https://id.vincere.io/oauth2/user", NULL, NULL, array(
                "x-api-key:" . $this->config->item('vincere_api_key'),
                "id-token:" . $access_token->id_token,
                    ));
            if($user) {
                $user = json_decode($user);
                if($user) {
                    var_dump($user);
                }
            }
            */

            /*
            echo "Loading Contacts" . "\n";
            $contacts = array();

            $matrix_vars = array(
                "fl" => implode(",", array(
                    "id",
                    "name"
                )),
                "sort" => "created_date desc"
            );
            $start = 0;
            $total = NULL;
            $attempts = 0;
            $limit = 100;

            do {
                echo "Loading Contacts " . $start . ".." . ($start + $limit) . "\n";
                $contacts_raw = $this->request->get_contents("https://" . $this->config->item('vincere_domain') . ".vincere.io/api/v2/" .
                    "contact/search/" . implode(";", array_map(function ($key, $value) {
                        return $key . "=" . $value;
                    }, array_keys($matrix_vars), $matrix_vars)), array(
                    "start" => $start,
                    "limit" => $limit,
                ), NULL, array(
                    "x-api-key:" . $this->config->item('vincere_api_key'),
                    "id-token:" . $access_token->id_token,
                ));
                if ($contacts_raw) {
                    $contacts_json = json_decode($contacts_raw);
                    if ($contacts_json && isset($contacts_json->result) && isset($contacts_json->result->total)) {
                        $total = $contacts_json->result->total;

                        $contacts = array_merge($contacts, $contacts_json->result->items);

                        $start += $limit;
                    } else {
                        $attempts++;
                    }
                } else {
                    $attempts++;
                }
            } while ($attempts < 5 && ($total === NULL || ($start < $total)));

            if (count($contacts) > 0) {
                echo "Loaded " . count($contacts) . " contacts in total" . "\n";

                var_dump($contacts[0]);
            }
            */

            echo "Loading vacancies" . "\n";
            $vacancies = array();

            $matrix_vars = array(
                "fl" => implode(",", array(
                    "id",
                    "job_title",
                    //"fe",
                    "sfe",
                    "industry",
                    "public_description",
                    "job_summary",
                    "open_date",
                    "closed_date",
                    "created_date",
                    "pay_rate",
                    "formatted_pay_rate",
                    "salary_from",
                    "formatted_salary_from",
                    "salary_to",
                    "formatted_salary_to",
                    "contract_length",
                    "location",
                    "job_type",
                    "employment_type",
                    //"published_date",
                    //"description",
                    "contact",
                    "salary_type",
                    "pay_interval",
                    "monthly_pay_rate",
                    "formatted_monthly_pay_rate",
                    "monthly_salary_from",
                    "formatted_monthly_salary_from",
                    "monthly_salary_to",
                    "formatted_monthly_salary_to",
                    "currency",
                    "company"
                )),
                "sort" => "closed_date desc"
            );
            $start = 0;
            $total = NULL;
            $attempts = 0;
            $limit = 100;

            do {
                //echo "Loading vacancies " . $start . ".." . ($start + $limit) . "\n";
                $vacancies_raw = $this->request->get_contents("https://" . $this->config->item('vincere_domain') . ".vincere.io/api/v2/" .
                    "job/search/" . implode(";", array_map(function ($key, $value) {
                        return $key . "=" . $value;
                    }, array_keys($matrix_vars), $matrix_vars)), array(
                    //"q" => "open_date:[" . date("Y-m-d", strtotime("- 30 day")) . " TO *]#",
                    "q" => "private_job:0#open_date:[" . date("Y-m-d", strtotime("- 90 day")) . " TO *]#",
                    //"q" => "private_job:0#",
                    "start" => $start,
                    "limit" => $limit,
                ), NULL, array(
                    "x-api-key:" . $this->config->item('vincere_api_key'),
                    "id-token:" . $access_token->id_token,
                ));
                if ($vacancies_raw) {
                    $vacancies_json = json_decode($vacancies_raw);
                    if ($vacancies_json && isset($vacancies_json->result) && isset($vacancies_json->result->total)) {
                        $total = $vacancies_json->result->total;

                        $vacancies = array_merge($vacancies, $vacancies_json->result->items);

                        $start += $limit;
                    } else {
                        $attempts++;
                    }
                } else {
                    $attempts++;
                }
            } while ($attempts < 5 && ($total === NULL || ($start < $total)));

            if (count($vacancies) > 0) {
                echo "Loaded " . count($vacancies) . " open vacancies in total" . "\n";

                $this->load->model('vacancies_model');
                $this->load->model('team_model');
                $this->load->model('locations_model');
                $this->load->model('industry_sectors_model');
                $remote_vacancies_ids = array();

                foreach ($vacancies as $vacancy) {
                    if (!isset($vacancy->id) || !isset($vacancy->job_title))
                        continue;

                    $ref = $vacancy->id;
                    $remote_vacancies_ids[] = $ref;

                    $consultant_id = 0;
                    if (isset($vacancy->contact) && isset($vacancy->contact->firstName) && $vacancy->contact->firstName && isset($vacancy->contact->lastName) && $vacancy->contact->lastName) {
                        $consultant = $this->team_model->get_row(array(
                            'firstname' => $vacancy->contact->firstName,
                            'lastname' => $vacancy->contact->lastName
                        ));

                        if ($consultant) {
                            $consultant_id = $consultant->user_id;
                        } else {
                            //echo "Vacancy # " . $vacancy->id . " " . $vacancy->job_title . " no consultant found in local team. Consultant name: " . $vacancy->contact->firstName . " " . $vacancy->contact->lastName . "\n";
                        }
                    } else {
                        //echo "Vacancy # " . $vacancy->id . " " . $vacancy->job_title . " no consultant found in local team. Consultant name missing" . "\n";
                    }

                    $contract_type = 'permanent';
                    if (isset($vacancy->job_type)) {
                        switch ($vacancy->job_type) {
                            case 'CONTRACT':
                                $contract_type = 'contract';
                                break;
                            case 'PERMANENT':
                                $contract_type = 'permanent';
                                break;
                        }
                    }

                    $salary_type = 'salary';
                    $salary_value = 'Negotiable';
                    if (isset($vacancy->salary_type)) {
                        switch ($vacancy->salary_type) {
                            case "DAILY":
                                $salary_type = 'daily';
                                break;
                            case "HOURLY":
                                $salary_type = 'hourly';
                                break;
                            case "ANNUAL":
                            default:
                                $salary_type = 'salary';
                                if ((isset($vacancy->salary_from) && floatval($vacancy->salary_from) > 0) || (isset($vacancy->salary_to) && floatval($vacancy->salary_to) > 0)) {
                                    $salary_parts = array();

                                    if (isset($vacancy->salary_from) && floatval($vacancy->salary_from) > 0) {
                                        $salary_parts[] = $vacancy->salary_from;
                                    }

                                    if (isset($vacancy->salary_to) && floatval($vacancy->salary_to) > 0) {
                                        $salary_parts[] = $vacancy->salary_to;
                                    }

                                    if (count($salary_parts) > 0)
                                        $salary_value = implode(" - ", $salary_parts);
                                } elseif (isset($vacancy->pay_rate) && $vacancy->pay_rate) {
                                    $salary_value = $vacancy->pay_rate;
                                }
                        }
                    }

                    $slug = preg_replace("/[^a-zA-Z0-9]+/is", "-", strtolower(trim($vacancy->job_title)));

                    $vacancy_data = array(
                        'ref' => $ref,
                        'consultant_id' => $consultant_id,
                        'title' => $vacancy->job_title,
                        'contract_type' => $contract_type,
                        'contract_length' => isset($vacancy->contract_length) ? $vacancy->contract_length : NULL,
                        'salary_type' => $salary_type,
                        'salary_value' => $salary_value,
                        'description' => isset($vacancy->public_description) ? $vacancy->public_description : '',
                        'client_name' => isset($vacancy->contact) ? $vacancy->contact->firstName . ' ' . $vacancy->contact->lastName : NULL,
                        'client_email' => isset($vacancy->contact) && isset($vacancy->contact->email) ? $vacancy->contact->email : NULL,
                        'postcode' => isset($vacancy->location) && isset($vacancy->location->post_code) && $vacancy->location->post_code ? $vacancy->location->post_code : NULL,
                        'meta_title' => $vacancy->job_title,
                        'meta_desc' => $vacancy->job_title,
                        'date' => date("Y-m-d H:i:s", strtotime($vacancy->open_date)),
                        'slug' => $slug,
                        'deleted' => FALSE
                    );

                    if ($this->vacancies_model->get_count(array('ref' => $ref))) {
                        $db_vacancy = $this->vacancies_model->get_row(array('ref' => $ref));

                        $check_slug = $this->vacancies_model->get_count(array('vacancy_id !=' => $db_vacancy->vacancy_id, 'slug' => $slug));
                        if ($check_slug > 0) {
                            $vacancy_data['slug'] = preg_replace("/[^a-zA-Z0-9]+/is", "-", strtolower(trim($vacancy->job_title))) . '-' . $vacancy->id;
                        }

                        if ($this->vacancies_model->update_row(array('vacancy_id' => $db_vacancy->vacancy_id), $vacancy_data)) {

                            $actual_locations = array();
                            $actual_sectors = array();
                            if (isset($vacancy->location->city) && $vacancy->location->city) {
                                $location_name = trim($vacancy->location->city);

                                if ($this->locations_model->get_count(array(
                                    'name' => $location_name
                                ))) {
                                    $location = $this->locations_model->get_row(array(
                                        'name' => $location_name
                                    ));
                                    $actual_locations[] = $location->location_id;

                                    if (!$this->vacancies_model->locations_get_count(array('vacancy_id' => $db_vacancy->vacancy_id, 'location_id' => $location->location_id))) {
                                        $this->vacancies_model->locations_add_row(array('vacancy_id' => $db_vacancy->vacancy_id, 'location_id' => $location->location_id));
                                    }
                                } else {
                                    if ($this->locations_model->add_row(array(
                                        'name' => $location_name
                                    ))) {
                                        $location_id = $this->locations_model->insert_id();
                                        $actual_locations[] = $location_id;

                                        $this->vacancies_model->locations_add_row(array('vacancy_id' => $db_vacancy->vacancy_id, 'location_id' => $location_id));
                                    }
                                }
                            }


                            if (isset($vacancy->industry) && is_array($vacancy->industry) && count($vacancy->industry)) {
                                foreach ($vacancy->industry as $industry) {
                                    $sector_name = trim($industry->description);

                                    if ($this->industry_sectors_model->get_count(array(
                                        'name' => $sector_name
                                    ))) {
                                        $sector = $this->industry_sectors_model->get_row(array(
                                            'name' => $sector_name
                                        ));
                                        $actual_sectors[] = $sector->industry_sector_id;

                                        if (!$this->vacancies_model->sectors_get_count(array('vacancy_id' => $db_vacancy->vacancy_id, 'industry_sector_id' => $sector->industry_sector_id))) {
                                            $this->vacancies_model->sectors_add_row(array('vacancy_id' => $db_vacancy->vacancy_id, 'industry_sector_id' => $sector->industry_sector_id));
                                        }
                                    } else {
                                        if ($this->industry_sectors_model->add_row(array(
                                            'name' => $sector_name
                                        ))) {
                                            $industry_sector_id = $this->industry_sectors_model->insert_id();
                                            $actual_locations[] = $industry_sector_id;

                                            $this->vacancies_model->sectors_add_row(array('vacancy_id' => $db_vacancy->vacancy_id, 'industry_sector_id' => $industry_sector_id));
                                        }
                                    }
                                }
                            }
                            $this->vacancies_model->locations_delete(array(
                                'vacancy_id' => $db_vacancy->vacancy_id,
                                "NOT FIND_IN_SET(`location_id`, '" . implode("', '", $actual_locations) . "')" => NULL
                            ));
                            $this->vacancies_model->sectors_delete(array(
                                'vacancy_id' => $db_vacancy->vacancy_id,
                                "NOT FIND_IN_SET(`industry_sector_id`, '" . implode("', '", $actual_sectors) . "')" => NULL
                            ));
                        } else {
                            $this->log("Database error on updating vacancy " . $ref);
                        }
                    } else {
                        $check_slug = $this->vacancies_model->get_count(array('slug' => $slug));
                        if ($check_slug > 0) {
                            $vacancy_data['slug'] = preg_replace("/[^a-zA-Z0-9]+/is", "-", strtolower(trim($vacancy->job_title))) . '-' . $vacancy->id;
                        }

                        if ($this->vacancies_model->add_row($vacancy_data)) {
                            $vacancy_id = $this->vacancies_model->insert_id();

                            if (isset($vacancy->location->city) && $vacancy->location->city) {
                                $location_name = trim($vacancy->location->city);

                                if ($this->locations_model->get_count(array(
                                    'name' => $location_name
                                ))) {
                                    $location = $this->locations_model->get_row(array(
                                        'name' => $location_name
                                    ));

                                    $this->vacancies_model->locations_add_row(array('vacancy_id' => $vacancy_id, 'location_id' => $location->location_id));
                                } else {
                                    if ($this->locations_model->add_row(array(
                                        'name' => $location_name
                                    ))) {
                                        $location_id = $this->locations_model->insert_id();

                                        $this->vacancies_model->locations_add_row(array('vacancy_id' => $vacancy_id, 'location_id' => $location_id));
                                    }
                                }
                            }

                            if (isset($vacancy->industry) && is_array($vacancy->industry) && count($vacancy->industry)) {
                                foreach ($vacancy->industry as $industry) {
                                    $sector_name = trim($industry->description);

                                    if ($this->industry_sectors_model->get_count(array(
                                        'name' => $sector_name
                                    ))) {
                                        $sector = $this->industry_sectors_model->get_row(array(
                                            'name' => $sector_name
                                        ));

                                        $this->vacancies_model->sectors_add_row(array('vacancy_id' => $vacancy_id, 'industry_sector_id' => $sector->industry_sector_id));
                                    } else {
                                        if ($this->industry_sectors_model->add_row(array(
                                            'name' => $sector_name
                                        ))) {
                                            $industry_sector_id = $this->industry_sectors_model->insert_id();

                                            $this->vacancies_model->sectors_add_row(array('vacancy_id' => $vacancy_id, 'industry_sector_id' => $industry_sector_id));
                                        }
                                    }
                                }
                            }
                        } else {
                            echo "Database error on adding vacancy " . $ref . "\n";
                        }
                    }
                }

                $count_delete = $this->vacancies_model->get_count(array(
                    "`vacancies`.`ref` NOT IN ('" . implode("','", $remote_vacancies_ids) . "')" => NULL,
                    'deleted' => FALSE
                ));
                if ($count_delete > 0) {
                    echo "Found " . $count_delete . " old vacancies, removing..." . "\n";
                    $this->vacancies_model->update(array(
                        "`vacancies`.`ref` NOT IN ('" . implode("','", $remote_vacancies_ids) . "')" => NULL
                    ), array('deleted' => TRUE));
                }

                echo "Finished" . "\n";
            }
        } else {
            echo "Access to Remote API expired (" . date("Y-m-d H:i:s", ($access_token->expires_in + $access_token->time)) . "), please refresh authorization." . "\n";
        }
    }
}