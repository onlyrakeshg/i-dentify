<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacancies extends BOLD_Controller
{
    private $plugin = 'vacancies';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('vacancies_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->vacancies_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->vacancies_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index($client_id = NULL)
    {
        $this->view_data['title'] = 'Vacancies Manager';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

		if(is_numeric($client_id)) {
			$this->load->model('clients_model');
			$this->view_data['client'] = $this->clients_model->get_row(array(
				'client_id' => $client_id
			));
		}
        $this->view_data['show'] = $client_id;

		$params = array(
			'deleted' => FALSE,
			'client_id' => NULL,
		);
		if($client_id !== NULL) $params['client_id'] = $client_id;
		if($client_id === 'old') $params['deleted'] = TRUE;
        $this->view_data['vacancies'] = $this->vacancies_model->get($params);

        $this->load->view('header', $this->view_data);
        $this->load->view('vacancies/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add($client_id = NULL)
    {
		if($client_id) {
			$this->load->model('clients_model');
			$this->view_data['client'] = $this->clients_model->get_row(array(
				'client_id' => $client_id
			));

			if(!$this->view_data['client']) redirect('clients');

			$this->view_data['client_id'] = $this->view_data['client']->client_id;
		} else
			$this->view_data['client_id'] = NULL;

        $this->view_data['title'] = 'Add Vacancy';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('title', 'Job Title', 'required|trim|min_length[1]|max_length[200]');
            $this->form_validation->set_rules('ref', 'Job Ref', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('sector_ids', 'Industries/Sectors', 'trim');
            $this->form_validation->set_rules('location_ids', 'Locations', 'trim');
            $this->form_validation->set_rules('contract_type', 'Contract Type', 'required|trim|in_list[permanent,temporary,fixed]');
            $this->form_validation->set_rules('contract_length', 'Contract Length', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('salary_type', 'Pay Frequency', 'required|trim|in_list[salary,hourly,daily]');
            $this->form_validation->set_rules('salary_value', 'Rate of pay', 'required|trim|min_length[1]|max_length[50]');
            $this->form_validation->set_rules('description', 'Description', 'required|trim|min_length[1]');
            $this->form_validation->set_rules('consultant_id', 'Consultant', 'required|intval|greater_than[0]');
            $this->form_validation->set_rules('client_name', 'Client Name', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('client_email', 'Client Email', 'trim|min_length[0]|max_length[150]|valid_email');
            $this->form_validation->set_rules('client_tel', 'Client Telephone Number', 'trim|min_length[0]|max_length[25]');
            $this->form_validation->set_rules('postcode', 'Postcode', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|min_length[0]|max_length[250]');
            $this->form_validation->set_rules('meta_desc', 'Meta Description', 'trim|min_length[0]|max_length[255]');
            $this->form_validation->set_rules('canonical', 'Canonical', 'trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('slug', 'Slug', 'trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('allow_on_portal', 'Display On Portal', 'trim');

            if ($this->form_validation->run()) {

                $slug = set_value('slug') ? set_value('slug') : NULL;
                if(!$slug) {
					$this->load->helper('slug_helper');
					$slug = gen_slug(set_value('title'));
				}

				$slug_base = $slug;
				$uid = rand(1111,9999);
				while($this->vacancies_model->get_count(array(
						'slug' => $slug
					)) > 0) {
					$slug = $slug_base . "-" . $uid;
					$uid = rand(1111,9999);
				}

                $data = array(
					'client_id' => $this->view_data['client_id'],
                    'title' => set_value('title'),
                    'ref' => set_value('ref'),
                    'contract_type' => set_value('contract_type'),
                    'contract_length' => set_value('contract_length') ? set_value('contract_length') : NULL,
                    'salary_type' => set_value('salary_type'),
                    'salary_value' => set_value('salary_value'),
                    'description' => set_value('description', NULL, FALSE),
                    'consultant_id' => set_value('consultant_id'),
                    'client_name' => set_value('client_name') ? set_value('client_name') : NULL,
                    'client_email' => set_value('client_email') ? set_value('client_email') : NULL,
                    'client_tel' => set_value('client_tel') ? set_value('client_tel') : NULL,
                    'postcode' => set_value('postcode') ? set_value('postcode') : NULL,
                    'meta_title' => set_value('meta_title') ? set_value('meta_title') : NULL,
                    'meta_keywords' => set_value('meta_keywords') ? set_value('meta_keywords') : NULL,
                    'meta_desc' => set_value('meta_desc') ? set_value('meta_desc') : NULL,
                    'canonical' => set_value('canonical') ? set_value('canonical') : NULL,
                    'slug' => $slug,
                    'deleted' => FALSE,
                    'date' => date('Y-m-d H:i:s'),
					'allow_on_portal' => (bool)(set_value('allow_on_portal') == '1'),
                );

                if ($this->vacancies_model->add_row($data)) {
                    $vacancy_id = $this->vacancies_model->insert_id();

                    if (is_array(set_value('sector_ids[]')) && count(set_value('sector_ids[]')) > 0) {
                        $data = array();
                        foreach (set_value('sector_ids[]') as $sector_id) {
                            $data[] = array(
                                'vacancy_id' => $vacancy_id,
                                'industry_sector_id' => $sector_id
                            );
                        }
                        $this->vacancies_model->sectors_add($data);
                    }

                    if (is_array(set_value('location_ids[]')) && count(set_value('location_ids[]')) > 0) {
                        $data = array();
                        foreach (set_value('location_ids[]') as $location_id) {
                            $data[] = array(
                                'vacancy_id' => $vacancy_id,
                                'location_id' => $location_id
                            );
                        }
                        $this->vacancies_model->locations_add($data);
                    }

                    $this->session->set_flashdata('success', 'Vacancy created successfully.');
                    redirect('vacancies' . ($this->view_data['client_id'] ? '/index/' . $this->view_data['client_id'] : ''));
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->view_data['company_name'] = $this->config->item('company_name');
        $this->load->model('industry_sectors_model');
        $this->view_data['industry_sectors'] = $this->industry_sectors_model->get();
        $this->load->model('locations_model');
        $this->view_data['locations'] = $this->locations_model->get();
        $this->load->model('team_model');
        $this->view_data['team'] = $this->team_model->get(array(
            'deleted' => FALSE
        ));

        $this->load->view('header', $this->view_data);
        $this->load->view('vacancies/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($vacancy_id)
    {
        if (!$vacancy_id) redirect('vacancies');

        $this->view_data['vacancy'] = $this->vacancies_model->get_row(
            array(
                'vacancy_id' => $vacancy_id
            )
        );
        if (!$this->view_data['vacancy']) redirect('vacancies');

        $this->view_data['title'] = 'Edit Vacancy';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('title', 'Job Title', 'required|trim|min_length[1]|max_length[200]');
            $this->form_validation->set_rules('ref', 'Job Ref', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('sector_ids', 'Industries/Sectors', 'trim');
            $this->form_validation->set_rules('location_ids', 'Locations', 'trim');
            $this->form_validation->set_rules('contract_type', 'Contract Type', 'required|trim|in_list[permanent,temporary,fixed]');
            $this->form_validation->set_rules('contract_length', 'Contract Length', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('salary_type', 'Pay Frequency', 'required|trim|in_list[salary,hourly,daily]');
            $this->form_validation->set_rules('salary_value', 'Rate of pay', 'required|trim|min_length[1]|max_length[50]');
            $this->form_validation->set_rules('description', 'Description', 'required|trim|min_length[1]');
            $this->form_validation->set_rules('consultant_id', 'Consultant', 'required|intval|greater_than[0]');
            $this->form_validation->set_rules('client_name', 'Client Name', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('client_email', 'Client Email', 'trim|min_length[0]|max_length[150]|valid_email');
            $this->form_validation->set_rules('client_tel', 'Client Telephone Number', 'trim|min_length[0]|max_length[25]');
            $this->form_validation->set_rules('postcode', 'Postcode', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|min_length[0]|max_length[250]');
            $this->form_validation->set_rules('meta_desc', 'Meta Description', 'trim|min_length[0]|max_length[255]');
            $this->form_validation->set_rules('canonical', 'Canonical', 'trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('slug', 'Slug', 'trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('allow_on_portal', 'Display On Portal', 'trim');

            if ($this->form_validation->run()) {
                $slug = set_value('slug') ? set_value('slug') : NULL;
                if(!$slug) {
					$this->load->helper('slug_helper');
					$slug = gen_slug(set_value('title'));
				}

				$slug_base = $slug;
				$uid = rand(1111,9999);
				while($this->vacancies_model->get_count(array(
						'vacancy_id !=' => $vacancy_id,
						'slug' => $slug
					)) > 0) {
					$slug = $slug_base . "-" . $uid;
					$uid = rand(1111,9999);
				}

                $data = array(
                    'title' => set_value('title'),
                    'ref' => set_value('ref'),
                    'contract_type' => set_value('contract_type'),
                    'contract_length' => set_value('contract_length') ? set_value('contract_length') : NULL,
                    'salary_type' => set_value('salary_type'),
                    'salary_value' => set_value('salary_value'),
                    'description' => set_value('description', NULL, FALSE),
                    'consultant_id' => set_value('consultant_id'),
                    'client_name' => set_value('client_name') ? set_value('client_name') : NULL,
                    'client_email' => set_value('client_email') ? set_value('client_email') : NULL,
                    'client_tel' => set_value('client_tel') ? set_value('client_tel') : NULL,
                    'postcode' => set_value('postcode') ? set_value('postcode') : NULL,
                    'meta_title' => set_value('meta_title') ? set_value('meta_title') : NULL,
                    'meta_keywords' => set_value('meta_keywords') ? set_value('meta_keywords') : NULL,
                    'meta_desc' => set_value('meta_desc') ? set_value('meta_desc') : NULL,
                    'canonical' => set_value('canonical') ? set_value('canonical') : NULL,
                    'slug' => $slug,
                    'deleted' => FALSE,
                    'date' => date('Y-m-d H:i:s'),
					'allow_on_portal' => (bool)(set_value('allow_on_portal') == '1'),
                );

                if ($this->vacancies_model->update_row(array('vacancy_id' => $vacancy_id), $data)) {

                    $this->vacancies_model->sectors_delete(array('vacancy_id' => $vacancy_id));
                    if (is_array(set_value('sector_ids[]')) && count(set_value('sector_ids[]')) > 0) {
                        $data = array();
                        foreach (set_value('sector_ids[]') as $sector_id) {
                            $data[] = array(
                                'vacancy_id' => $vacancy_id,
                                'industry_sector_id' => $sector_id
                            );
                        }
                        $this->vacancies_model->sectors_add($data);
                    }

                    $this->vacancies_model->locations_delete(array('vacancy_id' => $vacancy_id));
                    if (is_array(set_value('location_ids[]')) && count(set_value('location_ids[]')) > 0) {
                        $data = array();
                        foreach (set_value('location_ids[]') as $location_id) {
                            $data[] = array(
                                'vacancy_id' => $vacancy_id,
                                'location_id' => $location_id
                            );
                        }
                        $this->vacancies_model->locations_add($data);
                    }

                    $this->session->set_flashdata('success', 'Vacancy updated successfully.');
                    redirect('vacancies/edit/' . $vacancy_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->view_data['company_name'] = $this->config->item('company_name');
        $this->load->model('industry_sectors_model');
        $this->view_data['industry_sectors'] = $this->industry_sectors_model->get();
        $this->load->model('locations_model');
        $this->view_data['locations'] = $this->locations_model->get();
        $this->load->model('team_model');
        $this->view_data['team'] = $this->team_model->get(array(
            'deleted' => FALSE
        ));

        $this->load->view('header', $this->view_data);
        $this->load->view('vacancies/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($vacancy_id)
    {
        if (!$vacancy_id) redirect('vacancies');

        $this->view_data['vacancy'] = $this->vacancies_model->get_row(
            array(
                'vacancy_id' => $vacancy_id
            )
        );
        if (!$this->view_data['vacancy']) redirect('vacancies');


        if ($this->vacancies_model->update_row(array('vacancy_id' => $vacancy_id), array(
            'deleted' => TRUE
        ))) {
            $this->session->set_flashdata('success', 'Vacancy archived successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('vacancies');
    }

    public function expire($vacancy_id)
    {
        if (!$vacancy_id) redirect('vacancies');

        $this->view_data['vacancy'] = $this->vacancies_model->get_row(
            array(
                'vacancy_id' => $vacancy_id
            )
        );
        if (!$this->view_data['vacancy']) redirect('vacancies');

        $this->view_data['title'] = 'Expire Vacancy';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('expiry_reason', 'Reason', 'required|trim|min_length[1]|max_length[200]');

            if ($this->form_validation->run()) {

                $data = array(
                    'deleted' => TRUE,
                    'expiry_reason' => set_value('expiry_reason')
                );

                if ($this->vacancies_model->update_row(array('vacancy_id' => $vacancy_id), $data)) {
                    $this->session->set_flashdata('success', 'Vacancy expired successfully.');
                    redirect('vacancies');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('vacancies/expire', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function view($vacancy_id)
    {
        if (!$vacancy_id) redirect('vacancies');

        $this->view_data['vacancy'] = $this->vacancies_model->get_row(
            array(
                'vacancy_id' => $vacancy_id
            )
        );
        if (!$this->view_data['vacancy']) redirect('vacancies');

        $this->view_data['title'] = 'Vacancies Manager';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->model('library_model');
        $this->view_data['library'] = $this->library_model->get(array(
            'vacancy_id' => $this->view_data['vacancy']->vacancy_id
        ));

        $this->load->view('header', $this->view_data);
        $this->load->view('vacancies/view', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }
}
