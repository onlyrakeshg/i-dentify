<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends BOLD_Controller
{
    private $plugin = 'blog';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('blog_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->blog_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->blog_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index($show = NULL)
    {
        $this->view_data['title'] = 'Blog';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['blog'] = $this->blog_model->get(array(
            'deleted' => (bool)($show === 'old')
        ));

        $this->load->view('header', $this->view_data);
        $this->load->view('blog/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add()
    {
        $this->view_data['title'] = 'Add Blog Post';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');

        if ($this->input->post()) {

            $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('content', 'Content', 'required|trim|min_length[1]');
            $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|min_length[0]|max_length[250]');
            $this->form_validation->set_rules('meta_desc', 'Meta Description', 'trim|min_length[0]|max_length[255]');
            $this->form_validation->set_rules('canonical', 'Canonical', 'trim|min_length[1]');
            $this->form_validation->set_rules('slug', 'Slug', 'trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'title' => set_value('title'),
                    'content' => set_value('content', '', FALSE),
                    'meta_title' => set_value('meta_title') ? set_value('meta_title') : NULL,
                    'meta_keywords' => set_value('meta_keywords') ? set_value('meta_keywords') : NULL,
                    'meta_desc' => set_value('meta_desc') ? set_value('meta_desc') : NULL,
                    'canonical' => set_value('canonical') ? set_value('canonical') : NULL,
                    'date' => date('Y-m-d H:i:s'),
                    'deleted' => FALSE,
                    'slug' => set_value('slug') ? set_value('slug') : NULL
                );

                $this->upload->initialize(array(
                    'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size' => 10240,
                    'max_width' => 1920,
                    'max_height' => 1080,
                    'encrypt_name' => TRUE
                ));

                if ($this->upload->do_upload('image')) {
                    chmod($this->upload->data('full_path'), 0777);
                    $data['image'] = $this->upload->data('file_name');
                } else {
                    $this->view_data['error'] = $this->upload->display_errors('', '');
                    $this->view_data['image'] = NULL;
                }

                if ($this->blog_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'Blog entry created successfully.');
                    redirect('blog');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->view_data['company_name'] = $this->config->item('company_name');

        $this->load->view('header', $this->view_data);
        $this->load->view('blog/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($blog_id)
    {
        if (!$blog_id) redirect('blog');

        $this->view_data['blog'] = $this->blog_model->get_row(
            array(
                'blog_id' => $blog_id
            )
        );
        if (!$this->view_data['blog']) redirect('blog');

        $this->view_data['title'] = 'Edit Blog Post';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');

        if ($this->input->post()) {

            $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('content', 'Content', 'required|trim|min_length[1]');
            $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|min_length[0]|max_length[250]');
            $this->form_validation->set_rules('meta_desc', 'Meta Description', 'trim|min_length[0]|max_length[255]');
            $this->form_validation->set_rules('canonical', 'Canonical', 'trim|min_length[1]');
            $this->form_validation->set_rules('slug', 'Slug', 'trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'title' => set_value('title'),
                    'content' => set_value('content', '', FALSE),
                    'meta_title' => set_value('meta_title') ? set_value('meta_title') : NULL,
                    'meta_keywords' => set_value('meta_keywords') ? set_value('meta_keywords') : NULL,
                    'meta_desc' => set_value('meta_desc') ? set_value('meta_desc') : NULL,
                    'canonical' => set_value('canonical') ? set_value('canonical') : NULL,
                    'date' => date('Y-m-d H:i:s'),
                    'slug' => set_value('slug') ? set_value('slug') : NULL
                );

                $this->upload->initialize(array(
                    'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size' => 10240,
                    'max_width' => 1920,
                    'max_height' => 1080,
                    'encrypt_name' => TRUE
                ));

                if ($this->upload->do_upload('image')) {

                    if ($this->view_data['blog']->image)
                        unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['blog']->image);

                    chmod($this->upload->data('full_path'), 0777);
                    $data['image'] = $this->upload->data('file_name');
                } else {
                    $this->view_data['error'] = $this->upload->display_errors('', '');
                }

                if ($this->blog_model->update_row(array('blog_id' => $blog_id), $data)) {
                    $this->session->set_flashdata('success', 'Blog entry updated successfully.');
                    redirect('blog/edit/' . $blog_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->view_data['company_name'] = $this->config->item('company_name');

        $this->load->view('header', $this->view_data);
        $this->load->view('blog/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($blog_id)
    {
        if (!$blog_id) redirect('blog');

        $this->view_data['blog'] = $this->blog_model->get_row(
            array(
                'blog_id' => $blog_id
            )
        );
        if (!$this->view_data['blog']) redirect('blog');

        /* Remove image
        if ($this->view_data['blog']->image)
            unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['blog']->image);
        */

        if ($this->blog_model->update_row(array('blog_id' => $blog_id), array(
            'deleted' => TRUE
        ))) {
            $this->session->set_flashdata('success', 'Blog entry deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('blog');
    }
}
