<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analytics extends BOLD_Controller
{
    private $plugin = 'analytics';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('analytics_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->analytics_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->analytics_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        if (!file_exists(APPPATH . 'third_party/google-api-php-client')) {
            $this->session->set_flashdata('error', 'Analytics module will not work without API library - https://github.com/googleapis/google-api-php-client');
            redirect('dashboard');
        }

        if (!file_exists(APPPATH . 'third_party/google-api-php-client/vendor/autoload.php')) {
            $this->session->set_flashdata('error', 'Analytics module will not work without composer dependencies - run "composer install" in bash(ssh) inside API Library folder (inside third_party folder)');
            redirect('dashboard');
        }

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index()
    {
        $this->view_data['title'] = 'Google Analytics';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->view('header', $this->view_data);
        $this->load->view('analytics/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function ajax($source = NULL)
    {
        $response = array();

        $this->load->model('settings_model');
        $credentials_json = $this->settings_model->get_row(array('name' => 'analytics_credentials_json'));
        if (!$credentials_json || !isset($credentials_json->value) || empty($credentials_json->value)) {
            $response['error'] = 'Analytics module will not work without login credentials - to get credentials use instruction here - https://developers.google.com/analytics/devguides/reporting/core/v4/quickstart/service-php and save at Configure page.';
        } else {
            $credentials = json_decode($credentials_json->value, TRUE);
            if (!$credentials) {
                $response['error'] = 'Analytics module will not work without CORRECT login credentials -  to get credentials use instruction here - https://developers.google.com/analytics/devguides/reporting/core/v4/quickstart/service-php and save at Configure page.';
            } else {
                $view_id = $this->settings_model->get_row(array('name' => 'analytics_view_id'));

                if (!$view_id || !isset($view_id) || empty($view_id)) {
                    $response['error'] = 'Analytics module will not work without View ID - get it at Developers Console and save at Configure page.';
                } else {

                    try {
                        include APPPATH . 'third_party/google-api-php-client/vendor/autoload.php';

                        $client = new Google_Client();
                        $client->setApplicationName("Hello Analytics Reporting");
                        $client->setAuthConfig($credentials);
                        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
                        $analytics = new Google_Service_AnalyticsReporting($client);

                        $metrics = array();
                        $dimensions = array();
                        $orderBys = array();

                        switch ($source) {
                            case 'top':
                                $pageviews = new Google_Service_AnalyticsReporting_Metric();
                                $pageviews->setExpression("ga:pageviews");
                                $pageviews->setAlias("pageviews");
                                $metrics[] = $pageviews;

                                $pagePath = new Google_Service_AnalyticsReporting_Dimension();
                                $pagePath->setName("ga:pagePath");
                                $dimensions[] = $pagePath;

                                $order_pageviews = new Google_Service_AnalyticsReporting_OrderBy();
                                $order_pageviews->setFieldName('ga:pageviews');
                                $order_pageviews->setSortOrder('DESCENDING');
                                $orderBys[] = $order_pageviews;
                                break;
                            case 'sources':
                                $sources = new Google_Service_AnalyticsReporting_Dimension();
                                $sources->setName("ga:source");
                                $dimensions[] = $sources;
                                break;
                            case 'country':
                                $country = new Google_Service_AnalyticsReporting_Dimension();
                                $country->setName("ga:country");
                                $dimensions[] = $country;
                                break;
                            case 'devices':
                                $deviceCategory = new Google_Service_AnalyticsReporting_Dimension();
                                $deviceCategory->setName("ga:deviceCategory");
                                $dimensions[] = $deviceCategory;
                                break;
                            case 'sessions':
                                $sessions = new Google_Service_AnalyticsReporting_Metric();
                                $sessions->setExpression("ga:sessions");
                                $sessions->setAlias("sessions");
                                $metrics[] = $sessions;

                                $date = new Google_Service_AnalyticsReporting_Dimension();
                                $date->setName("ga:date");
                                $dimensions[] = $date;
                                break;
                            case 'pageviews':
                                $pageviews = new Google_Service_AnalyticsReporting_Metric();
                                $pageviews->setExpression("ga:pageviews");
                                $pageviews->setAlias("pageviews");
                                $metrics[] = $pageviews;

                                $date = new Google_Service_AnalyticsReporting_Dimension();
                                $date->setName("ga:date");
                                $dimensions[] = $date;
                                break;
                            case 'new_users':
                                $users = new Google_Service_AnalyticsReporting_Metric();
                                $users->setExpression("ga:newUsers");
                                $users->setAlias("new_users");
                                $metrics[] = $users;

                                $users = new Google_Service_AnalyticsReporting_Metric();
                                $users->setExpression("ga:users");
                                $users->setAlias("users");
                                $metrics[] = $users;

                                $date = new Google_Service_AnalyticsReporting_Dimension();
                                $date->setName("ga:date");
                                $dimensions[] = $date;

                                $userType = new Google_Service_AnalyticsReporting_Dimension();
                                $userType->setName("ga:userType");
                                $dimensions[] = $userType;
                                break;
                            case 'users':
                            default:
                                $users = new Google_Service_AnalyticsReporting_Metric();
                                $users->setExpression("ga:users");
                                $users->setAlias("users");
                                $metrics[] = $users;

                                $date = new Google_Service_AnalyticsReporting_Dimension();
                                $date->setName("ga:date");
                                $dimensions[] = $date;
                        }

                        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
                        $dateRange->setStartDate("30daysAgo");
                        $dateRange->setEndDate("today");

                        $request = new Google_Service_AnalyticsReporting_ReportRequest();
                        $request->setViewId((string)$view_id->value);
                        $request->setDateRanges($dateRange);
                        $request->setMetrics($metrics);
                        $request->setDimensions($dimensions);
                        $request->setOrderBys($orderBys);

                        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
                        $body->setReportRequests(array($request));
                        $reports = $analytics->reports->batchGet($body);

                        switch ($source) {
                            case 'top':
                                for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
                                    $report = $reports[$reportIndex];
                                    $header = $report->getColumnHeader();
                                    $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
                                    $rows = $report->getData()->getRows();

                                    for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                                        $row = $rows[$rowIndex];
                                        $dimensions = $row->getDimensions();
                                        $metrics = $row->getMetrics();
                                        $date_values = array();
                                        $date_values['path'] = $dimensions[0];
                                        if ($metrics) {
                                            for ($j = 0; $j < count($metrics); $j++) {
                                                $values = $metrics[$j]->getValues();
                                                for ($k = 0; $k < count($values); $k++) {
                                                    $entry = $metricHeaders[$k];
                                                    $date_values[$entry->getName()] = (int)$values[$k];
                                                }
                                            }
                                        }
                                        $response[] = $date_values;
                                    }
                                }
                                break;
                            case 'sources':
                            case 'country':
                            case 'devices':
                                for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
                                    $report = $reports[$reportIndex];
                                    $header = $report->getColumnHeader();
                                    $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
                                    $rows = $report->getData()->getRows();

                                    for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                                        $row = $rows[$rowIndex];
                                        $dimensions = $row->getDimensions();
                                        $metrics = $row->getMetrics();
                                        $date_values = array();
                                        $date_values['base'] = $dimensions[0];
                                        if ($metrics) {
                                            for ($j = 0; $j < count($metrics); $j++) {
                                                $values = $metrics[$j]->getValues();
                                                for ($k = 0; $k < count($values); $k++) {
                                                    $entry = $metricHeaders[$k];
                                                    $date_values[$entry->getName()] = (int)$values[$k];
                                                }
                                            }
                                        }
                                        $response[] = $date_values;
                                    }
                                }
                                break;
                            case 'new_users':
                                for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
                                    $report = $reports[$reportIndex];
                                    $header = $report->getColumnHeader();
                                    $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
                                    $rows = $report->getData()->getRows();

                                    for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                                        $row = $rows[$rowIndex];
                                        $dimensions = $row->getDimensions();
                                        $date = $dimensions[0];
                                        $userType = $dimensions[1];
                                        $metrics = $row->getMetrics();
                                        $date_values = array();
                                        $date_values['date'] = $date;
                                        $date_values['user_type'] = $userType;
                                        if ($metrics) {
                                            for ($j = 0; $j < count($metrics); $j++) {
                                                $values = $metrics[$j]->getValues();
                                                for ($k = 0; $k < count($values); $k++) {
                                                    $entry = $metricHeaders[$k];
                                                    $date_values[$entry->getName()] = (int)$values[$k];
                                                }
                                            }
                                        }
                                        $response[] = $date_values;
                                    }
                                }
                                break;
                            case 'sessions':
                            case 'pageviews':
                            case 'users':
                            default:
                                for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
                                    $report = $reports[$reportIndex];
                                    $header = $report->getColumnHeader();
                                    $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
                                    $rows = $report->getData()->getRows();

                                    for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                                        $row = $rows[$rowIndex];
                                        $dimensions = $row->getDimensions();
                                        $date = $dimensions[0];
                                        $metrics = $row->getMetrics();
                                        $date_values = array();
                                        $date_values['date'] = $date;
                                        if ($metrics) {
                                            for ($j = 0; $j < count($metrics); $j++) {
                                                $values = $metrics[$j]->getValues();
                                                for ($k = 0; $k < count($values); $k++) {
                                                    $entry = $metricHeaders[$k];
                                                    $date_values[$entry->getName()] = (int)$values[$k];
                                                }
                                            }
                                        }
                                        $response[] = $date_values;
                                    }
                                }
                        }
                    } catch (Exception $e) {
                        $response['error'] = $e->getMessage();
                    }
                }
            }
        }

        header("Content-type:application/json");
        echo json_encode($response);
        exit;
    }

    public function config()
    {
        $this->view_data['title'] = 'Analytics Config';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->model('settings_model');
        $this->load->library('form_validation');
        if ($this->input->post()) {

            $this->form_validation->set_rules('view_id', 'View ID', 'required|trim|intval|greater_than[0]');
            $this->form_validation->set_rules('credentials_json', 'Credentials JSON', 'required|trim|min_length[1]');
            $this->form_validation->set_rules('include_code', 'Include Code', 'required|trim|min_length[1]');

            if ($this->form_validation->run()) {
                if ($this->settings_model->get_count(array('name' => 'analytics_view_id')) === 0) {
                    $this->settings_model->add_row(array(
                        'name' => 'analytics_view_id',
                        'title' => 'Google Analytics View ID',
                        'value' => set_value('view_id')
                    ));
                } else {
                    $this->settings_model->update_row(array(
                        'name' => 'analytics_view_id'
                    ), array(
                        'title' => 'Google Analytics View ID',
                        'value' => set_value('view_id')
                    ));
                }

                if ($this->settings_model->get_count(array('name' => 'analytics_credentials_json')) === 0) {
                    $this->settings_model->add_row(array(
                        'name' => 'analytics_credentials_json',
                        'title' => 'Google Analytics Credentials JSON',
                        'value' => set_value('credentials_json', '', FALSE)
                    ));
                } else {
                    $this->settings_model->update_row(array(
                        'name' => 'analytics_credentials_json'
                    ), array(
                        'title' => 'Google Analytics Credentials JSON',
                        'value' => set_value('credentials_json', '', FALSE)
                    ));
                }

                if ($this->settings_model->get_count(array('name' => 'analytics_include_code')) === 0) {
                    $this->settings_model->add_row(array(
                        'name' => 'analytics_include_code',
                        'title' => 'Google Analytics Include Code',
                        'value' => set_value('include_code', '', FALSE)
                    ));
                } else {
                    $this->settings_model->update_row(array(
                        'name' => 'analytics_include_code'
                    ), array(
                        'title' => 'Google Analytics Include Code',
                        'value' => set_value('include_code', '', FALSE)
                    ));
                }
                $this->session->set_flashdata('success', 'Configuration updated.');
                redirect('analytics/config');
            }
        }

        $this->view_data['credentials_json'] = NULL;
        $credentials_json = $this->settings_model->get_row(array('name' => 'analytics_credentials_json'));
        if ($credentials_json && isset($credentials_json->value))
            $this->view_data['credentials_json'] = $credentials_json->value;

        $this->view_data['include_code'] = NULL;
        $include_code = $this->settings_model->get_row(array('name' => 'analytics_include_code'));
        if ($include_code && isset($include_code->value))
            $this->view_data['include_code'] = $include_code->value;

        $this->view_data['view_id'] = NULL;
        $view_id = $this->settings_model->get_row(array('name' => 'analytics_view_id'));
        if ($view_id && isset($view_id->value))
            $this->view_data['view_id'] = $view_id->value;

        $this->load->view('header', $this->view_data);
        $this->load->view('analytics/config', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

}