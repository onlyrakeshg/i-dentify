<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonials extends BOLD_Controller
{
    private $plugin = 'testimonials';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('testimonials_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->testimonials_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->testimonials_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index()
    {
        $this->view_data['title'] = 'Testimonials';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['testimonials'] = $this->testimonials_model->get();

        $this->load->view('header', $this->view_data);
        $this->load->view('testimonials/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add()
    {
        $this->view_data['title'] = 'Add Testimonial';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('author', 'Author', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('position', 'Position', 'required|trim|min_length[1]|max_length[150]');
            $this->form_validation->set_rules('text', 'Text', 'required|trim|min_length[1]');
            $this->form_validation->set_rules('date', 'Date', 'required|trim|min_length[1]|max_length[150]');

            if ($this->form_validation->run()) {
                $data = array(
                    'name' => set_value('name'),
                    'position' => set_value('position'),
                    'text' => set_value('text', '', FALSE),
                    'date' => set_value('date'),
                );

                if ($this->testimonials_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'Testimonial created successfully.');
                    redirect('testimonials');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('testimonials/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($testimonial_id)
    {
        if (!$testimonial_id) redirect('testimonials');

        $this->view_data['testimonial'] = $this->testimonials_model->get_row(
            array(
                'testimonial_id' => $testimonial_id
            )
        );
        if (!$this->view_data['testimonial']) redirect('testimonials');

        $this->view_data['title'] = 'Edit Testimonial';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('author', 'Author', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('position', 'Position', 'required|trim|min_length[1]|max_length[150]');
            $this->form_validation->set_rules('text', 'Text', 'required|trim|min_length[1]');
            $this->form_validation->set_rules('date', 'Date', 'required|trim|min_length[1]|max_length[150]');

            if ($this->form_validation->run()) {
                $data = array(
                    'name' => set_value('name'),
                    'position' => set_value('position'),
                    'text' => set_value('text', '', FALSE),
                    'date' => set_value('date'),
                );

                if ($this->testimonials_model->update_row(array('testimonial_id' => $testimonial_id), $data)) {
                    $this->session->set_flashdata('success', 'Testimonial updated successfully.');
                    redirect('testimonials/edit/' . $testimonial_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('testimonials/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($testimonial_id)
    {
        if (!$testimonial_id) redirect('testimonials');

        $this->view_data['testimonial'] = $this->testimonials_model->get_row(
            array(
                'testimonial_id' => $testimonial_id
            )
        );
        if (!$this->view_data['testimonial']) redirect('testimonials');

        if ($this->testimonials_model->delete_row(array('testimonial_id' => $testimonial_id))) {
            $this->session->set_flashdata('success', 'Testimonial deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('testimonials');
    }
}
