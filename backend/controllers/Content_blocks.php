<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content_blocks extends BOLD_Controller
{
    private $plugin = 'content_blocks';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('content_blocks_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->content_blocks_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->content_blocks_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index($show = NULL)
    {
        $this->view_data['title'] = 'Content Blocks';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['show'] = $show;

        $this->view_data['content_blocks'] = $this->content_blocks_model->get(array(
            'deleted' => (bool)($show === 'old')
        ));

        $this->load->view('header', $this->view_data);
        $this->load->view('content_blocks/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add()
    {
        $this->view_data['title'] = 'Add Content Block';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('identifier', 'Identifier', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('content', 'Content', 'required|trim|min_length[1]');

            if ($this->form_validation->run()) {
                $data = array(
                    'identifier' => set_value('identifier'),
                    'content' => set_value('content', '', FALSE),
                    'date' => date('Y-m-d H:i:s'),
                    'deleted' => FALSE
                );

                if ($this->content_blocks_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'Content block created successfully.');
                    redirect('content_blocks');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('content_blocks/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($block_id)
    {
        if (!$block_id) redirect('content_blocks');

        $this->view_data['content_block'] = $this->content_blocks_model->get_row(
            array(
                'block_id' => $block_id
            )
        );
        if (!$this->view_data['content_block']) redirect('content_blocks');

        $this->view_data['title'] = 'Edit Content Block';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');

        if ($this->input->post()) {

            $this->form_validation->set_rules('identifier', 'Identifier', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('content', 'Content', 'required|trim|min_length[1]');

            if ($this->form_validation->run()) {
                $data = array(
                    'identifier' => set_value('identifier'),
                    'content' => set_value('content', '', FALSE)
                );

                if ($this->content_blocks_model->update_row(array('block_id' => $block_id), $data)) {
                    $this->session->set_flashdata('success', 'Content block updated successfully.');
                    redirect('content_blocks/edit/' . $block_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('content_blocks/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($block_id)
    {
        if (!$block_id) redirect('content_blocks');

        $this->view_data['content_block'] = $this->content_blocks_model->get_row(
            array(
                'block_id' => $block_id
            )
        );
        if (!$this->view_data['content_block']) redirect('content_blocks');

        if ($this->content_blocks_model->update_row(array('block_id' => $block_id), array(
            'deleted' => TRUE
        ))) {
            $this->session->set_flashdata('success', 'Content block deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('content_blocks');
    }
}
