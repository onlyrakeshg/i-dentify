<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends BOLD_Controller
{
    private $plugin = 'settings';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        $this->load->model('settings_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->settings_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->settings_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index()
    {
        $this->view_data['title'] = 'Settings';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['settings'] = $this->settings_model->get();

        $this->load->view('header', $this->view_data);
        $this->load->view('settings/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($name)
    {
        if (!$name) redirect('settings');

        $this->view_data['setting'] = $this->settings_model->get_row(
            array(
                'name' => $name
            )
        );
        if (!$this->view_data['setting']) redirect('settings');

        $this->view_data['title'] = 'Edit Setting';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('value', 'Value', 'required|trim|min_length[1]');

            if ($this->form_validation->run()) {
                $data = array(
                    'value' => set_value('value', '', FALSE),
                );

                if ($this->settings_model->update_row(array('name' => $name), $data)) {
                    $this->session->set_flashdata('success', 'Settings entry updated successfully.');
                    redirect('settings/edit/' . $name);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('settings/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }
}
