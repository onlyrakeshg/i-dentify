<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Candidates_portal extends BOLD_Controller
{
    private $plugin = 'candidates_portal';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('candidates_portal_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->candidates_portal_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->candidates_portal_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index($show = NULL)
    {
        $this->view_data['title'] = 'Candidates Portal';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['candidates'] = $this->candidates_portal_model->get(array(
            'deleted' => (bool)($show === 'old')
        ));

        $this->load->view('header', $this->view_data);
        $this->load->view('candidates_portal/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add()
    {
        $this->view_data['title'] = 'Add Candidate';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');

        if ($this->input->post()) {
            $this->form_validation->set_rules('firstname', 'Firstname', 'required|trim|min_length[1]|max_length[150]');
            $this->form_validation->set_rules('lastname', 'Lastname', 'required|trim|min_length[1]|max_length[150]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|min_length[1]|max_length[150]|valid_email|is_unique[candidates_portal.email]');
            $this->form_validation->set_rules('tel', 'Telephone Number', 'trim|min_length[0]|max_length[50]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('title', 'Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('content', 'Content', 'trim|min_length[0]');

            if ($this->form_validation->run()) {
                $data = array(
                    'firstname' => set_value('firstname'),
                    'lastname' => set_value('lastname'),
                    'email' => set_value('email'),
                    'tel' => set_value('tel') ? set_value('tel') : NULL,
                    'title' => set_value('title') ? set_value('title') : NULL,
                    'password' => '*' . strtoupper(hash('sha1', pack('H*', hash('sha1', set_value('password'))))),
                    'content' => set_value('content', '', FALSE),
                    'date' => date('Y-m-d H:i:s'),
                    'deleted' => FALSE,
                );

                $upload_errors = array();

                if (isset($_FILES['photo']) && $_FILES['photo']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png',
                        'max_size' => 10240,
                        'max_width' => 19200,
                        'max_height' => 10800,
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('photo')) {
                        chmod($this->upload->data('full_path'), 0777);
                        $data['photo'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Photo: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['cv']) && $_FILES['cv']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'cvs' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'doc|docx|pdf|xls|xlsx|html|htm|rtf',
                        'max_size' => 10240,
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('cv')) {
                        chmod($this->upload->data('full_path'), 0777);
                        $data['cv'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "CV: " . $this->upload->display_errors('', '');
                    }
                }

                if (count($upload_errors) > 0)
                    $this->session->set_flashdata('error', implode("<br/>", $upload_errors));

                if ($this->candidates_portal_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'Candidate created successfully.');
                    redirect('candidates_portal');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->view_data['consultants'] = $this->team_model->get(array(
            'deleted' => FALSE
        ));

        $this->load->view('header', $this->view_data);
        $this->load->view('candidates_portal/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($candidate_id)
    {
        if (!$candidate_id) redirect('candidates_portal');

        $this->view_data['candidate'] = $this->candidates_portal_model->get_row(
            array(
                'candidate_id' => $candidate_id
            )
        );
        if (!$this->view_data['candidate']) redirect('candidates_portal');

        $this->view_data['title'] = 'Edit Candidate';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');

        if ($this->input->post()) {
            $this->form_validation->set_rules('firstname', 'Firstname', 'required|trim|min_length[1]|max_length[150]');
            $this->form_validation->set_rules('lastname', 'Lastname', 'required|trim|min_length[1]|max_length[150]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|min_length[1]|max_length[150]|valid_email');
            $this->form_validation->set_rules('tel', 'Telephone Number', 'trim|min_length[0]|max_length[50]');
            $this->form_validation->set_rules('password', 'Password', 'trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('title', 'Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('content', 'Content', 'trim|min_length[0]');

            if ($this->form_validation->run()) {
                $data = array(
                    'firstname' => set_value('firstname'),
                    'lastname' => set_value('lastname'),
                    'email' => set_value('email'),
                    'tel' => set_value('tel') ? set_value('tel') : NULL,
                    'title' => set_value('title') ? set_value('title') : NULL,
                    'content' => set_value('content', '', FALSE),
                );

                if (set_value('password'))
                    $data['password'] = '*' . strtoupper(hash('sha1', pack('H*', hash('sha1', set_value('password')))));

                $upload_errors = array();

                if (isset($_FILES['photo']) && $_FILES['photo']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png',
                        'max_size' => 10240,
                        'max_width' => 19200,
                        'max_height' => 10800,
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('photo')) {
                        if ($this->view_data['candidate']->photo)
                            unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['candidate']->photo);

                        chmod($this->upload->data('full_path'), 0777);
                        $data['photo'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Photo: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['cv']) && $_FILES['cv']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'cvs' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'doc|docx|pdf|xls|xlsx|html|htm|rtf',
                        'max_size' => 10240,
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('cv')) {
                        if ($this->view_data['candidate']->cv)
                            unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'cvs' . DIRECTORY_SEPARATOR . $this->view_data['candidate']->cv);

                        chmod($this->upload->data('full_path'), 0777);
                        $data['cv'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "CV: " . $this->upload->display_errors('', '');
                    }
                }

                if (count($upload_errors) > 0)
                    $this->session->set_flashdata('error', implode("<br/>", $upload_errors));


                if ($this->candidates_portal_model->update_row(array('candidate_id' => $candidate_id), $data)) {
                    $this->session->set_flashdata('success', 'Candidate updated successfully.');
                    redirect('candidates_portal/edit/' . $candidate_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->view_data['consultants'] = $this->team_model->get(array(
            'deleted' => FALSE
        ));

        $this->load->view('header', $this->view_data);
        $this->load->view('candidates_portal/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($candidate_id)
    {
        if (!$candidate_id) redirect('candidates_portal');

        $this->view_data['candidates_portal'] = $this->candidates_portal_model->get_row(
            array(
                'candidate_id' => $candidate_id
            )
        );
        if (!$this->view_data['candidates_portal']) redirect('candidates_portal');

        // Remove image
        if ($this->view_data['candidates_portal']->photo)
            unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['candidates_portal']->photo);

        if ($this->candidates_portal_model->delete_row(array('candidate_id' => $candidate_id))) {
            $this->session->set_flashdata('success', 'Candidate deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('candidates_portal');
    }
}
