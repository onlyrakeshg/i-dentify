<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bullhorn extends BOLD_Controller
{
    private $log_messages = array();

    public function __construct()
    {
        parent::__construct();

        $this->load->config('bullhorn');
        $this->load->model('bullhorn_model');

        $this->load->library('bullhorn_api', array(
            "client_id" => $this->config->item('bullhorn_client_id'),
            "client_secret" => $this->config->item('bullhorn_client_secret'),
            "redirect_uri" => site_url('bullhorn/auth')
        ));
    }

    public function auth()
    {
        if (!$this->input->get('code')) {
            redirect($this->bullhorn_api->get_oauth_link());
        } elseif ($this->input->get('code')) {
            $token = $this->bullhorn_api->get_access_token($this->input->get('code'));
            $http_code = $this->bullhorn_api->get_last_http_code();

            if ($http_code === 200) {

                $data = array(
                    'access_token' => $token->access_token,
                    'refresh_token' => $token->refresh_token,
                    'expires' => $token->expires_in,
                    'created' => time()
                );
                if ($this->bullhorn_model->add_row($data)) {
                    redirect('bullhorn/sync');
                }
            }
        }
    }

    public function sync()
    {
        $this->log("Starting data synchronization...");

        $this->load->model('vacancies_model');

        $this->load->model('candidates_portal_model');
        $candidates = $this->candidates_portal_model->get(array(
            'bh_candidate_id' => NULL,
            'bh_notes LIKE' => '%Integration access revoked%'
        ));

        $this->load->model('library_model');
        $applications = $this->library_model->get(array(
            'application_id' => NULL,
            'bh_notes LIKE' => '%Integration access revoked%'
        ));

        if (($candidates && count($candidates) > 0) || ($applications && count($applications) > 0)) {
            $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
            if ($access_token) {
                $this->log("Token loaded, expires in " . (((int)$access_token->created + (int)$access_token->expires) - time()) . " seconds");
                if (((int)$access_token->created + (int)$access_token->expires) < (int)time()) {
                    $this->log("Token expired");
                    $this->refresh_token($access_token);
                    $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
                }

                $bh_token = $this->bullhorn_api->get_login($access_token->access_token);
                if (isset($bh_token->errorCode) && $bh_token->errorCode == 400) {
                    $this->log($bh_token->errorMessage);
                    $this->refresh_token($access_token);
                    $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
                    $bh_token = $this->bullhorn_api->get_login($access_token->access_token);
                }

                if (isset($bh_token->BhRestToken) && isset($bh_token->restUrl)) {
                    if ($candidates && count($candidates) > 0) {
                        $this->log("Found " . count($candidates) . ' candidates with failed synchronization');

                        foreach ($candidates as $candidate) {
                            $bh_candidate_id = NULL;
                            $file_id = NULL;
                            $search_candidate = $this->bullhorn_api->get_json($bh_token->restUrl . "search/Candidate" . "?" . http_build_query(array("query" => "email:" . $candidate->email, "fields" => "id")), NULL, array("BHRestToken: " . $bh_token->BhRestToken));
                            if ($search_candidate && isset($search_candidate->total) && $search_candidate->total > 0) {
                                $candidates = array_values($search_candidate->data);
                                $bh_candidate_id = $candidates[0]->id;
                            } else {
                                $bh_data = array(
                                    "firstName" => $candidate->firstname, // Field name
                                    "lastName" => $candidate->lastname, // Surname
                                    "name" => $candidate->firstname . " " . $candidate->lastname,
                                    "email" => $candidate->email, // Personal Email
                                    "mobile" => $candidate->tel, // Mobile Phone
                                    "source" => "Website",
                                    "status" => "Available",
                                    "isDeleted" => FALSE,
                                );

                                $bh_candidate = $this->bullhorn_api->get_json($bh_token->restUrl . "entity/Candidate", json_encode($bh_data), array("Content-Type: application/json", "BHRestToken: " . $bh_token->BhRestToken), "PUT"); // Create candidate for each submission
                                if ($bh_candidate && $bh_candidate->changedEntityId) {
                                    $bh_candidate_id = $bh_candidate->changedEntityId;
                                }
                            }

                            if ($bh_candidate_id !== NULL) {
                                if ($this->candidates_portal_model->update_row(array(
                                    'candidate_id' => $candidate->candidate_id
                                ), array(
                                    'bh_candidate_id' => $bh_candidate_id,
                                    'bh_notes' => NULL
                                ))) {
                                    $this->log("Candidate #" . $candidate->candidate_id . ' profile uploaded.');
                                } else {
                                    $this->log("Candidate #" . $candidate->candidate_id . ' profile upload FAILED.');
                                }
                            }
                        }
                    }

                    if ($applications && count($applications) > 0) {
                        $this->log("Found " . count($applications) . ' applications with failed synchronization');

                        foreach ($applications as $application) {
                            $bh_candidate_id = NULL;
                            $application_id = NULL;

                            if ($application->bh_candidate_id) {
                                $bh_candidate_id = $application->bh_candidate_id;
                            } else {
                                if ($application->candidate_id) {
                                    $candidate = $this->candidates_portal_model->get_row(array(
                                        'candidate_id' => $application->candidate_id
                                    ));
                                    if ($candidate && $candidate->bh_candidate_id) {

                                        $bh_candidate_id = $candidate->bh_candidate_id;
                                        $get_candidate = $this->bullhorn_api->get_json($bh_token->restUrl . "entity/Candidate/" . $bh_candidate_id . "?fields=id,email", NULL, array("BHRestToken: " . $bh_token->BhRestToken));

                                        if ($get_candidate && isset($get_candidate->data) && isset($get_candidate->data->id) && $get_candidate->data->id) {
                                            $bh_candidate_id = $get_candidate->data->id;
                                        } else {
                                            $bh_candidate_id = NULL;
                                        }
                                    }
                                }

                                if (!$bh_candidate_id) {
                                    $bh_data = array(
                                        "name" => $application->name,
                                        "email" => $application->email, // Personal Email
                                        "mobile" => $application->tel, // Mobile Phone
                                        "source" => "Website",
                                        "status" => "Available",
                                        "isDeleted" => FALSE,
                                    );

                                    $name_parts = explode(" ", $application->name);
                                    if ($name_parts && is_array($name_parts) && count($name_parts) == 2) {
                                        list($firstname, $lastname) = $name_parts;
                                        $bh_data['firstName'] = $firstname;
                                        $bh_data['lastName'] = $lastname;
                                    }

                                    if ($application->filename) {
                                        $cv_path = FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'cvs' . DIRECTORY_SEPARATOR . $application->filename;
                                        if (file_exists($cv_path)) {
                                            $path_parts = pathinfo($cv_path);

                                            switch (strtolower($path_parts['extension'])) {
                                                case 'pdf':
                                                    include_once FCPATH . '/plugins/pdfparser/vendor/autoload.php';

                                                    try {
                                                        $parser = new \Smalot\PdfParser\Parser();
                                                        $pdf = $parser->parseFile($cv_path);

                                                        $text = @$pdf->getText();
                                                        if ($text) {
                                                            $bh_data['description'] = $text;
                                                        }
                                                    } catch (Exception $e) {
                                                        /*
                                                        echo $e->getMessage();
                                                        exit;
                                                        */
                                                    }

                                                    break;
                                                case 'doc':
                                                case 'docx':
                                                    include_once FCPATH . '/plugins/PHPDocumentParser/vendor/autoload.php';

                                                    try {
                                                        $parser = new \LukeMadhanga\DocumentParser();
                                                        $text = $parser->parseFromFile($cv_path);
                                                        if ($text) {
                                                            $bh_data['description'] = $text;
                                                        }
                                                    } catch (Exception $e) {
                                                        echo $e->getMessage();
                                                        exit;
                                                    }

                                                    break;
                                            }
                                        }
                                    }

                                    $search_candidate = $this->bullhorn_api->get_json($bh_token->restUrl . "search/Candidate" . "?" . http_build_query(array("query" => "email:" . $application->email, "fields" => "id")), NULL, array("BHRestToken: " . $bh_token->BhRestToken));
                                    if ($search_candidate && isset($search_candidate->total) && $search_candidate->total > 0) {
                                        $search_candidates = array_values($search_candidate->data);
                                        $bh_candidate_id = $search_candidates[0]->id;
                                    } else {
                                        $get_candidate = $this->bullhorn_api->get_json($bh_token->restUrl . "entity/Candidate", json_encode($bh_data), array("Content-Type: application/json", "BHRestToken: " . $bh_token->BhRestToken), "PUT"); // Create candidate for each submission
                                        if ($get_candidate && $get_candidate->changedEntityId) {
                                            $bh_candidate_id = $get_candidate->changedEntityId;
                                        }
                                    }
                                }
                            }

                            if ($bh_candidate_id) {
                                if ($application->filename) {
                                    $cv_path = FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'cvs' . DIRECTORY_SEPARATOR . $application->filename;
                                    if (file_exists($cv_path)) {
                                        $path_parts = pathinfo($cv_path);

                                        $bh_data = array(
                                            "externalID" => "CV",
                                            "fileContent" => base64_encode(file_get_contents($cv_path)),
                                            "fileType" => "SAMPLE",
                                            "name" => $application->name . " CV." . $path_parts['extension'],
                                            "contentType" => mime_content_type($cv_path),
                                            "description" => $application->name . " CV",
                                            "type" => "CV"
                                        );
                                        $file_submission = $this->bullhorn_api->get_json($bh_token->restUrl . "file/Candidate/" . $bh_candidate_id, json_encode($bh_data), array("Content-Type: application/json", "BHRestToken: " . $bh_token->BhRestToken), "PUT");
                                        if ($file_submission && isset($file_submission->fileId)) {
                                            $file_id = $file_submission->fileId;
                                        }
                                    }
                                }

                                if ($application->vacancy_id) {
                                    $vacancy = $this->vacancies_model->get_row(array(
                                        'vacancy_id' => $application->vacancy_id
                                    ));
                                    if ($vacancy && $vacancy->ref) {
                                        $data = array(
                                            "candidate" => array(
                                                "id" => $bh_candidate_id
                                            ),
                                            "jobOrder" => array(
                                                "id" => $vacancy->ref
                                            ),
                                            "source" => "web",
                                            "status" => "New Lead"
                                        );

                                        $bh_application = $this->bullhorn_api->get_json($bh_token->restUrl . "entity/JobSubmission", json_encode($data), array("Content-Type: application/json", "BHRestToken: " . $bh_token->BhRestToken), "PUT"); // Connect candidate with Job Order
                                        if ($bh_application && $bh_application->changedEntityId) {
                                            $application_id = $bh_application->changedEntityId;
                                        } else {
                                            $this->log("Application #" . $application->library_id . ' error: ' . var_export($bh_application, TRUE));
                                        }
                                    }
                                }
                            }

                            if ($bh_candidate_id || $application_id) {
                                if ($this->library_model->update_row(array(
                                    'library_id' => $application->library_id
                                ), array(
                                    'bh_candidate_id' => $bh_candidate_id,
                                    'application_id' => $application_id,
                                    'bh_notes' => NULL,
                                ))) {
                                    $this->log("Application #" . $application->library_id . ' uploaded.');
                                } else {
                                    $this->log("Application #" . $application->library_id . ' upload FAILED.');
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->log('</pre><a href="' . site_url('bullhorn/cronjob') . '">Continue</a>');
        $this->print_log();
    }

    public function cronjob()
    {
        $this->log("Starting loading vacancies...");
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            $this->log("Token loaded, expires in " . (((int)$access_token->created + (int)$access_token->expires) - time()) . " seconds");
            if (((int)$access_token->created + (int)$access_token->expires) < (int)time()) {
                $this->log("Token expired");
                $this->refresh_token($access_token);
                $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
            }

            $bh_token = $this->bullhorn_api->get_login($access_token->access_token);
            if (isset($bh_token->errorCode) && $bh_token->errorCode == 400) {
                $this->log($bh_token->errorMessage);
                $this->refresh_token($access_token);
                $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
                $bh_token = $this->bullhorn_api->get_login($access_token->access_token);
            }

            if (isset($bh_token->BhRestToken) && isset($bh_token->restUrl)) {

                $this->bullhorn_model->delete(array(
                    '`id` NOT IN (SELECT `id` FROM (SELECT `id` FROM `bullhorn_integration` ORDER BY `id` DESC LIMIT 10) `b`)' => NULL
                ));

                $this->log("API credentials received, loading vacancies");

                $params = array(
                    "BhRestToken" => $bh_token->BhRestToken,
                    "count" => 500,
                    "fields" => "id,isJobcastPublished,address,businessSectors,categories,clientContact,dateAdded,description,publicDescription,employmentType,hoursPerWeek,isDeleted,isOpen,isPublic,jobBoardList,owner,payRate,publishedCategory,salary,salaryUnit,title",
                    "where" => "isOpen = true and isPublic = 1 and isDeleted = false"
                );
                $vacancies = $this->bullhorn_api->get_json($bh_token->restUrl . "query/JobOrder" . "?" . http_build_query($params));
                if (isset($vacancies->data) && is_array($vacancies->data) && count($vacancies->data) > 0) {
                    $this->log("Loaded " . count($vacancies->data) . ", saving to database");

                    $this->load->model('vacancies_model');
                    $this->load->model('team_model');
                    $this->load->model('locations_model');
                    $this->load->model('clients_model');
                    $this->load->model('industry_sectors_model');
                    $remote_vacancies_ids = array();

                    foreach ($vacancies->data as $vacancy) {
                        $ref = $vacancy->id;
                        $remote_vacancies_ids[] = $ref;

                        $consultant_id = NULL;
                        if (isset($vacancy->owner) && isset($vacancy->owner->firstName) && $vacancy->owner->firstName && isset($vacancy->owner->lastName) && $vacancy->owner->lastName) {
                            $consultant = $this->team_model->get_row(array(
                                'firstname' => $vacancy->owner->firstName,
                                'lastname' => $vacancy->owner->lastName
                            ));

                            if ($consultant) {
                                $consultant_id = $consultant->user_id;
                            }
                        }

                        if ($this->vacancies_model->get_count(array('ref' => $ref, 'deleted' => FALSE))) {
                            $db_vacancy = $this->vacancies_model->get_row(array('ref' => $ref, 'deleted' => FALSE));
                            $contract_type = 'permanent';

                            switch ($vacancy->employmentType) {
                                case 'Contract':
                                case 'Contract To Hire':
                                    $contract_type = 'fixed';
                                    break;
                            }

                            $salary_type = 'salary';
                            if ($vacancy->salary > 0)
                                $salary_value = "&dollar;" . number_format($vacancy->salary);
                            elseif ($vacancy->salary == 0 && $vacancy->payRate)
                                $salary_value = "&dollar;" . number_format($vacancy->payRate);
                            else
                                $salary_value = 'Negotiable';
                            switch ($vacancy->salaryUnit) {
                                case 'Per Day':
                                    $salary_type = 'daily';
                                    if ($vacancy->salary > 0 || ($vacancy->salary == 0 && $vacancy->payRate))
                                        $salary_value .= " Per Day";
                                    break;
                                case 'Per Hour':
                                    $salary_type = 'hourly';
                                    if ($vacancy->salary > 0 || ($vacancy->salary == 0 && $vacancy->payRate))
                                        $salary_value .= " Per Hour";
                                    break;
                                case 'Salary':
                                    $salary_type = 'salary';
                                    if ($vacancy->salary > 0 || ($vacancy->salary == 0 && $vacancy->payRate))
                                        $salary_value .= " Per Annum";
                                    break;
                            }

                            $client_id = $db_vacancy->client_id;
                            if ($db_vacancy->client_id && $this->clients_model->get_count(array(
                                    'client_id' => $db_vacancy->client_id
                                )) === 0) {
                                $client_id = NULL;
                            }

                            $update_vacancy = array(
                                'client_id' => $client_id,
                                'consultant_id' => $consultant_id,
                                'title' => $vacancy->title,
                                'contract_type' => $contract_type,
                                'contract_length' => NULL,
                                'salary_type' => $salary_type,
                                'salary_value' => $salary_value,
                                'description' => $vacancy->publicDescription,
                                'client_name' => $vacancy->clientContact->firstName . ' ' . $vacancy->clientContact->lastName,
                                'client_email' => NULL,
                                'meta_title' => $vacancy->title,
                                'meta_desc' => $vacancy->title,
                                'date' => date("Y-m-d H:i:s", $vacancy->dateAdded / 1000),
                                'slug' => preg_replace("/[^a-zA-Z0-9]+/is", "-", strtolower(trim($vacancy->title)))
                            );

                            if ($this->vacancies_model->update_row(array('vacancy_id' => $db_vacancy->vacancy_id), $update_vacancy)) {
                                $this->vacancies_model->sectors_delete(array('vacancy_id' => $db_vacancy->vacancy_id));

                                if ($vacancy->categories->total > 0) {
                                    foreach ($vacancy->categories->data as $category) {
                                        if ($this->industry_sectors_model->get_count(array('name' => trim($category->name)))) {
                                            $industry_sector = $this->industry_sectors_model->get_row(array('name' => trim($category->name)));

                                            $this->vacancies_model->sectors_add_row(array('vacancy_id' => $db_vacancy->vacancy_id, 'industry_sector_id' => $industry_sector->industry_sector_id));
                                        } else {
                                            if ($this->industry_sectors_model->add_row(array('name' => trim($category->name)))) {
                                                $industry_sector_id = $this->industry_sectors_model->insert_id();
                                                $this->vacancies_model->sectors_add_row(array('vacancy_id' => $db_vacancy->vacancy_id, 'industry_sector_id' => $industry_sector_id));
                                            }
                                        }
                                    }
                                }

                                $this->vacancies_model->locations_delete(array('vacancy_id' => $db_vacancy->vacancy_id));
                                if ($vacancy->address->city) {
                                    $location_name = trim($vacancy->address->city);

                                    if ($this->locations_model->get_count(array('name' => $location_name))) {
                                        $location = $this->locations_model->get_row(array('name' => $location_name));

                                        $this->vacancies_model->locations_add_row(array('vacancy_id' => $db_vacancy->vacancy_id, 'location_id' => $location->location_id));
                                    } else {
                                        if ($this->locations_model->add_row(array('name' => $location_name))) {
                                            $location_id = $this->locations_model->insert_id();
                                            $this->vacancies_model->locations_add_row(array('vacancy_id' => $db_vacancy->vacancy_id, 'location_id' => $location_id));
                                        }
                                    }
                                }
                            } else {
                                $this->log("Database error on updating vacancy " . $ref);
                            }
                        } else {
                            $contract_type = 'permanent';

                            switch ($vacancy->employmentType) {
                                case 'Contract':
                                case 'Contract To Hire':
                                    $contract_type = 'fixed';
                                    break;
                            }

                            $salary_type = 'salary';
                            if ($vacancy->salary > 0)
                                $salary_value = "&dollar;" . number_format($vacancy->salary);
                            elseif ($vacancy->salary == 0 && $vacancy->payRate)
                                $salary_value = "&dollar;" . number_format($vacancy->payRate);
                            else
                                $salary_value = 'Negotiable';
                            switch ($vacancy->salaryUnit) {
                                case 'Per Day':
                                    $salary_type = 'daily';
                                    if ($vacancy->salary > 0 || ($vacancy->salary == 0 && $vacancy->payRate))
                                        $salary_value .= " Per Day";
                                    break;
                                case 'Per Hour':
                                    $salary_type = 'hourly';
                                    if ($vacancy->salary > 0 || ($vacancy->salary == 0 && $vacancy->payRate))
                                        $salary_value .= " Per Hour";
                                    break;
                                case 'Salary':
                                    $salary_type = 'salary';
                                    if ($vacancy->salary > 0 || ($vacancy->salary == 0 && $vacancy->payRate))
                                        $salary_value .= " Per Annum";
                                    break;
                            }

                            $add_vacancy = array(
                                'consultant_id' => $consultant_id,
                                'title' => $vacancy->title,
                                'ref' => $ref,
                                'contract_type' => $contract_type,
                                'contract_length' => NULL,
                                'salary_type' => $salary_type,
                                'salary_value' => $salary_value,
                                'description' => $vacancy->publicDescription,
                                'client_name' => $vacancy->clientContact->firstName . ' ' . $vacancy->clientContact->lastName,
                                'client_email' => NULL,
                                'meta_title' => $vacancy->title,
                                'meta_desc' => $vacancy->title,
                                'date' => date("Y-m-d H:i:s", $vacancy->dateAdded / 1000),
                                'slug' => preg_replace("/[^a-zA-Z0-9]+/is", "-", strtolower(trim($vacancy->title)))
                            );

                            if ($this->vacancies_model->add_row($add_vacancy)) {
                                $vacancy_id = $this->vacancies_model->insert_id();

                                if ($vacancy->categories->total > 0) {
                                    foreach ($vacancy->categories->data as $category) {
                                        if ($this->industry_sectors_model->get_count(array('name' => trim($category->name)))) {
                                            $industry_sector = $this->industry_sectors_model->get_row(array('name' => trim($category->name)));

                                            $this->vacancies_model->sectors_add_row(array('vacancy_id' => $vacancy_id, 'industry_sector_id' => $industry_sector->industry_sector_id));
                                        } else {
                                            if ($this->industry_sectors_model->add_row(array('name' => trim($category->name)))) {
                                                $industry_sector_id = $this->industry_sectors_model->insert_id();
                                                $this->vacancies_model->sectors_add_row(array('vacancy_id' => $vacancy_id, 'industry_sector_id' => $industry_sector_id));
                                            }
                                        }
                                    }
                                }

                                if ($vacancy->address->city) {
                                    $location_name = trim($vacancy->address->city);

                                    if ($this->locations_model->get_count(array('name' => $location_name))) {
                                        $location = $this->locations_model->get_row(array('name' => $location_name));

                                        $this->vacancies_model->locations_add_row(array('vacancy_id' => $vacancy_id, 'location_id' => $location->location_id));
                                    } else {
                                        if ($this->locations_model->add_row(array('name' => $location_name))) {
                                            $location_id = $this->locations_model->insert_id();
                                            $this->vacancies_model->locations_add_row(array('vacancy_id' => $vacancy_id, 'location_id' => $location_id));
                                        }
                                    }
                                }
                            } else {
                                $this->log("Database error on adding vacancy " . $ref);
                            }
                        }
                    }

                    $count_delete = $this->vacancies_model->get_count(array(
                        "`vacancies`.`ref` NOT IN ('" . implode("','", $remote_vacancies_ids) . "')" => NULL,
                        'deleted' => FALSE
                    ));
                    if ($count_delete > 0) {
                        $this->log("Found " . $count_delete . " old vacancies, removing...");
                        $this->vacancies_model->update(array(
                            "`vacancies`.`ref` NOT IN ('" . implode("','", $remote_vacancies_ids) . "')" => NULL
                        ), array('deleted' => TRUE));
                    }
                    $this->log("Finished");
                } elseif (isset($vacancies->data) && is_array($vacancies->data) && count($vacancies->data) === 0) {
                    $this->log("Loaded 0 vacancies, nothing to save");
                } else {
                    $this->log("Loading vacancies error. " . var_export($vacancies, TRUE) . " Closing application");
                    $this->print_log();
                }
            } else {
                $this->log("Loging error. " . var_export($bh_token, TRUE) . " Closing application");
                $this->print_log();
            }
        }

        $this->print_log();
    }

    private function log($message)
    {
        $this->log_messages[] = date("H:i:s Y-m-d") . " " . $message;
    }

    private function print_log()
    {
        echo "<pre>";
        echo implode("\n", $this->log_messages);
        exit;
    }

    private function get_state($full_name)
    {
        $states = array(
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming',
        );

        return array_search($full_name, $states);
    }

    private function refresh_token($access_token)
    {
        $token = $this->bullhorn_api->get_refresh_token($access_token->refresh_token);
        if (isset($token->access_token)) {
            $data = array(
                'access_token' => $token->access_token,
                'refresh_token' => $token->refresh_token,
                'expires' => $token->expires_in,
                'created' => time()
            );
            $this->bullhorn_model->add_row($data);

            $this->log("Token refreshed");
        } else {
            $this->log("Token cannot be refreshed. Closing application");
            $this->print_log();
        }
    }

}
