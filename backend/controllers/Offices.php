<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offices extends BOLD_Controller
{
    private $plugin = 'offices';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('offices_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->offices_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->offices_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('clients_model');
    }

    public function index($client_id = NULL)
    {
        if (!$client_id) redirect('clients');

        $this->view_data['client'] = $this->clients_model->get_row(
            array(
                'client_id' => $client_id
            )
        );
        if (!$this->view_data['client']) redirect('clients');

        $this->view_data['title'] = 'Offices';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $params = array(
            'deleted' => FALSE,
            'client_id' => $this->view_data['client']->client_id
        );
        $this->view_data['offices'] = $this->offices_model->get($params);

        $this->view_data['client_id'] = $this->view_data['client']->client_id;

        $this->load->view('header', $this->view_data);
        $this->load->view('offices/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add($client_id = NULL)
    {
        if (!$client_id) redirect('clients');

        $this->view_data['client'] = $this->clients_model->get_row(
            array(
                'client_id' => $client_id
            )
        );
        if (!$this->view_data['client']) redirect('clients');

        $this->view_data['title'] = 'Add Office';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('address', 'Address', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('coordinates', 'Coordinates', 'required|trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'client_id' => $this->view_data['client']->client_id,
                    'title' => set_value('title'),
                    'coordinates' => set_value('coordinates'),
                    'date' => date('Y-m-d H:i:s'),
                    'deleted' => FALSE
                );

                if ($this->offices_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'Office created successfully.');
                    redirect('offices/index/' . $client_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->view_data['client_id'] = (int)$client_id;

        $this->load->model('settings_model');
        $maps_api_key = $this->settings_model->get_row(array('name' => 'maps_api_key'));
        if ($maps_api_key && isset($maps_api_key->value))
            $this->view_data['maps_api_key'] = $maps_api_key->value;

        $this->load->view('header', $this->view_data);
        $this->load->view('offices/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($office_id)
    {
        if (!$office_id) redirect('offices');

        $this->view_data['office'] = $this->offices_model->get_row(
            array(
                'office_id' => $office_id
            )
        );
        if (!$this->view_data['office']) redirect('offices');

        $this->view_data['title'] = 'Edit Office';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('address', 'Address', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('coordinates', 'Coordinates', 'required|trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'title' => set_value('title'),
                    'coordinates' => set_value('coordinates'),
                    'date' => date('Y-m-d H:i:s')
                );

                if ($this->offices_model->update_row(array('office_id' => $office_id), $data)) {
                    $this->session->set_flashdata('success', 'Office updated successfully.');
                    redirect('offices/edit/' . $office_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->model('settings_model');
        $maps_api_key = $this->settings_model->get_row(array('name' => 'maps_api_key'));
        if ($maps_api_key && isset($maps_api_key->value))
            $this->view_data['maps_api_key'] = $maps_api_key->value;

        $this->load->view('header', $this->view_data);
        $this->load->view('offices/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($office_id)
    {
        if (!$office_id) redirect('offices');

        $this->view_data['office'] = $this->offices_model->get_row(
            array(
                'office_id' => $office_id
            )
        );
        if (!$this->view_data['office']) redirect('offices');

        if ($this->offices_model->update_row(array('office_id' => $office_id), array(
            'deleted' => TRUE
        ))) {
            $this->session->set_flashdata('success', 'Office deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('offices/index/' . $this->view_data['office']->client_id);
    }
}
