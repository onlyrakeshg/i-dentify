<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends BOLD_Controller
{
    private $plugin = 'locations';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('locations_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->locations_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->locations_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index()
    {
        $this->view_data['title'] = 'Locations';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['locations'] = $this->locations_model->get();

        $this->load->view('header', $this->view_data);
        $this->load->view('locations/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add()
    {
        $this->view_data['title'] = 'Add Location';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[1]|max_length[100]|is_unique[locations.name]');

            if ($this->form_validation->run()) {
                $data = array(
                    'name' => set_value('name')
                );

                if ($this->locations_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'Location created successfully.');
                    redirect('locations');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('locations/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($location_id)
    {
        if (!$location_id) redirect('locations');

        $this->view_data['location'] = $this->locations_model->get_row(
            array(
                'location_id' => $location_id
            )
        );
        if (!$this->view_data['location']) redirect('locations');

        $this->view_data['title'] = 'Edit Location';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'name' => set_value('name')
                );

                if ($this->locations_model->update_row(array('location_id' => $location_id), $data)) {
                    $this->session->set_flashdata('success', 'Location updated successfully.');
                    redirect('locations/edit/' . $location_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('locations/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($location_id)
    {
        if (!$location_id) redirect('locations');

        $this->view_data['location'] = $this->locations_model->get_row(
            array(
                'location_id' => $location_id
            )
        );
        if (!$this->view_data['location']) redirect('locations');

        if ($this->locations_model->delete_row(array('location_id' => $location_id))) {
            $this->session->set_flashdata('success', 'Location deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('locations');
    }
}
