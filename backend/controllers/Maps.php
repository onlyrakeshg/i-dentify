<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maps extends BOLD_Controller
{
    private $plugin = 'maps';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('maps_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->maps_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->maps_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');
    }

    public function index()
    {
        $this->view_data['title'] = 'Google Maps Integration';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->model('settings_model');
        $this->load->library('form_validation');
        if ($this->input->post()) {

            $this->form_validation->set_rules('maps_api_key', 'API Key', 'required|trim|min_length[1]');

            if ($this->form_validation->run()) {

                if ($this->settings_model->get_count(array('name' => 'maps_api_key')) === 0) {
                    $this->settings_model->add_row(array(
                        'name' => 'maps_api_key',
                        'title' => 'Google Maps API Key',
                        'value' => set_value('maps_api_key', '', FALSE)
                    ));
                } else {
                    $this->settings_model->update_row(array(
                        'name' => 'maps_api_key'
                    ), array(
                        'title' => 'Google Maps API Key',
                        'value' => set_value('maps_api_key', '', FALSE)
                    ));
                }

                $this->session->set_flashdata('success', 'Configuration updated.');
                redirect('maps');
            }
        }

        $this->view_data['maps_api_key'] = NULL;
        $maps_api_key = $this->settings_model->get_row(array('name' => 'maps_api_key'));
        if ($maps_api_key && isset($maps_api_key->value))
            $this->view_data['maps_api_key'] = $maps_api_key->value;

        $this->load->view('header', $this->view_data);
        $this->load->view('maps/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }
}