<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_content_pages extends BOLD_Controller
{
    private $plugin = 'clients_content_pages';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('clients_content_pages_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->clients_content_pages_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->clients_content_pages_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('clients_model');
    }

    public function index($client_id = NULL)
    {
        if (!$client_id) redirect('clients');

        $this->view_data['client'] = $this->clients_model->get_row(
            array(
                'client_id' => $client_id
            )
        );
        if (!$this->view_data['client']) redirect('clients');

        $this->view_data['title'] = 'Content Pages';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['clients_content_pages'] = $this->clients_content_pages_model->get(array(
            'client_id' => $this->view_data['client']->client_id,
            'deleted' => FALSE,
        ), 'sort', 'DESC');

        $this->view_data['client_id'] = (int)$this->view_data['client']->client_id;

        $this->load->view('header', $this->view_data);
        $this->load->view('clients_content_pages/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add($client_id = NULL)
    {
        if (!$client_id) redirect('clients');

        $this->view_data['client'] = $this->clients_model->get_row(
            array(
                'client_id' => $client_id
            )
        );
        if (!$this->view_data['client']) redirect('clients');

        $this->view_data['title'] = 'Add Content Page';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('image_lib');

        if ($this->input->post()) {

            $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('content', 'Content', 'required|trim|min_length[1]');
            $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|min_length[0]|max_length[250]');
            $this->form_validation->set_rules('meta_desc', 'Meta Description', 'trim|min_length[0]|max_length[255]');
            $this->form_validation->set_rules('canonical', 'Canonical', 'trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('slug', 'Slug', 'trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $sort = $this->clients_content_pages_model->get_max('sort', array(
                    'client_id' => $this->view_data['client']->client_id
                ));
                if (!$sort)
                    $sort = 1;
                else
                    $sort++;

                $data = array(
                    'client_id' => $this->view_data['client']->client_id,
                    'title' => set_value('title'),
                    'content' => set_value('content', '', FALSE),
                    'meta_title' => set_value('meta_title') ? set_value('meta_title') : NULL,
                    'meta_keywords' => set_value('meta_keywords') ? set_value('meta_keywords') : NULL,
                    'meta_desc' => set_value('meta_desc') ? set_value('meta_desc') : NULL,
                    'canonical' => set_value('canonical') ? set_value('canonical') : NULL,
                    'date' => date('Y-m-d H:i:s'),
                    'deleted' => FALSE,
                    'slug' => set_value('slug') ? set_value('slug') : NULL,
                    'sort' => $sort
                );

                $this->upload->initialize(array(
                    'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'encrypt_name' => TRUE
                ));

                if ($this->upload->do_upload('image')) {
                    chmod($this->upload->data('full_path'), 0777);

                    $image_size = getimagesize($this->upload->data('full_path'));
                    if ($image_size && count($image_size) >= 2) {
                        list($width, $height) = $image_size;
                        if ($width > 800) {
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = $this->upload->data('full_path');
                            $config['maintain_ratio'] = TRUE;
                            $config['width'] = 800;
                            $this->image_lib->clear();
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                            $this->image_lib->clear();
                        }
                    }

                    $data['image'] = $this->upload->data('file_name');
                } else {
                    $this->view_data['error'] = $this->upload->display_errors('', '');
                    $this->view_data['image'] = NULL;
                }

                if (isset($_FILES['content_image']) && $_FILES['content_image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('content_image')) {
                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['content_image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "content_image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['thumb']) && $_FILES['thumb']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('thumb')) {
                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['thumb'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "thumb: " . $this->upload->display_errors('', '');
                    }
                }

                if ($this->clients_content_pages_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'Content page created successfully.');
                    redirect('clients_content_pages/index/' . $this->view_data['client']->client_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->view_data['company_name'] = $this->config->item('company_name');
        $this->view_data['client_id'] = (int)$this->view_data['client']->client_id;

        $this->load->view('header', $this->view_data);
        $this->load->view('clients_content_pages/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($content_page_id)
    {
        if (!$content_page_id) redirect('clients_content_pages');

        $this->view_data['clients_content_page'] = $this->clients_content_pages_model->get_row(
            array(
                'clients_content_page_id' => $content_page_id
            )
        );
        if (!$this->view_data['clients_content_page']) redirect('clients_content_pages');

        $this->view_data['title'] = 'Edit Content Page';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('image_lib');

        if ($this->input->post()) {

            $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('content', 'Content', 'required|trim|min_length[1]');
            $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|min_length[0]|max_length[250]');
            $this->form_validation->set_rules('meta_desc', 'Meta Description', 'trim|min_length[0]|max_length[255]');
            $this->form_validation->set_rules('canonical', 'Canonical', 'trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('slug', 'Slug', 'trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'title' => set_value('title'),
                    'content' => set_value('content', '', FALSE),
                    'meta_title' => set_value('meta_title') ? set_value('meta_title') : NULL,
                    'meta_keywords' => set_value('meta_keywords') ? set_value('meta_keywords') : NULL,
                    'meta_desc' => set_value('meta_desc') ? set_value('meta_desc') : NULL,
                    'canonical' => set_value('canonical') ? set_value('canonical') : NULL,
                    'date' => date('Y-m-d H:i:s'),
                    'slug' => set_value('slug') ? set_value('slug') : NULL
                );

                $this->upload->initialize(array(
                    'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'encrypt_name' => TRUE
                ));

                if ($this->upload->do_upload('image')) {

                    if ($this->view_data['clients_content_page']->image)
                        @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_content_page']->image);

                    chmod($this->upload->data('full_path'), 0777);

                    $image_size = getimagesize($this->upload->data('full_path'));
                    if ($image_size && count($image_size) >= 2) {
                        list($width, $height) = $image_size;
                        if ($width > 800) {
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = $this->upload->data('full_path');
                            $config['maintain_ratio'] = TRUE;
                            $config['width'] = 800;
                            $this->image_lib->clear();
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                            $this->image_lib->clear();
                        }
                    }

                    $data['image'] = $this->upload->data('file_name');
                } else {
                    $this->view_data['error'] = $this->upload->display_errors('', '');
                }

                if (isset($_FILES['content_image']) && $_FILES['content_image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('content_image')) {
                        chmod($this->upload->data('full_path'), 0777);

                        if ($this->view_data['clients_content_page']->content_image)
                            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_content_page']->content_image);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['content_image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "content_image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['thumb']) && $_FILES['thumb']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('thumb')) {
                        chmod($this->upload->data('full_path'), 0777);

                        if ($this->view_data['clients_content_page']->thumb)
                            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_content_page']->thumb);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['thumb'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "thumb: " . $this->upload->display_errors('', '');
                    }
                }

                if ($this->clients_content_pages_model->update_row(array('clients_content_page_id' => $content_page_id), $data)) {
                    $this->session->set_flashdata('success', 'Content page updated successfully.');
                    redirect('clients_content_pages/edit/' . $content_page_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->view_data['company_name'] = $this->config->item('company_name');

        $this->load->view('header', $this->view_data);
        $this->load->view('clients_content_pages/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($content_page_id)
    {
        if (!$content_page_id) redirect('clients_content_pages');

        $this->view_data['clients_content_page'] = $this->clients_content_pages_model->get_row(
            array(
                'clients_content_page_id' => $content_page_id
            )
        );
        if (!$this->view_data['clients_content_page']) redirect('clients_content_pages');

        /* Remove image
        if ($this->view_data['clients_content_page']->image)
            unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_content_page']->image);
        */

        if ($this->clients_content_pages_model->update_row(array('clients_content_page_id' => $content_page_id), array(
            'deleted' => TRUE
        ))) {
            $this->session->set_flashdata('success', 'Content page deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('clients_content_pages' . (isset($this->view_data['clients_content_page']->client_id) && $this->view_data['clients_content_page']->client_id ? "/index/" . $this->view_data['clients_content_page']->client_id : ""));
    }

    public function sort($direction = 'up', $content_page_id = NULL)
    {
        if (!$content_page_id) redirect('clients_content_pages');

        $this->view_data['clients_content_page'] = $this->clients_content_pages_model->get_row(
            array(
                'clients_content_page_id' => $content_page_id
            )
        );
        if (!$this->view_data['clients_content_page']) redirect('clients_content_pages');

        switch ($direction) {
            case 'up':
                if ($this->view_data['clients_content_page']->sort !== NULL) {
                    $clients_content_page = $this->clients_content_pages_model->get_row(array(
                        'client_id' => $this->view_data['clients_content_page']->client_id,
                        'clients_content_page_id !=' => $this->view_data['clients_content_page']->clients_content_page_id,
                        'sort IS NOT NULL' => NULL,
                        'sort >' => $this->view_data['clients_content_page']->sort
                    ), 'sort', 'ASC');

                    if ($clients_content_page) {
                        list($this->view_data['clients_content_page']->sort, $clients_content_page->sort) = array($clients_content_page->sort, $this->view_data['clients_content_page']->sort);

                        $this->clients_content_pages_model->update_row(array('clients_content_page_id' => $clients_content_page->clients_content_page_id), array(
                            "sort" => $clients_content_page->sort
                        ));
                    }
                }

                if ($this->view_data['clients_content_page']->sort === NULL) {
                    $this->view_data['clients_content_page']->sort = $this->clients_content_pages_model->get_max('sort', array(
                        'client_id' => $this->view_data['clients_content_page']->client_id,
                        'clients_content_page_id !=' => $this->view_data['clients_content_page']->clients_content_page_id,
                        'sort IS NOT NULL' => NULL,
                    ));

                    if ($this->view_data['clients_content_page']->sort === NULL)
                        $this->view_data['clients_content_page']->sort = 1;
                    else
                        $this->view_data['clients_content_page']->sort++;

                }

                $this->clients_content_pages_model->update_row(array('clients_content_page_id' => $this->view_data['clients_content_page']->clients_content_page_id), array(
                    "sort" => $this->view_data['clients_content_page']->sort
                ));

                break;
            case 'down':
                if ($this->view_data['clients_content_page']->sort !== NULL) {
                    $clients_content_page = $this->clients_content_pages_model->get_row(array(
                        'client_id' => $this->view_data['clients_content_page']->client_id,
                        'clients_content_page_id !=' => $this->view_data['clients_content_page']->clients_content_page_id,
                        'sort IS NOT NULL' => NULL,
                        'sort <' => $this->view_data['clients_content_page']->sort
                    ), 'sort', 'DESC');

                    if ($clients_content_page) {
                        list($this->view_data['clients_content_page']->sort, $clients_content_page->sort) = array($clients_content_page->sort, $this->view_data['clients_content_page']->sort);

                        $this->clients_content_pages_model->update_row(array('clients_content_page_id' => $clients_content_page->clients_content_page_id), array(
                            "sort" => $clients_content_page->sort
                        ));
                    }
                }

                if ($this->view_data['clients_content_page']->sort === NULL) {
                    $this->view_data['clients_content_page']->sort = $this->clients_content_pages_model->get_min('sort', array(
                        'client_id' => $this->view_data['clients_content_page']->client_id,
                        'clients_content_page_id !=' => $this->view_data['clients_content_page']->clients_content_page_id,
                        'sort IS NOT NULL' => NULL,
                    ));

                    if ($this->view_data['clients_content_page']->sort === NULL)
                        $this->view_data['clients_content_page']->sort = 1;
                    else
                        $this->view_data['clients_content_page']->sort--;

                }

                $this->clients_content_pages_model->update_row(array('clients_content_page_id' => $this->view_data['clients_content_page']->clients_content_page_id), array(
                    "sort" => $this->view_data['clients_content_page']->sort
                ));
                break;
        }

        redirect('clients_content_pages' . (isset($this->view_data['clients_content_page']->client_id) && $this->view_data['clients_content_page']->client_id ? "/index/" . $this->view_data['clients_content_page']->client_id : ""));
    }

    public function duplicate($content_page_id = NULL)
    {
        if (!$content_page_id) redirect('clients_content_pages');

        $this->view_data['clients_content_page'] = $this->clients_content_pages_model->get_row(
            array(
                'clients_content_page_id' => $content_page_id
            )
        );
        if (!$this->view_data['clients_content_page']) redirect('clients_content_pages');

        $sort = $this->clients_content_pages_model->get_max('sort', array(
            'client_id' => $this->view_data['clients_content_page']->client_id ? (int)$this->view_data['clients_content_page']->client_id : NULL
        ));
        if (!$sort)
            $sort = 1;
        else
            $sort++;

        $slug = $this->view_data['clients_content_page']->slug . '-' . $sort;

        $data = array(
            'client_id' => $this->view_data['clients_content_page']->client_id,
            'title' => $this->view_data['clients_content_page']->title,
            'content' => $this->view_data['clients_content_page']->content,
            'meta_title' => $this->view_data['clients_content_page']->meta_title,
            'meta_keywords' => $this->view_data['clients_content_page']->meta_keywords,
            'meta_desc' => $this->view_data['clients_content_page']->meta_desc,
            'canonical' => $this->view_data['clients_content_page']->canonical,
            'date' => date('Y-m-d H:i:s'),
            'deleted' => FALSE,
            'slug' => $slug,
            'sort' => $sort
        );

        if ($this->view_data['clients_content_page']->image !== NULL) {
            $file_ext = pathinfo(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_content_page']->image, PATHINFO_EXTENSION);
            $new_file_name = md5(uniqid(mt_rand())) . '.' . $file_ext;

            if (copy(
                FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_content_page']->image,
                FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $new_file_name
            ))
                $data['image'] = $new_file_name;
        }

        if ($this->view_data['clients_content_page']->content_image !== NULL) {
            $file_ext = pathinfo(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_content_page']->content_image, PATHINFO_EXTENSION);
            $new_file_name = md5(uniqid(mt_rand())) . '.' . $file_ext;

            if (copy(
                FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_content_page']->content_image,
                FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $new_file_name
            ))
                $data['content_image'] = $new_file_name;
        }

        if ($this->view_data['clients_content_page']->thumb !== NULL) {
            $file_ext = pathinfo(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_content_page']->thumb, PATHINFO_EXTENSION);
            $new_file_name = md5(uniqid(mt_rand())) . '.' . $file_ext;

            if (copy(
                FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_content_page']->thumb,
                FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $new_file_name
            ))
                $data['thumb'] = $new_file_name;
        }

        if ($this->clients_content_pages_model->add_row($data)) {
            $this->session->set_flashdata('success', 'Content page duplicated successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('clients_content_pages' . (isset($this->view_data['clients_content_page']->client_id) && $this->view_data['clients_content_page']->client_id ? "/index/" . $this->view_data['clients_content_page']->client_id : ""));
    }
}
