<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends BOLD_Controller
{
    private $plugin = 'clients';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('clients_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->clients_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->clients_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');
    }

    public function index()
    {
        $this->view_data['title'] = 'Client Microsites';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['clients'] = $this->clients_model->get();

        $this->load->view('header', $this->view_data);
        $this->load->view('clients/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add()
    {
        $this->view_data['title'] = 'Add Client';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('image_lib');

        if ($this->input->post()) {
            $this->form_validation->set_rules('client_name', 'Client Name', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('ref', 'Ref', 'trim|min_length[0]|max_length[50]');
            $this->form_validation->set_rules('website', 'Website URL', 'trim|min_length[1]|max_length[50]');
            $this->form_validation->set_rules('company_size', 'Company Size', 'required|trim|intval|greater_than_equal_to[0]');
            $this->form_validation->set_rules('sector_ids', 'Industry', 'trim|intval|greater_than[0]');
            $this->form_validation->set_rules('location_ids', 'Headquarters', 'trim|intval|greater_than[0]');
            $this->form_validation->set_rules('content', 'Content', 'trim|min_length[0]');
            $this->form_validation->set_rules('tags', 'Top 5 Sectors', 'trim|min_length[0]|max_length[250]');
            $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|min_length[0]|max_length[250]');
            $this->form_validation->set_rules('meta_desc', 'Meta Description', 'trim|min_length[0]|max_length[255]');
            $this->form_validation->set_rules('vacancies_ids', 'Vacancies', 'trim|intval|greater_than[0]');

            if ($this->form_validation->run()) {
                if (!set_value('ref')) {
                    $ref = rand(100000, 999999);
                    while ($this->clients_model->get_row(array(
                        'ref' => $ref
                    ))) {
                        $ref = rand(100000, 999999);
                    }
                } else {
                    $ref = set_value('ref');
                    $c = 1;
                    while ($this->clients_model->get_row(array(
                        'ref' => $ref
                    ))) {
                        $ref = set_value('ref') . '-' . $c;
                    }
                }

                $data = array(
                    'ref' => $ref,
                    'client_name' => set_value('client_name'),
                    'website' => set_value('website'),
                    'company_size' => set_value('company_size'),
                    'content' => set_value('content', '', FALSE),
                    'tags' => set_value('tags') ? set_value('tags', '', FALSE) : NULL,
                    'created_date' => date('Y-m-d H:i:s'),
                    'meta_title' => set_value('meta_title') ? set_value('meta_title') : NULL,
                    'meta_keywords' => set_value('meta_keywords') ? set_value('meta_keywords') : NULL,
                    'meta_desc' => set_value('meta_desc') ? set_value('meta_desc') : NULL,
                );

                $upload_errors = array();

                if (isset($_FILES['image']) && $_FILES['image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg|mp4',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('image')) {
                        chmod($this->upload->data('full_path'), 0777);

                        $data['image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['logo']) && $_FILES['logo']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('logo')) {
                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 250) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 250;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['logo'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Logo: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['key_image']) && $_FILES['key_image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('key_image')) {
                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['key_image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Key Image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['overview_image']) && $_FILES['overview_image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('overview_image')) {
                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['overview_image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Key Image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['opportunities_image']) && $_FILES['opportunities_image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('opportunities_image')) {
                        chmod($this->upload->data('full_path'), 0777);

                        $data['opportunities_image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Job Opportunities Image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['open_graph_image']) && $_FILES['open_graph_image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('open_graph_image')) {
                        chmod($this->upload->data('full_path'), 0777);

                        $data['open_graph_image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Open Graph Image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['video']) && $_FILES['video']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'mp4',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('video')) {
                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['video'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Video: " . $this->upload->display_errors('', '');
                    }
                }

                if (count($upload_errors) > 0)
                    $this->session->set_flashdata('error', implode("<br/>", $upload_errors));

                if ($this->clients_model->add_row($data)) {
                    $client_id = $this->clients_model->insert_id();

                    if (is_array(set_value('sector_ids[]')) && count(set_value('sector_ids[]')) > 0) {
                        $data = array();
                        foreach (set_value('sector_ids[]') as $sector_id) {
                            $data[] = array(
                                'client_id' => $client_id,
                                'industry_sector_id' => $sector_id
                            );
                        }
                        $this->clients_model->sectors_add($data);
                    }

                    if (is_array(set_value('location_ids[]')) && count(set_value('location_ids[]')) > 0) {
                        $data = array();
                        foreach (set_value('location_ids[]') as $location_id) {
                            $data[] = array(
                                'client_id' => $client_id,
                                'location_id' => $location_id
                            );
                        }
                        $this->clients_model->locations_add($data);
                    }

                    if (is_array(set_value('vacancies_ids[]')) && count(set_value('vacancies_ids[]')) > 0) {
                        $this->load->model('vacancies_model');
                        foreach (set_value('vacancies_ids[]') as $vacancy_id) {
                            $this->vacancies_model->update_row(array('vacancy_id' => $vacancy_id), array(
                                'client_id' => $client_id
                            ));
                        }
                    }

                    $this->session->set_flashdata('success', 'Client created successfully.');
                    redirect('clients');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->model('industry_sectors_model');
        $this->view_data['industry_sectors'] = $this->industry_sectors_model->get();
        $this->load->model('locations_model');
        $this->view_data['locations'] = $this->locations_model->get();
        $this->load->model('vacancies_model');
        $this->view_data['vacancies'] = $this->vacancies_model->get(array(
            'deleted' => FALSE,
            '(`client_id` IS NULL)' => NULL
        ));

        $this->load->view('header', $this->view_data);
        $this->load->view('clients/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($client_id)
    {
        if (!$client_id) redirect('clients');

        $this->view_data['client'] = $this->clients_model->get_row(
            array(
                'client_id' => $client_id
            )
        );
        if (!$this->view_data['client']) redirect('clients');

        $this->view_data['title'] = 'Edit Client';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('image_lib');

        if ($this->input->post()) {
            $this->form_validation->set_rules('client_name', 'Client Name', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('ref', 'Ref', 'trim|min_length[0]|max_length[50]');
            $this->form_validation->set_rules('website', 'Website URL', 'trim|min_length[1]|max_length[50]');
            $this->form_validation->set_rules('company_size', 'Company Size', 'required|trim|intval|greater_than_equal_to[0]');
            $this->form_validation->set_rules('sector_ids', 'Industry', 'trim|intval|greater_than[0]');
            $this->form_validation->set_rules('location_ids', 'Headquarters', 'trim|intval|greater_than[0]');
            $this->form_validation->set_rules('content', 'Content', 'trim|min_length[0]');
            $this->form_validation->set_rules('tags', 'Top 5 Sectors', 'trim|min_length[0]|max_length[250]');
            $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|min_length[0]|max_length[250]');
            $this->form_validation->set_rules('meta_desc', 'Meta Description', 'trim|min_length[0]|max_length[255]');

            if ($this->form_validation->run()) {
                if (!set_value('ref')) {
                    $ref = rand(100000, 999999);
                    while ($this->clients_model->get_row(array(
                        'ref' => $ref,
                        'client_id !=' => $client_id,
                    ))) {
                        $ref = rand(100000, 999999);
                    }
                } else {
                    $ref = set_value('ref');
                    $c = 1;
                    while ($this->clients_model->get_row(array(
                        'ref' => $ref,
                        'client_id !=' => $client_id,
                    ))) {
                        $ref = set_value('ref') . '-' . $c;
                    }
                }

                $data = array(
                    'ref' => $ref,
                    'client_name' => set_value('client_name'),
                    'website' => set_value('website'),
                    'company_size' => set_value('company_size'),
                    'content' => set_value('content', '', FALSE),
                    'tags' => set_value('tags') ? set_value('tags', '', FALSE) : NULL,
                    'meta_title' => set_value('meta_title') ? set_value('meta_title') : NULL,
                    'meta_keywords' => set_value('meta_keywords') ? set_value('meta_keywords') : NULL,
                    'meta_desc' => set_value('meta_desc') ? set_value('meta_desc') : NULL,
                );

                $upload_errors = array();

                if (isset($_FILES['image']) && $_FILES['image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg|mp4',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('image')) {
                        if ($this->view_data['client']->image)
                            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['client']->image);

                        chmod($this->upload->data('full_path'), 0777);
                        $data['image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['logo']) && $_FILES['logo']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('logo')) {
                        if ($this->view_data['client']->logo)
                            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['client']->logo);

                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 250) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 250;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['logo'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Logo: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['key_image']) && $_FILES['key_image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('key_image')) {
                        if ($this->view_data['client']->key_image)
                            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['client']->key_image);

                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['key_image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Key Image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['overview_image']) && $_FILES['overview_image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('overview_image')) {
                        if ($this->view_data['client']->overview_image)
                            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['client']->overview_image);

                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['overview_image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Key Image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['opportunities_image']) && $_FILES['opportunities_image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('opportunities_image')) {
                        if ($this->view_data['client']->opportunities_image)
                            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['client']->opportunities_image);

                        chmod($this->upload->data('full_path'), 0777);

                        $data['opportunities_image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Job Opportunities Image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['open_graph_image']) && $_FILES['open_graph_image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('open_graph_image')) {
                        if ($this->view_data['client']->open_graph_image)
                            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['client']->open_graph_image);

                        chmod($this->upload->data('full_path'), 0777);

                        $data['open_graph_image'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Open Graph Image: " . $this->upload->display_errors('', '');
                    }
                }

                if (isset($_FILES['video']) && $_FILES['video']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'mp4',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('video')) {
                        if ($this->view_data['client']->videos)
                            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . $this->view_data['client']->videos);

                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['video'] = $this->upload->data('file_name');
                    } else {
                        $upload_errors[] = "Video: " . $this->upload->display_errors('', '');
                    }
                }

                if (count($upload_errors) > 0)
                    $this->session->set_flashdata('error', implode("<br/>", $upload_errors));


                if ($this->clients_model->update_row(array('client_id' => $client_id), $data)) {

                    $this->clients_model->sectors_delete(array('client_id' => $client_id));
                    if (is_array(set_value('sector_ids[]')) && count(set_value('sector_ids[]')) > 0) {
                        $data = array();
                        foreach (set_value('sector_ids[]') as $sector_id) {
                            $data[] = array(
                                'client_id' => $client_id,
                                'industry_sector_id' => $sector_id
                            );
                        }
                        $this->clients_model->sectors_add($data);
                    }

                    $this->clients_model->locations_delete(array('client_id' => $client_id));
                    if (is_array(set_value('location_ids[]')) && count(set_value('location_ids[]')) > 0) {
                        $data = array();
                        foreach (set_value('location_ids[]') as $location_id) {
                            $data[] = array(
                                'client_id' => $client_id,
                                'location_id' => $location_id
                            );
                        }
                        $this->clients_model->locations_add($data);
                    }

                    $this->load->model('vacancies_model');
                    $this->vacancies_model->update(array('client_id' => $client_id), array('client_id' => NULL));
                    if (is_array(set_value('vacancies_ids[]')) && count(set_value('vacancies_ids[]')) > 0) {
                        foreach (set_value('vacancies_ids[]') as $vacancy_id) {
                            $this->vacancies_model->update_row(array('vacancy_id' => $vacancy_id), array(
                                'client_id' => $client_id
                            ));
                        }
                    }

                    $this->session->set_flashdata('success', 'Client updated successfully.');
                    redirect('clients/edit/' . $client_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->model('industry_sectors_model');
        $this->view_data['industry_sectors'] = $this->industry_sectors_model->get();
        $this->load->model('locations_model');
        $this->view_data['locations'] = $this->locations_model->get();
        $this->load->model('vacancies_model');
        $this->view_data['vacancies'] = $this->vacancies_model->get(array(
            'deleted' => FALSE,
            '(`client_id` = ' . $client_id . ' OR `client_id` IS NULL)' => NULL
        ), NULL, NULL, 'client_id', 'DESC');

        $this->load->view('header', $this->view_data);
        $this->load->view('clients/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($client_id)
    {
        if (!$client_id) redirect('clients');

        $this->view_data['clients'] = $this->clients_model->get_row(
            array(
                'client_id' => $client_id
            )
        );
        if (!$this->view_data['clients']) redirect('clients');

        if ($this->view_data['clients']->image)
            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients']->image);

        if ($this->view_data['clients']->logo)
            @unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients']->logo);

        if ($this->clients_model->delete_row(array('client_id' => $client_id))) {
            $this->session->set_flashdata('success', 'Client deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('clients');
    }
}
