<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends BOLD_Controller
{
    private $plugin = 'team';
    private $version = '1.1.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('team_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->team_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->team_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        }

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index($show = NULL)
    {
        if (!$this->user)
            redirect('team/login');

        $this->view_data['title'] = 'Team Manager';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['team'] = $this->team_model->get(array(
            'deleted' => (bool)($show === 'old')
        ));

        $this->load->view('header', $this->view_data);
        $this->load->view('team/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function login()
    {
        if ($this->user)
            redirect('dashboard');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('email', 'Email Address', 'required|trim|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                if ($this->team_model->get_count(array(
                    "email" => set_value("email"),
                    "`password` = PASSWORD('" . set_value('password') . "')" => NULL
                ))) {
                    $user = $this->team_model->get_row(array(
                        "email" => set_value("email"),
                        "`password` = PASSWORD('" . set_value('password') . "')" => NULL
                    ));

                    $this->session->set_userdata('user_id', $user->user_id);
                    redirect('dashboard');
                } else
                    $this->view_data['errors'] = "Invalid email and/or password. Please check your data and try again";
            }
        }

        $this->view_data['title'] = 'Login';

        $this->load->view('login', $this->view_data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('team/login');
    }

    public function add()
    {
        if (!$this->user)
            redirect('team/login');

        $this->view_data['title'] = 'Add Team Member';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');

        if ($this->input->post()) {

            $this->form_validation->set_rules('firstname', 'First Name', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|min_length[1]|max_length[100]|valid_email');
            $this->form_validation->set_rules('tel', 'Telephone Number', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[1]|max_length[200]');
            $this->form_validation->set_rules('title', 'Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('description', 'Description', 'trim|min_length[0]');
            $this->form_validation->set_rules('linkedin', 'LinkedIn URL', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('slug', 'Slug', 'required|trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'firstname' => set_value('firstname'),
                    'lastname' => set_value('lastname'),
                    'email' => set_value('email'),
                    'tel' => set_value('tel') ? set_value('tel') : NULL,
                    'password' => '*' . strtoupper(hash('sha1', pack('H*', hash('sha1', set_value('password'))))),
                    'title' => set_value('title') ? set_value('title') : NULL,
                    'description' => set_value('description') ? set_value('description', NULL, FALSE) : NULL,
                    'linkedin' => set_value('linkedin') ? set_value('linkedin') : NULL,
                    'slug' => set_value('slug') ? set_value('slug') : NULL,
                    'deleted' => FALSE
                );

                $this->upload->initialize(array(
                    'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size' => 10240,
                    'max_width' => 1920,
                    'max_height' => 1080,
                    'encrypt_name' => TRUE
                ));

                if ($this->upload->do_upload('image')) {
                    chmod($this->upload->data('full_path'), 0777);
                    $data['image'] = $this->upload->data('file_name');
                } else {
                    $this->view_data['error'] = $this->upload->display_errors('', '');
                    $this->view_data['image'] = NULL;
                }

                if ($this->team_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'User created successfully.');
                    redirect('team');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('team/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($user_id)
    {
        if (!$this->user)
            redirect('team/login');

        if (!$user_id) redirect('team');

        $this->view_data['user'] = $this->team_model->get_row(
            array(
                'user_id' => $user_id
            )
        );
        if (!$this->view_data['user']) redirect('team');

        $this->view_data['title'] = 'Edit Team Member';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');

        if ($this->input->post()) {

            $this->form_validation->set_rules('firstname', 'First Name', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('tel', 'Telephone Number', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('password', 'Password', 'trim|min_length[0]|max_length[200]');
            $this->form_validation->set_rules('title', 'Title', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('description', 'Description', 'trim|min_length[0]');
            $this->form_validation->set_rules('linkedin', 'LinkedIn URL', 'trim|min_length[0]|max_length[100]');
            $this->form_validation->set_rules('slug', 'Slug', 'required|trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'firstname' => set_value('firstname'),
                    'lastname' => set_value('lastname'),
                    'tel' => set_value('tel') ? set_value('tel') : NULL,
                    'title' => set_value('title') ? set_value('title') : NULL,
                    'description' => set_value('description') ? set_value('description', NULL, FALSE) : NULL,
                    'linkedin' => set_value('linkedin') ? set_value('linkedin') : NULL,
                    'slug' => set_value('slug') ? set_value('slug') : NULL,
                );

                if (set_value('password'))
                    $data['password'] = '*' . strtoupper(hash('sha1', pack('H*', hash('sha1', set_value('password')))));

                $this->upload->initialize(array(
                    'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size' => 10240,
                    'max_width' => 1920,
                    'max_height' => 1080,
                    'encrypt_name' => TRUE
                ));

                if ($this->upload->do_upload('image')) {

                    if ($this->view_data['user']->image)
                        unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['user']->image);

                    chmod($this->upload->data('full_path'), 0777);
                    $data['image'] = $this->upload->data('file_name');
                } else {
                    $this->view_data['error'] = $this->upload->display_errors('', '');
                }

                if ($this->team_model->update_row(array('user_id' => $user_id), $data)) {
                    $this->session->set_flashdata('success', 'User updated successfully.');
                    redirect('team/edit/' . $user_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('team/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($user_id)
    {
        if (!$this->user)
            redirect('team/login');

        if (!$user_id) redirect('team');

        $this->view_data['user'] = $this->team_model->get_row(
            array(
                'user_id' => $user_id
            )
        );
        if (!$this->view_data['user']) redirect('team');

        /* Remove image
        if ($this->view_data['user']->image)
            unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['user']->image);
        */

        if ($this->team_model->update_row(array('user_id' => $user_id), array(
            'deleted' => TRUE
        ))) {
            $this->session->set_flashdata('success', 'User deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('team');
    }
}
