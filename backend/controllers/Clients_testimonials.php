<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_testimonials extends BOLD_Controller
{
    private $plugin = 'clients_testimonials';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('clients_testimonials_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->clients_testimonials_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->clients_testimonials_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('clients_model');
    }

    public function index($client_id = NULL)
    {
        if (!$client_id) redirect('clients');

        $this->view_data['client'] = $this->clients_model->get_row(
            array(
                'client_id' => $client_id
            )
        );
        if (!$this->view_data['client']) redirect('clients');

        $this->view_data['title'] = 'Testimonials';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $params = array(
            'deleted' => FALSE,
            'client_id' => $this->view_data['client']->client_id
        );
        $this->view_data['clients_testimonials'] = $this->clients_testimonials_model->get($params);

        $this->view_data['client_id'] = $this->view_data['client']->client_id;

        $this->load->view('header', $this->view_data);
        $this->load->view('clients_testimonials/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add($client_id = NULL)
    {
        if (!$client_id) redirect('clients');

        $this->view_data['client'] = $this->clients_model->get_row(
            array(
                'client_id' => $client_id
            )
        );
        if (!$this->view_data['client']) redirect('clients');

        $this->view_data['title'] = 'Add Testimonial';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('image_lib');

        if ($this->input->post()) {

            $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('content', 'Content', 'trim|min_length[1]');
            $this->form_validation->set_rules('vimeo_video_id', 'Vimeo Video ID', 'trim|min_length[1]');

            if ($this->form_validation->run()) {
                $data = array(
                    'client_id' => $this->view_data['client']->client_id,
                    'title' => set_value('title'),
                    'content' => set_value('content', '', FALSE),
                    'vimeo_video_id' => set_value('vimeo_video_id') ? set_value('vimeo_video_id') : NULL,
                    'date' => date('Y-m-d H:i:s'),
                    'deleted' => FALSE
                );

                if (isset($_FILES['image']) && $_FILES['image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('image')) {
                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['image'] = $this->upload->data('file_name');
                    } else {
                        $this->view_data['error'] = $this->upload->display_errors('', '');
                        $this->view_data['image'] = NULL;
                    }
                }

                if ($this->clients_testimonials_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'Testimonial created successfully.');
                    redirect('clients_testimonials/index/' . $client_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->view_data['client_id'] = $this->view_data['client']->client_id;

        $this->load->view('header', $this->view_data);
        $this->load->view('clients_testimonials/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($clients_testimonial_id)
    {
        if (!$clients_testimonial_id) redirect('clients_testimonials');

        $this->view_data['clients_testimonial'] = $this->clients_testimonials_model->get_row(
            array(
                'clients_testimonial_id' => $clients_testimonial_id
            )
        );
        if (!$this->view_data['clients_testimonial']) redirect('clients_testimonials');

        $this->view_data['title'] = 'Edit Testimonial';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('image_lib');

        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('content', 'Content', 'trim|min_length[1]');
            $this->form_validation->set_rules('vimeo_video_id', 'Vimeo Video ID', 'trim|min_length[1]');

            if ($this->form_validation->run()) {
                $data = array(
                    'title' => set_value('title'),
                    'content' => set_value('content', '', FALSE),
                    'vimeo_video_id' => set_value('vimeo_video_id') ? set_value('vimeo_video_id') : NULL,
                    'date' => date('Y-m-d H:i:s')
                );

                if (isset($_FILES['image']) && $_FILES['image']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $this->upload->initialize(array(
                        'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'encrypt_name' => TRUE
                    ));

                    if ($this->upload->do_upload('image')) {

                        if ($this->view_data['clients_testimonial']->image)
                            unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_testimonial']->image);

                        chmod($this->upload->data('full_path'), 0777);

                        $image_size = getimagesize($this->upload->data('full_path'));
                        if ($image_size && count($image_size) >= 2) {
                            list($width, $height) = $image_size;
                            if ($width > 800) {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload->data('full_path');
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 800;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                        }

                        $data['image'] = $this->upload->data('file_name');
                    } else {
                        $this->view_data['error'] = $this->upload->display_errors('', '');
                    }
                }

                if ($this->clients_testimonials_model->update_row(array('clients_testimonial_id' => $clients_testimonial_id), $data)) {
                    $this->session->set_flashdata('success', 'Testimonial updated successfully.');
                    redirect('clients_testimonials/edit/' . $clients_testimonial_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('clients_testimonials/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($clients_testimonial_id)
    {
        if (!$clients_testimonial_id) redirect('clients_testimonials');

        $this->view_data['clients_testimonial'] = $this->clients_testimonials_model->get_row(
            array(
                'clients_testimonial_id' => $clients_testimonial_id
            )
        );
        if (!$this->view_data['clients_testimonial']) redirect('clients_testimonials');

        /* Remove image
        if ($this->view_data['clients_testimonial']->image)
            unlink(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->view_data['clients_testimonial']->image);
        */

        if ($this->clients_testimonials_model->update_row(array('clients_testimonial_id' => $clients_testimonial_id), array(
            'deleted' => TRUE
        ))) {
            $this->session->set_flashdata('success', 'Testimonial deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('clients_testimonials/index/' . $this->view_data['clients_testimonial']->client_id);
    }
}
