<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends BOLD_Controller
{
    private $plugin = 'dashboard';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        $this->load->model('dashboard_model');
        if (!$this->plugins_model->plugin_is_set() || !$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->dashboard_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->dashboard_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index()
    {
        $this->view_data['title'] = 'Dashboard';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['statistics'] = array();
        foreach ($this->plugins as $plugin) {
            if (isset($plugin['widget_statistic_enabled']) && $plugin['widget_statistic_enabled']) {
                $this->load->model($plugin['slug'] . "_model");
                $this->view_data['statistics'][] = array(
                    "icon" => $plugin['icon'],
                    "title" => $plugin['widget_statistic_title'],
                    "count" => isset($plugin['widget_statistic_override_number']) ? number_format($plugin['widget_statistic_override_number'], 0) : number_format($this->{$plugin['slug'] . "_model"}->get_count(), 0)
                );
            }
        }

        $this->view_data['widgets'] = array();
        foreach ($this->plugins as $plugin) {
            if (isset($plugin['widget_block_enabled']) && $plugin['widget_block_enabled']) {
                $view_data = $plugin['widget_block_view_data']($this);
                $this->view_data['widgets'][$plugin['widget_block_sort']] = $this->load->view($plugin['slug'] . "/" . "widget", $view_data, TRUE);
            }
        }
        ksort($this->view_data['widgets']);

        $this->load->view('header', $this->view_data);
        $this->load->view('index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }
}
