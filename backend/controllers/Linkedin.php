<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linkedin extends BOLD_Controller
{
    private $plugin = 'linkedin';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('linkedin_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->linkedin_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->linkedin_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');

        $this->load->model('bullhorn_model');
        $access_token = $this->bullhorn_model->get_row(array(), 'created', 'DESC');
        if ($access_token) {
            if (time() - ((int)$access_token->created + (int)$access_token->expires) > 30000) {
                $this->view_data['notification'] = "BullHorn Integration need to be reauthorized. Click <a href='" . site_url('bullhorn/auth') . "'>HERE</a> to continue";
            }
        }
    }

    public function index()
    {
        $this->view_data['title'] = 'LinkedIn Integration';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->model('settings_model');
        $this->load->library('form_validation');
        if ($this->input->post()) {

            $this->form_validation->set_rules('linkedin_id', 'App Client ID', 'required|trim|min_length[1]');
            $this->form_validation->set_rules('linkedin_secret', 'App Secret', 'required|trim|min_length[1]');

            if ($this->form_validation->run()) {

                if ($this->settings_model->get_count(array('name' => 'linkedin_id')) === 0) {
                    $this->settings_model->add_row(array(
                        'name' => 'linkedin_id',
                        'title' => 'LinkedIn App Client ID',
                        'value' => set_value('linkedin_id', '', FALSE)
                    ));
                } else {
                    $this->settings_model->update_row(array(
                        'name' => 'linkedin_id'
                    ), array(
                        'title' => 'LinkedIn App Client ID',
                        'value' => set_value('linkedin_id', '', FALSE)
                    ));
                }

                if ($this->settings_model->get_count(array('name' => 'linkedin_secret')) === 0) {
                    $this->settings_model->add_row(array(
                        'name' => 'linkedin_secret',
                        'title' => 'LinkedIn App Secret',
                        'value' => set_value('linkedin_secret', '', FALSE)
                    ));
                } else {
                    $this->settings_model->update_row(array(
                        'name' => 'linkedin_secret'
                    ), array(
                        'title' => 'LinkedIn App Secret',
                        'value' => set_value('linkedin_secret', '', FALSE)
                    ));
                }
                $this->session->set_flashdata('success', 'Configuration updated.');
                redirect('linkedin');
            }
        }

        $this->view_data['linkedin_id'] = NULL;
        $linkedin_id = $this->settings_model->get_row(array('name' => 'linkedin_id'));
        if ($linkedin_id && isset($linkedin_id->value))
            $this->view_data['linkedin_id'] = $linkedin_id->value;

        $this->view_data['linkedin_secret'] = NULL;
        $linkedin_secret = $this->settings_model->get_row(array('name' => 'linkedin_secret'));
        if ($linkedin_secret && isset($linkedin_secret->value))
            $this->view_data['linkedin_secret'] = $linkedin_secret->value;

        $this->load->view('header', $this->view_data);
        $this->load->view('linkedin/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }
}