<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjobs extends BOLD_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function reed() // https://www.reed.co.uk/developers/Recruiter
    {
        $this->load->model('vacancies');
        $this->load->config('reed');

        /* Permitted Data Values */

        $jobType = array(
            "permanent" => 1,
            "contract" => 2,
            "temporary" => 4
        );

        $locations = array( // TODO: update indexes to fit locations ID
            1 => "East Anglia",
            2 => "East Midlands",
            3 => "London",
            4 => "North East",
            5 => "North West",
            6 => "Northern Ireland",
            7 => "Scotland",
            244 => "South West",
            313 => "Wales",
            314 => "West Midlands",
            612 => "Yorkshire",
            823 => "UK",
            901 => "Eire",
            1012 => "Europe",
            1075 => "South East"
        );


        /* Functions */

        function send_request($url, $method, $data, $api_key, $client_id)
        {
            $user_agent = 'Mozilla/5.0 (X11; Linux x86_64) PHP/' . phpversion();
            $host = "www.reed.co.uk";
            $dt = new DateTime('now', new DateTimeZone('UTC'));
            $timestamp = $dt->format('Y-m-d\TH:i:s') . '+00:00';

            $base_string = $method . $user_agent . urldecode($url) . $host . $timestamp;
            $signature = base64_encode(hash_hmac('sha1', $base_string, $api_key, TRUE));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "ContentType: application/x-www-form-urlencoded",
                "Method: " . $method,
                "User-Agent: " . $user_agent,
                "X-ApiSignature: " . $signature,
                "X-ApiClientId: " . $client_id,
                "X-TimeStamp: " . $timestamp,
            ));
            curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);

            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);

            if ($output)
                $output = json_decode($output);

            return array($output, $info);
        }

        function post_advert($data, $api_key, $client_id, &$message = NULL)
        {
            $url = "https://www.reed.co.uk/recruiter/api/1.0/jobs";
            $method = "POST";

            list($message, $info) = send_request($url, $method, $data, $api_key, $client_id);

            return $info['http_code'] === 201;
        }


        /* ------ EXAMPLE ------ */
        /* Loading data from database */

        $conn = new mysqli("localhost", "root", "a", "jobsfeed");
        if ($conn->connect_error) die($conn->connect_error);

        $query = $conn->query("SELECT * FROM ignite_vacancies;");
        if ($conn->error) die($conn->error);

        $message = NULL;
        while ($row = $query->fetch_object()) {
            $new_post_data = array(
                /* Required */
                "username" => $this->config->item('reed_username'),                    //Your reed.co.uk account username.
                "expiryInDays" => 42,                       //The number of days the job has to be on the website. Max of 42.
                "jobType" => $jobType[$row->contract_type], //The type of the job. Possible values: 1 = Permanent, 2 = Contract, 4 = Temporary
                "workingHours" => 3,                        //The job working hours. Possible values: 1 = Full time, 2 = Part time, 3 = Full or Part time
                "description" => $row->description,         //The job description must be between 150 and 6,500 characters. It cannot contain email addresses, URLs or phone numbers.
                "title" => $row->title,                     //The job title must be no more than 255 characters. It cannot contain email addresses, html, URLs or phone numbers.
                "townName" => $locations[$row->location],   //The job location (e.g. WC2B 5LX, London, Newcastle upon Tyne, Covent Garden). Must be no more than 255 characters. It cannot contain email addresses, html, URLs or phone numbers. It is recommended to provide a UK postcode as this allows for more accurate searching. The townName parameter can only accept postcode and names of towns within the UK.

                /* For optional check URL: https://www.reed.co.uk/developers/Recruiter#job-POST */
            );

            if (post_advert($new_post_data, $this->config->item('reed_posting_key'), $this->config->item('reed_client_id'), $message)) {
                echo "Post successfully added<br/>";
            } else {
                echo "Error occurred while posing job to Reed.co.uk: " . $message->message . "<br/>";
                break;
            }
        }

        exit;
    }

}