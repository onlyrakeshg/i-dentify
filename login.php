<?php $page_allowed = 1;

include("inc/support/common.php");
include("backend/config/config.php");

if (isset($_POST['log_in'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $chkUserExists = $db->prepare("SELECT * FROM `candidates_portal` WHERE `email` = :email AND `password` = PASSWORD(:password) LIMIT 0,1");
    $chkUserExists->execute(array('email' => $email, 'password' => $password));
    $totalUsers = $chkUserExists->rowCount();
    $chkUserExists = $chkUserExists->fetch();
    if ($totalUsers > 0) {
        $_SESSION['candidate_logged_in'] = 1;
        $_SESSION['candidate_id'] = $chkUserExists['candidate_id'];
        header("Location: " . $websiteURL . "profile");
    } else {
        header("Location: " . $websiteURL . "talent-community?loginfailed=1");
    }
}

if (isset($_POST['submit'])) {
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $name = $firstname . " " . $lastname;
    $email = $_POST['email'];
    $tel = $_POST['tel'];
    $password = $_POST['password'];
    $password2 = $_POST['password2'];

    if ($firstname == "") {
        $error['firstname'] = '1';
    }
    if ($lastname == "") {
        $error['lastname'] = '1';
    }
    if ($email == "") {
        $error['email'] = '1';
    }
    if ($tel == "") {
        $error['tel'] = '1';
    }
    if ($password == "") {
        $error['password'] = '1';
    }
    if ($password2 == "") {
        $error['password2'] = '1';
    }
    if ($password != $password2) {
        $error['password_mismatch'] = '1';
    }

    $file = uploadFile('cv', false, true);

    $gdpr = $_POST['gdpr'];

    $_SESSION['state'] = rand(00000000, 99999999);

    $chkUserExists = $db->prepare("SELECT * FROM `candidates_portal` WHERE `email` = :email LIMIT 0,1");
    $chkUserExists->execute(array('email' => $email));
    $totalUsers = $chkUserExists->rowCount();
    $chkUserExists = $chkUserExists->fetch();
    if ($totalUsers > 0) {
        $error['email_already_exists'] = 1;
    }

    if (!is_array($error)) {

        $createProfile = $db->prepare("INSERT INTO `candidates_portal` SET `firstname` = :firstname, `lastname` = :lastname, `email` = :email, `tel` = :tel, `password` = PASSWORD(:password), `date` = CURDATE(), `cv` = :cv");

        $createProfile->execute(array('firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'tel' => $tel, 'password' => $password, "cv" => $file['filename']));

        $latestUser = $db->lastInsertId();

        $chkUserExists = $db->prepare("SELECT * FROM `candidates_portal` WHERE `candidate_id` = :id LIMIT 0,1");
        $chkUserExists->execute(array('id' => $latestUser));
        $totalUsers = $chkUserExists->rowCount();
        $chkUserExists = $chkUserExists->fetch();

        /* BullHorn Integration */

        include_once "inc/support/bullhorn.php";

        $access_token = get_token();
        if ($access_token) {
            if (((int)$access_token->created + (int)$access_token->expires) < (int)time()) {
                refresh_token($access_token);
                $access_token = get_token();
            }

            $bh_token = $bh->get_login($access_token->access_token);
            if (isset($bh_token->errorCode) && $bh_token->errorCode == 400) {
                refresh_token($access_token);
                $access_token = get_token();
                $bh_token = $bh->get_login($access_token->access_token);
            }

            if (isset($bh_token->BhRestToken) && isset($bh_token->restUrl)) {
                $bh_data = array(
                    "firstName" => $firstname, // Field name
                    "lastName" => $lastname, // Surname
                    "name" => $firstname . " " . $lastname,
                    "email" => $email, // Personal Email
                    "mobile" => $tel, // Mobile Phone
                    "source" => "Website",
                    "status" => "Available",
                    "isDeleted" => FALSE,
                );


                $bh_candidate_id = NULL;
                $file_id = NULL;
                $search_candidate = $bh->get_json($bh_token->restUrl . "search/Candidate" . "?" . http_build_query(array("query" => "email:" . $email, "fields" => "id")), NULL, array("BHRestToken: " . $bh_token->BhRestToken));
                if ($search_candidate && isset($search_candidate->total) && $search_candidate->total > 0) {
                    $candidates = array_values($search_candidate->data);
                    $bh_candidate_id = $candidates[0]->id;
                } else {
                    $candidate = $bh->get_json($bh_token->restUrl . "entity/Candidate", json_encode($bh_data), array("Content-Type: application/json", "BHRestToken: " . $bh_token->BhRestToken), "PUT"); // Create candidate for each submission
                    if ($candidate && $candidate->changedEntityId) {
                        $bh_candidate_id = $candidate->changedEntityId;
                    }
                }

                if ($bh_candidate_id !== NULL) {
                    $updateProfile = $db->prepare("
                            UPDATE
                                `candidates_portal`
                            SET
                                `bh_candidate_id` = :bh_candidate_id
                            WHERE
                                `candidate_id` = :candidate_id
                                LIMIT 1;
                            ");

                    $params = array(
                        'bh_candidate_id' => $bh_candidate_id,
                        'candidate_id' => $chkUserExists['candidate_id'],
                    );

                    $updateProfile->execute($params);
                }
            } else {
                //echo "No Access Token 1";
            }
        } else {
            //echo "No Access Token";
        }

        /* END BullHorn Integration */


        if ($totalUsers > 0) {
            $_SESSION['candidate_logged_in'] = 1;
            $_SESSION['candidate_id'] = $chkUserExists['candidate_id'];
            header("Location: " . $websiteURL . "profile");
        }

    } else {

        echo "ERROR";

        foreach ($error as $i => $value) {

            echo $i . " = " . $value . "<br>";

        }

    }

}

$page = "Login / Register";

?>
<!DOCTYPE html>
<html lang="en">


<head>

    <?php include("inc/head-includes.php"); ?>

    <link href="<?= $config['website_url']; ?>css/profile.css" rel="stylesheet">

    <style type="text/css">
        .banner .curved-element {
            border: 0px;
        }
    </style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">
    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
                <?php include("inc/header.php"); ?>
            </div>
        </div>
        <div class="content">
            <div class="banner small-banner  about-bn">


                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">Login / Register</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>

            <section class="vacancy-page-info" id="login">
                <div class="container">
                    <div class="container">
                        <div class="left-col col-md-4 col-sm-12">
                            <h2><i class="fa fa-lock" aria-hidden="true"></i> Log In</h2>
                            <div class="other-cont apply-login">
                                <form method="post">
                                    <div class="other-contents">
                                        <?php if (isset($_GET['loginfailed']) && $_GET['loginfailed'] == 1) { ?>
                                            <span style="color: red;">
                                Login details invalid. Please check and re-try.
                            </span>
                                            <br/>
                                        <?php } ?>
                                        <label for="email">Email Address</label>
                                        <input type="text" placeholder="email@example.com" name="email">
                                        <label for="email">Password</label>
                                        <input type="password" name="password">
                                        <input type="submit" name="log_in" value="Log In">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="right-col col-md-8 col-sm-12">
                            <div id="vacancy-information" class="apply-form nomargintop">
                                <h1>Create a New Account</h1>
                                <h2>By creating an account below you'll get these benefits:</h2>
                                <ul>

                                    <li><i class="fas fa-check-double"></i>&nbsp;&nbsp;Weekly job alerts based on your
                                        chosen criteria.
                                    </li>
                                    <li><i class="fas fa-check-double"></i>&nbsp;&nbsp;Ability to create your own
                                        profile & upload your CV.
                                    </li>
                                    <li><i class="fas fa-check-double"></i>&nbsp;&nbsp;Fast track your applications
                                        using your saved profile details.
                                    </li>
                                    <li><i class="fas fa-check-double"></i>&nbsp;&nbsp;Keep track of your previous
                                        application history.
                                    </li>
                                </ul>
                                <p>&nbsp;</p>
                                <form action="" method="post" enctype="multipart/form-data">
                                    <div class="form_column">
                                        <input type="text" name="firstname" placeholder="Firstname" required
                                               <?php

                                               $expName = explode(" ", $_GET['name']);

                                               if (isset($_GET['name'])) { ?>value="<?= $expName[0]; ?>"
                                               <?php } else { ?>value="<?= $_POST['firstname']; ?>"<?php } ?>
                                               <?php if ($error['firstname'] == 1) { ?>style="border-color: red;"<?php } ?>>
                                    </div>
                                    <div class="form_column">
                                        <input type="text" name="lastname" placeholder="Lastname" required
                                               <?php

                                               $expName = explode(" ", $_GET['name']);

                                               if (isset($_GET['name'])) { ?>value="<?= $expName[1]; ?>"
                                               <?php } else { ?>value="<?= $_POST['firstname']; ?>"<?php } ?>
                                               <?php if ($error['lastname'] == 1) { ?>style="border-color: red;"<?php } ?>>
                                    </div>
                                    <div class="form_column">
                                        <input type="email" name="email" placeholder="Email Address" required
                                               <?php if (isset($_GET['name'])) { ?>value="<?= $_GET['email']; ?>"
                                               <?php } else { ?>value="<?= $_POST['firstname']; ?>"<?php } ?>
                                               <?php if ($error['email'] == 1) { ?>style="border-color: red;"<?php } ?>>
                                    </div>
                                    <div class="form_column">
                                        <input type="tel" name="tel" placeholder="Telephone Number" required
                                               pattern="^\+?\d{0,13}"
                                               value="<?= $_POST['tel']; ?>"
                                               <?php if ($error['tel'] == 1) { ?>style="border-color: red;"<?php } ?>>
                                    </div>
                                    <div class="form_column">
                                        <input type="password" name="password" placeholder="Create New Password"
                                               required
                                               <?php if ($error['password'] == 1 || $error['password_mismatch'] == 1) { ?>style="border-color: red;"<?php } ?>>
                                    </div>
                                    <div class="form_column">
                                        <input type="password" name="password2" placeholder="Confirm New Password"
                                               required
                                               <?php if ($error['password2'] == 1 || $error['password_mismatch'] == 1) { ?>style="border-color: red;"<?php } ?>>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form_column_full">
                                        <label for="gdpr" class="gdpr">
                                            <input name="gdpr" type="checkbox" id="gdpr" value="1" required> I
                                            have read and agree
                                            with the <a href="<?= $config['website_url']; ?>privacy/" target="_blank">Privacy
                                                Policy</a>
                                            including
                                            GDPR guidelines.
                                        </label>
                                        <input type="submit" name="submit" id="submit" value="Create Your New Account">
                                    </div>
                                </form>
                            </div>
                        </div>
            </section>

        </div>
        <?php include("inc/footer.php"); ?>

        <?php include("inc/modals.php"); ?>

        <?php include("inc/footer-js.php"); ?>

</body>
</html>