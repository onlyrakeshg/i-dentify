<?php 

$page_allowed = 1;
include("inc/support/common.php");include("backend/config/config.php"); 

if(isset($_GET['id'])) {
	
	$id = $_GET['id'];
	
	$getArticle = $db->prepare("SELECT * FROM `blog` WHERE `slug` = :id");
	$getArticle->execute(array("id" => $id));
	$getArticle = $getArticle->fetch();
	
	if(!$getArticle) {
		header('Location: '.$websiteURL.'blog');
		exit;
	}

	
} else {
$getNews = $db->prepare("SELECT * FROM `blog` WHERE `deleted` = '0' ORDER BY `blog_id` DESC");
$getNews->execute();
$newsCount = $getNews->rowCount();
$getNews = $getNews->fetchAll();
}


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php include("inc/head-includes.php"); ?>
	
	<style type="text/css">
		.about-holder { margin-top: 70px; margin-bottom: 70px;  }
		.banner .curved-element { border: 0px; }
		.content { margin-bottom: 70px; }
	</style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">
    
    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
                <?php include("inc/header.php"); ?>
            </div>
        </div>
        <div class="content">
            <div class="banner small-banner about-bn">
                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">Blog</span>
                                        <!--<h1 class="heading">Lorem Ipsum is simply dummy text</h1>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>
            <div class="main-content">
                
                <div class="white-bg content text-center">
                    
                    <div class="container">
                        <div class="row no-gutters match-height about-holder">
							
							
                            <?php if(isset($getArticle)) { ?>
								<h1 class="blogarticleheading"><?php echo $getArticle['title']; ?></h1>
								<div class="row blogarticle">
									<div class="col-sm-12">	
										<?php echo $getArticle['content']; ?>
									</div>
								</div>
							<?php } else { ?>
								<div class="row blogsec">
									<?php $count = 1; foreach ($getNews as $row) { ?>
										<div class="col-md-4 col-sm-4">
											<article class="postbox">
												<figure><a href="<?php echo $config['website_url'];?>blog/<?php echo $row['slug']; ?>"><img src="<?= $config['website_url']; ?>uploads/images/<?= $row['image']; ?>" alt="blog-image" width="100%"/></a></figure>
												<h4><a href="<?php echo $config['website_url'];?>blog/<?php echo $row['slug']; ?>"><?php echo $row['title']; ?></a></h4>
												<p><?php echo substr(strip_tags($row['content']), 0, 100); ?>...</p>
											</article>
										</div>
									<?php if($count % 3 == 0):?><div class="clearfix"></div><?php endif; ?>
									<?php $count++; } ?>
								</div>
							<?php } ?>	

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("inc/footer.php"); ?>

<?php include("inc/modals.php"); ?>

<?php include("inc/footer-js.php"); ?>




</body>
</html>