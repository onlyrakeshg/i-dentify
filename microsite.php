<?php

$page_allowed = 1;

require_once "inc/support/common.php";

ini_set('display_errors', 1);

$ref = get('ref');

$page = "companyprofile";

$getClient = $db->prepare("
    SELECT
        *
    FROM
        `clients`
    WHERE
        `ref` = :ref
;");
$getClient->execute(array("ref" => $ref));

if ($getClient->rowCount() === 0) {
    header("Location: " . siteUrl());
    exit;
}

$getClient = $getClient->fetch();
$client_id = $getClient['client_id'];

$getClientSectors = $db->prepare("
    SELECT
        `cs`.*,
        `is`.`name` AS `industry_sector_name`
    FROM
        `clients_sectors` `cs`
    LEFT JOIN
        `industry_sectors` `is` ON `is`.`industry_sector_id` = `cs`.`industry_sector_id`
    WHERE
        `cs`.`client_id` = :client_id
;");
$getClientSectors->execute(array("client_id" => $client_id));
$getClientSectors = $getClientSectors->fetchAll();

$getClientLocations = $db->prepare("
    SELECT
        `cl`.*,
        `l`.`name` AS `location_name`
    FROM
        `clients_locations` `cl`
    LEFT JOIN
        `locations` `l` ON `l`.`location_id` = `cl`.`location_id`
    WHERE
        `cl`.`client_id` = :client_id
;");
$getClientLocations->execute(array("client_id" => $client_id));
$getClientLocations = $getClientLocations->fetchAll();

$getPhotos = $db->prepare("
    SELECT
        *
    FROM
        `photos`
    WHERE
          `client_id` = :client_id AND
          `deleted` = FALSE
;");
$getPhotos->execute(array("client_id" => $client_id));
$getPhotos = $getPhotos->fetchAll();

if (is_array($getPhotos) && count($getPhotos) > 0) {
    $photosURLs = array_map(function ($photo) {
        if ($photo['image'])
            return $photo['image'];
    }, $getPhotos);
}

$getVacancies = $db->prepare("
    SELECT
        *
    FROM
        `vacancies`
    WHERE
        `client_id` = :client_id AND
        `deleted` = FALSE
;");
$getVacancies->execute(array("client_id" => $client_id));
$getVacancies = $getVacancies->fetchAll();

if (is_array($getVacancies) && count($getVacancies) > 0) {
    foreach ($getVacancies as $i => $vacancy) {
        $getVacancies[$i]['sectors'] = array();
        $getVacancySectors = $db->prepare("
            SELECT
                `vs`.*,
                `is`.`name` AS `industry_sector_name`
            FROM
                `vacancies_sectors` `vs`
            LEFT JOIN
                `industry_sectors` `is` ON `is`.`industry_sector_id` = `vs`.`industry_sector_id`
            WHERE
                `vs`.`vacancy_id` = :vacancy_id
        ;");
        $getVacancySectors->execute(array("vacancy_id" => $vacancy['vacancy_id']));
        $getVacancySectors = $getVacancySectors->fetchAll();
        if (is_array($getVacancySectors) && count($getVacancySectors) > 0)
            $getVacancies[$i]['sectors'] = $getVacancySectors;
        $getVacancies[$i]['locations'] = array();
        $getVacancyLocations = $db->prepare("
            SELECT
                `vl`.*,
                `l`.`name` AS `location_name`
            FROM
                `vacancies_locations` `vl`
            LEFT JOIN
                `locations` `l` ON `l`.`location_id` = `vl`.`location_id`
            WHERE
                  `vl`.`vacancy_id` = :vacancy_id
        ;");
        $getVacancyLocations->execute(array("vacancy_id" => $vacancy['vacancy_id']));
        $getVacancyLocations = $getVacancyLocations->fetchAll();
        if (is_array($getVacancyLocations) && count($getVacancyLocations) > 0)
            $getVacancies[$i]['locations'] = $getVacancyLocations;
    }
}

$getContentPages = $db->prepare("
    SELECT
        *
    FROM
        `clients_content_pages`
    WHERE
        `client_id` = :client_id AND
        `deleted` = FALSE
    ORDER BY
        `sort` DESC
;");
$getContentPages->execute(array("client_id" => $client_id));
$getContentPages = $getContentPages->fetchAll();

$getTestimonials = $db->prepare("
    SELECT
        *
    FROM
        `clients_testimonials`
    WHERE
        `client_id` = :client_id AND
        `deleted` = FALSE
;");
$getTestimonials->execute(array("client_id" => $client_id));
$getTestimonials = $getTestimonials->fetchAll();

$getVideos = $db->prepare("
    SELECT
        *
    FROM
        `videos`
    WHERE
        `client_id` = :client_id AND
        `deleted` = FALSE
;");
$getVideos->execute(array("client_id" => $client_id));
$getVideos = $getVideos->fetchAll();

$getOffices = $db->prepare("
    SELECT
        *
    FROM
        `offices`
    WHERE
        `client_id` = :client_id AND
        `deleted` = FALSE
;");
$getOffices->execute(array("client_id" => $client_id));
$getOffices = $getOffices->fetchAll();

if (is_array($getOffices) && count($getOffices) > 0) {
    $markers = array();
    foreach ($getOffices as $office) {
        list($lat, $lng) = explode(",", $office['coordinates']);
        $markers[] = array(
            "lat" => floatval($lat),
            "lng" => floatval($lng),
        );
    }
}

$getMapsKey = $db->prepare("
    SELECT
        *
    FROM
        `settings`
    WHERE
        `name` = :name
    LIMIT 1
;");
$getMapsKey->execute(array("name" => "maps_api_key"));
$getMapsKey = $getMapsKey->fetch();

if ($getMapsKey)
    $maps_api_key = $getMapsKey['value'];

$getOG = $db->prepare("
    SELECT
        *
    FROM
        `settings`
    WHERE
        `name` = :og_name
    LIMIT 0,1");
$getOG->execute(array("og_name" => "client_microsite_ogi"));
$getOG = $getOG->fetch();

$getHandle = $db->prepare("
    SELECT
        *
    FROM
        `settings`
    WHERE
          `name` = :twitter_handle
    LIMIT 0,1");
$getHandle->execute(array("twitter_handle" => "twitter_handle"));
$getHandle = $getHandle->fetch();

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $getClient['meta_desc']; ?>">
    <meta name="author" content="Identify">

    <meta property="og:title" content="<?php echo $getClient['meta_title']; ?>">
    <meta property="og:description" content="<?php echo $getClient['meta_desc']; ?>">
    <meta property="og:keywords" content="<?php echo $getClient['meta_keywords']; ?>">
    <?php if ($getOG['value']) { ?>
        <meta property="og:image"
              content="<?php echo siteUrl('uploads/images/' . $getOG['value']); ?>">
    <?php } ?>
    <meta property="og:url" content="<?php echo siteUrl('microsite/' . $getClient['ref']); ?>">

    <meta name="twitter:title" content="<?php echo $getClient['meta_title']; ?>">
    <meta name="twitter:description" content="<?php echo $getClient['meta_desc']; ?>">
    <meta name="twitter:keywords" content="<?php echo $getClient['meta_keywords']; ?>">
    <?php if ($getOG['value']) { ?>
        <meta name="twitter:image"
              content="<?php echo siteUrl() . 'uploads/images/' . $getOG['value']; ?>">
    <?php } ?>

    <title>Identify / <?php echo $getClient['client_name']; ?></title>

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-9ralMzdK1QYsk4yBY680hmsb4/hJ98xK3w0TIaJ3ll4POWpWUYaA2bRjGGujGT8w" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600,700" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo siteUrl('slick/slick.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo siteUrl('slick/slick-theme.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo siteUrl('css/styles.css'); ?>">
    <link rel="stylesheet" href="<?php echo siteUrl('css/responsive.css'); ?>">
    <link rel="stylesheet" href="<?php echo siteUrl('css/animate.css'); ?>">
    <link rel="stylesheet" href="<?php echo siteUrl('css/drop_uploader.css'); ?>">
    <link rel="stylesheet" href="<?php echo siteUrl('css/microsites.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo siteUrl('blocks/lightbox2-master/dist/css/lightbox.css'); ?>">
    <link rel="stylesheet" href="<?php echo siteUrl('css/bootstrap-reboot.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo siteUrl('css/bootstrap.css'); ?>">
    <link rel="stylesheet" href="<?php echo siteUrl('css/bootstrap-grid.min.css'); ?>">

    <link rel="icon" href="<?php echo siteUrl('images/favicon.png'); ?>" type="image/png"/>

    <style type="text/css">
        <?php if($getClient['image']) { ?>

        .overvideo-inner {
            background: url("<?php echo siteUrl() . 'uploads/images/'. $getClient['image']; ?>") center top no-repeat !important;
            background-size: cover !important;
        }

        <?php } ?>

        <?php if($getClient['opportunities_image']) { ?>

        .company-sec.sub-page::before {
            background: url(<?php echo siteUrl() . 'uploads/images/'. $getClient['opportunities_image']; ?>) bottom center no-repeat;
            background-size: cover;
        }

        <?php } ?>

        .banner .curved-element {
            background: #f9f9f9;
            width: 150%;
            height: 50%;
            bottom: -40%;
            left: -25%;
            border-radius: 60%;
        }

        .small-banner {
            height: 600px !important;
        }

        .small-banner .heading-text {
            margin-top: 0 !important;
            display: inline-block !important;
        }

        .overlay.open {
            z-index: 10000;
        }

        .top-header {
            padding-top: 32px;
        }

        .vacancy-details {
            min-height: 150px;
        }

        .vacancy-details li {
            display: block !important;
            width: 100% !important;
            border-radius: 0 !important;
            padding: 9px 0;
        }

        .vacancy-details li span {
            padding-left: 10px !important;
        }

        .brand img {
            width: 100%;
            max-width: 262px;
        }
    </style>
</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">
    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
                <a href="#" id="navbtn"><i class="fal fa-bars"></i></a>
                <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-6">
                        <a href="<?php echo siteUrl(); ?>" class="brand"> <img
                                    src="<?php echo siteUrl('images/logo.png'); ?>"
                                    alt="Bold Identities"> </a>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-6">
                        <div class="loc-part text-right">
                            <div class="row">
                                <div class="col-12">
                                    <div class="header-social">
                                        <a href="https://www.instagram.com/explore/locations/558036051346350/identify-hr-resourcing-solutions-ltd/?hl=en"
                                           target="_blank">
                                            <i class="fab fa-instagram" aria-hidden="true"></i>
                                        </a>
                                        <a href="https://www.facebook.com/identifyhr/" target="_blank">
                                            <i class="fab fa-facebook" aria-hidden="true"></i>
                                        </a>
                                        <a href="https://www.linkedin.com/company/identify-hr-resourcing-solutions-ltd/"
                                           target="_blank">
                                            <i class="fab fa-linkedin" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <?php if (isset($_SESSION['candidate_logged_in'])) { ?>
                                        <a href="<?php echo siteUrl('profile'); ?>" class="login-register">
                                            <i class="fal fa-user"></i>&nbsp;&nbsp;Profile
                                        </a>
                                        &nbsp;&nbsp;
                                        <a href="<?php echo siteUrl('logout'); ?>" class="login-register">
                                            <i class="fal fa-lock-alt"></i>&nbsp;&nbsp;Log Out
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php echo siteUrl('login'); ?>" class="login-register">
                                            <i class="fal fa-lock-alt"></i>&nbsp;&nbsp;Login / Register
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="basic-nav">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="banner small-banner about-bn">
                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="main-logo wow fadeInUp"
                                         style="background: url(<?php echo siteUrl('uploads/images/' . $getClient['logo']); ?>) center center no-repeat;">
                                    </div>
                                    <h2 class="main-title maxwidth90 wow fadeInUp" data-wow-delay="1s" data-wow-duration="1s">
                                        <?php echo $getClient['client_name']; ?>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (stristr($getClient['image'], "mp4")) { ?>
                        <video class="m-visible" id="bgvid" playsinline
                               autoplay muted loop>
                            <source src="<?php echo siteUrl() . 'uploads/images/' . $getClient['image']; ?>"
                                    type="video/mp4">
                        </video>
                    <?php } ?>
                </div>
                <div class="curved-element"></div>
            </div>

            <div class="main-wrapper">
                <div class="employer-sec-page">
                    <div class="container">
                        <div class="employer-cont">
                            <ul>
                                <li>
                                    <div class="img-sec"
                                         style="height:450px; background: url(<?php echo $getClient['key_image'] ? siteUrl() . 'uploads/images/' . $getClient['key_image'] : (isset($photosURLs) ? siteUrl() . 'uploads/images/' . $photosURLs[array_rand($photosURLs)] : siteUrl() . 'images/key-bg.png'); ?>) center center no-repeat; background-size: cover; border-top-left-radius: 10px;">
                                    </div>
                                </li>
                                <li>
                                    <div class="img-cont">
                                        <h1 title="keyinfo" style="margin-bottom: 20px;">Key
                                            <span>Information</span><br></h1>
                                        <ul>
                                            <li>
                                                <span>Website</span>
                                                <span><a href="<?php echo $getClient['website']; ?>" target="_blank">
                                            <?php echo str_replace("https://", "", $getClient['website']); ?>
                                        </a></span>
                                            </li>
                                            <li>
                                                <span>Company Size</span>
                                                <span><?php echo number_format($getClient['company_size'], 0); ?></span>
                                            </li>
                                            <li>
                                                <span>Headquarters</span>
                                                <span><a id="overview">
                                            <?php echo implode(", ", array_map(function ($location) {
                                                return $location['location_name'];
                                            }, $getClientLocations)); ?>
                                        </a></span>
                                            </li>
                                            <li>
                                                <span>Industry</span>
                                                <span><?php echo implode(", ", array_map(function ($industry_sector) {
                                                        return $industry_sector['industry_sector_name'];
                                                    }, $getClientSectors)); ?></span>
                                            </li>
                                            <li>
                                                <span>Sectors</span>
                                                <span><?php echo str_replace(",", ", ", $getClient['tags']); ?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <div class="img-cont">
                                        <p>
                                            <?php echo $getClient['content']; ?>
                                        </p>
                                    </div>
                                </li>
                                <li><a id="jobs"></a>
                                    <div class="img-sec"
                                         style="height:450px; background: url(  <?php echo $getClient['overview_image'] ? siteUrl() . 'uploads/images/' . $getClient['overview_image'] : (isset($photosURLs) ? siteUrl() . 'uploads/images/' . $photosURLs[array_rand($photosURLs)] : siteUrl() . 'images/key-bg.png'); ?>) center center no-repeat; background-size: cover; border-bottom-right-radius: 10px;">
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php if (is_array($getVacancies) && count($getVacancies) > 0) { ?>
                    <div class="company-sec sub-page">
                        <div class="container">
                            <div class="latest-sect company-cont sub-cont">
                                <h2 class="sub-head wow fadeInUp"> Job <span>Opportunities</span></h2>
                                <div class="inner-sect slide-sec">
                                    <ul id="vacancies-slider" class="slides-sect">
                                        <?php foreach ($getVacancies as $vacancy) { ?>
                                            <li>
                                                <div class="img-cont sub-page">
                                                    <h2 style="min-height:100px;">
                                                        <?php echo $vacancy['title']; ?>
                                                    </h2>
                                                    <ul class="vacancy-details">
                                                        <li>
                                                            <i class="fal fa-map-marker-alt" aria-hidden="true"></i>
                                                            <span>
                                                         <?php echo implode(", ", array_map(function ($location) {
                                                             return $location['location_name'];
                                                         }, $vacancy['locations'])); ?>
                                                        </span>
                                                        </li>
                                                        <li>
                                                            <i class="fal fa-clock"></i>
                                                            <span>
                                                            <?php echo $vacancy['contract_type']; ?>
                                                        </span>
                                                        </li>
                                                        <li>
                                                            <i class="fal fa-coins"></i>
                                                            <span>
                                                            <?php echo $vacancy['salary_value']; ?>
                                                        </span>
                                                        </li>
                                                    </ul>

                                                    <a href="<?php echo siteUrl("vacancy/" . $vacancy['slug']); ?>"
                                                       class="view-job">
                                                        <i class="fab fa-readme"></i>
                                                        read more
                                                    </a>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <a id="more"></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if (is_array($getContentPages) && count($getContentPages) > 0) { ?>
                    <div class="find-more-sec">
                        <div class="container">
                            <h1 class="heading wow fadeInUp">Find out <span>more</span></h1>
                            <div class="find-more-cont">
                                <ul id="pages-slider" class="slides-sect">
                                    <?php foreach ($getContentPages as $content_page) { ?>
                                        <li>
                                            <div class="img-sec"
                                                 style="background: url(<?php echo siteUrl() . 'uploads/images/' . $content_page['image']; ?>) center center no-repeat; height:250px; background-size: cover; border-radius: 10px;"
                                                 data-toggle="modal"
                                                 data-target="#content_page_<?php echo $content_page['clients_content_page_id']; ?>">
                                            </div>
                                            <div class="img-cont" style="padding-left:0px;">
                                                <h2 class="text-center">
                                                    <a href="#" data-toggle="modal"
                                                       data-target="#content_page_<?php echo $content_page['clients_content_page_id']; ?>">
                                                        <?php echo html_entity_decode($content_page['title']); ?>
                                                    </a>
                                                </h2>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php foreach ($getContentPages as $content_page) { ?>
                        <div class="modal fade"
                             id="content_page_<?php echo $content_page['clients_content_page_id']; ?>"
                             tabindex="-1" role="dialog"
                             aria-labelledby="content_page_<?php echo $content_page['clients_content_page_id']; ?>Title"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            id="content_page_<?php echo $content_page['clients_content_page_id']; ?>Title">

                                            <?php echo $content_page['title']; ?>
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="content-page-body">
                                            <img src="<?php echo siteUrl() . 'uploads/images/' . $content_page['image']; ?>"
                                                 style="float: left; margin-right: 20px;  width: 400px; margin-top: 10px;">
                                            <?php echo html_entity_decode(str_replace("&nbsp;", " ", $content_page['content'])); ?>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                <?php } ?>
                <?php if (is_array($getTestimonials) && count($getTestimonials) > 0) { ?>
                    <div class="testi-sec" id="testimonials">
                        <div class="container">
                            <div class="testi-cont">
                                <div id="testisec" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <?php foreach ($getTestimonials as $i => $testimonial) { ?>
                                            <div class="carousel-item <?php echo $i === 0 ? 'active' : ''; ?> wow fadeInUp">
                                                <ul>
                                                    <?php if ($testimonial['image']) { ?>
                                                        <li>
                                                            <div class="img-sec"
                                                                 style="background-image: url('<?php echo siteUrl() . 'uploads/images/' . $testimonial['image']; ?>'); background-size: cover;"
                                                                <?php if ($testimonial['vimeo_video_id']) { ?>
                                                                    data-toggle="modal"
                                                                    data-target="#testimonial-<?php echo $testimonial['clients_testimonial_id']; ?>"
                                                                <?php } ?>></div>
                                                            <?php if ($testimonial['vimeo_video_id']) { ?>
                                                                <img src="<?php echo siteUrl(); ?>images/play-btn.png"
                                                                     data-toggle="modal"
                                                                     data-target="#testimonial-<?php echo $testimonial['clients_testimonial_id']; ?>"
                                                                     width="100"
                                                                     class="play-button">
                                                            <?php } ?>
                                                            <div class="img-cont"></div>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                    <?php } ?>
                                                    <li <?php if (!$testimonial['image']) { ?>style="width:100%;text-align:center;"<?php } ?>>
                                                        <h3 class="testi-cont-text">
                                                            <?php echo strip_tags($testimonial['content']); ?>
                                                            <br/>
                                                            <br/>
                                                            <p><strong><?php echo $testimonial['title']; ?>
                                                                    , <?php echo $getClient['client_name']; ?></strong>
                                                            </p>
                                                        </h3>
                                                    </li>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <a class="carousel-control-prev" href="#testisec" data-slide="prev">
                                        <img src="<?php echo siteUrl(); ?>images/slide-arrow.png">
                                    </a>
                                    <a class="carousel-control-next" href="#testisec" data-slide="next">
                                        <img src="<?php echo siteUrl(); ?>images/slide-arrow-2.png">
                                    </a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php foreach ($getTestimonials as $i => $testimonial) { ?>
                        <?php if ($testimonial['vimeo_video_id']) { ?>
                            <div class="modal fade"
                                 id="testimonial-<?php echo $testimonial['clients_testimonial_id']; ?>"
                                 style="margin-top: 60px"
                                 tabindex="-1" role="dialog"
                                 aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="background-color: transparent;">
                                        <button type="button" class="close text-right"
                                                data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times; Close</span>
                                        </button>
                                        <div class='vimeo embed-container'>
                                            <iframe class="vimeo"
                                                    src="https://player.vimeo.com/video/<?php echo $testimonial['vimeo_video_id']; ?>?title=0&byline=0&portrait=0"
                                                    frameborder="0" webkitallowfullscreen
                                                    mozallowfullscreen
                                                    allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <div class="clearfix"></div>
                <?php } ?>
                <?php if (is_array($getPhotos) && count($getPhotos) > 0) { ?>
                    <div class="gallery-title-sec">
                        <div class="container">
                            <h1 class="heading wow fadeInUp">Photo <span>Gallery</span></h1>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="gallery-sec">
                        <ul id="photos-slider">
                            <?php foreach ($getPhotos as $photo) { ?>
                                <li style="background: url(<?php echo siteUrl() . 'uploads/images/' . $photo['image']; ?>) center center no-repeat; background-size: cover;">
                                    <a href="<?php echo siteUrl() . 'uploads/images/' . $photo['image']; ?>"
                                       data-lightbox="image-1" data-title="<?php echo $photo['title']; ?>"
                                       style="width: 100%; height: 100%; display: block;">
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php foreach ($getPhotos as $photo) { ?>
                        <div class="modal fade" id="photo_<?php echo $photo['photo_id']; ?>"
                             tabindex="-1" role="dialog"
                             aria-labelledby="photo_<?php echo $photo['photo_id']; ?>Title"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            id="photo_<?php echo $photo['photo_id']; ?>Title">
                                            <?php echo $photo['title']; ?>
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <img src="<?php echo siteUrl() . 'uploads/images/' . $photo['image']; ?>"
                                             class="img-fluid" style="width:100%">

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                <?php } ?>
                <?php if (is_array($getVideos) && count($getVideos) > 0) { ?>
                    <div class="employer-sec-page">
                        <div class="container">
                            <div class="employer-cont">
                                <?php if (is_array($getVideos) && count($getVideos) > 0) { ?>
                                    <ul>
                                        <li style="min-height: 150px">
                                            <div class="img-cont sub-page">
                                                <h1 class="heading" id="video-heading">
                                                    <?php echo $getVideos[0]['title']; ?>
                                                </h1>
                                            </div>
                                        </li>
                                        <li style="padding-top: 0px; padding-bottom: 0px; height: 300px;overflow: hidden;">
                                            <div class="img-sec">
                                                <div id="videosec" class="carousel slide" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <?php foreach ($getVideos as $i => $video) { ?>
                                                            <div class="carousel-item <?php echo $i === 0 ? 'active' : ''; ?>"
                                                                 data-title="<?php echo $video['title']; ?>"
                                                                 data-toggle="modal"
                                                                 data-target="#vimeo-<?php echo $video['video_id']; ?>"
                                                                 style="background: url(<?php echo siteUrl() . 'uploads/images/' . $video['image'] ?>) no-repeat; background-size:cover;height:350px;text-align:center;padding-top:20%">
                                                                <img src="<?php echo siteUrl(); ?>images/play-button.png"
                                                                     width="100"
                                                                     class="play-button"
                                                                     style="max-width: 100px; margin-top: 0; margin-left: 0;">
                                                            </div>
                                                            <div class="modal fade"
                                                                 id="vimeo-<?php echo $video['video_id']; ?>"
                                                                 tabindex="-1" role="dialog"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content"
                                                                         style="background-color: transparent;">
                                                                        <button type="button" class="close text-right"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times; Close</span>
                                                                        </button>
                                                                        <div class='vimeo embed-container'>
                                                                            <iframe class="vimeo"
                                                                                    src="https://player.vimeo.com/video/<?php echo $video['vimeo_video_id']; ?>?title=0&byline=0&portrait=0"
                                                                                    frameborder="0"
                                                                                    webkitallowfullscreen
                                                                                    mozallowfullscreen
                                                                                    allowfullscreen></iframe>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <a class="carousel-control-prev" href="#videosec" data-slide="prev">
                                                        <img src="<?php echo siteUrl(); ?>images/slide-arrow.png">
                                                    </a>
                                                    <a class="carousel-control-next" href="#videosec" data-slide="next">
                                                        <img src="<?php echo siteUrl(); ?>images/slide-arrow-2.png">
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <?php } ?>
                <?php if (is_array($getOffices) && count($getOffices) > 0) { ?>
                    <div class="our-location" id="offices">
                        <div class="container">
                            <div class="location-cont">
                                <h1 class="heading wow fadeInUp">Office <span>Locations</span></h1>
                                <div id="office-locations">
                                    <?php
                                    foreach ($getOffices as $i => $office) {
                                        ?>
                                        <span style="cursor: pointer;"
                                              onclick="mapZoom(this, '<?php echo $office['coordinates']; ?>');"><?php echo $office['title']; ?></span>
                                        <?php
                                        if ($i + 1 !== count($getOffices))
                                            echo ' / ';
                                    }
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="map-cont">
                                    <div class="map" id="map"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php include("inc/footer.php"); ?>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/wow/0.1.12/wow.min.js"></script>
<script src="https://player.vimeo.com/api/player.js"></script>
<script src="<?php echo siteUrl('blocks/lightbox2-master/dist/js/lightbox.js'); ?>"></script>
<script src="<?php echo siteUrl('slick/slick.min.js'); ?>"></script>
<script src="<?php echo siteUrl('js/bootstrap.js'); ?>"></script>
<script src="<?php echo siteUrl('blocks/bg-slide/jquery.bgswitcher.js'); ?>"></script>
<script>
    $(function () {
        $(this).scrollTop(0);
        $(document).scroll(function () {
            if ($(window).width() > 767) {
                var y = $(this).scrollTop();
                if (y > 800) {
                    $('#company-subnav').fadeIn();
                } else {
                    $('#company-subnav').fadeOut();
                }
            }
        });

        $('#page-content-wrapper').click(function (e) {
            $('#wrapper').removeClass('toggled');
        });

        $('#menu-toggle').on('click', function (e) {
            if ($('#wrapper').hasClass("toggled")) {
                $("body").addClass('active-body');
            } else {
                $("body").removeClass('active-body');
            }
        });

        $("#navbtn").click(function () {
            $(this).toggleClass("active");
            $(".overlay").toggleClass("open");
            // this line ▼ prevents content scroll-behind
            $("body").toggleClass("locked");
            $("main .about-us-block").toggleClass("hidden");
            $("main .about-us-block .about-text").toggleClass("hidden");
        });

        $("#close-menu").click(function () {
            $(this).toggleClass("active");
            $(".overlay").toggleClass("open");
            // this line ▼ prevents content scroll-behind
            $("body").toggleClass("locked");
            $("main .about-us-block").toggleClass("hidden");
            $("main .about-us-block .about-text").toggleClass("hidden");
        });

        $(".overlay").scroll(function () {
            var scroll = $(".overlay").scrollTop();

            //>=, not <=
            if (scroll >= 10) {
                //clearHeader, not clearheader - caps H
                $("#navToggle").addClass("hide");
            } else {
                $("#navToggle").removeClass("hide");
            }
        });

        new WOW().init();

        <?php if (is_array($getVideos) && count($getVideos) > 0) { ?>
        <?php foreach ($getVideos as $i => $video) { ?>
        $('#vimeo-<?php echo $video['video_id']; ?>').on('hidden.bs.modal', function (e) {
            var iframe = $(this).find('iframe.vimeo');
            if (iframe.length > 0) {
                var player = new Vimeo.Player(iframe[0]);
                player.pause();
            }
        });
        <?php } ?>
        <?php } ?>
        $('#vacancies-slider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            autoplay: true,
            rows: 1,
            arrows: true,
            prevArrow: '<a class="slick-arrow carousel-control-prev content-pages-arrow-prev"><img src="<?php echo siteUrl(); ?>images/arrow-l.png"></a>',
            nextArrow: '<a class="slick-arrow carousel-control-next content-pages-arrow-next"><img src="<?php echo siteUrl(); ?>images/arrow-r.png"></a>',
            responsive: [
                {
                    breakpoint: 1030,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
        $('#pages-slider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            autoplay: true,
            rows: 1,
            arrows: true,
            prevArrow: '<a class="slick-arrow carousel-control-prev content-pages-arrow-prev"><img src="<?php echo siteUrl(); ?>images/arrow-l.png"></a>',
            nextArrow: '<a class="slick-arrow carousel-control-next content-pages-arrow-next"><img src="<?php echo siteUrl(); ?>images/arrow-r.png"></a>',
            responsive: [
                {
                    breakpoint: 1030,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
        $('#photos-slider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            autoplay: true,
            rows: 1,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1030,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
        $('#videosec').on('slide.bs.carousel', function () {
            $('#video-heading').fadeOut();
        });
        $('#videosec').on('slid.bs.carousel', function () {
            $('#video-heading').text($('#videosec').find('.carousel-item.active').data('title'));
            $('#video-heading').fadeIn();
        });
        if ($('#videosec .carousel-inner div.carousel-item').length === 1) {
            $('#videosec .carousel-control-prev').hide();
            $('#videosec .carousel-control-next').hide();
        }
        if ($('#testisec .carousel-inner div.carousel-item').length === 1) {
            $('#testisec .carousel-control-prev').hide();
            $('#testisec .carousel-control-next').hide();
        }
    });
    var markers = <?php echo isset($markers) && is_array($markers) && count($markers) > 0 ? json_encode($markers) : "[]"; ?>,
        map;

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            icon: "<?php echo siteUrl() . 'images/map-pin.png'; ?>",
            center: markers.length > 0 ? markers[0] : null,
            scrollwheel: true,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true,
            styles: [
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e9e9e9"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 29
                        },
                        {
                            "weight": 0.2
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 18
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        },
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#dedede"
                        },
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "saturation": 36
                        },
                        {
                            "color": "#333333"
                        },
                        {
                            "lightness": 40
                        }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        },
                        {
                            "lightness": 19
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#fefefe"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#fefefe"
                        },
                        {
                            "lightness": 17
                        },
                        {
                            "weight": 1.2
                        }
                    ]
                }
            ]
        });
        $(markers).each(function (i, marker) {
            new google.maps.Marker({
                position: marker,
                map: map,
                icon: "<?php echo siteUrl() . 'images/map-pin.png'; ?>"
            });
        });
    }

    function mapZoom(el, coordinates) {
        $('.map-location-active').removeClass('map-location-active');
        $(el).addClass('map-location-active');
        coordinates = coordinates.split(',');
        if (coordinates.length === 2) {
            map.panTo({
                lat: parseFloat(coordinates[0].trim()),
                lng: parseFloat(coordinates[1].trim()),
            });
            map.setZoom(10);
        }
    }

    function imgZoom(img) {
        if (!$(img).attr('width').length)
            $(img).attr('width', 300);
        else
            $(img).attr('width', '')
    }
</script>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?callback=initMap&key=<?php echo isset($maps_api_key) && $maps_api_key ? $maps_api_key : ""; ?>"></script>
</body>
</html>
