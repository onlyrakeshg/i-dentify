<?php

$page_allowed = 1;
include("inc/support/common.php");
include("backend/config/config.php");


if (!isset($_SESSION['candidate_logged_in'])) {
    header("Location: " . $config['website_url'] . "login");
}

$candidate_id = $_SESSION['candidate_id'];

$getCandidate = $db->prepare("SELECT * FROM `candidates_portal` WHERE `candidate_id` = :candidate_id");
$getCandidate->execute(array("candidate_id" => $candidate_id));
$getCandidate = $getCandidate->fetch();

$getApplications = $db->prepare("SELECT DISTINCT `vacancy_id` FROM `cv_library` WHERE `candidate_id` = :candidateID ORDER BY `library_id` DESC");
$getApplications->execute(array("candidateID" => $getCandidate['candidate_id']));
$totalApplications = $getApplications->rowCount();
$getApplications = $getApplications->fetchAll();

if (isset($_POST['update'])) {

    $file = uploadFile('cv', false, true);

    $image = $_FILES['photo']['name'];

    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $tel = $_POST['tel'];
    $location = $_POST['location'];
    $job_title = $_POST['job_title'];
    $interview_availability = $_POST['interview_availability'];
    $status = $_POST['status'];
    $password = $_POST['password'];
    $password2 = $_POST['password2'];
    $sectors = implode(",", $_POST['sectors']);
    $locations = $_POST['locations'];

    $updateProfile = $db->prepare("
        UPDATE
            `candidates_portal`
        SET
            `firstname` = :firstname,
            `lastname` = :lastname,
            `email` = :email,
            `tel` = :tel,
            `status` = :status,
            `job_title` = :job_title,
            `location` = :location,
            `interview_availability` = :interview_availability,
            `alerts_sectors` = :sectors,
            `alerts_locations` = :locations
        WHERE
            `candidate_id` = :candidate_id
            LIMIT 1;
        ");

    $params = array(
        'firstname' => $firstname,
        'lastname' => $lastname,
        'email' => $email,
        'tel' => $tel,
        'status' => $status,
        'job_title' => $job_title,
        'location' => $location,
        'interview_availability' => $interview_availability,
        "sectors" => $sectors,
        "locations" => $locations,
        'candidate_id' => $candidate_id,
    );

    if ($updateProfile->execute($params)) {

        $bh_notes = array();

        /* BullHorn Integration */

        include_once "inc/support/bullhorn.php";

        $access_token = get_token();
        if ($access_token) {
            if (((int)$access_token->created + (int)$access_token->expires) < (int)time()) {
                refresh_token($access_token);
                $access_token = get_token();
            }

            $bh_token = $bh->get_login($access_token->access_token);
            if (isset($bh_token->errorCode) && $bh_token->errorCode == 400) {
                refresh_token($access_token);
                $access_token = get_token();
                $bh_token = $bh->get_login($access_token->access_token);
            }

            if (isset($bh_token->BhRestToken) && isset($bh_token->restUrl)) {
                $bh_data = array(
                    "firstName" => $firstname, // Field name
                    "lastName" => $lastname, // Surname
                    "name" => $firstname . " " . $lastname,
                    "email" => $email, // Personal Email
                    "mobile" => $tel, // Mobile Phone
                    "address" => array(
                        "city" => $location, // City
                    ),
                    "occupation" => $job_title, // Current Job Title
                    "employmentPreference" => array(
                        $status
                    ), //  Employment Preference
                    "source" => "Website",
                    "status" => "Available",
                    "isDeleted" => FALSE,
                );

                if ($file['filename']) {
                    $cv_path = $global_path . '/uploads/cvs/' . $file['filename'];
                    if (file_exists($cv_path)) {
                        $path_parts = pathinfo($cv_path);

                        switch (strtolower($path_parts['extension'])) {
                            case 'pdf':
                                include_once __DIR__ . '/plugins/pdfparser/vendor/autoload.php';

                                try {
                                    $parser = new \Smalot\PdfParser\Parser();
                                    $pdf = $parser->parseFile($cv_path);

                                    $text = $pdf->getText();
                                    if ($text) {
                                        $bh_data['description'] = $text;
                                    }
                                } catch (Exception $e) {
                                    /*
                                    echo $e->getMessage();
                                    exit;
                                    */
                                }

                                break;
                            case 'doc':
                            case 'docx':
                                include_once __DIR__ . '/plugins/PHPDocumentParser/vendor/autoload.php';

                                try {
                                    $parser = new \LukeMadhanga\DocumentParser();
                                    $text = $parser->parseFromFile($cv_path);
                                    if ($text) {
                                        $bh_data['description'] = $text;
                                    }
                                } catch (Exception $e) {
                                    echo $e->getMessage();
                                    exit;
                                }

                                break;
                        }
                    }
                }

                $bh_candidate_id = NULL;
                $file_id = NULL;
                $search_candidate = $bh->get_json($bh_token->restUrl . "search/Candidate" . "?" . http_build_query(array("query" => "email:" . $email, "fields" => "id")), NULL, array("BHRestToken: " . $bh_token->BhRestToken));
                if ($search_candidate && isset($search_candidate->total) && $search_candidate->total > 0) {
                    $candidates = array_values($search_candidate->data);
                    $bh_candidate_id = $candidates[0]->id;

                    unset($bh_data['email']);
                    $bh_data['id'] = $bh_candidate_id;
                    $candidate = $bh->get_json($bh_token->restUrl . "entity/Candidate/" . $bh_candidate_id, json_encode($bh_data), array("Content-Type: application/json", "BHRestToken: " . $bh_token->BhRestToken), "POST");
                } else {
                    $candidate = $bh->get_json($bh_token->restUrl . "entity/Candidate", json_encode($bh_data), array("Content-Type: application/json", "BHRestToken: " . $bh_token->BhRestToken), "PUT"); // Create candidate for each submission
                    if ($candidate && $candidate->changedEntityId) {
                        $bh_candidate_id = $candidate->changedEntityId;
                    } else {
                        $bh_notes[] = "Creating candidate record failed.<br/>Raw response: <pre>" . var_export($candidate, TRUE) . "</pre>";
                    }
                }

                if ($bh_candidate_id) {

                    $updateProfile = $db->prepare("UPDATE `candidates_portal` SET `bh_candidate_id` = :bh_candidate_id WHERE `candidate_id` = :candidate_id");
                    $updateProfile->execute(array('bh_candidate_id' => $bh_candidate_id, 'candidate_id' => $candidate_id));

                    if ($file['filename']) {
                        $cv_path = $global_path . '/uploads/cvs/' . $file['filename'];
                        if (file_exists($cv_path)) {
                            $path_parts = pathinfo($cv_path);

                            $bh_data = array(
                                "externalID" => "CV",
                                "fileContent" => base64_encode(file_get_contents($cv_path)),
                                "fileType" => "SAMPLE",
                                "name" => $firstname . " " . $lastname . " CV." . $path_parts['extension'],
                                "contentType" => mime_content_type($cv_path),
                                "description" => $firstname . " " . $lastname . " CV",
                                "type" => "CV"
                            );
                            $file_submission = $bh->get_json($bh_token->restUrl . "file/Candidate/" . $bh_candidate_id, json_encode($bh_data), array("Content-Type: application/json", "BHRestToken: " . $bh_token->BhRestToken), "PUT");
                            if ($file_submission && isset($file_submission->fileId)) {
                                $file_id = $file_submission->fileId;
                            } else {
                                $bh_notes[] = "Uploading CV failed.<br/>Raw response: <pre>" . var_export($file_submission, TRUE) . "</pre>";
                            }
                        }
                    }
                } else {
                    $bh_notes[] = "Candidate not found at BullHorn, unable to update.";
                }
            } else {
                $bh_notes[] = "Integration access revoked [Code #0002], please perform authorization again.";
            }
        } else {
            $bh_notes[] = "Integration access revoked [Code #0001], please perform authorization again.";
        }

        $updateProfile = $db->prepare("UPDATE `candidates_portal` SET `bh_notes` = :bh_notes WHERE `candidate_id` = :candidate_id LIMIT 1;");
        $updateProfile->execute(array(
            'bh_notes' => is_array($bh_notes) && count($bh_notes) > 0 ? implode("<br>", $bh_notes) : NULL,
            'candidate_id' => $candidate_id
        ));

        /* END BullHorn Integration */

        if ($file['filename'] != "") {

            $updateProfile = $db->prepare("UPDATE `candidates_portal` SET `cv` = :cv WHERE `candidate_id` = :candidate_id");
            $updateProfile->execute(array('cv' => $file['filename'], 'candidate_id' => $candidate_id));

        }

        if ($image != "") {


            if ($getCandidate['photo'] != "") {

                unlink('images/candidates/' . $getCandidate['photo']);

            }


            $image_name = setNewImageName($image);

            $image = new SimpleImage($_FILES["photo"]["tmp_name"]);

            if ($image->getWidth() > 900) {

                $image->resizeToWidth(900);

            }

            $image->save('images/candidates/' . $image_name);

            $updateProfile = $db->prepare("UPDATE `candidates_portal` SET `photo` = :photo WHERE `candidate_id` = :candidate_id");
            $updateProfile->execute(array('photo' => $image_name, 'candidate_id' => $candidate_id));

        }

        if ($password != "") {
            if ($password2 == "") {
                $error['password2'] = 1;
            }
            if ($password != $password2) {
                $error['password_mismatch'] = 1;
            }
            if (!is_array($error)) {
                $updateProfile = $db->prepare("UPDATE `candidates_portal` SET `password` = PASSWORD(:password) WHERE `candidate_id` = :candidate_id");
                $updateProfile->execute(array('password' => $password, 'candidate_id' => $candidate_id));
            }
        }
        if (!is_array($error)) {
            header("Location: " . $config['website_url'] . "profile?updated=1");
        }
    }
}
$page = "candidate-profile";

?>
<!DOCTYPE html>
<html lang="en">


<head>

    <?php include("inc/head-includes.php"); ?>

    <link href="<?= $config['website_url']; ?>css/profile.css" rel="stylesheet">

    <link rel="stylesheet"
          href="<?= $config['website_url']; ?>blocks/selectize.js-master/dist/css/selectize.default.css">

    <style type="text/css">
        .selectize-control .option .title {
            display: block;
        }

        .selectize-control .option .url {
            font-size: 12px;
            display: block;
            color: #a0a0a0;
        }

        .selectize-control .item a {
            color: #006ef5;
        }

        .selectize-control .item.active a {
            color: #303030;
        }

        pre {
            display: none;
        }

        .item {
            background: #f78021 !important;
            border-color: #d06510 ! impoortant;
        }

        .item a {
            color: #fff !important;
        }

        .selectize-control.multi .selectize-input > div {
            border-color: #d61b42 !important;
            font-size: 14px;
        }

        .value {
            display: none;
        }

        .theme-selector {
            display: none;
        }

        .selectize-input {
            padding: 10px;
            box-shadow: none;
            font-weight: 400;
            border: #ccc 1px solid;
            background: #f9f9f9;
            font-size: 18px;
        }

        .selectize-input.has-items {
            padding: 10px !important;
        }

        .selectize-control.plugin-remove_button [data-value] .remove {
            border-color: #d06510;
        }

        .candidate-profile .drop_uploader.drop_zone span.bfile {
            background: none;
        }
    </style>

    <style type="text/css">
        .banner .curved-element {
            border: 0px;
        }
    </style>

</head>
<body>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">
    <div id="page-content-wrapper">
        <div class="top-header" id="myHeader">
            <div class="container">
                <?php include("inc/header.php"); ?>
            </div>
        </div>
        <div class="content">
            <div class="banner small-banner  about-bn">


                <div class="overvideo-content">
                    <div class="overvideo-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="heading-text">
                                        <span class="subheading">Your Profile</span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="curved-element"></div>
            </div>


            <section class="vacancy-page-info candidate-profile">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 left-col">

                            <div class="other-cont">

                                <div class="other-contents">

                                    <h3>Suggested Vacancies</h3>

                                    <?php

                                    if ($getCandidate['alerts_sectors'] == "") {

                                        $getRelatedVacancies = $db->prepare("SELECT * FROM `vacancies` ORDER BY `vacancy_id` ASC LIMIT 0,8");
                                        $getRelatedVacancies->execute();
                                        $getRelatedVacancies = $getRelatedVacancies->fetchAll();

                                    } else {

                                        $expSectors = array_filter(explode(",", $getCandidate['alerts_sectors']));

                                        $sqlGetSectors = "SELECT * FROM `vacancies` WHERE `deleted` = '0' AND `vacancy_id` IN (SELECT `vacancy_id` FROM `vacancies_sectors` WHERE ";

                                        foreach ($expSectors as $i => $value) {

                                            $sqlGetSectors .= "`industry_sector_id` = '$value' OR ";

                                        }

                                        $sqlGetSectors = substr($sqlGetSectors, 0, -4);

                                        $sqlGetSectors .= ") LIMIT 0,8";

                                        //echo $sqlGetSectors;

                                        $getRelatedVacancies = $db->prepare($sqlGetSectors);
                                        $getRelatedVacancies->execute();
                                        $getRelatedVacancies = $getRelatedVacancies->fetchAll();

                                    }


                                    foreach ($getRelatedVacancies as $row) { ?>

                                        <a href="<?= $websiteURL; ?>vacancy/<?= $row['slug']; ?>"
                                           class="suggested"><?= $row['title']; ?></a>

                                    <?php } ?>

                                </div>

                            </div>


                            <?php if ($totalApplications > 0) { ?>

                                <div class="other-cont">

                                    <div class="other-contents">

                                        <h3>My Applications</h3>

                                        <?php foreach ($getApplications as $row) {

                                            $ref = $row['vacancyID'];

                                            $getVacancy = $db->prepare("SELECT * FROM `vacancies` WHERE `ref` = :ref");
                                            $getVacancy->execute(array("ref" => $ref));
                                            $getVacancy = $getVacancy->fetch();


                                            ?>

                                            <a href="<?= $websiteURL; ?>vacancy/<?= $getVacancy['slug']; ?>"
                                               class="suggested"><?= $getVacancy['title']; ?></a>

                                        <?php } ?>

                                    </div>

                                </div>

                            <?php } ?>

                            <!--<a href="<?= $config['website_url']; ?>profile/?gdprrequest=1" onclick="return confirm('This will submit a request to us to give you a breakdown of all personal data we hold about you. This can take up to 7 working days. Please click OK to confirm this request.')" class="account-btn">Make GDPR Request</a>
                
				<a href="<?= $config['website_url']; ?>delete-account" onclick="return confirm('Are you sure you wish to remove your account? This will remove your personal details from our database and cannot be undone.')" class="account-btn">Delete My Account</a>-->

                        </div>


                        <div class=" col-md-8 right-col">

                            <form action="" method="post" enctype="multipart/form-data">
                                <div id="vacancy-information" class="nomargintop">

                                    <h1>Your Profile</h1>
                                    <h2>Personal Information</h2>
                                    <hr/>
                                    <?php if ($_GET['updated'] == 1) { ?>
                                        <div class="confirm-bar" style="margin-bottom: 20px;">
                                            <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            Your profile has been updated
                                        </div><?php } ?>
                                    <?php
                                    if ($_GET['gdprsent'] == 1) {
                                        ?>
                                        <div class="confirm-bar">
                                            <div class="container">
                                                Thank you for your request. We will process it ASAP.
                                            </div>
                                        </div> <?php
                                    }
                                    ?>
                                    <div class="clr"></div>
                                    <div class="profile_column">
                                        <div class="info_label">First Name</div>
                                        <div class="info_value"><input type="text" name="firstname"
                                                                       value="<?= $getCandidate['firstname']; ?>"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="profile_column right">
                                        <div class="info_label">Last Name</div>
                                        <div class="info_value"><input type="text" name="lastname"
                                                                       value="<?= $getCandidate['lastname']; ?>"></div>
                                        <div class="clr"></div>
                                    </div>
                                    <div class="profile_column">
                                        <div class="info_label">Email Address</div>
                                        <div class="info_value"><input type="text" name="email"
                                                                       value="<?= $getCandidate['email']; ?>">
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                    <div class="profile_column right">
                                        <div class="info_label">Telephone</div>
                                        <div class="info_value"><input type="text" name="tel"
                                                                       value="<?= $getCandidate['tel']; ?>">
                                        </div>

                                        <div class="clr"></div>

                                    </div>


                                    <div class="clearfix"></div>

                                    <h2 style="margin-top: 30px;">Employment Information</h2>

                                    <hr/>

                                    <div class="profile_column">

                                        <div class="info_label">Location</div>

                                        <div class="info_value">

                                            <div class="control-group">

                                                <input type="text" name="location" id="location"
                                                       value="<?= $getCandidate['location']; ?>">

                                            </div>

                                        </div>


                                        <div class="clr"></div>

                                    </div>


                                    <div class="profile_column right">

                                        <div class="info_label">Current Job Title</div>

                                        <div class="info_value"><input type="text" name="job_title"
                                                                       value="<?= $getCandidate['job_title']; ?>"></div>

                                        <div class="clr"></div>

                                    </div>

                                    <div class="profile_column">

                                        <div class="info_label">Current Employment Status</div>

                                        <div class="info_value"><input type="text" name="status"
                                                                       value="<?= $getCandidate['status']; ?>"></div>

                                        <div class="clr"></div>

                                    </div>


                                    <div class="profile_column right">

                                        <div class="info_label">Interview Availability</div>

                                        <div class="info_value"><input type="text" name="interview_availability"
                                                                       value="<?= $getCandidate['interview_availability']; ?>">
                                        </div>

                                        <div class="clr"></div>

                                    </div>


                                    <div class="clearfix"></div>

                                    <?php if ($getCandidate['cv'] != "") { ?>

                                        <a class="download-cv"
                                           href="<?= $websiteURL; ?>uploads/cvs/<?= $getCandidate['cv']; ?>"
                                           target="_blank">
                                            Download Current CV
                                        </a>

                                    <?php } ?>

                                    <h2>Your CV</h2><br>

                                    <input type="file" name="cv">

                                    <div class="clearfix"></div>

                                    <h2>Job Alerts</h2>

                                    <hr/>

                                    <div class="info_label">Desired Locations</div>

                                    <div class="profile_column_full">

                                        <input type="text" name="locations" id="select-links"
                                               value="<?= $getCandidate['alerts_locations']; ?>"
                                               placeholder="Start typing to search...">

                                        <script>
                                            // <select id="select-links"></select>

                                            $('#select-links').selectize({
                                                plugins: ['remove_button'],
                                                theme: 'links',
                                                maxItems: 10,
                                                valueField: 'id',
                                                searchField: 'title',
                                                options: [
                                                    <?php

                                                    $getLocations = $db->prepare("SELECT * FROM `locations` ORDER BY `location_id` ASC");
                                                    $getLocations->execute();
                                                    $getLocations = $getLocations->fetchAll();

                                                    foreach($getLocations as $location) { ?>

                                                    {
                                                        id: <?= $location['location_id']; ?>,
                                                        title: '<?= $location['name']; ?>'
                                                    },

                                                    <?php } ?>
                                                ],
                                                render: {
                                                    option: function (data, escape) {
                                                        return '<div class="option">' +
                                                            '<span class="title">' + escape(data.title) + '</span>' +
                                                            '</div>';
                                                    },
                                                    item: function (data, escape) {
                                                        return '<div class="item"><a href="' + escape(data.url) + '">' + escape(data.title) + '</a></div>';
                                                    }
                                                },
                                                create: function (input) {
                                                    return {
                                                        id: 0,
                                                        title: input,
                                                        url: '#'
                                                    };
                                                }
                                            });
                                        </script>
                                    </div>

                                    <div class="info_label">Desired Sectors</div>

                                    <?php

                                    $getSectors = $db->prepare("SELECT * FROM `industry_sectors` ORDER BY `industry_sector_id` ASC");
                                    $getSectors->execute();
                                    $getSectors = $getSectors->fetchAll();

                                    $expChosenSectors = explode(",", $getCandidate['alerts_sectors']);

                                    foreach ($getSectors as $row) {

                                        ?>

                                        <div class="ja-sector"
                                             <?php if (in_array($row['industry_sector_id'], $expChosenSectors)) { ?>style="border-color: #999;background: #f9f9f9;"<?php } ?>>

                                            <label>

                                                <input name="sectors[]" type="checkbox"
                                                       value="<?= $row['industry_sector_id']; ?>" <?php if (in_array($row['industry_sector_id'], $expChosenSectors)) {
                                                    echo "checked";
                                                } ?>><?= $row['name']; ?>

                                            </label>

                                        </div>

                                    <?php } ?>

                                    <div class="clearfix"></div>

                                    <h2>Change Password</h2>
                                    <hr/>
                                    <div class="profile_column">
                                        <div class="info_label">New Password</div>
                                        <div class="info_value"><input type="password" name="password"
                                                                       <?php if ($error['password_mismatch'] == 1) { ?>style="border-color: red;"<?php } ?>>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                    <div class="profile_column right">
                                        <div class="info_label">Confirm New Password</div>
                                        <div class="info_value"><input type="password" name="password2"
                                                                       <?php if ($error['password_mismatch'] == 1 || $error['password2'] == 1) { ?>style="border-color: red;"<?php } ?>>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                    <div class="profile_column">
                                        <input type="submit" value="Update Profile" name="update">
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>


        </div>
        <?php include("inc/footer.php"); ?>

        <?php include("inc/modals.php"); ?>

        <?php include("inc/footer-js.php"); ?>

        <script src="<?= $config['website_url']; ?>blocks/selectize.js-master/dist/js/standalone/selectize.js"></script>
        <script src="<?= $config['website_url']; ?>blocks/selectize.js-master/examples/js/index.js"></script>

        <script>
            $('#select-links').selectize({
                plugins: ['remove_button'],
                theme: 'links',
                maxItems: 10,
                valueField: 'id',
                searchField: 'title',
                options: [
                    <?php

                    $getLocations = $db->prepare("SELECT * FROM `locations` ORDER BY `name` ASC");
                    $getLocations->execute();
                    $getLocations = $getLocations->fetchAll();

                    foreach($getLocations as $location) { ?>

                    {id: <?php echo $location['location_id']; ?>, title: '<?php echo $location['name']; ?>'},

                    <?php } ?>
                ],
                render: {
                    option: function (data, escape) {
                        return '<div class="option">' +
                            '<span class="title">' + escape(data.title) + '</span>' +
                            '</div>';
                    },
                    item: function (data, escape) {
                        return '<div class="item"><a href="' + escape(data.url) + '">' + escape(data.title) + '</a></div>';
                    }
                },
                create: false
            });
        </script>

</body>
</html>