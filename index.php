<?php
$page_allowed = 1;
include("inc/support/common.php");
include("backend/config/config.php"); $page = "home"; 

$getJobs = $db->prepare("SELECT * FROM `vacancies` WHERE (`client_id` IS NULL OR (`client_id` IS NOT NULL AND `allow_on_portal` = TRUE)) AND `deleted` = '0' ORDER BY `vacancy_id` DESC LIMIT 0,3");
$getJobs->execute();
$getJobs = $getJobs->fetchAll();

$pageID = 1;

$content = $db->prepare("SELECT * FROM `content_pages` WHERE `content_page_id` = :content_page_id LIMIT 1;");
$content->execute(array(
    "content_page_id" => $pageID
));
$content = $content->fetch();


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo $content['meta_title']; ?></title>
    <link href='<?php echo $content['canonical']; ?>' rel='canonical'>
    <meta content='<?php echo $content['meta_desc']; ?>' name='Description'>
    <meta content='<?php echo $content['meta_keywords']; ?>' name='Keywords'>
    <meta content='<?php echo $content['meta_title']; ?>' name='twitter:title'>
    <meta content='<?php echo $content['meta_desc']; ?>' name='twitter:description'>
    <meta property="og:title" content="<?php echo $content['meta_title']; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:description" content="<?php echo $content['meta_desc']; ?>"/>
    <meta property="og:image" content="<?= $config['website_url'];?>images/og.jpg"/>
    <?php include("inc/head-includes.php"); ?>

</head>
<body>
	<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0&appId=838422009515614&autoLogAppEvents=1"></script>
<?php include("inc/menu.php"); ?>
<div class="page-wrapper" id="wrapper">
	
<div id="page-content-wrapper">
    <div class="top-header" id="myHeader">
        <div class="container">
            <?php include("inc/header.php"); ?>
        </div>
    </div>
    <div class="content">
        <div class="video banner large-banner hero-with-search">
            <div class="video-replacement banner-home"></div>
            <div class="video-content" style="background:url('<?= $config['website_url'];?>images/index-bg.jpg') center center; background-size:cover;">
            </div>
            <div class="overvideo-content">
                <div class="overvideo-inner">
                    <div class="container">
                        <div class="row home_pg">
                            <div class="col-xs-12 col-md-12">
                                <div class="heading-text text-center">
                                    <span class="subheading wow fadeIn">Identify:</span>
                                    <h1 class="heading wow fadeIn"><span id="typed3"></span></h1>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row search-element wow fadeIn" data-wow-delay="0.7s">
                            
                                <form method="POST" action="<?= $config['website_url'];?>vacancies" id="SearchBar">
                                    <div class="search-bar search-bar2">
                                        <input name="keyword" id="keywords" type="text"
                                               placeholder="Job title or Keywords..." class="search-text">
                                        <span class="search-submit">
                                            <input type="submit" value="submit">
                                            <i class="fal fa-search"></i>
                                        </span>
                                    </div>
                                </form>
                                <a class="btn i-btn start-hiring" href="<?= $config['website_url'];?>clients/" >
                                    <span class="home">Employer? Start Hiring</span>
                                </a>
				            
                        </div>
                        
                    </div>
                </div>

            </div>
            <div class="curved-element"></div>

        </div>

            <div class="content wow fadeInUp ">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="text-content rte wow fadeInLeft">
                            <?= getContentPage(1); ?>
                        </div>
                    </div>
                </div>
            </div>
            </div>
		
		
		<div class="main-content">
            <div class=" wow fadeInRight">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="heading largeheading">Featured <span>Jobs</span></h3>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="slider single-item multiple-items">
                    

		<?php foreach($getJobs as $row) { 
	
		  $vacancyID = $row['vacancy_id'];
	
		  $getLocationLink = $db->prepare("SELECT * FROM `vacancies_locations` WHERE `vacancy_id` = :vacancyID");
		  $getLocationLink->execute(array("vacancyID"=>$vacancyID));
		  $getLocationLink = $getLocationLink->fetch();
		  
		  $getLocations = $db->prepare("SELECT * FROM `locations` WHERE `location_id` = :locationID");
		  $getLocations->execute(array("locationID"=>$getLocationLink['location_id']));
		  $getLocations = $getLocations->fetch();

		?>
		

						
						
						<div class="slider-inner-block2">
                        <h4 class="heading"><a href="<?= $config['website_url']; ?>vacancy/<?= $row['slug']; ?>" tabindex="0"><?= $row['title']; ?></a>
                            
                        </h4>
                        <ul>
                            <li>
                                <i class="fal fa-map-marker-alt"></i>&nbsp;&nbsp;&nbsp;<span><?= $getLocations['name']; ?></span>
                            </li>
                            <li>
                                <i class="fal fa-clock"></i>&nbsp;&nbsp;<span><?= $row['contract_type']; ?></span>
                            </li>
                        </ul>
                        <a href="<?= $config['website_url']; ?>vacancy/<?= $row['slug']; ?>" class="link-slider" tabindex="0"><i class="fab fa-readme"></i>&nbsp;&nbsp;Read more</a>
                       </div>

    <?php } ?>	                   

                </div>
                </div>
                <div class="text-center see-more">
                    <a href="<?= $config['website_url'];?>vacancies" class="btn-ghost-primary btn"><span>View All&nbsp;&nbsp;&nbsp;<i class="fal fa-arrow-from-left"></i></span></a>
                </div>
            </div>
            <div class="content text-image wow fadeInUp">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="text-content">
                                <h2 class="heading largeheading" style="text-align:left;">
                                    About Us</span>
                                </h2>
                                <p>
                                    The founder of Identify HR, Denise Baxter, has been on both the HR and recruiter side of the desks, therefore she knows what great service looks and feels like and the team always delivers upon this.
                                </p>

								<a href="<?= $config['website_url'];?>team" class="meet-team-btn">Meet Our Team&nbsp;&nbsp;&nbsp;<i class="fal fa-arrow-from-left"></i></a>

                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="image-content">
                                <div class="about-img" id="about-img">
			
									<div>
										<img src="images/home-slide1.jpg" alt=""/>
									</div>

									<div>
										<img src="images/home-slide2.jpg" alt=""/>
									</div>

									<div>
										<img src="images/home-slide3.jpg" alt=""/>
									</div>

									<div>
										<img src="images/home-slide4.jpg" alt=""/>
									</div>


								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	
			<!-- sectors-->
            <div class=" wow fadeInLeft">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="heading largeheading">Our <span>Sectors</span></h3>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="slider single-item multiple-items">
                    
						<div class="slider-inner-block3">
							<div class="sector-block"> <a href="<?= $config['website_url'];?>sectors_hr"><img src="<?= $config['website_url'];?>images/sectors-hr.jpg"/></a>
								<div class="sector-info">
								  <h3><a href="<?= $config['website_url'];?>sectors_hr">HUMAN RESOURCES</a></h3>
									<a href="<?= $config['website_url'];?>sectors_hr" class="link-slider" tabindex="0">...Learn more</a>
								</div>	
							</div>		
						</div>
						<div class="slider-inner-block3">
							<div class="sector-block"> <a href="<?= $config['website_url'];?>sectors_tech"><img src="<?= $config['website_url'];?>images/sectors-tech.jpg"/></a>
								<div class="sector-info">
								  <h3><a href="<?= $config['website_url'];?>sectors_tech">TECHNOLOGY</a></h3>
									<a href="<?= $config['website_url'];?>sectors_tech" class="link-slider" tabindex="0">...Learn more</a>
								</div>	
							</div>		
						</div>
						<div class="slider-inner-block3">
							<div class="sector-block"> <a href="<?= $config['website_url'];?>sectors_ict"><img src="<?= $config['website_url'];?>images/sectors-ict.jpg"/></a>
								<div class="sector-info">
								  <h3><a href="<?= $config['website_url'];?>sectors_ict">ICT</a></h3>
									<a href="<?= $config['website_url'];?>sectors_ict " class="link-slider" tabindex="0">...Learn more</a>
								</div>	
							</div>		
						</div>
						<div class="slider-inner-block3">
							<div class="sector-block"> <a href="<?= $config['website_url'];?>sectors_digital"><img src="<?= $config['website_url'];?>images/sectors-dig.jpg"/></a>
								<div class="sector-info">
								  <h3><a href="<?= $config['website_url'];?>sectors_digital">DIGITAL</a></h3>
									<a href="<?= $config['website_url'];?>sectors_digital" class="link-slider" tabindex="0">...Learn more</a>
								</div>	
							</div>		
						</div>
                    </div>
                </div>
            </div>
        
        
        <!-- Our Services -->
        
        <div class="for-slider wow fadeInLeft">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="heading largeheading">Our <span>Services</span></h3>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="slider single-item multiple-items">
                    
						<div class="slider-inner-block3">
							<div class="sector-block"> <a href="<?= $config['website_url'];?>services_permanent"><img src="<?= $config['website_url'];?>images/services-perm.jpg"/></a>
								<div class="sector-info">
								  <h3><a href="<?= $config['website_url'];?>services_permanent">Permanent Recruitment</a></h3>
									<a href="<?= $config['website_url'];?>services_permanent" class="link-slider" tabindex="0">...Learn more</a>
								</div>	
							</div>		
						</div>
						<div class="slider-inner-block3">
							<div class="sector-block"> <a href="<?= $config['website_url'];?>services_contract"><img src="<?= $config['website_url'];?>images/services-cont.jpg"/></a>
								<div class="sector-info">
								  <h3><a href="<?= $config['website_url'];?>services_contract">Contract Recruitment</a></h3>
									<a href="<?= $config['website_url'];?>services_contract" class="link-slider" tabindex="0">...Learn more</a>
								</div>	
							</div>		
						</div>
						<div class="slider-inner-block3">
							<div class="sector-block"> <a href="<?= $config['website_url'];?>services_managed"><img src="<?= $config['website_url'];?>images/services-man.jpg"/></a>
								<div class="sector-info">
								  <h3><a href="<?= $config['website_url'];?>services_managed">Managed Services</a></h3>
									<a href="<?= $config['website_url'];?>services_managed" class="link-slider" tabindex="0">...Learn more</a>
								</div>	
							</div>		
						</div>
						<div class="slider-inner-block3">
							<div class="sector-block"> <a href="<?= $config['website_url'];?>services_hr"><img src="<?= $config['website_url'];?>images/services-hr1.jpg"/></a>
								<div class="sector-info">
								  <h3><a href="<?= $config['website_url'];?>services_hr">HR Services</a></h3>
									<a href="<?= $config['website_url'];?>services_hr" class="link-slider" tabindex="0">...Learn more</a>
								</div>	
							</div>		
						</div>
						<div class="slider-inner-block3">
							<div class="sector-block"> <a href="<?= $config['website_url'];?>services_hcm"><img src="<?= $config['website_url'];?>images/services-hcm.jpg"/></a>
								<div class="sector-info">
								  <h3><a href="<?= $config['website_url'];?>services_hcm">HR & HCM Software Advisory</a></h3>
									<a href="<?= $config['website_url'];?>services_hcm" class="link-slider" tabindex="0">...Learn more</a>
								</div>	
							</div>		
						</div>
                    </div>
                </div>
            </div>
		
            		
		
		
        </div>
    </div>
	            <div class="for-slider wow fadeInLeft">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="heading largeheading">Get <span>Social</span></h3>
                        </div>
                    </div>
                </div>
					<div class="row"  style="margin-bottom:50px;">
						<div class="col-md-4 col-md-offset-2">
							<div class="twitter-feed">
							<a class="twitter-timeline" data-height="700" href="https://twitter.com/hridentify"></a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
							</div>	
						</div>
						<div class="col-md-4">
						<div class="fb-page" data-href=" https://www.facebook.com/identifyhr" data-tabs="timeline" data-width="500" data-height="700" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="true"><blockquote cite=" https://www.facebook.com/identifyhr" class="fb-xfbml-parse-ignore"><a href=" https://www.facebook.com/identifyhr">Identify</a></blockquote></div>
						</div>	
					</div>	
            </div>		


	
    <?php include("inc/footer.php"); ?>

<?php //include("inc/modals.php"); ?>

<?php include("inc/footer-js.php"); ?>

</body>
</html>