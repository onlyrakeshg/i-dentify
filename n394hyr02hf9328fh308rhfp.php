<?php

$page_allowed = 1;

include_once "inc/support/common.php";
include_once "backend/config/config.php";
include_once 'blocks/phpmailer/class.phpmailer.php';

$getCandidates = $db->prepare("SELECT * FROM `candidates_portal` WHERE `deleted` = FALSE AND `alerts_sectors` IS NOT NULL AND `alerts_locations` IS NOT NULL AND `alerts_locations` <> ''");
$getCandidates->execute();
$getCandidates = $getCandidates->fetchAll();

foreach ($getCandidates as $candidate) {

    $getVacancies = $db->prepare("
        SELECT
            *
        FROM
            `vacancies` `v`
        WHERE
            `v`.`deleted` = FALSE
            AND
            `v`.`vacancy_id` IN (SELECT `vs`.`vacancy_id` FROM `vacancies_sectors` `vs` WHERE FIND_IN_SET(`vs`.`industry_sector_id`, :sectors))
            AND
            `v`.`vacancy_id` IN (SELECT `vl`.`vacancy_id` FROM `vacancies_locations` `vl` WHERE FIND_IN_SET(`vl`.`location_id`, :locations))
        ORDER BY
            RAND();
    ");
    $getVacancies->execute(array(
        "sectors" => $candidate['alerts_sectors'],
        "locations" => $candidate['alerts_locations']
    ));
    $getVacancies = $getVacancies->fetchAll();

    if ($getVacancies && is_array($getVacancies) && count($getVacancies) > 0) {

        $mail = new PHPMailer();
        $mail->SetFrom("noreply@talentwebinc.com", "TalentWeb");
        $mail->AddAddress($candidate['email'], $candidate['firstname'] . " " . $candidate['lastname']);
        $mail->Subject = "TalentWeb - Your Job Alerts";

        ob_start();
        include 'blocks/phpmailer/templates/job-alerts.php';
        $mail->MsgHTML(ob_get_contents());
        ob_end_clean();

        $sent = $mail->Send();

        echo ($sent ? "Sent: " : "Error: ") . $candidate['firstname'] . " " . $candidate['lastname'];


    }

}